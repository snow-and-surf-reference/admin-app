import { IonApp, IonLoading, IonRouterOutlet, IonSplitPane, setupIonicReact } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";
import "@ionic/react/css/display.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/float-elements.css";
/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/typography.css";
import { RoleType } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { coachRoles, editorRoles, managerRoles, salesRoles } from "app/data/defaultRoles";
import { initFirebase } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAuthUser, selectAuthUserIsInit } from "app/slices/authSlice";
import { RootState } from "app/store";
import Checkout from "components/Customers/Checkout";
import NewBooking from "components/Customers/NewBooking";
import OrderPreview from "components/Customers/OrderPreview";
import PaymentCompleted from "components/Customers/PaymentCompleted";
import AuthCheck from "components/Global/AuthCheck";
import NoPermissionPage from "components/Global/NoPermissionPage";
import CafeCashier from "pages/Cafe/CafeCashier";
import CafeCategories from "pages/Cafe/CafeCategories";
import CafeItems from "pages/Cafe/CafeItems";
import CafeOrderQueue from "pages/Cafe/CafeOrderQueue";
import CoachList from "pages/Coach/CoachList";
import CoachSchedules from "pages/Coach/CoachSchedules";
import CreateCoach from "pages/Coach/CreateCoach";
import NewCoachSchedule from "pages/Coach/NewCoachSchedule";
import Coupons from "pages/Coupons/Coupons";
import CreateSubAccount from "pages/CreateSubAccount";
import CustomerClasses from "pages/CustomerClasses/CustomerClasses";
import FaqList from "pages/Faq/FaqList";
import GearDetails from "pages/Gears/GearDetails";
import GearList from "pages/Gears/GearList";
import Login from "pages/Login";
import HomepageSlide from "pages/PosContents/HomepageSlide";
import HomepageSlides from "pages/PosContents/HomepageSlides";
import SearchSessionDetails from "pages/Sessions/SearchSessionDetails";
import SessionDetails from "pages/Sessions/SessionDetails";
import SessionSearch from "pages/Sessions/SessionSearch";
import UpcomingSessionsList from "pages/Sessions/UpcomingList";
import UserSearch from "pages/Sessions/UserSearch";
import Cashier from "pages/Shops/Cashier";
import CreateItem from "pages/Shops/CreateItem";
import EditItem from "pages/Shops/EditItem";
import InventoryManagement from "pages/Shops/InventoryManagement";
import RefundShopItems from "pages/Shops/RefundShopItems";
import RemovedItem from "pages/Shops/RemovedItem";
import RetailReport from "pages/Shops/RetailReport";
import ShopManagement from "pages/Shops/ShopManagement";
import StockCount from "pages/Shops/StockCount";
import SignUp from "pages/SignUp";
import CreateStaff from "pages/Staff/CreateStaff";
import StaffList from "pages/Staff/StaffList";
import NewPackage from "pages/TopUp/NewPackage";
import TopUp from "pages/TopUp/TopUp";
import VenueCaps from "pages/Venue/VenueCaps";
import VenueMaintenances from "pages/Venue/VenueMaintenances";
import WristCheck from "pages/WristCheck/WristCheck";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";
import "./App.css";
import Menu from "./components/Menu";
import "./theme/style.scss";
/* Theme variables */
import CoachReport from "pages/Coach/CoachReport";
import GroupClassSlots from "pages/GroupClasses/GroupClassSlots";
import GroupClassTypesPage from "pages/GroupClasses/GroupClassTypesPage";
import NewGroupClassSlot from "pages/GroupClasses/NewGroupClassSlot";
import OtherContents from "pages/PageContents/OtherContents";
import PageContents from "pages/PageContents/PageContents";
import Pricing from "pages/Pricing/Pricing";
import EntranceReport from "pages/Sessions/EntranceReport";
import SubAccountSearch from "pages/Sessions/SubAccountSearch";
import TopUpReport from "pages/TopUp/TopUpReport";
import "./theme/variables.css";
import NewBookingV2 from "components/Customers/NewBookingV2";

setupIonicReact({
    mode: "ios",
});

interface RoleCheckedRouteProps {
    path: string;
    exact?: boolean;
    component: React.FC | any;
    allowedRoles: RoleType[];
}

const RoleCheckedRoute: React.FC<RoleCheckedRouteProps> = (props) => {
    const { path, exact, component, allowedRoles } = props;
    const authUser = useAppSelector(selectAuthUser);
    const authUserIsInit = useAppSelector(selectAuthUserIsInit);
    if (!authUserIsInit || !authUser) {
        return null;
    }
    return (
        <Route path={path} exact={exact || false}>
            {allowedRoles.includes(authUser.role) ? component : <NoPermissionPage />}
        </Route>
    );
};

const App: React.FC = () => {
    const isLoading = useSelector((state: RootState) => state.global.isLoading);
    const authUserIsInit = useAppSelector(selectAuthUserIsInit);
    useEffect(() => {
        initFirebase();
    }, []);

    // useEffect(() => {
    //     console.log(`authUserIsInit`, authUserIsInit);
    // }, [authUserIsInit]);

    return (
        <IonApp>
            {authUserIsInit && (
                <>
                    <IonReactRouter>
                        <IonSplitPane contentId="main" when="lg">
                            <Menu />
                            <IonRouterOutlet id="main">
                                <AuthCheck>
                                    <Switch>
                                        <Route path="/login" exact>
                                            <Login />
                                        </Route>
                                        <RoleCheckedRoute
                                            path="/sessions/upcoming"
                                            exact={true}
                                            allowedRoles={salesRoles}
                                            component={<UpcomingSessionsList />}
                                        />
                                        <RoleCheckedRoute
                                            path="/session/search"
                                            exact={true}
                                            allowedRoles={salesRoles}
                                            component={<SessionSearch />}
                                        />
                                        <RoleCheckedRoute path="/session/search/:id" allowedRoles={salesRoles} component={<SearchSessionDetails />} />
                                        <RoleCheckedRoute path="/session/:id" allowedRoles={salesRoles} component={<SessionDetails />} />
                                        <RoleCheckedRoute
                                            path="/customers/sub-account"
                                            exact={true}
                                            allowedRoles={salesRoles}
                                            component={<SignUp />}
                                        />
                                        <RoleCheckedRoute
                                            path="/customers/sub-account/create"
                                            exact={true}
                                            allowedRoles={salesRoles}
                                            component={<CreateSubAccount />}
                                        />
                                        <RoleCheckedRoute
                                            path="/sub-account/search"
                                            exact={true}
                                            allowedRoles={salesRoles}
                                            component={<SubAccountSearch />}
                                        />
                                        <RoleCheckedRoute path="/newBooking" exact={true} allowedRoles={salesRoles} component={<NewBooking />} />
                                        <RoleCheckedRoute path="/newBookingV2" exact={true} allowedRoles={salesRoles} component={<NewBookingV2 />} />
                                        <RoleCheckedRoute path="/orderPreview" exact={true} allowedRoles={salesRoles} component={<OrderPreview />} />
                                        <RoleCheckedRoute path="/checkout/:sessionId" allowedRoles={salesRoles} component={<Checkout />} />
                                        <RoleCheckedRoute
                                            path="/orderCompleted/:sessionId"
                                            allowedRoles={salesRoles}
                                            component={<PaymentCompleted />}
                                        />
                                        <RoleCheckedRoute
                                            path="/customers/search"
                                            exact={true}
                                            allowedRoles={salesRoles}
                                            component={<UserSearch />}
                                        />
                                        <RoleCheckedRoute path="/wrist-check" allowedRoles={salesRoles} component={<WristCheck />} />
                                        <RoleCheckedRoute path="/staff/list" allowedRoles={managerRoles} component={<StaffList />} />
                                        <RoleCheckedRoute path="/staff/create" exact={true} allowedRoles={managerRoles} component={<CreateStaff />} />
                                        <RoleCheckedRoute path="/staff/removed" allowedRoles={managerRoles} component={<StaffList viewRemoved />} />
                                        <RoleCheckedRoute path="/coach/create" exact={true} allowedRoles={managerRoles} component={<CreateCoach />} />
                                        <RoleCheckedRoute path="/coach/list" allowedRoles={managerRoles} component={<CoachList />} />
                                        <RoleCheckedRoute path="/coach/report/:id" allowedRoles={managerRoles} component={<CoachReport />} />
                                        <RoleCheckedRoute path="/coach/removed" allowedRoles={managerRoles} component={<CoachList viewRemoved />} />
                                        <RoleCheckedRoute path="/gears/management" exact allowedRoles={managerRoles} component={<GearList />} />
                                        <RoleCheckedRoute path="/gears/management/:id" allowedRoles={managerRoles} component={<GearDetails />} />
                                        <RoleCheckedRoute
                                            path="/coach/schedule"
                                            exact={true}
                                            allowedRoles={coachRoles}
                                            component={<CoachSchedules />}
                                        />
                                        <RoleCheckedRoute
                                            path="/newCoachSchedule"
                                            exact={true}
                                            allowedRoles={coachRoles}
                                            component={<NewCoachSchedule />}
                                        />
                                        <RoleCheckedRoute
                                            path="/venue/availability"
                                            exact={true}
                                            allowedRoles={managerRoles}
                                            component={<VenueCaps />}
                                        />
                                        <RoleCheckedRoute
                                            path="/venue/maintenance"
                                            exact={true}
                                            allowedRoles={managerRoles}
                                            component={<VenueMaintenances />}
                                        />
                                        <RoleCheckedRoute path="/top-up/management" allowedRoles={managerRoles} component={<TopUp />} exact />
                                        <RoleCheckedRoute path="/shop/management" allowedRoles={managerRoles} component={<ShopManagement />} exact />
                                        <RoleCheckedRoute
                                            path="/inventory/management/stock"
                                            allowedRoles={managerRoles}
                                            component={<StockCount />}
                                            exact
                                        />
                                        <RoleCheckedRoute
                                            path="/shop/removed"
                                            allowedRoles={managerRoles}
                                            component={<ShopManagement viewRemoved={true} />}
                                            exact
                                        />
                                        <RoleCheckedRoute path="/shop/cashier" allowedRoles={managerRoles} component={<Cashier />} exact />
                                        <RoleCheckedRoute path="/shop/refund" allowedRoles={managerRoles} component={<RefundShopItems />} exact />
                                        <RoleCheckedRoute path="/shop/report" allowedRoles={managerRoles} component={<RetailReport />} exact />
                                        <RoleCheckedRoute path="/inventory/new-item" allowedRoles={managerRoles} component={<CreateItem />} />
                                        <RoleCheckedRoute
                                            path="/inventory/management"
                                            allowedRoles={managerRoles}
                                            component={<InventoryManagement />}
                                            exact
                                        />
                                        <RoleCheckedRoute path="/inventory/removed" allowedRoles={managerRoles} component={<RemovedItem />} exact />
                                        <RoleCheckedRoute path="/inventory/management/:id" allowedRoles={managerRoles} component={<EditItem />} />
                                        <RoleCheckedRoute
                                            path="/top-up/management/new"
                                            exact={true}
                                            allowedRoles={managerRoles}
                                            component={<NewPackage />}
                                        />
                                        <RoleCheckedRoute path="/coupons" exact={true} allowedRoles={managerRoles} component={<Coupons />} />

                                        <RoleCheckedRoute
                                            path="/top-up/management/:id"
                                            allowedRoles={managerRoles}
                                            component={<NewPackage isEdit />}
                                        />
                                        <RoleCheckedRoute path="/top-up/report" allowedRoles={managerRoles} component={<TopUpReport />} />
                                        <RoleCheckedRoute path="/entrance/report" allowedRoles={managerRoles} component={<EntranceReport />} />
                                        <RoleCheckedRoute
                                            path="/customerClasses"
                                            exact={true}
                                            allowedRoles={managerRoles}
                                            component={<CustomerClasses />}
                                        />
                                        <RoleCheckedRoute path="/faq" exact={true} allowedRoles={editorRoles} component={<FaqList />} />
                                        <RoleCheckedRoute
                                            path="/homepageSlides"
                                            exact={true}
                                            allowedRoles={editorRoles}
                                            component={<HomepageSlides />}
                                        />
                                        <RoleCheckedRoute path="/homepageSlide/:id" allowedRoles={editorRoles} component={<HomepageSlide />} />
                                        <RoleCheckedRoute path="/cafe/cats" exact={true} allowedRoles={managerRoles} component={<CafeCategories />} />
                                        <RoleCheckedRoute path="/cafe/cats/:catId" allowedRoles={managerRoles} component={<CafeItems />} />
                                        <RoleCheckedRoute path="/cafe/cashier" exact={true} allowedRoles={managerRoles} component={<CafeCashier />} />
                                        <RoleCheckedRoute
                                            path="/cafe/queue"
                                            exact={true}
                                            allowedRoles={managerRoles}
                                            component={<CafeOrderQueue />}
                                        />

                                        <RoleCheckedRoute
                                            path="/groupClasses/presets"
                                            exact={true}
                                            allowedRoles={managerRoles}
                                            component={<GroupClassTypesPage />}
                                        />
                                        <RoleCheckedRoute
                                            path="/groupClasses/slots"
                                            exact={true}
                                            allowedRoles={coachRoles}
                                            component={<GroupClassSlots />}
                                        />
                                        <RoleCheckedRoute
                                            path="/groupClasses/newSlot"
                                            exact={true}
                                            allowedRoles={coachRoles}
                                            component={<NewGroupClassSlot />}
                                        />

                                        <RoleCheckedRoute path="/pricing" exact={true} allowedRoles={managerRoles} component={<Pricing />} />

                                        <RoleCheckedRoute path="/pageContents" allowedRoles={editorRoles} component={<PageContents />} />
                                        <RoleCheckedRoute path="/otherContents" allowedRoles={editorRoles} component={<OtherContents />} />

                                        <Redirect to={"/sessions/upcoming"}></Redirect>
                                    </Switch>
                                </AuthCheck>
                            </IonRouterOutlet>
                        </IonSplitPane>
                    </IonReactRouter>
                </>
            )}
            <IonLoading spinner={"dots"} isOpen={!authUserIsInit} />
            <IonLoading isOpen={isLoading} message="Loading"></IonLoading>
        </IonApp>
    );
};

export default App;

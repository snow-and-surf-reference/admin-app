import {
    IonAccordion,
    IonAccordionGroup,
    IonButton,
    IonCard,
    IonCardContent,
    IonChip,
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonMenu,
} from "@ionic/react";
import { RoleType } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { logoutStaff } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAuthUser } from "app/slices/authSlice";
import { setConnectToScanner, setWristbandId } from "app/slices/wristbandSlice";
import { RootState } from "app/store";
import {
    addCircleOutline,
    bagCheckOutline,
    barbellOutline,
    barcodeOutline,
    bodyOutline,
    bookOutline,
    briefcaseOutline,
    calendarNumberOutline,
    calendarOutline,
    cashOutline,
    clipboardOutline,
    constructOutline,
    desktopOutline,
    diamondOutline,
    documentOutline,
    fastFoodOutline,
    footstepsOutline,
    giftOutline,
    homeOutline,
    listOutline,
    newspaperOutline,
    peopleOutline,
    search,
    settingsOutline,
    ticketOutline,
    timeOutline,
    trashOutline,
    trophyOutline,
    watchOutline,
} from "ionicons/icons";
import { useCallback, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import "./Menu.css";

interface PageTabsProps {
    icon: string;
    title: string;
    link: string;
    closeMenu?: () => void;
    permission: RoleType[];
}

interface Tab {
    mainIcon: string;
    main: string;
    value: string;
    pages: PageTabsProps[];
    permission?: RoleType[];
}

interface PageAccordionProps {
    tabs: Tab;
    closeMenu: () => void;
}

const tabs: Tab[] = [
    {
        mainIcon: calendarNumberOutline,
        main: "Sessions",
        value: "sessions",
        pages: [
            {
                icon: timeOutline,
                title: "Upcoming",
                link: `/sessions/upcoming`,
                permission: ["admin", "sales", "manager"],
            },
            {
                icon: search,
                title: "Search",
                link: `/session/search`,
                permission: ["admin", "sales", "manager"],
            },
        ],
    },
    {
        mainIcon: peopleOutline,
        main: "Customers",
        value: "customers",
        pages: [
            {
                icon: addCircleOutline,
                title: "Create Sub Account",
                link: `/customers/sub-account`,
                permission: ["admin", "sales", "manager"],
            },
            {
                icon: search,
                title: "Search main account",
                link: `/customers/search`,
                permission: ["admin", "sales", "manager"],
            },
            {
                icon: search,
                title: "Search sub account",
                link: `/sub-account/search`,
                permission: ["admin", "sales", "manager"],
            },
        ],
    },
    {
        mainIcon: bodyOutline,
        main: "Staff",
        value: "staff",
        pages: [
            {
                icon: addCircleOutline,
                title: "Create staff",
                link: `/staff/create`,
                permission: ["admin", "manager"],
            },
            {
                icon: listOutline,
                title: "Staff List",
                link: `/staff/list`,
                permission: ["admin", "manager"],
            },
            // {
            //     icon: briefcaseOutline,
            //     title: "Role & Permissions",
            //     link: `/staff/permissions`,
            //     permission: ["admin"],
            // },
            {
                icon: trashOutline,
                title: "Removed Staff",
                link: `/staff/removed`,
                permission: ["admin", "manager"],
            },
        ],
    },
    {
        mainIcon: bodyOutline,
        main: "Coach",
        value: "coach",
        pages: [
            {
                icon: addCircleOutline,
                title: "Create coach",
                link: `/coach/create`,
                permission: ["admin", "manager"],
            },
            {
                icon: listOutline,
                title: "Coach List",
                link: `/coach/list`,
                permission: ["admin", "manager"],
            },
            {
                icon: briefcaseOutline,
                title: "Coach schedule",
                link: `/coach/schedule`,
                permission: ["admin", "manager", "coach"],
            },
            {
                icon: trashOutline,
                title: "Removed Coach",
                link: `/coach/removed`,
                permission: ["admin", "manager"],
            },
        ],
    },
    {
        mainIcon: barbellOutline,
        main: "Group Class",
        value: "groupClass",
        pages: [
            {
                icon: constructOutline,
                title: "Class Presets",
                link: `/groupClasses/presets`,
                permission: ["admin", "manager"],
            },
            {
                icon: clipboardOutline,
                title: "Class Slots",
                link: `/groupClasses/slots`,
                permission: ["admin", "manager", "coach"],
            },
        ],
    },
    {
        mainIcon: settingsOutline,
        main: "Admins",
        value: "admins",
        pages: [
            {
                icon: addCircleOutline,
                title: "Venue maintenance",
                link: `/venue/maintenance`,
                // !!!! ADMIN CONTROL PRIVATE HIRE - 9 OR OTHER VALUE !!!!
                permission: ["admin", "manager"],
            },
            {
                icon: calendarOutline,
                title: "Venue availability",
                link: `/venue/availability`,
                permission: ["admin"],
            },
            {
                icon: cashOutline,
                title: "Pricing",
                link: `/pricing`,
                permission: ["admin"],
            },
            {
                icon: footstepsOutline,
                title: "Gears management",
                link: `/gears/management`,
                permission: ["admin", "manager"],
            },
            {
                icon: giftOutline,
                title: "TopUp packages",
                link: `/top-up/management`,
                permission: ["admin"],
            },
            {
                icon: documentOutline,
                title: "TopUp reports",
                link: `/top-up/report`,
                permission: ["admin", "manager"],
            },
            {
                icon: documentOutline,
                title: "Entrance reports",
                link: `/entrance/report`,
                permission: ["admin", "manager"],
            },
            {
                icon: ticketOutline,
                title: `Coupons Management`,
                link: `/coupons`,
                permission: [`admin`, `manager`],
            },
            {
                icon: trophyOutline,
                title: "Customer Classes",
                link: `/customerClasses`,
                permission: ["admin"],
            },
            {
                icon: listOutline,
                title: "FAQ",
                link: `/faq`,
                permission: ["admin", "manager", "editor"],
            },
            {
                icon: desktopOutline,
                title: "Homepage Slides",
                link: `/homepageSlides`,
                permission: ["admin", "manager", "editor"],
            },
            {
                icon: newspaperOutline,
                title: "Page Contents",
                link: `/pageContents`,
                permission: ["admin", "manager", "editor"],
            },
            {
                icon: newspaperOutline,
                title: "Other Contents",
                link: `/otherContents`,
                permission: ["admin", "manager", "editor"],
            },
        ],
    },
    {
        mainIcon: giftOutline,
        main: "Shops",
        value: "shops",
        pages: [
            {
                icon: homeOutline,
                title: "Shop management",
                link: `/shop/management`,
                permission: ["admin", "manager"],
            },
            {
                icon: homeOutline,
                title: "Removed Shops",
                link: `/shop/removed`,
                permission: ["admin", "manager"],
            },
            {
                icon: giftOutline,
                title: "Inventory management",
                link: `/inventory/management`,
                permission: ["admin", "manager"],
            },
            {
                icon: trashOutline,
                title: "Removed shop item",
                link: `/inventory/removed`,
                permission: ["admin", "manager"],
            },
            {
                icon: cashOutline,
                title: "Cashier",
                link: `/shop/cashier`,
                permission: ["admin", "manager"],
            },
            {
                icon: cashOutline,
                title: "Refund items",
                link: `/shop/refund`,
                permission: ["admin", "manager"],
            },
            {
                icon: documentOutline,
                title: "Retail report",
                link: `/shop/report`,
                permission: ["admin", "manager"],
            },
        ],
    },
    {
        mainIcon: fastFoodOutline,
        main: "Cafe",
        value: "cafe",
        pages: [
            {
                icon: bagCheckOutline,
                title: "Cashier",
                link: `/cafe/cashier`,
                permission: ["admin", "manager"],
            },
            {
                icon: clipboardOutline,
                title: "Order Queue",
                link: `/cafe/queue`,
                permission: ["admin", "manager"],
            },
            {
                icon: bookOutline,
                title: "Menu Management",
                link: `/cafe/cats`,
                permission: ["admin", "manager"],
            },
        ],
    },
    {
        mainIcon: watchOutline,
        main: "Wristband check",
        value: "wristband",
        pages: [
            {
                icon: watchOutline,
                title: "Wristband check",
                link: `/wrist-check`,
                permission: ["admin", "manager", "sales"],
            },
        ],
    },
];

const Menu: React.FC = () => {
    const authUser = useAppSelector(selectAuthUser);
    const menuRef = useRef<HTMLIonMenuElement | null>(null);
    const portRef = useRef<SerialPort | null>(null);
    const readerShouldCloseRef = useRef(false);
    const readerRef = useRef<ReadableStreamDefaultReader | null>(null);
    const dispatch = useDispatch();
    const connectToScanner = useSelector((state: RootState) => state.wristband.connectToScanner);

    const closePort = async (port: SerialPort) => {
        try {
            await readerRef.current?.releaseLock();
            await port.close();
        } catch (e) {
            console.error(e);
        }
    };

    const connectNfcReader = useCallback(async () => {
        try {
            if (!portRef.current) {
                const port = await navigator.serial.requestPort();
                await port.open({ baudRate: 9600 });
                portRef.current = port;
            }

            const reader = portRef.current.readable?.getReader();
            let buf = new Uint8Array(0);

            if (reader) {
                const endPortUsage = async () => {
                    reader.cancel();
                    // await readableStreamClosed.catch(() => {});
                    reader.releaseLock();
                    portRef.current?.close();
                };
                const loop = async () => {
                    if (readerShouldCloseRef.current) {
                        await endPortUsage();
                        readerShouldCloseRef.current = false;
                        return;
                    }
                    try {
                        const { value } = await reader.read();
                        if (value) {
                            const originalBuf = buf;
                            buf = new Uint8Array(originalBuf.length + value.length);
                            buf.set(originalBuf);
                            buf.set(value, originalBuf.length);
                            // console.log(buf);

                            // 02 is the start of text and 03 is the end of text
                            const message = {
                                start: buf.indexOf(0x02),
                                end: buf.indexOf(0x03),
                            };

                            if (message.start !== -1 && message.end !== -1) {
                                dispatch(setWristbandId(new TextDecoder("ascii").decode(buf.slice(message.start + 1, message.end)).trim()));
                                buf = new Uint8Array(0);
                            }
                        }
                    } catch (error) {
                        console.error(error);
                    }
                    requestAnimationFrame(loop);
                };
                loop();
            }
        } catch (err) {
            closePort(portRef.current!);
            console.error(err);
        }
    }, [dispatch]);

    useEffect(() => {
        if (connectToScanner) {
            readerShouldCloseRef.current = false;
            connectNfcReader();
        } else {
            readerShouldCloseRef.current = true;
            dispatch(setWristbandId(""));
        }
    }, [connectToScanner, connectNfcReader, dispatch]);

    const handleConnectScanner = async () => {
        if (!!portRef.current) return;
        const port = await navigator.serial.requestPort();
        await port.open({ baudRate: 9600 });
        portRef.current = port;
        dispatch(setConnectToScanner(true));
    };

    const handleLogout = async () => {
        await logoutStaff();
        window.location.assign(`/login`);
    };

    return (
        <>
            <IonMenu contentId="main" type="overlay" className="main-menu" ref={menuRef} style={{ opacity: authUser ? "1" : "0" }}>
                <IonContent>
                    {authUser ? (
                        <>
                            <IonCard>
                                <IonCardContent>
                                    <IonLabel>
                                        {authUser ? <h3>{authUser.email}</h3> : "Loading..."}
                                        {authUser ? <p>{authUser.role}</p> : null}
                                    </IonLabel>

                                    <br />
                                    {authUser && !!authUser.allowance && authUser?.allowance > 0 && (
                                        <>
                                            <IonChip>
                                                <IonIcon icon={diamondOutline} />
                                                <IonLabel>{Number(authUser?.wallet.balance).toLocaleString()}</IonLabel>
                                            </IonChip>
                                            <br />
                                        </>
                                    )}
                                    <br />
                                    <IonButton
                                        expand="block"
                                        size="small"
                                        color={"medium"}
                                        onClick={() => {
                                            handleLogout();
                                        }}
                                    >
                                        Logout
                                    </IonButton>
                                </IonCardContent>
                            </IonCard>
                            <IonAccordionGroup>
                                {tabs.map((i) => (
                                    <PageAccordion
                                        key={i.main}
                                        tabs={i}
                                        closeMenu={() => {
                                            menuRef.current?.close();
                                        }}
                                    />
                                ))}
                            </IonAccordionGroup>
                            <IonButton
                                className="ion-padding"
                                disabled={connectToScanner}
                                expand="block"
                                onClick={handleConnectScanner}
                                color={connectToScanner ? "medium" : "success"}
                            >
                                <IonIcon icon={barcodeOutline} slot="start" />
                                {connectToScanner ? "Connected to scanner" : "Connect scanner"}
                            </IonButton>
                        </>
                    ) : null}
                </IonContent>
            </IonMenu>
        </>
    );
};

export default Menu;

export const PageTabs = (props: PageTabsProps) => {
    const history = useHistory();
    const authUser = useAppSelector(selectAuthUser);
    return (
        <>
            {authUser && props.permission.includes(authUser.role) ? (
                <IonItem
                    button
                    onClick={() => {
                        if (props.closeMenu) {
                            props.closeMenu();
                        }
                        history.push(props.link);
                    }}
                    style={{ paddingLeft: "40px" }}
                >
                    {/* {props.title.includes("Refund") && (
                        <IonText slot="start" style={{ fontSize: "1.5em" }}>
                            💸
                        </IonText>
                    )} */}
                    {/* {!props.title.includes("Refund") &&  */}
                    <IonIcon icon={props.icon} slot="start" color="medium" />

                    <IonLabel>{props.title}</IonLabel>
                </IonItem>
            ) : null}
        </>
    );
};

export const PageAccordion = (props: PageAccordionProps) => {
    return (
        <IonAccordion value={props.tabs.value}>
            <IonItem slot="header">
                <IonIcon icon={props.tabs.mainIcon} slot="start" />
                <IonLabel>{props.tabs.main}</IonLabel>
            </IonItem>
            <IonList slot="content">
                {props.tabs.pages.map((i) => (
                    <PageTabs
                        key={i.link}
                        icon={i.icon}
                        title={i.title}
                        link={i.link}
                        permission={i.permission}
                        closeMenu={() => props.closeMenu()}
                    />
                ))}
            </IonList>
        </IonAccordion>
    );
};

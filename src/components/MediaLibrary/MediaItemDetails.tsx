import { IonButton, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonTitle, IonToolbar } from "@ionic/react";
import { getFileByPath } from "app/firebase";
import LoadingCard from "components/Global/LoadingCard";
import { chevronBack, copyOutline } from "ionicons/icons";
import ImageComp from "pages/Cafe/ImageComp";
import { useEffect, useState } from "react";
import { MediaListItem } from "./MediaListContent";

interface Props {
    onDismiss: () => void;
    item: MediaListItem | null;
}

const MediaItemDetails: React.FC<Props> = (props) => {
    const { onDismiss, item } = props;
    const [src, setSrc] = useState<string>("");

    useEffect(() => {
        const getSrc = async (path: string) => {
            const url = await getFileByPath(path);
            setSrc(url);
        };
        if (!item) {
            setSrc("");
        } else {
            getSrc(item.path);
        }
    }, [item]);

    return (
        <>
            <IonHeader>
                <IonToolbar>
                    <IonButton
                        fill="clear"
                        color={"medium"}
                        slot="start"
                        onClick={() => {
                            onDismiss();
                        }}
                    >
                        <IonIcon icon={chevronBack} slot="start" />
                    </IonButton>
                    <IonTitle>Media Item Details</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                {!item || !src ? (
                    <LoadingCard />
                ) : (
                    <>
                        {item.folder === "contentImages" ? (
                            <ImageComp
                                src={src}
                                alt={item.name}
                                width={`100%`}
                                height={`50%`}
                                style={{
                                    margin: "0",
                                }}
                            />
                        ) : null}

                        <IonList inset color="light">
                            <IonItem color={"light"}>
                                <IonLabel>
                                    <p>File Name</p>
                                    <h4>
                                        <b>{item.name}</b>
                                    </h4>
                                </IonLabel>
                            </IonItem>
                            <IonItem color={"light"}>
                                <IonLabel>
                                    <p>URL</p>
                                    <h4>
                                        <b>{src}</b>
                                    </h4>
                                </IonLabel>
                                <IonButton slot="end" fill="clear" color={"secondary"}>
                                    <IonIcon icon={copyOutline} slot="start" />
                                    <IonLabel>Copy</IonLabel>
                                </IonButton>
                                </IonItem>
                                <IonItem color={"light"}>
                                <IonLabel>
                                    <p>Display HTML</p>
                                    <h4>
                                        <b>{`<img src="${src}" alt="${item.name}" />`}</b>
                                    </h4>
                                </IonLabel>
                                    <IonButton slot="end" fill="clear" color={"secondary"}
                                        onClick={() => {
                                            navigator.clipboard.writeText(`<img src="${src}" alt="${item.name}" />`);
                                        }}
                                    >
                                    <IonIcon icon={copyOutline} slot="start" />
                                    <IonLabel>Copy</IonLabel>
                                </IonButton>
                            </IonItem>
                            <IonItem color={"light"}>
                                <IonLabel>
                                    <p>Download Button HTML</p>
                                    <h4>
                                        <b>{`<a class="button" href="${src}">Download</a>`}</b>
                                    </h4>
                                </IonLabel>
                                <IonButton slot="end" fill="clear" color={"secondary"}>
                                    <IonIcon icon={copyOutline} slot="start" />
                                    <IonLabel>Copy</IonLabel>
                                </IonButton>
                            </IonItem>
                        </IonList>
                    </>
                )}
            </IonContent>
        </>
    );
};

export default MediaItemDetails;

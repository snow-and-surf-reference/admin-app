import { IonButton, IonCard, IonCardContent, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonSegment, IonSegmentButton, IonTitle, IonToolbar } from "@ionic/react";
import { listAllMediaFiles } from "app/firebase";
import LoadingCard from "components/Global/LoadingCard";
import { closeOutline, cloudUploadOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { MediaLibraryPage } from "./MediaLibraryModal";

export interface MediaListItem {
    name: string;
    folder: string;
    path: string;
}

interface Props {
    onDismiss: () => void;
    onChangePage: (page: MediaLibraryPage) => void;
    onSelectItem: (item: MediaListItem) => void;
}

const MediaListContent: React.FC<Props> = (props) => {
    const { onDismiss, onChangePage, onSelectItem } = props;
    const [items, setItems] = useState<MediaListItem[] | null>(null);
    const [itemCat, setItemCat] = useState("contentImages");

    useEffect(() => {
        const featchList = async () => {
            const res = await listAllMediaFiles();
            console.log(res);
            setItems(res);
        };
        featchList();
    }, []);

    return (
        <>
            <IonHeader>
                <IonToolbar>
                    <IonButton
                        fill="clear"
                        color={"medium"}
                        slot="start"
                        onClick={() => {
                            onDismiss();
                        }}
                    >
                        <IonIcon icon={closeOutline} slot="start" />
                    </IonButton>
                    <IonButton
                        fill="clear"
                        color={'danger'}
                        slot='end'
                        onClick={() => {
                            onChangePage('upload')
                        }}
                    >
                        <IonIcon icon={cloudUploadOutline} slot="start" />
                        <IonLabel>Upload</IonLabel>
                    </IonButton>
                    <IonTitle>Media Library</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent color={"light"}>
                {!items ? (
                    <LoadingCard />
                ) : (
                    <>
                        <IonCard>
                            <IonCardContent>
                                <IonSegment
                                    value={itemCat}
                                    onIonChange={(e) => {
                                        setItemCat(e.detail.value!);
                                    }}
                                >
                                    <IonSegmentButton value="contentImages">Images</IonSegmentButton>
                                    <IonSegmentButton value="videos">Videos</IonSegmentButton>
                                    <IonSegmentButton value="files">Files</IonSegmentButton>
                                </IonSegment>
                            </IonCardContent>
                        </IonCard>

                        <IonCard>
                            <IonList inset>
                                    {
                                        items.filter(x => x.folder === itemCat).map(y => (
                                            <IonItem
                                                key={`media-item-${y.path}`}
                                                button
                                                onClick={() => {
                                                    onChangePage('item');
                                                    onSelectItem(y);
                                                }}
                                            >
                                                <IonLabel>
                                                    <h3>{y.name}</h3>
                                                    <p>{y.path}</p>
                                                </IonLabel>
                                            </IonItem>
                                        ))
                                }
                            </IonList>
                        </IonCard>
                    </>
                )}
            </IonContent>
        </>
    );
};

export default MediaListContent;

import { IonButton, IonContent, IonHeader, IonIcon, IonTitle, IonToolbar } from "@ionic/react";
import LoadingCard from "components/Global/LoadingCard";
import { chevronBack } from "ionicons/icons";

interface Props{
    onDismiss: () => void;
}

const MediaUpload: React.FC<Props> = (props) => {
    const { onDismiss } = props;
    return (
        <>
            <IonHeader>
                <IonToolbar>
                    <IonButton
                        fill="clear"
                        color={'medium'}
                        slot='start'
                        onClick={() => {
                            onDismiss()
                        }}
                    >
                        <IonIcon icon={chevronBack} slot='start' />
                    </IonButton>
                    <IonTitle>Upload Media / Files</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <LoadingCard/>
            </IonContent>
        </>
    )
}

export default MediaUpload;
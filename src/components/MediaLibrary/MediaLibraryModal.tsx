import { IonModal } from "@ionic/react";
import { useState } from "react";
import MediaItemDetails from "./MediaItemDetails";
import MediaListContent, { MediaListItem } from "./MediaListContent";
import MediaUpload from "./MediaUpload";

export type MediaLibraryPage = "list" | "upload" | "item";

interface Props {
    isOpen: boolean;
    onDismiss: () => void;
    targetPage?: MediaLibraryPage;
}

// this is the modal holding the media library pages
const MediaLibraryModal: React.FC<Props> = (props) => {
    const { isOpen, onDismiss, targetPage } = props;
    const [page, setPage] = useState<MediaLibraryPage>(targetPage ? targetPage : "list");
    const [item, setItem] = useState<MediaListItem | null>(null);

    return (
        <IonModal
            isOpen={isOpen}
            onDidDismiss={() => {
                setPage("list");
                onDismiss();
            }}
        >
            {page === "list" ? (
                <MediaListContent
                    onDismiss={onDismiss}
                    onChangePage={(page: MediaLibraryPage) => {
                        setPage(page);
                    }}
                    onSelectItem={(item) => {
                        setItem(item);
                    }}
                />
            ) : page === "item" ? (
                <MediaItemDetails
                    onDismiss={() => {
                        setPage("list");
                    }}
                    item={item}    
                />
            ) : (
                <MediaUpload
                    onDismiss={() => {
                        setPage("list");
                    }}
                />
            )}
        </IonModal>
    );
};

export default MediaLibraryModal;

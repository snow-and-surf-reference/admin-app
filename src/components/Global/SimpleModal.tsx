import { IonModal, IonContent, IonGrid, IonRow, IonCol, IonText, IonButton } from "@ionic/react";
import { ModalHeader } from "pages/Sessions/UserSearch";
import React from "react";

interface SimpleModalProps {
    headerText: string;
    onConfirm: () => void;
    onCancel: () => void;
    text?: string;
    isOpen: boolean;
    bodyChild?: React.ReactNode;
    confirmButtonText?: string;
}

function SimpleModal(props: SimpleModalProps) {
    return (
        <IonModal isOpen={props.isOpen} onDidDismiss={props.onCancel}>
            <ModalHeader text={props.headerText} />
            <IonContent>
                <IonGrid className="flex-column-between">
                    <IonRow />
                    <IonRow className="full-w">
                        {props.bodyChild ? (
                            props.bodyChild
                        ) : (
                            <IonCol className="ion-text-center">
                                <IonText>{props.text}</IonText>
                            </IonCol>
                        )}
                    </IonRow>
                    <IonRow className="full-w">
                        <IonCol>
                            <IonButton expand="block" onClick={props.onConfirm}>
                                {props.confirmButtonText ?? "CONFIRM"}
                            </IonButton>
                            <IonButton expand="block" color="medium" onClick={props.onCancel}>
                                CANCEL
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonModal>
    );
}

export default SimpleModal;

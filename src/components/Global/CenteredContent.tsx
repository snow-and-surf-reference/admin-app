import { IonContent } from "@ionic/react";

type FlexDirection = "column" | "inherit" | "-moz-initial" | "initial" | "revert" | "unset" | "column-reverse" | "row" | "row-reverse" | undefined;
interface Props{
	direction?: string;
	className?: string;
	style?: any;
	children?: React.ReactNode;
}

const CenteredContent: React.FC<Props> = (props) => {
    const { direction, className, style, children } = props

	return (
		<IonContent
			className={className ? className : ''}
            style={style ? style : {}}
        >
            <div
                className="fluid"
				style={{
                    width: "100%",
					minHeight: "100%",
					position: "relative",
					display: "flex",
					justifyContent: "center",
					alignItems: "center",
					flexDirection: direction as FlexDirection || "column",
				}}
			>
				{children}
			</div>
		</IonContent>
	);
};

export default CenteredContent;

import { useEffect } from 'react';
import { Staff } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { useAppSelector } from "app/hooks";
import { selectAuthUser, selectAuthUserIsInit } from "app/slices/authSlice";
import { useHistory, useLocation } from "react-router";

interface Props {
	children: any;
}

const authPaths = [
	`login`,
	`sign-up`
]

const AuthCheck: React.FC<Props> = (props) => {
	const authUserIsInit: boolean = useAppSelector(selectAuthUserIsInit);
	const authUser: Staff | null = useAppSelector(selectAuthUser);
	const location = useLocation();
	const history = useHistory();

	useEffect(() => {
        if (!authUserIsInit) { return };

		if (authPaths.filter(path => location.pathname.includes(path)).length > 0) {
			if (authUser) {
				history.replace('/');
				return
			}
        } else {
            if (!authUser) {
                history.replace('/login');
				return
			}
			
        }
	}, [authUser, authUserIsInit, location, history])

	return props.children;
};

export default AuthCheck;

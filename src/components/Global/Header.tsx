import { IonButton, IonButtons, IonHeader, IonIcon, IonMenuButton, IonTitle, IonToolbar } from "@ionic/react";
import { arrowBackOutline } from "ionicons/icons";
import { useHistory } from "react-router";

interface Props {
    title: string;
    back?: boolean | string;
    extraFunction?: () => void;
}

const Header: React.FC<Props> = (props) => {
    const { title, back, extraFunction } = props;
    const history = useHistory();

    return (
        <IonHeader>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonMenuButton />
                    {back === true ? (
                        <IonButton onClick={() => (extraFunction ? extraFunction() : history.goBack())}>
                            <IonIcon icon={arrowBackOutline} slot="icon-only" />
                        </IonButton>
                    ) : typeof back === "string" ? (
                        <IonButton onClick={() => history.push(back)}>
                            <IonIcon icon={arrowBackOutline} slot="icon-only" />
                        </IonButton>
                    ) : null}
                </IonButtons>
                <IonTitle>{title}</IonTitle>
            </IonToolbar>
        </IonHeader>
    );
};

interface HeaderWithButtonProps {
    title: string;
    back?: boolean;
    onClick: () => void;
    buttonText: string;
    color?: string;
}

export const HeaderWithButton: React.FC<HeaderWithButtonProps> = (props) => {
    const { title, back, onClick, buttonText, color } = props;
    const history = useHistory();

    return (
        <IonHeader>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonMenuButton />
                    {back && (
                        <IonButton onClick={() => history.goBack()}>
                            <IonIcon icon={arrowBackOutline} slot="icon-only" />
                        </IonButton>
                    )}
                </IonButtons>
                <IonTitle>{title}</IonTitle>
                <IonButton color={color} slot="end" onClick={onClick}>
                    {buttonText}
                </IonButton>
            </IonToolbar>
        </IonHeader>
    );
};

export default Header;

import { IonItem, IonLabel } from "@ionic/react";
import React from "react";

function CancelPolicy() {
    return (
        <IonItem>
            <IonLabel>
                <p>{"・ 7+ days before session - 100% refund"}</p>
                <p>{"・ Within 7 days to 48 hours prior session - 50% refund"}</p>
                <p>{"・ Within 48 hours prior session - No refund"}</p>
            </IonLabel>
        </IonItem>
    );
}

export default CancelPolicy;

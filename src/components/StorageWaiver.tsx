import { IonCol, IonRow } from "@ionic/react";
import React from "react";

function StorageWaiver() {
    const terms = [
        `You warrant that throughout the term of this Agreement all items (“Goods”) that are entrusted to us are and a shall remain your property or that you have the expressed permission and authority of the owner of the Goods to use our services in accordance with this Agreement.`,
        `Snow & Surf does not warrant that the storage space used is a suitable place or means of storage for any particular goods.`,
        `All Goods must be packed inside a suitable bag (as determined solely by Snow & Surf) and that each bag must not exceed 20kg.  You are responsible to carefully pack all Goods and protect them with protective wrapping or padding as you determine necessary.`,
        `We or anyone acting on our behalf may at any time without notifying you open your bags to inspect your Goods.`,
        `Goods stored must not include prohibited, illegal stolen, perishable and flammable items. Other prohibited Goods include but are not limited to chemicals, drugs, hazardous or toxic materials of any kind; food or perishable goods of any kind; flammable, firearms, weapons or explosives of any kind; items which emit any kind of odor or fumes; plants or creatures (whether living or dead); liquids or compressed gases; illegal drugs, counterfeit goods, stolen property or illegal items of any kind; personal property that would result in the violation of any law or regulation of any governmental authority; any items that, in our determination, danger the safety and health of people in our facility and the environment.`,
        `You understand and agree that, you waive all rights and claims against Snow & Surf arising in any way directly or indirectly from the arrangement contemplated herein.  Snow & Surf shall not in any event be held liable or responsible for any damage or loss to the Goods or its containing bag.`,
        `You acknowledge and accept that there is a risk that the Goods (including its containing bag) may be accidentally scratched, dented, chipped, marred or damaged.  As such, you understand and agree that you waive all rights and claims against Snow & Surf should your item(s) be damaged (including but not limited to natural growth of mood or mildew) or stolen.`,
        `We may refuse to store any Goods or may return to you any Goods, at your cost, at any time, if we solely believe that the storage or continued storage of such Goods would represent a risk to the safety of any person, the security of the storage facilities, or any other Goods stored at the storage facilities.`,
        `You may terminate our service at any time however, there are no refunds for termination for the then-current service period.  No refund will be made if the storage service is terminated in any way.`,
        `Upon termination of our service (including by way of early termination in accordance with the terms of this Agreement or by completion of the agreed storage duration), you acknowledge and agree that you are responsible for the removal of the Goods no later than 7 calendar days and in failure to do so, Snow & Surf shall have the rights to dispose the Goods (and its containing bag) at its own discretion without notifying you.`,
        `You agree to indemnify, defend and hold us, our employees, agents, suppliers and directors harmless, from and against all claims, liabilities, damages, costs, expenses, losses and legal fees arising out of any breach of the Agreement by you.  This clause also applies to any other liabilities arising out of your use of any other service provided by Snow & Surf.`,
        `This Agreement and any disputes or claims arising out of or in connection with it or its subject matter or formation shall be governed by and construed in accordance with the laws of Hong Kong.`,
    ];

    return (
        <>
            <IonRow className="ion-padding">
                <IonCol style={{ textAlign: "center" }}>
                    <strong>{`Storage Usage Agreement ("Agreement")`}</strong>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    {`By accepting the service, you expressly agree to the following terms which constitute an agreement between you and Snow and Surf
                Limited (“Snow & Surf”):`}
                </IonCol>
            </IonRow>
            {terms.map((i, idx) => (
                <IonRow key={idx}>
                    <IonCol size="1" style={{ textAlign: "center" }}>
                        {idx + 1}
                    </IonCol>
                    <IonCol>{i}</IonCol>
                </IonRow>
            ))}
            <br />
        </>
    );
}

export default StorageWaiver;

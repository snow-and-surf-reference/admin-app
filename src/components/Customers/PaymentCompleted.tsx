import {
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardTitle,
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonPage,
    IonSpinner,
} from "@ionic/react";
import * as SessionType from "@tsanghoilun/snow-n-surf-interface/types/session";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { printOutline, shareSocialOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { getSessionById } from "../../app/firebase";
import { useSearchParams } from "../../app/hooks/useSearchParams";
import Header from "../../components/Global/Header";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const PaymentCompleted: React.FC = () => {
    const { sessionId } = useParams<{ sessionId: string }>();
    const searchParams = useSearchParams();
    const [session, setSession] = useState<SessionType.Session | undefined | null>(undefined);
    // const authUser = useAppSelector(selectAuthUser);

    const history = useHistory();

    useEffect(() => {
        if (!sessionId) {
            return;
        }
        const getSession = async () => {
            const res = await getSessionById(sessionId);
            setSession(res);
        };
        getSession();
    }, [sessionId]);

    useEffect(() => {
        searchParams.forEach((item) => {
            // console.log(item);
        });
    }, [searchParams]);

    useEffect(() => console.log("payment confirm > session", session), [session]);
    // useEffect(() => console.log('payment confirm > authuser', authUser), [authUser]);

    return (
        <IonPage>
            <Header title={`Order Confrimed`} />
            <IonContent className="fluid">
                {session === undefined ? (
                    // || !authUser
                    <>
                        <IonCard className="ion-text-center" color={"dark"}>
                            <IonCardContent>
                                <IonLabel>
                                    <h3>Loading...</h3>
                                </IonLabel>
                                <IonSpinner name="dots" />
                            </IonCardContent>
                        </IonCard>
                    </>
                ) : !session ? null : (
                    <>
                        <IonCard color="success" className="ion-text-center">
                            <IonCardHeader>
                                <IonCardTitle>You Booking is Confirmed!</IonCardTitle>
                            </IonCardHeader>
                        </IonCard>
                        <IonCard>
                            <IonCardContent>
                                <div className="ion-margin ion-text-center">
                                    <p>Order# (Use This to Check In!)</p>
                                    <div className="large-order-no">{session.bookingNumber}</div>
                                </div>

                                <IonLabel className="ion-margin ion-text-center">
                                    <p>
                                        Thank You for your booking! Please see below for the details of your order. <br />
                                        An Email has also been sent to{" "}
                                        <IonLabel color="tertiary">
                                            <b>{session.bookingUser?.email}</b>
                                        </IonLabel>{" "}
                                        containing the booking, order and payment details.
                                    </p>
                                </IonLabel>
                                <br />
                                <IonList lines="none">
                                    <IonItem>
                                        <IonLabel>
                                            <p>Order Number</p>
                                        </IonLabel>
                                        <IonLabel slot="end">
                                            <h3>
                                                <b>{session.bookingNumber}</b>
                                            </h3>
                                        </IonLabel>
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel>
                                            <p>Order Time</p>
                                        </IonLabel>
                                        <IonLabel slot="end">
                                            <h3>
                                                <b>{dayjs.tz(Number(session.confirmedAt)).format(`DD MMM YYYY, hh:mmA`)}</b>
                                            </h3>
                                        </IonLabel>
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel>
                                            <p>Booking</p>
                                        </IonLabel>
                                        <IonLabel slot="end" className="ion-text-end">
                                            <h3>
                                                {session.playType === "ski" || session.playType === "sb" || session.playType === "snow"
                                                    ? "Ski/Snowboarding"
                                                    : session.playType === "all"
                                                    ? "Full Park Rental"
                                                    : "Surf"}
                                            </h3>
                                            <h4>{dayjs.tz(Number(session.start)).format(`DD MMM YY, hh:mmA`)}</h4>
                                            <h4>{Math.abs(dayjs.tz(Number(session.start)).diff(dayjs.tz(Number(session.end)), "hours"))} Hour(s)</h4>
                                        </IonLabel>
                                    </IonItem>
                                    <br />
                                    <IonItem>
                                        <IonButtons slot="end">
                                            <IonButton color="warning">
                                                <IonIcon icon={printOutline} slot="icon-only" />
                                            </IonButton>
                                            <IonButton color="secondary">
                                                <IonIcon icon={shareSocialOutline} slot="icon-only" />
                                            </IonButton>
                                        </IonButtons>
                                    </IonItem>
                                    <br />
                                    <IonButton color={`light`} expand="block" onClick={() => history.push(`/`)}>
                                        Back To Home
                                    </IonButton>
                                </IonList>
                            </IonCardContent>
                        </IonCard>
                    </>
                )}
            </IonContent>
        </IonPage>
    );
};

export default PaymentCompleted;

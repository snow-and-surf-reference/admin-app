import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCheckbox,
    IonChip,
    IonCol,
    IonContent,
    IonFab,
    IonGrid,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonLoading,
    IonPage,
    IonRow,
    IonSpinner
} from "@ionic/react";
import * as SessionType from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { selectAuthUser } from "app/slices/authSlice";
import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import {
    alertCircleOutline,
    arrowForwardCircleOutline,
    closeCircleOutline,
    diamondOutline,
    ticketOutline,
    timeOutline,
    walletOutline
} from "ionicons/icons";
import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { cancelLockedSession, getCustomerById, getSessionById, paySession } from "../../app/firebase";
import { useAppSelector } from "../../app/hooks";
import useBreakpoints from "../../app/hooks/useBreakpoints";
import Header from "../../components/Global/Header";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);
dayjs.extend(duration);

const NullSessionUI: React.FC = () => {
    const history = useHistory();

    return (
        <IonCard className="ion-text-center" color={`dark`}>
            <IonCardHeader>
                <IonCardTitle>Oh Snaps!</IonCardTitle>
            </IonCardHeader>
            <IonCardContent>
                <IonLabel>
                    <br />
                    <h3>
                        <b>Looks like the session doesn't exist, or it is already expired.</b>
                    </h3>
                    <br />
                </IonLabel>
                <IonButton
                    color={"warning"}
                    onClick={() => {
                        history.push(`/newBooking/single`);
                    }}
                >
                    Book Again
                </IonButton>
            </IonCardContent>
        </IonCard>
    );
};

const Checkout: React.FC = () => {
    const { sessionId } = useParams<{ sessionId: string }>();
    const [session, setSession] = useState<SessionType.Session | undefined | null>(undefined);
    const [bookingCustomer, setBookingCustomer] = useState<Customer | null>(null);
    const authStaff = useAppSelector(selectAuthUser);
    const [paying, setPaying] = useState(false);
    const [disable, setDisable] = useState(true);
    const history = useHistory();
    const [timer, setTimer] = useState<string | null>(null);
    const [cancelTime, setCancelTime] = useState<number | null>(null);
    const { isSm, isMd, isXs } = useBreakpoints();
    const [showCancelAlert, setShowCancelAlert] = useState(false);
    const [showLoading, setShowLoading] = useState(false);
    const [useStaffCredit, setUseStaffCredit] = useState(false);

    useEffect(() => {
        if (!sessionId) {
            return;
        }
        const getSession = async () => {
            const res = await getSessionById(sessionId);
            setSession(res);
            if (res && res.status === `confirmed`) {
                history.replace(`/sessionDetails/${res.id}`);
                return
            }
            if (res && res.lockedAt && res.lockedAt.valueOf() > Date.now()) {
                setCancelTime(dayjs(res.lockedAt).valueOf());
            }
        };
        getSession();
    }, [sessionId, history]);

    useEffect(() => {
        if (!cancelTime) {
            return;
        }
        setInterval(() => {
            setTimer(dayjs.duration(dayjs(cancelTime).diff(dayjs())).format(`mm:ss`));
        }, 1000);
    }, [cancelTime]);

    useEffect(() => {
        if (!cancelTime || !timer) {
            return;
        }
        const timeLeft = dayjs.duration(dayjs(cancelTime).diff(dayjs())).asSeconds();
        if (timeLeft <= 0) {
            history.go(0);
        }
    }, [timer, cancelTime, history]);

    // use Effect to check booking customer, session, the wallet balance and staff credit usage etc, for disable paying
    useEffect(() => {
        if (!bookingCustomer || !session) {
            setDisable(true);
            return
        }
        // set a let for temp total
        let tempTotal = session.totalCredits;

        // check if using staff credit
        if (useStaffCredit && authStaff) {
            tempTotal = Math.max(session.totalCredits - authStaff.wallet.balance, 0);
        }

        // check if wallet balance is enough to pay
        console.log('temp total before check', tempTotal)
        if (tempTotal > bookingCustomer.wallet.balance) {
            setDisable(true);
            return
        }

        setDisable(false);
    }, [session, bookingCustomer, useStaffCredit, authStaff]);

    useEffect(() => {
        if (!session) {
            return;
        }
        const customerId = session.bookingUser?.id;
        const getCustomer = async () => {
            if (!customerId) {
                return;
            }
            const customer = await getCustomerById(customerId);
            if (!customer) {
                return;
            }
            setBookingCustomer(customer);
        };
        getCustomer();
    }, [session]);

    const handlePay = async () => {
        setPaying(true);
        if (!bookingCustomer || !session) {
            return;
        }
        const customerId = bookingCustomer.id;
        const res = await paySession(session, customerId, useStaffCredit);
        if (res === "success") {
            setPaying(false);
            history.push(`/orderCompleted/${session.id}`);
        } else {
            setPaying(false);
        }
    };

    return (
        <IonPage>
            <Header title={`Checkout Order`} />
            <IonContent className="fluid">
                <IonCardHeader className="ion-text-center">
                    <IonCardTitle>Checkout</IonCardTitle>
                </IonCardHeader>
                {session === undefined ? (
                    <IonCard color={`dark`}>
                        <IonCardContent className="ion-text-center">
                            <IonLabel>
                                <h3>
                                    <b>Loading Your Order</b>
                                </h3>
                            </IonLabel>
                            <IonSpinner name="dots" />
                        </IonCardContent>
                    </IonCard>
                ) : session === null || bookingCustomer === null ? (
                    // || Number(session.lockedAt) < Date.now()
                    <NullSessionUI />
                ) : (
                    <>
                        <IonCard>
                            <IonCardHeader>
                                <IonCardTitle>
                                    <b>Order Details</b>
                                </IonCardTitle>
                            </IonCardHeader>
                            <IonCardContent>
                                <IonList className="ion-no-padding">
                                    <IonItem className="ion-no-padding" lines="none">
                                        <IonLabel>
                                            <h3>{session.pax} x Park Entrance</h3>
                                            <p>
                                                {dayjs.tz(session.start).format(`DD MMM YYYY, HH:mm`)} - {dayjs.tz(session.end).format(`HH:mm`)}
                                                {` (${dayjs.tz(session.end).diff(dayjs.tz(session.start), "hours")} hour${
                                                    dayjs.tz(session.end).diff(dayjs.tz(session.start), "hours") > 1 ? `s` : ``
                                                })`}
                                            </p>
                                        </IonLabel>
                                        <IonChip color="dark">
                                            <IonIcon icon={diamondOutline} size="small" />
                                            <IonLabel>
                                                <h3>
                                                    <b>{session.subTotals.entrances.toLocaleString()}</b>
                                                </h3>
                                            </IonLabel>
                                        </IonChip>
                                    </IonItem>
                                    {session.subTotals.gears > 0 ? (
                                        <IonItem className="ion-no-padding" lines="none">
                                            <IonLabel>
                                                <h3>{session.gears.length} x Gears Rentals</h3>
                                            </IonLabel>
                                            <IonChip color="dark">
                                                <IonIcon icon={diamondOutline} size="small" />
                                                <IonLabel>
                                                    <h3>
                                                        <b>{session.subTotals.gears.toLocaleString()}</b>
                                                    </h3>
                                                </IonLabel>
                                            </IonChip>
                                        </IonItem>
                                    ) : null}

                                    {session.subTotals.coach > 0 ? (
                                        <IonItem className="ion-no-padding" lines="none">
                                            <IonLabel>
                                                <h3>Coaching Fee</h3>
                                                <p>{session.sessionType === "groupClass" ? `Group Class` : "Private Training"}</p>
                                            </IonLabel>
                                            <IonChip color="dark">
                                                <IonIcon icon={diamondOutline} size="small" />
                                                <IonLabel>
                                                    <h3>
                                                        <b>{session.subTotals.coach.toLocaleString()}</b>
                                                    </h3>
                                                </IonLabel>
                                            </IonChip>
                                        </IonItem>
                                    ) : null}

                                    {session.subTotals.privateHire > 0 ? (
                                        <IonItem className="ion-no-padding" lines="none">
                                            <IonLabel>
                                                <h3>
                                                    {session.sessionType === "privateBelt"
                                                        ? "Private Snow Belt"
                                                        : session.sessionType === "privateLane"
                                                        ? "Private Surf Lane"
                                                        : session.sessionType === "fullSnow"
                                                        ? "Private Booking Full Snow Venue"
                                                        : session.sessionType === "fullSurf"
                                                        ? "Private Booking Full Surf Venue"
                                                        : "Full Park Booking"}
                                                </h3>
                                                <p>
                                                    For {dayjs(session.end).diff(dayjs(session.start), "hours")} Hour
                                                    {dayjs(session.end).diff(dayjs(session.start), "hours") > 1 ? `s` : ``}
                                                </p>
                                            </IonLabel>
                                            <IonChip color="dark">
                                                <IonIcon icon={diamondOutline} size="small" />
                                                <IonLabel>
                                                    <h3>
                                                        <b>{session.subTotals.privateHire.toLocaleString()}</b>
                                                    </h3>
                                                </IonLabel>
                                            </IonChip>
                                        </IonItem>
                                    ) : null}

                                    {session.memberDiscount && !session.usedCoupon ? (
                                        <IonItem className="ion-no-padding" lines="none">
                                            <IonLabel>
                                                <h3>Member Discount</h3>
                                            </IonLabel>
                                            <IonChip color="danger">
                                                <IonIcon icon={diamondOutline} size="small" />
                                                <IonLabel>
                                                    <h3>
                                                        <b>
                                                            {Math.ceil(
                                                                Number(
                                                                    0 -
                                                                        Object.values(session.subTotals).reduce((p, c) => p + c, 0) *
                                                                            session.memberDiscount
                                                                )
                                                            )}
                                                        </b>
                                                    </h3>
                                                </IonLabel>
                                            </IonChip>
                                        </IonItem>
                                    ) : null}

                                    {session.usedCoupon ? (
                                        <>
                                            <IonItem className="ion-no-padding" lines="none">
                                                <IonLabel>
                                                    <h3>Coupon</h3>
                                                    <p>{session.usedCoupon.code}</p>
                                                </IonLabel>
                                                <IonChip color="danger">
                                                    <IonIcon icon={diamondOutline} size="small" />
                                                    <IonLabel>
                                                        <h3>
                                                            <b>
                                                                -
                                                                {session.usedCoupon.unit === "credit"
                                                                    ? session.usedCoupon.discount
                                                                    : Math.ceil(
                                                                          Object.values(session.subTotals).reduce((a, b) => a + b, 0) *
                                                                              (session.usedCoupon.discount / 100)
                                                                      )}
                                                            </b>
                                                        </h3>
                                                    </IonLabel>
                                                </IonChip>
                                            </IonItem>
                                        </>
                                    ) : null}
                                </IonList>
                            </IonCardContent>
                        </IonCard>
                        {authStaff && authStaff.allowance && authStaff.wallet.balance && (
                            <IonCard color={"medium"}>
                                <IonCardHeader>
                                    <IonCardTitle>Staff Credits</IonCardTitle>
                                </IonCardHeader>
                                <IonCardContent>
                                    <IonItem color={`medium`} lines="none">
                                        <IonCheckbox
                                            slot="start"
                                            checked={useStaffCredit}
                                            onIonChange={(e) => {
                                                setUseStaffCredit((u) => !u);
                                            }}
                                        />
                                        <IonLabel>
                                            <h3>
                                                <b>Use My Staff Credits</b>
                                            </h3>
                                            <p>{authStaff.wallet.balance.toLocaleString()}</p>
                                            {useStaffCredit && (
                                                <p>{` New Balance: ${Math.max(
                                                    authStaff.wallet.balance - session.totalCredits,
                                                    0
                                                ).toLocaleString()}`}</p>
                                            )}
                                        </IonLabel>
                                    </IonItem>
                                </IonCardContent>
                            </IonCard>
                        )}

                        <IonCard color={`warning`}>
                            <IonCardHeader>
                                <IonCardTitle>
                                    <b>Payment</b>
                                </IonCardTitle>
                                <IonCardSubtitle>
                                    <IonLabel color={"tertiary"}>Pay By Credits</IonLabel>
                                </IonCardSubtitle>
                            </IonCardHeader>
                            <IonCardContent className="ion-no-padding">
                                <IonGrid className="ion-no-padding">
                                    <IonRow>
                                        <IonCol>
                                            <IonList inset color="dark">
                                                <IonListHeader color={"dark"}>
                                                    <IonIcon icon={walletOutline} size="large" />
                                                    &nbsp;My Wallet
                                                </IonListHeader>
                                                <IonItem color={"dark"}>
                                                    <IonIcon icon={diamondOutline} size="small" slot="start" />
                                                    <IonLabel>
                                                        <h1>
                                                            <b>{bookingCustomer.wallet.balance.toLocaleString()}</b>
                                                        </h1>
                                                        {session.totalCredits < bookingCustomer.wallet.balance && (
                                                            <IonLabel color="warning">
                                                                <p>
                                                                    Afterward:{" "}
                                                                    {(
                                                                        bookingCustomer.wallet.balance -
                                                                        (useStaffCredit
                                                                            ? Math.max(session.totalCredits - (authStaff?.wallet.balance || 0), 0)
                                                                            : session.totalCredits)
                                                                    ).toLocaleString()}
                                                                </p>
                                                            </IonLabel>
                                                        )}
                                                    </IonLabel>
                                                </IonItem>
                                            </IonList>
                                            {session.totalCredits > bookingCustomer.wallet.balance ? (
                                                <IonItem color={"warning"} lines="none">
                                                    <IonIcon icon={alertCircleOutline} slot="start" color="danger" />
                                                    <IonLabel color="danger">
                                                        <p>Customer Do not have enough credits</p>
                                                    </IonLabel>
                                                </IonItem>
                                            ) : null}
                                        </IonCol>
                                        <IonCol
                                            sizeXs="12"
                                            sizeSm="12"
                                            sizeMd="1"
                                            sizeLg="1"
                                            sizeXl="1"
                                            class="ion-justify-content-center ion-align-items-center"
                                            style={{ display: "flex" }}
                                        >
                                            <IonIcon icon={arrowForwardCircleOutline} size="large" />
                                        </IonCol>
                                        <IonCol>
                                            <IonList inset>
                                                <IonListHeader>Order Total</IonListHeader>
                                                <IonItem>
                                                    <IonIcon icon={diamondOutline} size="small" slot="start" />
                                                    <IonLabel>
                                                        <h1>
                                                            <b>
                                                                {useStaffCredit
                                                                    ? Math.max(
                                                                          session.totalCredits - (authStaff?.wallet.balance || 0),
                                                                          0
                                                                      ).toLocaleString()
                                                                    : session.totalCredits.toLocaleString()}
                                                            </b>
                                                        </h1>
                                                    </IonLabel>
                                                </IonItem>
                                            </IonList>
                                        </IonCol>
                                    </IonRow>
                                    <IonRow>
                                        <IonCol className="ion-text-center ion-padding">
                                            <IonButton
                                                color="tertiary"
                                                size="large"
                                                disabled={paying || disable}
                                                onClick={() => handlePay()}
                                            >
                                                {paying ? (
                                                    <>
                                                        <IonSpinner />
                                                    </>
                                                ) : (
                                                    <>
                                                        <IonIcon icon={ticketOutline} slot="start" />
                                                        <IonLabel>
                                                            <h1>
                                                                <b>PAY NOW</b>
                                                            </h1>
                                                        </IonLabel>
                                                    </>
                                                )}
                                            </IonButton>
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>
                            </IonCardContent>
                        </IonCard>

                        <IonGrid className="ion-text-end">
                            <IonRow>
                                <IonCol>
                                    {isXs || isSm || isMd ? (
                                        <IonButton color={`light`} fill="outline" expand="block" onClick={() => setShowCancelAlert(true)}>
                                            <IonIcon icon={closeCircleOutline} slot="start" color="medium" />
                                            <IonLabel color={`medium`}>
                                                <h3>
                                                    <b>Cancel Booking</b>
                                                </h3>
                                            </IonLabel>
                                        </IonButton>
                                    ) : (
                                        <IonButton color={`light`} fill="outline" onClick={() => setShowCancelAlert(true)}>
                                            <IonIcon icon={closeCircleOutline} slot="start" color="medium" />
                                            <IonLabel color={`medium`}>
                                                <h3>
                                                    <b>Cancel Booking</b>
                                                </h3>
                                            </IonLabel>
                                        </IonButton>
                                    )}
                                </IonCol>
                            </IonRow>
                        </IonGrid>

                        {timer && (
                            <IonFab vertical="top" horizontal="end" slot="fixed">
                                <IonChip className="solid-bg-chip">
                                    <IonIcon icon={timeOutline} />
                                    <IonLabel>{timer}</IonLabel>
                                </IonChip>
                            </IonFab>
                        )}
                    </>
                )}
            </IonContent>
            <IonAlert
                isOpen={showCancelAlert}
                onDidDismiss={() => {
                    setShowCancelAlert(false);
                }}
                header={`Cancel Booking`}
                message={`Are you sure?`}
                buttons={[
                    {
                        text: `Confirm`,
                        role: "confirm",
                        handler: async () => {
                            setShowLoading(true);
                            if (session) {
                                const res = await cancelLockedSession(session);
                                if (res === "success") {
                                    setShowLoading(false);
                                    setShowCancelAlert(false);
                                    history.push(`/newBooking`);
                                }
                            } else {
                                setShowLoading(false);
                                setShowCancelAlert(false);
                                history.push(`/newBooking`);
                            }
                        },
                    },
                    {
                        text: `Back`,
                        role: "cancel",
                    },
                ]}
            />
            <IonLoading isOpen={showLoading} message={`Cancelling Your Booking`} />
        </IonPage>
    );
};

export default Checkout;

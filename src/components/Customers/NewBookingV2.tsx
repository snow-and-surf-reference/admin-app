import {
    IonAccordion,
    IonAccordionGroup,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonChip,
    IonCol,
    IonContent,
    IonFooter,
    IonGrid,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonPage,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonSpinner,
    IonToolbar
} from "@ionic/react";
import * as SessionTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import { selectAuthUser as selectAuthStaff } from "app/slices/authSlice";
import { selectPricing, selectPricingIsInit } from "app/slices/pricingSlice";
import Holidays from "date-holidays";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import {
    addCircleOutline,
    arrowForwardCircleOutline,
    arrowForwardOutline,
    bagCheckOutline,
    checkmarkCircleOutline,
    closeCircleOutline,
    diamondOutline,
    removeCircleOutline,
    ticketOutline
} from "ionicons/icons";
import { useEffect, useRef, useState } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import { MdOutlineDownhillSkiing } from "react-icons/md";
import { useHistory, useParams } from "react-router";
import { useAppSelector } from "../../app/hooks";
import useBreakpoints from "../../app/hooks/useBreakpoints";
import { selectTempSession, setTempSession } from "../../app/slices/sessionsSlice";
import { selectCustomer as selectAuthUser } from "../../app/slices/usersSlice";
import { selectCapacities } from "../../app/slices/venueSlice";
import { store } from "../../app/store";
import Header from "../../components/Global/Header";
import "../../theme/calendar.scss";
import GearSizeModal from "./GearSizeModal";
import GroupClassSlotModal from "./GroupClassSlotModal";
import PrivateGearCard from "./PrivateGearCard";
import TimeSlotModalV2 from "./TimeSlotModalV2";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

var hd = new Holidays();
hd.init(`HK`);

// this indicates the available times and slots on the selected date
type DayStatus = "Checking..." | "Very Busy" | "Available" | "Limited Available" | "Sold Out";

export interface PhSource {
    date: string;
    start: Date;
    end: Date;
    name: string;
    rule: string;
    type: string;
}
export interface GearsByPax {
    idx: number;
    type: SessionTypes.PlayType;
    gears: SessionTypes.BookingGear[];
    selected: boolean;
}

const privateTypes = ["privateBelt", "privateLane", "fullSnow", "fullSurf", "fullPark"];

const NewBookingV2: React.FC = () => {
    const history = useHistory();
    const { presetType } = useParams<{ presetType: string }>();
    const [showTimeSlotModal, setShowTimeSlotModal] = useState(false);
    const [calAccValue, setCalAccValue] = useState<string | undefined>("cal");
    const [showSizeModal, setShowSizeModal] = useState(false);
    const existTempSession = useAppSelector(selectTempSession);
    const [session, setSession] = useState(existTempSession);
    const caps = useAppSelector(selectCapacities);
    const [dayStatus, setDayStatus] = useState<DayStatus>("Checking...");
    const [disabledSubmit, setDisableSubmit] = useState(true);
    const [gearByPaxList, setGearByPaxList] = useState<GearsByPax[]>([
        {
            idx: 0,
            type: `ski`,
            gears: [],
            selected: false,
        },
    ]);
    const authUser = useAppSelector(selectAuthUser);
    const [daySlots, setDaySlots] = useState<number | "loading">("loading");
    const { isXs, isSm, isMd } = useBreakpoints();
    const calAccRef = useRef<null | HTMLIonAccordionGroupElement>(null);
    const isStaff = true;
    const authStaff = useAppSelector(selectAuthStaff);
    const [isHalfPast, setIsHalfPast] = useState(false);

    // states for group class
    const [showGroupClassSlotModal, setShowGroupClassSlotModal] = useState(false);
    const [groupClassDayStatus, setGroupClassDayStatus] = useState<DayStatus>("Checking...");
    const [groupClassDaySlots, setGroupClassDaySlots] = useState<number | "loading">("loading");

    // get global pricing
    const prices = useAppSelector(selectPricing);
    const pricesIsInit = useAppSelector(selectPricingIsInit);

    // get last year, this year and next year ph
    const todayJs = dayjs.tz(session.sessionDate);
    let ph3Years: PhSource[] = [];
    const phThisYear: PhSource[] = hd.getHolidays(todayJs.format(`YYYY`));
    const phLastYear: PhSource[] = hd.getHolidays(todayJs.subtract(1, "year").format(`YYYY`));
    const phNextYear: PhSource[] = hd.getHolidays(todayJs.add(1, "year").format(`YYYY`));
    ph3Years.push(...phLastYear, ...phThisYear, ...phNextYear);

    // logging the session
    // useEffect(() => console.log('session changed', session), [session]);

    // effect to preset the session type from the url
    useEffect(() => {
        const legitSessionType: SessionTypes.SessionType[] = [
            "single",
            "groupClass",
            "privateClass",
            "privateBelt",
            "privateLane",
            "fullSnow",
            "fullSurf",
            "fullPark",
        ];
        if (!presetType || !legitSessionType.includes(presetType as SessionTypes.SessionType)) {
            return;
        }

        let tempPresetType: SessionTypes.SessionType = presetType as SessionTypes.SessionType;
        if (!isStaff) {
            const allowedTypes: SessionTypes.SessionType[] = ["single", "groupClass", "privateClass", "privateBelt"];
            if (!allowedTypes.includes(tempPresetType)) {
                tempPresetType = "single";
            }
        }
        setSession((ss) => Object.assign({ ...ss }, { sessionType: tempPresetType }));
    }, [presetType, isStaff]);

    // Effect to update redux temp session from state
    useEffect(() => {
        // console.log('session sub totals', session.subTotals);
        store.dispatch(setTempSession(session));
    }, [session]);

    // effect to put authUser into booking
    useEffect(() => {
        // console.log('Booking User changed: ', authUser);
        if (!authUser) {
            history.push(`/customers/search`);
            return;
        }
        setSession((s) => Object.assign({ ...s }, { bookingUser: authUser }));
    }, [authUser, history]);

    // effect to add booking staff into booking once exist
    useEffect(() => {
        if (!authStaff) {
            return;
        }
        setSession((s) => Object.assign({ ...s }, { bookingStaff: authStaff }));
    }, [authStaff]);

    // effect to reset the play type and the pax max
    useEffect(() => {
        const tempSessionType: SessionTypes.SessionType = session.sessionType;
        const tempOldType: SessionTypes.PlayType = session.playType;
        const tempOldPax: number = session.pax;
        let newPlayType: SessionTypes.PlayType | null = null;
        let newPax: number = tempOldPax;

        if (
			tempSessionType === 'groupClass' &&
			tempOldPax <= 5 &&
			(tempOldType === 'ski' || tempOldType === 'sb')
		) {
			return
		}

        if (!caps) {
			return;
		}
		const Slope1Max =
			caps.capacities.find((x) => x.venue.name === "Slope 1")?.max || 0;
		const Slope2Max =
			caps.capacities.find((x) => x.venue.name === "Slope 2")?.max || 0;
		const Lane1Max =
			caps.capacities.find((x) => x.venue.name === "Surf Lane 1")?.max || 0;
		const Lane2Max =
			caps.capacities.find((x) => x.venue.name === "Surf Lane 2")?.max || 0;
		const SnowMax = Slope1Max + Slope2Max;
		const SurfMax = Lane1Max + Lane2Max;
		const ParkMax = SnowMax + SurfMax;
		const privateBeltCustomMax = 4;

        if (tempSessionType === "single") {
			if (tempOldType === "snow" || tempOldType === "surf") {
				newPlayType = tempOldType;
			} else {
				newPlayType = "snow";
			}
			tempOldPax > Slope1Max && (newPax = Slope1Max);
		} else if (
			tempSessionType === "groupClass" ||
			tempSessionType === "privateClass"
		) {
			if (tempOldType === "ski" || tempOldType === "sb") {
				newPlayType = tempOldType;
			} else {
				newPlayType = "ski";
			}
			if (tempSessionType === "groupClass") {
				tempOldPax > 5 && (newPax = 5);
			} else {
				tempOldPax > 3 && (newPax = 3);
			}
		} else if (
			tempSessionType === "privateBelt" ||
			tempSessionType === "fullSnow"
		) {
			if (tempOldType === "snow") {
				newPlayType = tempOldType;
			} else {
				newPlayType = "snow";
			}
			if (tempSessionType === "privateBelt") {
				newPax = privateBeltCustomMax;
			} else {
				newPax = SnowMax;
			}
		} else if (
			tempSessionType === "privateLane" ||
			tempSessionType === "fullSurf"
		) {
			if (tempOldType === "surf") {
				newPlayType = tempOldType;
			} else {
				newPlayType = `surf`;
			}
			if (tempSessionType === "privateLane") {
				newPax = Lane1Max;
			} else {
				newPax = SurfMax;
			}
		} else if (tempSessionType === "fullPark") {
			newPlayType = "all";
			newPax = ParkMax;
		}

        setSession((s) =>
            Object.assign(
                { ...s },
                {
                    playType: newPlayType,
                    pax: newPax,
                }
            )
        );

        setCalAccValue("cal");
    }, [session.sessionType, session.playType, session.pax, caps]);

    // effect to remove group class if session type changed
    useEffect(() => {
        const newSessionType = session.sessionType;
		if (newSessionType !== 'groupClass') {
			setSession((s) =>
				Object.assign(
					{ ...s },
					{
						groupClass: null,
					}
				)
			);
		}
    }, [session.sessionType]);

    // effect to update the maximum person allowed by session type
    useEffect(() => {
        if (privateTypes.includes(session.sessionType)) {
            let newPax: number = 0;
            switch (session.sessionType) {
                case "privateBelt":
                    newPax = 6;
                    break;
                case "privateLane":
                    newPax = 6;
                    break;
                case "fullSnow":
                    newPax = 12;
                    break;
                case "fullSurf":
                    newPax = 12;
                    break;
                case "fullPark":
                    newPax = 24;
                    break;
                default:
                    newPax = 6;
                    break;
            }

            setSession((s) =>
                Object.assign(
                    { ...s },
                    {
                        pax: newPax,
                    }
                )
            );
        }
    }, [session.sessionType]);

    // Effect when session date is changed
    useEffect(() => {
        if (!caps || session.sessionType === 'groupClass') {
            console.log('session is group class, normal date effect will not run')
			return;
		}

        setSession((s) =>
            Object.assign(
                { ...s },
                {
                    start: null,
                    end: null,
                }
            )
        );

        setDayStatus(
            daySlots === "loading"
                ? "Checking..."
                : daySlots <= 0
                ? `Sold Out`
                : daySlots <= 0.3
                ? `Very Busy`
                : daySlots <= 0.6
                ? `Limited Available`
                : `Available`
        );
    }, [session.sessionDate, session.playType, session.pax, caps, daySlots, session.sessionType]);

    // Effect when session date is changed (group class)
    useEffect(() => {
        if (!caps) {
            console.log('session is not group class, group date effect will not run')
            return;
        }

        setSession((s) =>
            Object.assign(
                { ...s },
                {
                    start: null,
                    end: null,
                }
            )
        );

        setGroupClassDayStatus(
            groupClassDaySlots === "loading"
                ? "Checking..."
                : groupClassDaySlots <= 0
                ? `Sold Out`
                : groupClassDaySlots <= 0.3
                ? `Very Busy`
                : groupClassDaySlots <= 0.6
                ? `Limited Available`
                : `Available`
        );
    }, [session.sessionDate, session.playType, session.pax, caps, groupClassDaySlots]);

    // effect for gearByPax reseting
    useEffect(() => {
        const tempGearByPaxList: GearsByPax[] = [];
        [...Array(session.pax)].forEach((x, idx) => {
            const type = session.playType === "snow" || session.playType === "ski" || session.playType === "all" ? "ski" : "sb";
            const newGearByPax: GearsByPax = {
                idx,
                type,
                gears: [],
                selected: false,
            };
            tempGearByPaxList.push(newGearByPax);
        });
        setGearByPaxList(tempGearByPaxList);
    }, [session.pax, session.sessionDate, session.start, session.end, session.playType]);

    // effect to toggle submit and update price
    useEffect(() => {
        if (!pricesIsInit || !prices) { return };

        if (!session.start || !session.end || !session.bookingUser) {
            setDisableSubmit(true);
        } else {
            setDisableSubmit(false);
        }

        // recal entrence cost
        const playHours = Math.abs(dayjs.tz(session.start).diff(dayjs.tz(session.end), "hours")) || 0;
        const sessionUnitPrice = prices.enterance.price;
        const pax = session.pax;
        const paxTotal =
            session.sessionType === "single" || session.sessionType === "groupClass" || session.sessionType === "privateClass"
                ? sessionUnitPrice * playHours * pax
                : 0;

        // recal gears
        let gearTotal = 0;
        const gearStandardPrice = prices.gear_standard.price;
        const gearPremiumPrice = prices.gear_premium.price;
        let bookedGears = {
            standard: {
                shoe: 0,
                board: 0
            },
            premium: {
                shoe: 0,
                board: 0
            }
        }
        session.gears.forEach((sg) => {
            // gearTotal += sg.gear.prices.find((x) => x.gearClass === sg.class)?.unitPrice || 0;
            const usage = sg.gear.gearUsage;
            const gearClass = sg.class;
            bookedGears[gearClass][usage]++;
        });

        // console.log(bookedGears);
        gearTotal = (
            Math.max(
                bookedGears['standard']['shoe'],
                bookedGears['standard']['board'],
            ) * gearStandardPrice
        ) + (
            Math.max(
                bookedGears['premium']['shoe'],
                bookedGears['premium']['board'],
            ) * gearPremiumPrice
        )

        // console.log(`gear subtotal after recal:`, gearTotal);

        // recal coach
        let coachTotal = 0;
        let privateHirePrice = 0;
        if (session.pax) {
            switch (session.sessionType) {
                case `groupClass`:
                    coachTotal = pax * prices.group_class.price * playHours;
                    break;
                case `privateClass`:
                    coachTotal = prices.personal_training.price * playHours;
                    break;
                case `privateBelt`:
                    privateHirePrice = prices.default_snow_belt.price * playHours;
                    break;
                case `privateLane`:
                    privateHirePrice = prices.default_surf_lane.price * playHours;
                    break;
                case `fullSnow`:
                    privateHirePrice = prices.default_snow_venue.price * playHours;
                    break;
                case `fullSurf`:
                    privateHirePrice = prices.default_surf_venue.price * playHours;
                    break;
                case `fullPark`:
                    privateHirePrice = prices.default_full_park.price * playHours;
                    break;

                default:
                    break;
            }
        }

        let subTotals: SessionTypes.SubTotals = {
            entrances: paxTotal,
            gears: gearTotal,
            coach: coachTotal,
            privateHire: privateHirePrice,
        };

        setSession((s) =>
            Object.assign(
                { ...s },
                {
                    totalCredits: Object.values(subTotals).reduce((a, b) => a + b, 0),
                    subTotals,
                }
            )
        );
    }, [session.start, session.end, session.pax, session.gears, session.sessionType, prices, session.bookingUser, pricesIsInit]);

    // effect to update the gear list in session, from the updated gear by persons list
    useEffect(() => {
        let tempGearList: SessionTypes.BookingGear[] = [];
        gearByPaxList.forEach((x) => {
            x.gears
                .filter((y) => y.size)
                .forEach((z) => {
                    const tempGearSize: SessionTypes.BookingGear = {
                        size: z.size,
                        class: z.class,
                        gear: z.gear,
                    };
                    tempGearList.push(tempGearSize);
                });
        });
        setSession((s) =>
            Object.assign(
                { ...s },
                {
                    gears: tempGearList,
                }
            )
        );
    }, [gearByPaxList]);

    // effect to update the 'gear by persons' state
    const updateGearRentals = (gearsByPax: GearsByPax) => {
        let TempGearByPaxList = [...gearByPaxList];
        let tempNewGearByPax = Object.assign({ ...gearsByPax }, { selected: false });
        TempGearByPaxList.forEach((x) => {
            x.selected = false;
        });
        TempGearByPaxList[gearsByPax.idx] = tempNewGearByPax;
        setGearByPaxList(TempGearByPaxList);
    };

    return (
        <>
            <IonPage>
                <Header
                    title={`New Booking`}
                    back={`/customers/search`}
                />
                <IonContent>
                    <br />
                    <IonCard color={`light`}>
                        <IonItem lines="none" color={`light`}>
                            <IonLabel>
                                <h3>
                                    <b>Booking for Customer: {session.bookingUser?.usrname}</b>
                                </h3>
                                <p>
                                    <b>Member #{session.bookingUser?.memberId}</b>
                                </p>
                            </IonLabel>
                        </IonItem>
                    </IonCard>

                    <IonGrid className="ion-no-padding">
                        <IonRow>
                            <IonCol>
                                <IonCard
                                    className={`ion-no-margin ion-margin-start ion-margin-end ${isXs || isSm || isMd ? `ion-margin-bottom` : ``}`}
                                >
                                    <IonItem lines="none" color={"tertiary"}>
                                        <IonLabel
                                            className="ion-justify-content-start ion-align-items-center"
                                            style={{ display: "flex" }}
                                            position="stacked"
                                        >
                                            <IonIcon icon={ticketOutline} size="small" className="ion-margin-end" />
                                            <b>Session Type</b>
                                        </IonLabel>
                                        <IonSelect
                                            value={session.sessionType}
                                            interface="action-sheet"
                                            onIonChange={(e) => {
                                                //set session type
                                                setSession((ss) =>
                                                    Object.assign(
                                                        { ...ss },
                                                        {
                                                            sessionType: e.detail.value! as SessionTypes.SessionType,
                                                        }
                                                    )
                                                );
                                            }}
                                        >
                                            <IonSelectOption value={"single"}>Park Entrance Only</IonSelectOption>
                                            {!authUser?.isPartner ? (
												<>
													<IonSelectOption value={"groupClass"}>
														Group Class
													</IonSelectOption>
													<IonSelectOption value={"privateClass"}>
														Private Class
													</IonSelectOption>
												</>
											) : null}
                                            {isStaff ? (
                                                <>
                                                    <IonSelectOption value={"privateBelt"}>Private Snow Belt</IonSelectOption>
                                                    <IonSelectOption value={"privateLane"}>Private Surf Lane</IonSelectOption>
                                                    <IonSelectOption value={"fullSnow"}>Private Snow Venue</IonSelectOption>
                                                    <IonSelectOption value={"fullSurf"}>Private Surf Venue</IonSelectOption>
                                                    <IonSelectOption value={"fullPark"}>Private Park Hire</IonSelectOption>
                                                </>
                                            ) : (
												!authUser?.isPartner ? (
													<>
													<IonSelectOption value={"privateBelt"}>
														Private Snow Belt
													</IonSelectOption>
													<IonSelectOption value={"privateLane"}>
														Private Surf Lane
													</IonSelectOption>
												</>
												) : null
											)}
                                        </IonSelect>
                                    </IonItem>
                                </IonCard>
                            </IonCol>
                            {session.sessionType === `groupClass` ? null : (
                                <IonCol>
                                    <IonCard className={`ion-no-margin ion-margin-start ion-margin-end`}>
                                        <IonItem lines="none" color={"secondary"}>
                                            <IonLabel
                                                className="ion-justify-content-start ion-align-items-center"
                                                style={{ display: "flex" }}
                                                position="stacked"
                                            >
                                                <MdOutlineDownhillSkiing size={18} className="ion-margin-end" />
                                                <b>{`Game Type`}</b>
                                            </IonLabel>
                                            <IonSelect
                                                value={session.playType}
                                                interface="action-sheet"
                                                onIonChange={(e) => {
                                                    setSession((ss) =>
                                                        Object.assign(
                                                            { ...ss },
                                                            {
                                                                playType: e.detail.value! as SessionTypes.PlayType,
                                                            }
                                                        )
                                                    );
                                                }}
                                            >
                                                {session.sessionType === "single" ||
                                                session.sessionType === "privateBelt" ||
                                                session.sessionType === "fullSnow" ? (
                                                    <IonSelectOption value={"snow"}>{`Ski & Snowboard`}</IonSelectOption>
                                                ) : null}

                                                {session.sessionType === "single" ||
                                                session.sessionType === "privateLane" ||
                                                session.sessionType === "fullSurf" ? (
                                                    <IonSelectOption value={"surf"}>{`Surf`}</IonSelectOption>
                                                ) : null}

                                                {session.sessionType === "fullPark" ? (
                                                    <IonSelectOption value={"all"}>{`Snow & Surf`}</IonSelectOption>
                                                ) : null}

                                                {session.sessionType === "privateClass" ? (
                                                    <>
                                                        <IonSelectOption value={"ski"}>{`Ski`}</IonSelectOption>
                                                        <IonSelectOption value={"sb"}>{`Snowboard`}</IonSelectOption>
                                                    </>
                                                ) : null}
                                            </IonSelect>
                                        </IonItem>
                                    </IonCard>
                                </IonCol>
                            )}
                        </IonRow>
                    </IonGrid>
                    {!["privateBelt", "privateLane", "fullSnow", "fullSurf", "fullPark"].includes(session.sessionType) && !authUser?.isPartner ? (
                        <IonCard className="">
                        <IonItem lines="none">
                            <IonLabel>
                                <h2>
                                    <b>
                                        For{" "}
                                        <span className="ion-hide-md-down">
                                            How Many Person?
                                        </span>
                                    </b>
                                </h2>
                            </IonLabel>
                            <IonButtons slot="end">
                                <IonButton
                                    disabled={session.pax <= 1}
                                    onClick={() => {
                                        setSession(
                                            Object.assign(
                                                { ...session },
                                                {
                                                    pax: session.pax - 1,
                                                }
                                            )
                                        );
                                    }}
                                >
                                    <IonIcon slot="icon-only" icon={removeCircleOutline} />
                                </IonButton>
                                <IonChip>
                                    {session.pax} {`Person${session.pax > 1 ? "s" : ""}`}
                                </IonChip>
                                <IonButton
                                    disabled={
                                        session.sessionType === "single"
                                            ? session.pax >= 6
                                            : session.sessionType === "groupClass"
                                            ? session.pax >= 5
                                            : session.sessionType === "privateClass"
                                            ? session.pax >= 3
                                            : session.sessionType === "fullSnow"
                                            ? session.pax >= 15
                                            : session.sessionType === "fullSurf"
                                            ? session.pax >= 12
                                            : session.pax >= 16
                                    }
                                    onClick={() => {
                                        setSession(
                                            Object.assign(
                                                { ...session },
                                                {
                                                    pax: session.pax + 1,
                                                }
                                            )
                                        );
                                    }}
                                >
                                    <IonIcon slot="icon-only" icon={addCircleOutline} />
                                </IonButton>
                            </IonButtons>
                        </IonItem>
                    </IonCard>
                    ) : null}

                    <IonCard className="fliud">
                        <IonCardContent>
                            <IonAccordionGroup value={calAccValue} ref={calAccRef}>
                                <IonAccordion value="cal">
                                    <IonItem
                                        slot="header"
                                        onClick={() => {
                                            if (calAccValue) {
                                                setCalAccValue(undefined);
                                            } else {
                                                setCalAccValue("cal");
                                            }
                                        }}
                                    >
                                        <IonLabel>
                                            <h2>
                                                <b>{calAccValue ? `Select Your Date` : dayjs.tz(session.sessionDate).format(`DD MMM YY`)}</b>
                                            </h2>
                                        </IonLabel>
                                    </IonItem>
                                    <div slot="content">
                                        <Calendar
                                            onChange={(value: Date) => {
                                                const newDateString = dayjs(value).startOf("day").format(`YYYY-MM-DD`);
                                                const newDateTz = dayjs.tz(`${newDateString}T00:00:00`).toDate();
                                                setSession(Object.assign({ ...session }, { sessionDate: newDateTz }));
                                            }}
                                            value={dayjs(`${dayjs.tz(session.sessionDate).format(`YYYY-MM-DD`)}T00:00:00`).toDate()}
                                            minDate={dayjs.tz(Date.now()).toDate()}
                                            maxDate={dayjs.tz(Date.now()).endOf("day").add(60, "day").toDate()}
                                            tileClassName={({ activeStartDate, date, view }) => {
                                                const isPh =
                                                    ph3Years.filter((x) => date.valueOf() >= x.start.valueOf() && date.valueOf() < x.end.valueOf())
                                                        .length > 0;
                                                if (isPh) {
                                                    return "calendar-ph-tile";
                                                } else {
                                                    return "";
                                                }
                                            }}
                                        />
                                    </div>
                                </IonAccordion>
                            </IonAccordionGroup>
                            {!session.start && !session.end ? (
                                <IonItem lines="none">
                                    <IonLabel>
                                        <h2>
                                            <b>{session.sessionType === "groupClass" ? `Select Class` : `Select Your Time`}</b>
                                        </h2>
                                    </IonLabel>
                                </IonItem>
                            ) : null}

                            {session.sessionType !== "groupClass" ? (
                                <IonList inset>
                                    {!session.start || !session.end ? (
                                        <IonItem
                                            style={{
                                                cursor: "pointer",
                                            }}
                                            disabled={dayStatus === `Checking...` || dayStatus === `Sold Out`}
                                            color={
                                                dayStatus === `Sold Out`
                                                    ? `medium`
                                                    : dayStatus === `Very Busy`
                                                    ? `danger`
                                                    : dayStatus === `Limited Available`
                                                    ? `warning`
                                                    : dayStatus === `Available`
                                                    ? `success`
                                                    : `dark`
                                            }
                                            onClick={() => {
                                                setCalAccValue("cal");
                                                setShowTimeSlotModal(true);
                                            }}
                                        >
                                            <IonLabel>
                                                <h3>{dayStatus}</h3>
                                            </IonLabel>
                                            {dayStatus === `Checking...` ? (
                                                <IonSpinner slot="end" />
                                            ) : (
                                                <IonIcon
                                                    icon={dayStatus === `Sold Out` ? closeCircleOutline : arrowForwardCircleOutline}
                                                    slot="end"
                                                />
                                            )}
                                        </IonItem>
                                    ) : (
                                        <IonItem color="secondary" button detail={false} onClick={() => setShowTimeSlotModal(true)}>
                                            <IonLabel>
                                                <h3>
                                                    {dayjs.tz(session.start).format(`HH:mm`)}-{dayjs.tz(session.end).format(`HH:mm`)}
                                                    {` (${dayjs.tz(session.end).diff(dayjs.tz(session.start), "hours")} Hour${
                                                        dayjs.tz(session.end).diff(dayjs.tz(session.start), "hours") > 1 ? `s` : ``
                                                    })`}
                                                </h3>
                                            </IonLabel>
                                            <IonIcon icon={checkmarkCircleOutline} slot="end" />
                                        </IonItem>
                                    )}
                                </IonList>
                            ) : (
                                /** this is for group class */
                                <IonList inset>
                                    {!session.start || !session.end ? (
                                        <IonItem
                                            style={{
                                                cursor: "pointer",
                                            }}
                                            disabled={groupClassDayStatus === `Checking...` || groupClassDayStatus === `Sold Out`}
                                            color={
                                                groupClassDayStatus === `Sold Out`
                                                    ? `medium`
                                                    : groupClassDayStatus === `Very Busy`
                                                    ? `danger`
                                                    : groupClassDayStatus === `Limited Available`
                                                    ? `warning`
                                                    : groupClassDayStatus === `Available`
                                                    ? `success`
                                                    : `dark`
                                            }
                                            onClick={() => {
                                                setCalAccValue("cal");
                                                setShowGroupClassSlotModal(true);
                                            }}
                                        >
                                            <IonLabel>
                                                <h3>{groupClassDayStatus}</h3>
                                            </IonLabel>
                                            {groupClassDayStatus === `Checking...` ? (
                                                <IonSpinner slot="end" />
                                            ) : (
                                                <IonIcon
                                                    icon={groupClassDayStatus === `Sold Out` ? closeCircleOutline : arrowForwardCircleOutline}
                                                    slot="end"
                                                />
                                            )}
                                        </IonItem>
                                    ) : (
                                        <IonItem color="secondary" button detail={false} onClick={() => setShowGroupClassSlotModal(true)}>
                                            {session.groupClass ? (
                                                <IonLabel>
                                                    <h3>{dayjs.tz(session.sessionDate).format(`DD MMM YYYY`)}</h3>
                                                    <h4>{`${dayjs.tz(session.start).format(`HH:mm`)} - ${dayjs.tz(session.end).format(`HH:mm`)}`}</h4>
                                                    <h4>
                                                        <b>{session.groupClass.classPreset.name.en}</b>
                                                    </h4>
                                                </IonLabel>
                                            ) : (
                                                <IonLabel>Please select a class</IonLabel>
                                            )}
                                            <IonIcon icon={checkmarkCircleOutline} slot="end" />
                                        </IonItem>
                                    )}
                                </IonList>
                            )}
                            <IonItem lines="none">
                                <IonButtons slot="start">
                                    <IonLabel slot="start">
                                        <h2>
                                            <b>Park Entrance: </b>
                                        </h2>
                                    </IonLabel>
                                    <IonChip slot="start">
                                        <IonIcon icon={diamondOutline} />
                                        <IonLabel>
                                            <b>{session.subTotals.entrances.toLocaleString()}</b>
                                        </IonLabel>
                                    </IonChip>
                                </IonButtons>
                            </IonItem>
                        </IonCardContent>
                    </IonCard>
                    {session.start && session.end && (session.sessionType === "groupClass" || session.sessionType === "privateClass") && (
                        <IonCard id="coach-details">
                            <IonCardContent>
                                <IonAccordionGroup value={"coach"}>
                                    <IonAccordion value="coach">
                                        <IonItem slot="header">
                                            <IonLabel>
                                                <h2>
                                                    <b>Coaching</b>
                                                </h2>
                                            </IonLabel>
                                        </IonItem>
                                        <IonList slot="content" lines="none">
                                            {session.sessionType === "groupClass" ? (
                                                <IonItem>
                                                    <IonButtons slot="start">
                                                        <IonLabel slot="start">
                                                            <h3>
                                                                <b>Group Class</b>
                                                            </h3>
                                                            <p>{prices?.group_class.price} credits per pax / hour</p>
                                                        </IonLabel>
                                                    </IonButtons>
                                                </IonItem>
                                            ) : (
                                                <IonItem>
                                                    <IonButtons slot="start">
                                                        <IonLabel slot="start">
                                                            <h3>
                                                                <b>Private Training</b>
                                                                {" - "}
                                                                {session.playType === "ski"
                                                                    ? "Ski"
                                                                    : session.playType === "sb"
                                                                    ? "Snowboard"
                                                                    : "Surf"}
                                                            </h3>
                                                                <p>{prices?.personal_training.price} credits per group / hour</p>
                                                        </IonLabel>
                                                    </IonButtons>
                                                </IonItem>
                                            )}
                                        </IonList>
                                    </IonAccordion>
                                </IonAccordionGroup>
                                <br />
                                <IonItem lines="none">
                                    <IonButtons slot="start">
                                        <IonLabel slot="start">
                                            <h2>
                                                <b>Subtotal: </b>
                                            </h2>
                                        </IonLabel>
                                        <IonChip slot="start">
                                            <IonIcon icon={diamondOutline} />
                                            <IonLabel>
                                                <b>{session.subTotals.coach.toLocaleString()}</b>
                                            </IonLabel>
                                        </IonChip>
                                    </IonButtons>
                                </IonItem>
                            </IonCardContent>
                        </IonCard>
                    )}

                    {session.start &&
                        session.end &&
                        (session.sessionType === "privateBelt" ||
                            session.sessionType === "privateLane" ||
                            session.sessionType === "fullSurf" ||
                            session.sessionType === "fullSnow" ||
                            session.sessionType === "fullPark") &&
                        isStaff && (
                            <IonCard color={"dark"}>
                                <IonCardContent>
                                    <IonItem slot="header" color={"dark"}>
                                        <IonLabel>
                                            <h2>
                                                <b>Private Hire Custom Pricing</b>
                                            </h2>
                                            <h3>
                                                <b>
                                                    {session.sessionType === "privateBelt"
                                                        ? `Private Snow Slope 1`
                                                        : session.sessionType === "privateLane"
                                                        ? "Private Surf Lane 1"
                                                        : session.sessionType === "fullSnow"
                                                        ? "Full Snow Venue"
                                                        : session.sessionType === "fullSurf"
                                                        ? "Full Surf Venue"
                                                        : "Full Park"}{" "}
                                                    for {dayjs(session.end).diff(dayjs(session.start), "hours")} Hour(s)
                                                </b>
                                            </h3>
                                        </IonLabel>
                                    </IonItem>
                                    <IonList inset slot="content" lines="none">
                                        <IonItem color={"light"}>
                                            <IonIcon icon={diamondOutline} slot="start" size="small" />
                                            <IonLabel position="floating">Hire Price</IonLabel>
                                            <IonInput
                                                type="text"
                                                value={session.subTotals.privateHire.toLocaleString()}
                                                onIonChange={(e) => {
                                                    setSession((ss) =>
                                                        Object.assign(
                                                            { ...ss },
                                                            {
                                                                subTotals: Object.assign(
                                                                    { ...session.subTotals },
                                                                    {
                                                                        privateHire: Number(e.detail.value!.replace(/\D/g, "") || 0),
                                                                    }
                                                                ),
                                                            }
                                                        )
                                                    );
                                                }}
                                            />
                                        </IonItem>
                                    </IonList>
                                </IonCardContent>
                            </IonCard>
                        )}

                    {session.start && session.end && session.playType !== "surf" ? (
                        privateTypes.includes(session.sessionType) ? (
                            <PrivateGearCard
                                setParentSession={(session: SessionTypes.Session) => {
                                    setSession(session);
                                }}
                            />
                        ) : (
                            <>
                                <IonCard>
                                    <IonCardContent>
                                        <IonAccordionGroup value={"gears"}>
                                            <IonAccordion value="gears">
                                                <IonItem slot="header">
                                                    <IonLabel>
                                                        <h2>
                                                            <b>Gear Rental</b>
                                                        </h2>
                                                    </IonLabel>
                                                </IonItem>
                                                <IonList slot="content" lines="none">
                                                    {gearByPaxList.map((item, idx) => (
                                                        <IonItem key={`gear-person-item-${idx}`}>
                                                            <IonLabel>
                                                                <h3>
                                                                    <b>{`Player ${idx + 1}`}</b>
                                                                </h3>
                                                                {item.gears.filter((x) => x.size).length <= 0 ? (
                                                                    <p>No Gear Rented</p>
                                                                ) : (
                                                                    item.gears
                                                                        .filter((x) => x.size)
                                                                        .map((gg) => (
                                                                            <IonLabel key={`rented-gear-${gg.gear.id}-${gg.size}-${item.idx}`}>
                                                                                <p>
                                                                                    {gg.gear.name}
                                                                                    <br />
                                                                                    {String(gg.class).toUpperCase()}{" "}
                                                                                    {`${gg.size}${gg.gear.unit.name}`}
                                                                                    <br />
                                                                                    <br />
                                                                                </p>
                                                                            </IonLabel>
                                                                        ))
                                                                )}
                                                            </IonLabel>
                                                            <IonButton
                                                                fill="solid"
                                                                size="small"
                                                                color={`secondary`}
                                                                onClick={() => {
                                                                    let tempList = [...gearByPaxList];
                                                                    tempList.forEach((x) => (x.selected = false));
                                                                    Object.assign(tempList[idx], {
                                                                        selected: true,
                                                                    });
                                                                    setGearByPaxList(tempList);
                                                                    setShowSizeModal(true);
                                                                }}
                                                            >
                                                                <IonIcon
                                                                    icon={arrowForwardOutline}
                                                                    // slot='icon-only'
                                                                    className="ion-hide-md-up"
                                                                />
                                                                <IonLabel className="ion-hide-md-down">
                                                                    <b>Change</b>
                                                                </IonLabel>
                                                            </IonButton>
                                                        </IonItem>
                                                    ))}
                                                </IonList>
                                            </IonAccordion>
                                        </IonAccordionGroup>
                                        <br />
                                        <IonItem lines="none">
                                            <IonButtons slot="start">
                                                <IonLabel slot="start">
                                                    <h2>
                                                        <b>Subtotal: </b>
                                                    </h2>
                                                </IonLabel>
                                                <IonChip slot="start">
                                                    <IonIcon icon={diamondOutline} />
                                                    <IonLabel>
                                                        <b>{session.subTotals.gears.toLocaleString()}</b>
                                                    </IonLabel>
                                                </IonChip>
                                            </IonButtons>
                                        </IonItem>
                                    </IonCardContent>
                                </IonCard>
                            </>
                        )
                    ) : null}
                </IonContent>
                <IonFooter color="dark" className="dark">
                    <IonToolbar
                        color="dark"
                        className="fluid"
                        style={{
                            padding: `1rem`,
                        }}
                    >
                        <IonLabel slot="start">
                            <h2 className="ion-hide-md-down">
                                <b>Order Total:</b>
                            </h2>
                        </IonLabel>
                        <IonChip color="light">
                            <IonIcon icon={diamondOutline} />
                            <IonLabel>
                                <b>{session.totalCredits.toLocaleString()} credits</b>
                            </IonLabel>
                        </IonChip>
                        <IonButtons slot="end">
                            <IonButton
                                fill="solid"
                                color={disabledSubmit ? "medium" : "warning"}
                                size="large"
                                disabled={disabledSubmit}
                                onClick={() => {
                                    history.push(`/orderPreview`);
                                }}
                            >
                                <IonIcon slot="start" icon={bagCheckOutline} />
                                <IonLabel>Preview</IonLabel>
                            </IonButton>
                        </IonButtons>
                    </IonToolbar>
                </IonFooter>
                <TimeSlotModalV2
                    isOpen={showTimeSlotModal}
                    onClose={() => {
                        setShowTimeSlotModal(false);
                    }}
                    onSubmit={(sessionClone) => {
                        setShowTimeSlotModal(false);
                        setSession(sessionClone);
                        setCalAccValue(undefined);
                    }}
                    onUpdate={(daySlots) => {
                        setDaySlots(daySlots);
                    }}
                    session={session}
                    caps={caps}
                    isHalfPast={isHalfPast}
                    onChangeHalfPast={() => setIsHalfPast(!isHalfPast)}
                />
                <GroupClassSlotModal
                    isOpen={showGroupClassSlotModal}
                    onClose={() => {
                        setShowGroupClassSlotModal(false);
                    }}
                    onSubmit={(sessionClone) => {
                        setShowGroupClassSlotModal(false);
                        setSession(sessionClone);
                        setCalAccValue(undefined);
                    }}
                    onUpdate={(daySlots) => {
                        setGroupClassDaySlots(daySlots);
                    }}
                    session={session}
                    caps={caps}
                    isHalfPast={isHalfPast}
                    onChangeHalfPast={() => setIsHalfPast(!isHalfPast)}
                />
                <GearSizeModal
                    isOpen={showSizeModal}
                    onClose={() => {
                        setShowSizeModal(false);
                    }}
                    onSubmit={(gearsByPax: GearsByPax) => {
                        updateGearRentals(gearsByPax);
                        setShowSizeModal(false);
                    }}
                    gearByPax={gearByPaxList.find((x) => x.selected)}
                />
            </IonPage>
        </>
    );
};

export default NewBookingV2;

import {
    IonAccordion,
    IonAccordionGroup,
    IonButton,
    IonCard,
    IonCardContent,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonModal,
    IonRow,
    IonTitle,
    IonToolbar
} from "@ionic/react";
import * as GearTypes from "@tsanghoilun/snow-n-surf-interface/types/gear";
import * as SessionTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import { getCounterByDateV2 } from "app/firebase";
import { handleGearsStock } from "app/functions/handleSlots";
import { useAppSelector } from "app/hooks";
import { selectGears } from "app/slices/gearSlice";
import { selectPricing, selectPricingIsInit } from "app/slices/pricingSlice";
import { selectTempSession, setTempSession } from "app/slices/sessionsSlice";
import { store } from "app/store";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { closeCircle, swapHorizontalOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

interface Props{
    setParentSession: (session: SessionTypes.Session) => void;
}

const PrivateGearCard: React.FC<Props> = (props) => {
    const { setParentSession } = props;
    const sessionRaw = useAppSelector(selectTempSession);
    const [session, setSession] = useState(sessionRaw);
    const allGearsRaw = useAppSelector(selectGears);
    const [allGears, setAllGears] = useState(allGearsRaw);
    const [isPremium, setIsPremium] = useState(false);
    const [showGearSize, setShowGearSize] = useState("");
    // get global pricing
    const prices = useAppSelector(selectPricing);
    const pricesIsInit = useAppSelector(selectPricingIsInit);

    useEffect(() => {
        setSession(sessionRaw);
    }, [sessionRaw]);

    // effect to update the gear availability
    useEffect(() => {
        
        // const thisMonthCounter = monthCounters.find((x) => dayjs.tz(x.month).startOf("month").isSame(dayjs.tz(session.start).startOf("month")));
        // if (!thisMonthCounter || !session || !session.start || !session.end) {
        //     return;
        // }
        // const tempGears = handleGearsStock(
		// 	allGearsRaw,
		// 	thisMonthCounter,
		// 	session
		// );

		// setAllGears(tempGears);

        const getLatestData = async () => {
			if (!session || !session.start || !session.end) {
				return
			}
			const thisMonthCounter = await getCounterByDateV2(session.sessionDate);
			const tempGears = handleGearsStock(
				allGearsRaw,
				thisMonthCounter,
				session
			);
			setAllGears(tempGears);
		}

        getLatestData();
        
    }, [session, allGearsRaw]);

    const addGear = (gear: GearTypes.Gear, size: number | string) => {
        // create temp booking gear out of params
        const tempBookingGear: SessionTypes.BookingGear = {
            class: isPremium ? "premium" : "standard",
            gear,
            size,
        };
        const tempBookingGears: SessionTypes.BookingGear[] = [...session.gears];
        tempBookingGears.push(tempBookingGear);


        if (!pricesIsInit || !prices) { return };

        let gearTotal = 0;
        const gearStandardPrice = prices.gear_standard.price;
        const gearPremiumPrice = prices.gear_premium.price;
        let bookedGears = {
            standard: {
                shoe: 0,
                board: 0
            },
            premium: {
                shoe: 0,
                board: 0
            }
        }
        tempBookingGears.forEach((sg) => {
            const usage = sg.gear.gearUsage;
            const gearClass = sg.class;
            bookedGears[gearClass][usage]++;
        });

        gearTotal = (
            Math.max(
                bookedGears['standard']['shoe'],
                bookedGears['standard']['board'],
            ) * gearStandardPrice
        ) + (
            Math.max(
                bookedGears['premium']['shoe'],
                bookedGears['premium']['board'],
            ) * gearPremiumPrice
        )

        let subTotals = { ...session.subTotals };
        subTotals.gears = gearTotal;
        let totalCredits = Object.values(subTotals).reduce((a, b) => a + b, 0);

        const tempSession: SessionTypes.Session = Object.assign(
            { ...session },
            {
                gears: tempBookingGears,
                totalCredits,
                subTotals,
            }
        );

        setParentSession(tempSession)
        store.dispatch(
            setTempSession(
                tempSession
            )
        );
    };

    const removeGear = (idx: number) => {
        if (!pricesIsInit || !prices) { return };

        let tempBookingGears: SessionTypes.BookingGear[] = [...session.gears];
        tempBookingGears.splice(idx, 1);

        let gearTotal = 0;
        const gearStandardPrice = prices.gear_standard.price;
        const gearPremiumPrice = prices.gear_premium.price;
        let bookedGears = {
            standard: {
                shoe: 0,
                board: 0
            },
            premium: {
                shoe: 0,
                board: 0
            }
        }
        tempBookingGears.forEach((sg) => {
            const usage = sg.gear.gearUsage;
            const gearClass = sg.class;
            bookedGears[gearClass][usage]++;
        });

        gearTotal = (
            Math.max(
                bookedGears['standard']['shoe'],
                bookedGears['standard']['board'],
            ) * gearStandardPrice
        ) + (
            Math.max(
                bookedGears['premium']['shoe'],
                bookedGears['premium']['board'],
            ) * gearPremiumPrice
        )

        let subTotals = { ...session.subTotals };
        subTotals.gears = gearTotal;
        let totalCredits = Object.values(subTotals).reduce((a, b) => a + b, 0);

        const tempSession: SessionTypes.Session = Object.assign(
            { ...session },
            {
                gears: tempBookingGears,
                totalCredits,
                subTotals,
            }
        );

        setParentSession(tempSession)
        store.dispatch(
            setTempSession(
                tempSession
            )
        );
    }

    return (
        <>
            <IonCard>
                <IonCardContent>
                    <IonAccordionGroup value={`privateGear`}>
                        <IonAccordion value="privateGear">
                            <IonItem slot="header">
                                <IonLabel>
                                    <h2>
                                        <b>Gear Rental</b>
                                    </h2>
                                </IonLabel>
                            </IonItem>
                            <IonGrid slot="content">
                                <IonRow>
                                    <IonCol>
                                        <IonCard color={isPremium ? `warning` : `light`}>
                                            <IonCardContent>
                                                <IonItem color={isPremium ? `warning` : `light`} lines="none">
                                                    <IonLabel>
                                                        <h2>
                                                            <b>{`Select ${isPremium ? `Premium` : `Standard`} Gears`}</b>
                                                        </h2>
                                                    </IonLabel>
                                                    <IonButton
                                                        size="small"
                                                        shape="round"
                                                        color={isPremium ? "medium" : `warning`}
                                                        onClick={() => {
                                                            setIsPremium(!isPremium);
                                                        }}
                                                    >
                                                        <IonIcon icon={swapHorizontalOutline} />
                                                        <IonLabel className="ion-margin-start ion-margin-end">
                                                            {isPremium ? "Switch To Standard" : "Switch to Premium"}
                                                        </IonLabel>
                                                    </IonButton>
                                                </IonItem>
                                                <IonList inset>
                                                    {allGears.map((x) => {
                                                        return (
                                                            <IonItem key={`gear-item-${x.id}`}>
                                                                <IonLabel>{x.name}</IonLabel>
                                                                <IonButton
                                                                    onClick={() => {
                                                                        setShowGearSize(x.id);
                                                                    }}
                                                                >
                                                                    Select Size
                                                                </IonButton>
                                                                <IonModal
                                                                    isOpen={showGearSize === x.id}
                                                                    onDidDismiss={() => {
                                                                        setShowGearSize("");
                                                                    }}
                                                                >
                                                                    <IonHeader>
                                                                        <IonToolbar color={"dark"}>
                                                                            <IonTitle>{x.name}</IonTitle>
                                                                        </IonToolbar>
                                                                    </IonHeader>
                                                                    <IonContent>
                                                                        <IonList>
                                                                            {x.stock.map((y) => {
                                                                                return (
                                                                                    <IonItem
                                                                                        key={`gear-item-${x.id}-class-${y.class}-size-${y.size}`}
                                                                                        button
                                                                                        disabled={y.qty <= 0}
                                                                                        onClick={() => {
                                                                                            addGear(x, y.size);
                                                                                            setShowGearSize("");
                                                                                        }}
                                                                                    >{`${y.size} (${x.unit.name}) - ${
                                                                                        y.qty > 0 ? `${y.qty} Left` : "OUT OF STOCK"
                                                                                    }`}</IonItem>
                                                                                );
                                                                            })}
                                                                        </IonList>
                                                                    </IonContent>
                                                                </IonModal>
                                                            </IonItem>
                                                        );
                                                    })}
                                                </IonList>
                                            </IonCardContent>
                                        </IonCard>
                                        <IonCard color={"light"}>
                                            <IonCardContent>
                                                <IonItem color={"light"} lines="none">
                                                    <IonLabel>
                                                        <h2>
                                                            <b>Selected Gears</b>
                                                        </h2>
                                                    </IonLabel>
                                                </IonItem>
                                                <IonList color="light">
                                                    {session.gears.length <= 0 ? (
                                                        <IonItem color={"light"} lines="none">
                                                            No Gear Selected
                                                        </IonItem>
                                                    ) : (
                                                        session.gears.map((x, idx) => {
                                                            return (
                                                                <IonItem
                                                                    key={`session-gears-${idx}-item-${x.class}-${x.gear}-${x.size}`}
                                                                    color="light"
                                                                >
                                                                    <IonLabel>
                                                                        {`${x.class.toUpperCase()} ${x.gear.name} (${x.size} ${x.gear.unit.name})`}
                                                                    </IonLabel>
                                                                    <IonButton slot="end" color={'danger'} fill='clear'
                                                                        onClick={() => {
                                                                            removeGear(idx)
                                                                        }}
                                                                    >
                                                                        <IonIcon slot="icon-only" icon={closeCircle} />
                                                                    </IonButton>
                                                                </IonItem>
                                                            );
                                                        })
                                                    )}
                                                </IonList>
                                            </IonCardContent>
                                        </IonCard>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </IonAccordion>
                    </IonAccordionGroup>
                </IonCardContent>
            </IonCard>
        </>
    );
};

export default PrivateGearCard;

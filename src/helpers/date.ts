import { Timestamp } from "firebase/firestore";

export const formatDate = (date: Date) => {
  return date.toLocaleDateString("en-CA");
};
export const formatTime = (date: Date) => {
  return date.toTimeString().substring(0, 5);
};

export const duration = (start: Date, end: Date) => {
  return end.getHours() - start.getHours();
};

export const fromFirebaseTime = (date: Timestamp) => {
  return date.toDate();
};

export const toFirebaseTime = (date: Date) => {
  return Timestamp.fromDate(date);
};

export const withinValidTime = (date: number, sessionEndTime: number) => {
  const today = new Date(new Date().toLocaleDateString()).valueOf(); //today 00:00
  return date > today && date < sessionEndTime;
};

export const isToday = (date: number) => {
  const today = new Date(new Date().toLocaleDateString()).valueOf(); //today 00:00
  const today2359 = new Date(new Date().toLocaleDateString() + ", 23:59:59").valueOf();
  return date > today && date < today2359;
};

export const fromDB = (data: any) => {
	if (typeof data === 'string') {
		return data
	}
	const result = {};
	for (const [key, value] of Object.entries(data)) {
		if (typeof value === `undefined` || !value) {
			Object.assign(result, {
				[key]: value
			})
		} else if (value instanceof Timestamp) {
			Object.assign(result, {
				[key]: value.toDate()
			})
		} else if (typeof value === `object`) {
			if (Array.isArray(value)) {
				const newValue: any[] = [];
				value.forEach(item => {
					newValue.push(fromDB(item));
				});
				Object.assign(result, {
					[key]: newValue
				})	
			} else {
				Object.assign(result, {
					[key]: fromDB(value)
				})	
			}
		} else {
			Object.assign(result, {
				[key]:value
			})
		}
	}
	return result;
}

export const toDB = (data: any) => {
	if (typeof data === 'string') {
		return data
	}
	const result = {};
	for (const [key, value] of Object.entries(data)) {
		if (typeof value === `undefined` || !value) {
			Object.assign(result, {
				[key]: value
			})
		}else if (value instanceof Date) {
			Object.assign(result, {
				[key]: Timestamp.fromDate(value)
			})
		} else if (typeof value === `object`) {
			if (Array.isArray(value)) {
				const newValue: any[] = [];
				value.forEach(item => {
					newValue.push(toDB(item));
				});
				Object.assign(result, {
					[key]: newValue
				})	
			} else {
				Object.assign(result, {
					[key]: toDB(value)
				})	
			}
		} else {
			Object.assign(result, {
				[key]:value
			})
		}
	}
	return result;
}

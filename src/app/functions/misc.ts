import * as SessionTypes from '@tsanghoilun/snow-n-surf-interface/types/session';
import dayjs from 'dayjs';
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";

// import json and type for ph
import hkph from '../../app/data/hkph.json';

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

export interface PhObj {
    start: Date;
    end: Date;
}

export const getMonthSessionCsv = (sessions: SessionTypes.Session[]) => {
    console.log(sessions);

    let csvArray: Array<any> = [];

    const dataMime = "data:text/csv;charset=utf-8,";
    const header = `start, end, pax, booking_reference, email, phone, member ID\n`;

    sessions.forEach(x => {
        csvArray.push(
            [
                dayjs.tz(x.start).format(`YYYY-MM-DD HH:mm`),
                dayjs.tz(x.end).format(`YYYY-MM-DD HH:mm`),
                x.pax,
                x.bookingNumber,
                x.bookingUser?.email,
                x.bookingUser?.phone,
                x.bookingUser?.memberId
            ]
        )
    })

    const csvContent = `${dataMime}${header}${csvArray.map(x => x.join(`,`)).join(`\n`)}`;

    console.log(csvArray, csvContent);

    return csvContent;
}

export const getHkph = () => {
    const ph2raw = hkph.vcalendar[0].vevent;
    let ph2: PhObj[] = [];
    ph2raw.forEach(x => {
        const start = dayjs.tz(dayjs(String(x.dtstart[0]))).toDate();
        const end = dayjs.tz(dayjs(String(x.dtend[0]))).toDate();
        ph2.push({ start, end } as PhObj);
    });

    return ph2;
}
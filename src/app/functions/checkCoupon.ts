import { Coupon, Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";

export interface checkCouponParams {
    coupon: Coupon;
    authUser: Customer;
    session: Session;
}

export const checkCoupon = (params: checkCouponParams) => {
    console.log(`check Coupon params`, params);
    const { coupon, authUser, session } = params;

    let result: boolean = true;
    let message: string = `coupon is valid to be used`;

    if (!coupon.isForever && coupon.validUntil.valueOf() < Date.now()) {
        result = false;
        message = `The coupon has expired`;
    }

    if (coupon.oncePerUser && authUser.usedCouponIds && authUser.usedCouponIds.includes(coupon.id)) {
        result = false;
        message = `You have already used this coupon.`;
    }
    if (!coupon.canReuse && coupon.qty <= 0) {
        result = false;
        message = `This coupon is already invalid.`;
    }
    // if all coupon condition is good, now check if the coupon discount is greating than class discount
    const rawTotal = Object.values({ ...session.subTotals }).reduce((a, b) => a + b, 0);
    const couponDiscount = !coupon
        ? 0
        : coupon.unit === "credit"
        ? coupon.discount
        : Math.ceil(rawTotal * (coupon.discount / 100));
    const couponedTotal = rawTotal - couponDiscount;
    console.log(couponedTotal, couponDiscount, session.totalCredits);
    if (couponedTotal >= session.totalCredits) {
        result = false;
        message = `Coupon discount is less than / same as member discount.`
    }

    return { result, message };
};

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "app/store";
import { Price } from "pages/Pricing/Pricing";

interface PricingState {
	pricing: Price | null;
	isInit: boolean;
}

const initialState: PricingState = {
	pricing: null,
	isInit: false
};

export const pricingSlice = createSlice({
    name: "pricing",
    initialState,
    reducers: {
        setPricing: (state: PricingState, action: PayloadAction<Price>) => {
            state.pricing = action.payload;
        },
        setPricingIsInit: (state: PricingState, action: PayloadAction<boolean>) => {
            state.isInit = action.payload;
        },
    },
});

export const selectPricing = (state: RootState) => state.pricing.pricing;
export const selectPricingIsInit = (state: RootState) => state.pricing.isInit;

// Action creators are generated for each case reducer function
export const { setPricing, setPricingIsInit } = pricingSlice.actions;

export default pricingSlice.reducer;

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { CafeCat, CafeItem, CafeOrder } from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { RootState } from "app/store";

interface cafeState {
  cats: CafeCat[];
  items: CafeItem[];
  orders: CafeOrder[];
  isInit: boolean;
}

const initialState: cafeState = {
  cats: [],
  items: [],
  orders: [],
  isInit: false,
};

export const cafeSlice = createSlice({
  name: "cafe",
  initialState,
  reducers: {
    setAllCafeCats: (state: cafeState, action: PayloadAction<CafeCat[]>) => {
      state.cats = action.payload;
    },
    setAllCafeItems: (state: cafeState, action: PayloadAction<CafeItem[]>) => {
      state.items = action.payload;
    },
    setCafeIsInit: (state: cafeState, action: PayloadAction<boolean>) => {
      state.isInit = action.payload;
    },
    setCafeOrderQueue: (state: cafeState, action: PayloadAction<CafeOrder[]>) => {
      state.orders = action.payload;
    },
  },
});


export const selectCafeIsInit = (state: RootState) => state.cafe.isInit;
export const selectCafeCats = (state: RootState) => state.cafe.cats;
export const selectAllCafeItems = (state: RootState) => state.cafe.items;
export const selectCafeOrderQueue = (state: RootState) => state.cafe.orders;
export const selectCafeItemsByCatId = (catId: string) => (state: RootState) => state.cafe.items.filter(x => x.cat.id === catId);

// Action creators are generated for each case reducer function
export const { setAllCafeCats,setAllCafeItems, setCafeOrderQueue, setCafeIsInit } = cafeSlice.actions;

export default cafeSlice.reducer;

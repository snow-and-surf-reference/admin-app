import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Staff } from "@tsanghoilun/snow-n-surf-interface/types/user";

interface StaffState {
  staffList: Staff[];
}

const initialState: StaffState = {
  staffList: [],
};

export const staffSlice = createSlice({
  name: "staffs",
  initialState,
  reducers: {
    setStaffList: (state: StaffState, action: PayloadAction<Staff[]>) => {
      state.staffList = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setStaffList } = staffSlice.actions;

export default staffSlice.reducer;

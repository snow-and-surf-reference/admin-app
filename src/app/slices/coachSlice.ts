import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Coach } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { RootState } from "app/store";

interface CoachState {
  coachList: Coach[];
}

const initialState: CoachState = {
  coachList: [],
};

export const coachSlice = createSlice({
  name: "coaches",
  initialState,
  reducers: {
    setCoachList: (state: CoachState, action: PayloadAction<Coach[]>) => {
      state.coachList = action.payload;
    },
  },
});

export const selectAllCoaches = (state: RootState) => state.coaches.coachList;

// Action creators are generated for each case reducer function
export const { setCoachList } = coachSlice.actions;

export default coachSlice.reducer;

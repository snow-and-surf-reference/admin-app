import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface MessageModalState {
  message: string;
}

const initialState: MessageModalState = {
  message: "",
};

export const messageModalSlice = createSlice({
  name: "messageModal",
  initialState,
  reducers: {
    setMessageModal: (
      state: MessageModalState,
      action: PayloadAction<string>
    ) => {
      state.message = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setMessageModal } = messageModalSlice.actions;

export default messageModalSlice.reducer;

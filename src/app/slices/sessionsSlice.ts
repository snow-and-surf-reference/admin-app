import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Session, MonthCounters, Coupon } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { defaultSingleSession } from "app/data/defaultSession";
import { RootState } from "app/store";
import dayjs from "dayjs";
import timezone from 'dayjs/plugin/timezone';
import utc from 'dayjs/plugin/utc';

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

interface SessionsState {
  // queue: IPendingCheckInUser[];
  loadedSessions: Session[];
  isInit: boolean;
  queriedSessions: Session[];
  tempSession: Session;
  monthCounters: MonthCounters[];
  coupons: Coupon[];
  couponIsInit: boolean;
}

const initialState: SessionsState = {
  // queue: [],
  loadedSessions: [],
  isInit: false,
  queriedSessions: [],
  tempSession: defaultSingleSession,
  monthCounters: [],
  coupons: [],
  couponIsInit: false
};

export const sessionsSlice = createSlice({
  name: "sessions",
  initialState,
  reducers: {
    setLoadedSessions: (state: SessionsState, action: PayloadAction<Session[]>) => {
      const NewSessions: Session[] = action.payload;
      let existingSessions: Session[] = [...state.loadedSessions];
      NewSessions.forEach((item) => {
        const idx = existingSessions.findIndex((x) => x.id === item.id);
        if (idx < 0) {
          existingSessions.push(item);
        } else {
          existingSessions[idx] = item;
        }
      });
      // console.log(`existingSessions`, existingSessions);
      state.loadedSessions = existingSessions;
    },
    setSessionsIsInit: (state: SessionsState, action: PayloadAction<boolean>) => {
      state.isInit = action.payload;
    },
    setQueriedSessions: (state: SessionsState, action: PayloadAction<Session[]>) => {
      state.queriedSessions = action.payload;
    },
    setTempSession: (state: SessionsState, action: PayloadAction<Session>) => {
      localStorage.setItem('temp-session', JSON.stringify(action.payload));
      state.tempSession = action.payload;
    },
    // setMonthCountersByMonth: (state: SessionsState, action: PayloadAction<MonthCounters>) => {
    //   const idx = state.monthCounters.findIndex(x => x.id === action.payload.id);
    //   if (idx < 0) {
    //     state.monthCounters.push(action.payload)
    //   } else {
    //     state.monthCounters[idx] = action.payload
    //   }
    // },
    setAllCoupons: (state: SessionsState, action: PayloadAction<Coupon[]>) => {
      state.coupons = action.payload
    },
    setCouponsIsInit: (state: SessionsState, action: PayloadAction<boolean>) => {
      state.couponIsInit = action.payload
    }
  },
});

export const selectLoadedSessions = (state: RootState) => state.sessions.loadedSessions;
export const selectQueriedSessions = (state: RootState) => state.sessions.queriedSessions;
export const selectSessionsIsInit = (state: RootState) => state.sessions.isInit;
export const selectLoadedSessionById = (id: string) => (state: RootState) => state.sessions.loadedSessions.find((x) => x.id === id);
export const selectViewSession = (id: string) => (state: RootState) => state.sessions.queriedSessions.find((x) => x.id === id);
export const selectTempSession = (state: RootState) => state.sessions.tempSession;
// export const selectMonthCounters = (state: RootState) => state.sessions.monthCounters;
// export const selectMonthCountersByMonth = (date: Date) => (state: RootState) => { 
//   const startOfMonth = dayjs.tz(date).startOf('month');
//   const endOfMonth = dayjs.tz(date).endOf('month');
//   const counter: MonthCounters | undefined = state.sessions.monthCounters.find(x =>
//     (x.month.valueOf() >= startOfMonth.valueOf()) && (x.month.valueOf() <= endOfMonth.valueOf())
//   );
//   return counter;
// };
export const selectAllCoupons = (state: RootState) => state.sessions.coupons;
export const selectCouponsIsInit = (state: RootState) => state.sessions.isInit;

// Action creators are generated for each case reducer function
export const {
  // setCheckInQueue,
  setLoadedSessions,
  setSessionsIsInit,
  setQueriedSessions,
  setTempSession,
  // setMonthCountersByMonth,
  setAllCoupons,
  setCouponsIsInit
} = sessionsSlice.actions;

export default sessionsSlice.reducer;

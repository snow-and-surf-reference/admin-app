import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { RootState } from "../store";

interface UsersState {
    customer: Customer | null;
    users: Customer[];
    checkInUser: Customer | null;
    parentAccountUser: Customer | null;
    subAccounts: Customer[] | null;
    mobileSearchResults: Customer[] | null;
}

const initialState: UsersState = {
    customer: null,
    users: [],
    checkInUser: null,
    parentAccountUser: null,
    subAccounts: null,
    mobileSearchResults: null,
};

export const usersSlice = createSlice({
    name: "users",
    initialState,
    reducers: {
        setUsers: (state: UsersState, action: PayloadAction<Customer[]>) => {
            state.users = action.payload;
        },
        setCheckInUser: (state: UsersState, action: PayloadAction<Customer | null>) => {
            state.checkInUser = action.payload;
        },
        setCustomer: (state: UsersState, action: PayloadAction<Customer | null>) => {
            // console.log('set customer state triggered');
            state.customer = action.payload;
        },
        setParentAccount: (state: UsersState, action: PayloadAction<Customer | null>) => {
            state.parentAccountUser = action.payload;
        },
        setMobileSearchResults: (state: UsersState, action: PayloadAction<Customer[] | null>) => {
            state.mobileSearchResults = action.payload;
        },
        setSubAccounts: (state: UsersState, action: PayloadAction<Customer[] | null>) => {
            state.subAccounts = action.payload;
        },
        clearSearch: (state: UsersState) => {
            state.subAccounts = null;
            state.parentAccountUser = null;
            state.customer = null;
        },
    },
});

// Selectors goes here
export const selectUsers = (state: RootState) => {
    return state.users.users;
};

export const selectCustomer = (state: RootState) => state.users.customer;

// Action creators are generated for each case reducer function
export const { setUsers, setCheckInUser, setCustomer, setParentAccount, setSubAccounts, clearSearch, setMobileSearchResults } = usersSlice.actions;

export default usersSlice.reducer;

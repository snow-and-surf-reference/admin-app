import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";

import { RootState } from "../store";
import { BasketItem, RefundShopOrder, Shop, ShopItem, ShopOrder } from "@tsanghoilun/snow-n-surf-interface/types/shop";

interface ShopItemState {
    shops: Shop[];
    shopItems: ShopItem[];
    shopBasket: BasketItem[];
    shopOrders: ShopOrder[];
    refundOrders: RefundShopOrder[];
}

const initialState: ShopItemState = {
    shops: [],
    shopItems: [],
    shopBasket: [],
    shopOrders: [],
    refundOrders: [],
};

const shopItemSlice = createSlice({
    name: "shopItems",
    initialState,
    reducers: {
        setAllShops: (state: ShopItemState, action: PayloadAction<Shop[]>) => {
            state.shops = action.payload;
        },
        setAllShopItems: (state: ShopItemState, action: PayloadAction<ShopItem[]>) => {
            state.shopItems = action.payload;
        },
        setToShopBasket: (state: ShopItemState, action: PayloadAction<ShopItem>) => {
            const item = { ...action.payload, qty: 1 };
            const foundItem = state.shopBasket.find((i) => i.id === item.id);
            if (!foundItem) {
                state.shopBasket = [...state.shopBasket, item];
            } else {
                const temp = state.shopBasket.slice();
                temp.splice(state.shopBasket.indexOf(foundItem), 1, { ...foundItem, qty: foundItem.qty + 1 });
                state.shopBasket = temp;
            }
        },
        setBasketItemArray: (state: ShopItemState, action: PayloadAction<BasketItem[]>) => {
            state.shopBasket = action.payload;
        },
        setShopOrders: (state: ShopItemState, action: PayloadAction<ShopOrder[]>) => {
            state.shopOrders = action.payload;
        },
        setRefundShopOrders: (state: ShopItemState, action: PayloadAction<RefundShopOrder[]>) => {
            state.refundOrders = action.payload;
        },
    },
});

export const { setAllShopItems, setToShopBasket, setBasketItemArray, setAllShops, setShopOrders, setRefundShopOrders } = shopItemSlice.actions;

export const selectShopItemsById = (id: string) => (state: RootState) => state.shop.shopItems.find((i) => i.id === id);

export default shopItemSlice.reducer;

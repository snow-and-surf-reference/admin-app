import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface WristbandState {
    connectToScanner: boolean;
    wristbandId: string;
}

const initialState: WristbandState = {
    connectToScanner: false,
    wristbandId: "",
};

export const wristbandSlice = createSlice({
    name: "wristband",
    initialState,
    reducers: {
        setWristbandId: (state: WristbandState, action: PayloadAction<string>) => {
            state.wristbandId = action.payload;
        },
        setConnectToScanner: (state: WristbandState, action: PayloadAction<boolean>) => {
            state.connectToScanner = action.payload;
        },
    },
});

// Action creators are generated for each case reducer function
export const { setWristbandId, setConnectToScanner } = wristbandSlice.actions;

export default wristbandSlice.reducer;

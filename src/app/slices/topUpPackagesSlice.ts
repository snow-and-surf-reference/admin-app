import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { TopUpPackages } from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import { RootState } from "app/store";

interface PackagesState {
  packages: TopUpPackages[];
}

const initialState: PackagesState = {
  packages: [],
};

export const packagesSlice = createSlice({
  name: "packages",
  initialState,
  reducers: {
    setPackages: (state: PackagesState, action: PayloadAction<TopUpPackages[]>) => {
      state.packages = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setPackages } = packagesSlice.actions;
export const getPackageById = (id: string) => (state: RootState) => state.packages.packages.find((i) => i.id === id);

export default packagesSlice.reducer;

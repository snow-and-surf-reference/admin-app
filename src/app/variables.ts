import { PaymentType } from "@tsanghoilun/snow-n-surf-interface/types/shop";
import { Customer, Staff } from "@tsanghoilun/snow-n-surf-interface/types/user";
import dayjs from "dayjs";
import { store } from "./store";

//collection
export const customers = "customers";
export const sessions = "sessions";
export const staffs = "staffs";
export const coaches = "coaches";
export const gears = "gears";
export const storage = "storage";
export const topUpRecords = "topUpRecords";
export const topUpPackages = "topUpPackages";
export const counters = "counters";
export const shopItems = "shopItems";
export const shopOrders = "shopOrders";
export const shops = "shops";
export const shopRefundOrders = "shopRefundOrders";

//storage
export const images = "images";
export const contentImages = "contentImages";
export const waivers = "waivers";

export interface ITopUp {
    createdAt: Date;
    credit: number; // (to user wallet
    amount: number; // (actual money paid),
    paymentMethod: "cash" | "credit card";
    customer: Customer | null;
    staff: Staff | null;
    action: "add" | "refund" | "debit";
    status: "pending" | "success" | "cancelled" | "failed";
    transactions: IPayment[];
}

export interface IPayment {
    amount: number;
    paymentMethod: "cash" | "credit card";
    status: "pending" | "success" | "cancelled" | "failed";
    createdAt: Date;
}

//shop
// shop order number format: "SNS00000001"
export const getLastOrderNo = () => {
    const orders = store.getState().shop.shopOrders;
    const lastOrder = orders[orders.length - 1];
    const newNo = "SNS" + (parseInt(lastOrder.orderNo.split("SNS")[1]) + 1).toString().padStart(8, "0");
    return newNo;
};

export const minMonth = dayjs.tz(new Date()).subtract(8, "M").format("YYYY-MM");
export const thisMonth = dayjs.tz(new Date()).format("YYYY-MM");
export const maxMonth = dayjs.tz(new Date()).add(3, "M").format("YYYY-MM");

export const shopPaymentMethods: { name: string; type: PaymentType }[] = [
    { name: "Cash", type: "cash" },
    { name: "American Express", type: "ae" },
    { name: "Visa / Master", type: "cc" },
    { name: "FPS", type: "fps" },
    { name: "PayMe", type: "payme" },
    { name: "AliPay", type: "alipay" },
    { name: "WeChat Pay", type: "wechat" },
    { name: "Octopus", type: "octopus" },
];

export const singleAlertBtn = (func: () => void) => [
    {
        text: "OK",
        handler: () => {
            func();
        },
    },
];

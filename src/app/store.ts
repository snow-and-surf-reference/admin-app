import { configureStore } from "@reduxjs/toolkit";
// import authUserReducer from './slices/authUserSlice'
import usersReducer from "./slices/usersSlice";
import messageModalReducer from "./slices/messageModalSlice";
import sessionsSlice from "./slices/sessionsSlice";
import globalReducer from "./slices/globalSlice";
import staffReducer from "./slices/staffSlice";
import coachReducer from "./slices/coachSlice";
import venueSlice from "./slices/venueSlice";
import gearSlice from "./slices/gearSlice";
import authSlice from "./slices/authSlice";
import packagesReducer from "./slices/topUpPackagesSlice";
import faqSlice from "./slices/faqSlice";
import wristbandSlice from "./slices/wristbandSlice";
import customerClassesSlice from "./slices/customerClassesSlice";
import shopItemSlice from "./slices/shopItemSlice";
import cafeSlice from "./slices/cafeSlice";
import posContentSlice from "./slices/posContentSlice";
import groupClassSlice from "./slices/groupClassesSlice";
import pricingSlice from "./slices/pricingSlice";
import pageContentsSlice from "./slices/pageContentsSlice";
import otherContentsSlice from "./slices/otherContentsSlice";

export const store = configureStore({
    reducer: {
        authUser: authSlice,
        users: usersReducer,
        messageModal: messageModalReducer,
        sessions: sessionsSlice,
        global: globalReducer,
        staffs: staffReducer,
        coaches: coachReducer,
        venue: venueSlice,
        gears: gearSlice,
        packages: packagesReducer,
        faq: faqSlice,
        wristband: wristbandSlice,
        customerClasses: customerClassesSlice,
        shop: shopItemSlice,
        cafe: cafeSlice,
        posContent: posContentSlice,
        groupClass: groupClassSlice,
        pricing: pricingSlice,
        pageContents: pageContentsSlice,
        otherContents: otherContentsSlice
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;

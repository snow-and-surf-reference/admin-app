import { RoleType } from '@tsanghoilun/snow-n-surf-interface/types/user';

export const defaultRoles: RoleType[] = [
    'admin',
    'sales',
    'editor',
    'manager',
    'coach'
];

export const salesRoles: RoleType[] = [
    'admin',
    'sales',
    'manager'
]

export const editorRoles: RoleType[] = [
    'admin',
    'manager',
    'editor'
]

export const managerRoles: RoleType[] = [
    'admin',
    'manager'
]

export const coachRoles: RoleType[] = [
    'admin',
    'manager',
    'coach'
]
import { DayOfWeek } from "@tsanghoilun/snow-n-surf-interface/types/global";


export const defaultDaysOfWeek: DayOfWeek[] = [
    { index: 0, day: `sunday` },
    { index: 1, day: `monday` },
    { index: 2, day: `tuesday` },
    { index: 3, day: `wednesday` },
    { index: 4, day: `thursday` },
    { index: 5, day: `friday` },
    { index: 6, day: `saturday` },
]
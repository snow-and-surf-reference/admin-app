import { Session } from '@tsanghoilun/snow-n-surf-interface/types/session';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);


export const defaultSingleSession: Session = {
    id: 'new',
    sessionType: 'single',
    bookingNumber: 'temp',
    checkedInUsers: [],
    sessionDate: dayjs.tz(Date.now()).endOf('day').add(1, 'day').toDate(),
    start: null,
    end: null,
    pax: 1,
    playType: 'snow',
    gears: [],
    isCheckedIn: false,
    bookingUser: null,
    waivers: [],
    status: 'pending',
    subTotals: {
        entrances: 0,
        gears: 0,
        coach: 0,
        privateHire: 0
    },
    totalCredits: 0,
    createdAt: dayjs.tz(Date.now()).toDate(),
    lockedAt: null,
    confirmedAt: null,
    cancelledAt: null,
    refundedAt: null,
    groupClass: null,
    memberDiscount: null
};
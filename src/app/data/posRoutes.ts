export interface PosRoute{
    name: string;
    path: string;
}

const posRoutes = () => {
    return [
        {
            name: `homepage`,
            path: `/`
        },
        {
            name: `Park Ride Booking (Snow)`,
            path: `/newBookingSnow`
        },
        {
            name: `Park Ride Booking (Surf)`,
            path: `/newBookingSurf`
        },
        {
            name: `Group Class Booking`,
            path: `/newBooking/groupClass`
        },
        {
            name: `Private Training`,
            path: `/newBooking/privateClass`
        },
        {
            name: `Signup`,
            path: `/signup`
        },
        {
            name: `Coaching`,
            path: `/coaching`
        },
        {
            name: `About us`,
            path: `/about`
        },
        {
            name: `FAQ`,
            path: `/faq`
        },

    ] as PosRoute[];
}

export default posRoutes;
// Import the functions you need from the SDKs you need
import { CafeCat, CafeItem, CafeOrder, CafeOrderStatus } from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { IAccess, ICircletData } from "@tsanghoilun/snow-n-surf-interface/types/circlet";
import * as CoachTypes from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { CoachSchedule } from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { FaqCat, QNA } from "@tsanghoilun/snow-n-surf-interface/types/faq";
import * as GearTypes from "@tsanghoilun/snow-n-surf-interface/types/gear";
import { Gear, GearStorage } from "@tsanghoilun/snow-n-surf-interface/types/gear";
import { PaymentMethod } from "@tsanghoilun/snow-n-surf-interface/types/global";
import * as GroupClassTypes from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import { HomePageSlide } from "@tsanghoilun/snow-n-surf-interface/types/homePage";
import { OtherContent, PageContent } from "@tsanghoilun/snow-n-surf-interface/types/page";
import { BookingGear, Coupon, MonthCounters, MonthSession, PlayType, Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { RefundShopOrder, Shop, ShopItem, ShopOrder } from "@tsanghoilun/snow-n-surf-interface/types/shop";
import { TopupOrder, TopUpPackages } from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import { Coach, Customer, CustomerClass, Staff, User } from "@tsanghoilun/snow-n-surf-interface/types/user";
import * as VenueTypes from "@tsanghoilun/snow-n-surf-interface/types/venue";
import { MediaListItem } from "components/MediaLibrary/MediaListContent";
import dayjs from "dayjs";
import { deleteApp, FirebaseError, getApp, getApps, initializeApp } from "firebase/app";
import {
    createUserWithEmailAndPassword,
    getAuth,
    onAuthStateChanged,
    sendPasswordResetEmail,
    signInWithEmailAndPassword,
    signOut,
} from "firebase/auth";
import {
    addDoc,
    arrayRemove,
    arrayUnion,
    collection,
    deleteDoc,
    deleteField,
    doc,
    DocumentData,
    getDoc,
    getDocs,
    getFirestore,
    increment,
    limit,
    onSnapshot,
    orderBy,
    query,
    QueryDocumentSnapshot,
    runTransaction,
    setDoc,
    Timestamp,
    updateDoc,
    where,
    writeBatch,
} from "firebase/firestore";
import { deleteObject, getDownloadURL, getStorage, listAll, ref, StorageReference, uploadBytes } from "firebase/storage";
import { fromDB, toDB, toFirebaseTime } from "helpers/date";
import { Price } from "pages/Pricing/Pricing";
import randomize from "randomatic";
import { v4 as uuidv4 } from "uuid";
import { defaultSingleSession } from "./data/defaultSession";
import { checkAvailability, checkGearOnly } from "./functions/handleSlots";
import { getMonthSessionCsv } from "./functions/misc";
import { IPendingCheckInUser } from "./models/user";
import { setAuthUser, setAuthUserIsInit } from "./slices/authSlice";
import { setAllCafeCats, setAllCafeItems, setCafeIsInit, setCafeOrderQueue } from "./slices/cafeSlice";
import { setCoachList } from "./slices/coachSlice";
import { setCustomerClasses } from "./slices/customerClassesSlice";
import { setAllFaqCat, setAllFaqQuestion, setFaqIsInit } from "./slices/faqSlice";
import { setAllGears } from "./slices/gearSlice";
import { setIsLoading } from "./slices/globalSlice";
import { setGroupClassPresets } from "./slices/groupClassesSlice";
import { setAllOtherContents } from "./slices/otherContentsSlice";
import { setAllPageContents } from "./slices/pageContentsSlice";
import { setHomepageSlides } from "./slices/posContentSlice";
import { setPricing, setPricingIsInit } from "./slices/pricingSlice";
import { setAllCoupons, setCouponsIsInit, setLoadedSessions, setQueriedSessions, setSessionsIsInit, setTempSession } from "./slices/sessionsSlice";
import { setAllShopItems, setAllShops, setRefundShopOrders, setShopOrders, setToShopBasket } from "./slices/shopItemSlice";
import { setStaffList } from "./slices/staffSlice";
import { setPackages } from "./slices/topUpPackagesSlice";
import { setCheckInUser, setCustomer, setMobileSearchResults, setParentAccount, setSubAccounts } from "./slices/usersSlice";
import { setCapacities } from "./slices/venueSlice";
import { store } from "./store";
import {
    coaches,
    contentImages,
    counters,
    customers,
    gears,
    images,
    sessions,
    shopItems,
    shopOrders,
    shopRefundOrders,
    shops,
    staffs,
    storage,
    topUpPackages,
    topUpRecords,
} from "./variables";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: process.env.REACT_APP_Firebase_apiKey,
    authDomain: process.env.REACT_APP_Firebase_authDomain,
    projectId: process.env.REACT_APP_Firebase_projectId,
    storageBucket: process.env.REACT_APP_Firebase_storageBucket,
    messagingSenderId: process.env.REACT_APP_Firebase_messagingSenderId,
    appId: process.env.REACT_APP_Firebase_appId,
    measurementId: process.env.REACT_APP_Firebase_measurementId,
};

// const app = initializeApp(firebaseConfig);

let allUnSubs: Array<() => void> = [];

export const initFirebase = async () => {
    if (getApps().length <= 0) {
        // Initialize Firebase
        const app = initializeApp(firebaseConfig);
        // console.log(`New Firebase App initialised`);
        const auth = getAuth(app);
        onAuthStateChanged(auth, async (user) => {
            // console.log("auth changed");
            if (user) {
                // User is signed in, see docs for a list of available properties
                // https://firebase.google.com/docs/reference/js/firebase.User
                const uid = user.uid;
                const db = getFirestore();
                const docRef = doc(db, staffs, uid);
                const ss = await getDoc(docRef);

                if (ss.exists()) {
                    const unSub = onSnapshot(docRef, async (sp) => {
                        const data = fromDB(sp.data());
                        const id = sp.id;
                        const authStaff: Staff = {
                            id,
                            ...data,
                        } as Staff;
                        store.dispatch(setAuthUser(authStaff));
                        store.dispatch(setAuthUserIsInit(true));
                    });
                    GlobalListener();
                    allUnSubs.push(unSub);
                } else {
                    console.log("no auth user found in DB");
                    store.dispatch(setAuthUser(null));
                    store.dispatch(setAuthUserIsInit(true));
                }
            } else {
                console.log("user signed out");
                store.dispatch(setAuthUser(null));
                store.dispatch(setAuthUserIsInit(true));
            }
        });
    }
    // else {
    //     console.log(`Firebase App already previously initialised`);
    // }
};

export function isFirebaseError(obj: unknown): obj is FirebaseError {
    return obj instanceof Error && "code" in Error;
}

export const loginStaff = async (email: string, password: string) => {
    const auth = getAuth();
    try {
        await signInWithEmailAndPassword(auth, email, password);
        return "success";
    } catch (error) {
        return "error";
    }
};

export const logoutStaff = async () => {
    const auth = getAuth();
    await signOut(auth);
    if (allUnSubs.length) {
        allUnSubs.forEach((x) => x);
    }
    return "success";
};

export const uploadImage = async (file: File, to?: string) => {
    const storage = getStorage();
    const storageRef = ref(storage, `${to ? to : images}/${new Date().valueOf()}`);
    const res = await uploadBytes(storageRef, file);
    const url = await getDownloadURL(res.ref);
    return url;
};

// export const registerNewUser = async (user: Customer, password: string, userImage: File) => {
//   store.dispatch(setIsLoading(true));
//   // Create an admin app
//   const adminApp = initializeApp(firebaseConfig, "temp");
//   // get auth
//   const auth = getAuth(adminApp);
//   try {
//     // create user in auth
//     const userCredential = await createUserWithEmailAndPassword(auth, user.email, password);
//     // create user in DB
//     const { id, dob, ...userData } = user;

//     await setDoc(doc(getFirestore(), customers, userCredential.user.uid), {
//       ...userData,
//       isSub: false,
//       memberId: await getNewMemberID(),
//       dob: toFirebaseTime(dob),
//       createdAt: toFirebaseTime(new Date()),
//       realImage: await uploadImage(userImage),
//     });
//     await sendEmailVerification(userCredential.user);
//     // delete admin app
//     deleteApp(adminApp);

//     return "success";
//   } catch (error: any) {
//     console.error(error);
//     return error.code
//   } finally {
//     store.dispatch(setIsLoading(false));
//   }
// };

export const registerNewStaff = async (user: Staff, password: string) => {
    store.dispatch(setIsLoading(true));
    // Create an admin app
    const adminApp = initializeApp(firebaseConfig, "temp");
    // get auth
    const auth = getAuth(adminApp);
    try {
        // create user in auth
        const userCredential = await createUserWithEmailAndPassword(auth, user.email, password);
        // create user in DB
        const { id, ...userData } = user;
        await setDoc(doc(getFirestore(), staffs, userCredential.user.uid), {
            ...userData,
            createdAt: toFirebaseTime(new Date()),
        });
        // delete admin app
        deleteApp(adminApp);
        return "success";
    } catch (error: any) {
        return error.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

export const resetStaffPassword = async (staff: Staff) => {
    const auth = getAuth(getApp());
    try {
        await sendPasswordResetEmail(auth, staff.email);
        return "success";
    } catch (error) {
        return error;
    }
};

export const registerNewCoach = async (user: Coach, password: string) => {
    store.dispatch(setIsLoading(true));
    const adminApp = initializeApp(firebaseConfig, "temp");
    const auth = getAuth(adminApp);
    try {
        const userCredential = await createUserWithEmailAndPassword(auth, user.email, password);
        const { id, ...userData } = user;
        await setDoc(doc(getFirestore(), coaches, userCredential.user.uid), {
            ...userData,
            createdAt: toFirebaseTime(new Date()),
        });
        // delete admin app
        deleteApp(adminApp);
        return "success";
    } catch (error: any) {
        return error.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

const getNewMemberID = async () => {
    const code = randomize(`000000`, 6);
    const q = query(collection(getFirestore(), customers), where(`memberId`, `==`, String(code)));
    const codeDocs = await getDocs(q);
    if (codeDocs.size === 0) {
        return code;
    } else {
        await getNewMemberID();
    }
};

export const verifyUserEmailManually = async (id: string) => {
    try {
        await setDoc(doc(getFirestore(), `customers/${id}`), {
            emailVerified: true
        }, { merge: true });
        return 'success';   
    } catch (error) {
        return 'error';
    }
}

export const createSubAccount = async (accountDetails: Omit<Customer, "id">) => {
    store.dispatch(setIsLoading(true));
    const db = getFirestore();
    const subAccount = toDB(accountDetails);
    try {
        await addDoc(collection(db, customers), { ...subAccount, memberId: await getNewMemberID() });
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

export const getUserByMemberIdType = async (id: string, accountType: "main" | "sub" | null) => {
    const db = getFirestore();
    try {
        const q =
            accountType === "main"
                ? query(collection(db, customers), where(`memberId`, `==`, id), where("isSub", "==", false))
                : accountType === "sub"
                ? query(collection(db, customers), where(`memberId`, `==`, id), where("isSub", "==", true))
                : query(collection(db, customers), where(`memberId`, `==`, id));
        const sss = await getDocs(q);
        if (sss.empty) {
            return "User not found";
        } else {
            sss.forEach((doc) => {
                const data = fromDB(doc.data());
                const id = doc.id;
                if (accountType === "sub") {
                    store.dispatch(setSubAccounts([{ id, ...data }] as Customer[]));
                } else if (accountType === "main") {
                    store.dispatch(setParentAccount({ id, ...data } as Customer));
                } else {
                    store.dispatch(setCheckInUser({ id, ...data } as Customer));
                }
            });
            return "success";
        }
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};
export const getUserByMemberId = async (id: string, parentAccount?: boolean, isSub?: boolean) => {
    const db = getFirestore();
    try {
        const q = query(collection(db, customers), where(`memberId`, `==`, id));
        const sss = await getDocs(q);
        if (sss.empty) {
            return "User not found";
        } else {
            sss.forEach((doc) => {
                const data = fromDB(doc.data());
                const id = doc.id;
                if (!parentAccount && isSub) {
                    store.dispatch(setSubAccounts([{ id, ...data }] as Customer[]));
                } else if (parentAccount && !isSub) {
                    store.dispatch(setParentAccount({ id, ...data } as Customer));
                } else {
                    store.dispatch(setCheckInUser({ id, ...data } as Customer));
                }
            });
            return "success";
        }
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};

export const getUserByEmail = async (emailRaw: string, parentAccount?: boolean) => {
    const db = getFirestore();
    const email = emailRaw.toLowerCase();
    if (parentAccount) {
        try {
            const q = query(collection(db, customers), where("email", "==", email), where("isSub", "==", false));
            const sss = await getDocs(q);
            if (sss.empty) {
                return "User not found";
            } else {
                sss.forEach((doc) => {
                    const data = fromDB(doc.data());
                    const id = doc.id;
                    store.dispatch(setParentAccount({ id, ...data } as Customer));
                });
                return "success";
            }
        } catch (error: any) {
            console.error(error);
            return error.code;
        }
    } else {
        try {
            const q = query(collection(db, customers), where("email", "==", email));
            const sss = await getDocs(q);
            if (sss.empty) {
                return "User not found";
            } else {
                sss.forEach((doc) => {
                    const data = fromDB(doc.data());
                    const id = doc.id;
                    store.dispatch(setCustomer({ id, ...data } as Customer));
                });
                return "success";
            }
        } catch (error: any) {
            console.error(error);
            return error.code;
        }
    }
};

export const getSubAccountsByEmailOrPhone = async (input: string, type: "email" | "phone") => {
    const db = getFirestore();
    try {
        const q =
            type === "email"
                ? query(collection(db, customers), where("email", "==", input.toLowerCase()), where("isSub", "==", true))
                : query(collection(db, customers), where("phone", "==", input), where("isSub", "==", true));
        const sss = await getDocs(q);
        let temp: Customer[] = [];
        if (sss.empty) {
            return "User not found";
        } else {
            sss.forEach((doc) => {
                const data = fromDB(doc.data());
                const id = doc.id;
                temp.push({ id, ...data } as Customer);
            });
            store.dispatch(setSubAccounts(temp));
            return "success";
        }
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};

export const getMainAccByMemberId = async (memberID: string) => {
    const db = getFirestore();
    try {
        const q = query(collection(db, customers), where("memberId", "==", memberID), where("isSub", "==", false));
        const sss = await getDocs(q);
        if (sss.empty) {
            return "User not found";
        } else {
            sss.forEach((doc) => {
                const data = fromDB(doc.data());
                const id = doc.id;
                store.dispatch(setParentAccount({ id, ...data } as Customer));
            });
            return "success";
        }
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};

export const getMainAccByPhone = async (phone: string) => {
    const db = getFirestore();
    try {
        const q = query(collection(db, customers), where("phone", "==", phone), where("isSub", "==", false));
        const sss = await getDocs(q);
		let results:Customer[] = [];
        if (sss.empty) {
            return "User not found";
        } else {
            sss.forEach((doc) => {
                const data = fromDB(doc.data());
                const id = doc.id;
				results.push({id, ...data} as Customer)
            });
			store.dispatch(setMobileSearchResults(results));
            return "success";
        }
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};
export const getBookingById = async (id: string) => {
    const res = await getDoc(doc(getFirestore(), `sessions/${id}`));
    return res;
};
export const getBookingByBookingNumber = async (bookingNumber: string) => {
    const db = getFirestore();
    try {
        const q = query(collection(db, sessions), where("bookingNumber", "==", bookingNumber), orderBy("start", "desc"));
        const sss = await getDocs(q);
        let sessionArr: Session[] = [];
        if (sss.empty) {
            return "Session not found";
        } else {
            sss.forEach((doc) => {
                const data = doc.data();
                const temp = fromDB({ id: doc.id, ...data });
                sessionArr.push(temp as Session);
            });
            store.dispatch(setQueriedSessions(sessionArr));
            return "success";
        }
    } catch (e: any) {
        console.error(e);
        return e.code;
    }
};
export const getBookingByPhoneNumber = async (phone: string) => {
    const db = getFirestore();
    try {
        const q = query(collection(db, sessions), where("bookingUser.phone", "==", phone), orderBy("start", "desc"));
        const sss = await getDocs(q);
        let sessionArr: Session[] = [];
        if (sss.empty) {
            return "Session not found";
        } else {
            sss.forEach((doc) => {
                const data = doc.data();
                const temp = fromDB({ id: doc.id, ...data });
                sessionArr.push(temp as Session);
            });
            store.dispatch(setQueriedSessions(sessionArr));
            return "success";
        }
    } catch (e: any) {
        console.error(e);
        return e.code;
    }
};

export const getSessionByCustomerId = async (customerId: string, isSub?: boolean) => {
    const db = getFirestore();
    try {
        const q = isSub
            ? query(collection(db, sessions), where("checkedInUsers", "array-contains", customerId), orderBy("sessionDate", "desc"))
            : query(collection(db, sessions), where("bookingUser.id", "==", customerId), orderBy("sessionDate", "desc"));
        const sss = await getDocs(q);
        let sessionArr: Session[] = [];
        if (sss.empty) {
            return "No history of session";
        } else {
            sss.forEach((doc) => {
                const data = doc.data();
                const temp = fromDB({ id: doc.id, ...data });
                sessionArr.push(temp as Session);
            });
            store.dispatch(setQueriedSessions(sessionArr));
            return "success";
        }
    } catch (e: any) {
        console.error(e);
        return e.code;
    }
};

export const getTopupOrdersByCustomerId = async (id: string) => {
    let message: string = "";
    let topups: TopupOrder[] = [];
    try {
        const q = query(collection(getFirestore(), `topUpRecords`), where("authUser.id", "==", id), orderBy("createdAt", "desc"));
        const sss = await getDocs(q);
        if (sss.empty) {
            return {
                message: "No Topup History",
                topups,
            };
        } else {
            sss.forEach((doc) => {
                const data = doc.data();
                const temp = fromDB({ id: doc.id, ...data });
                topups.push(temp as TopupOrder);
            });
            message = "success";
        }
    } catch (e: any) {
        console.error(e);
        message = e.code;
    }
    return {
        message,
        topups,
    };
};

export const getEntranceSessionsByDate = async (date: Date, isMonth?: boolean) => {
    const db = getFirestore();
    const begin = dayjs
        .tz(date)
        .startOf(isMonth ? "M" : "D")
        .toDate();
    const end = dayjs
        .tz(date)
        .endOf(isMonth ? "M" : "D")
        .toDate();
    try {
        const q = query(
            collection(db, sessions),
            where("sessionDate", ">=", Timestamp.fromDate(begin)),
            where("sessionDate", "<=", Timestamp.fromDate(end)),
            where("status", "==", "confirmed"),
            orderBy("sessionDate", "desc")
        );

        const sss = await getDocs(q);
        let sessionArr: Session[] = [];
        if (sss.empty) {
            return "No history of session";
        } else {
            sss.forEach((doc) => {
                const data = doc.data();
                const temp = fromDB({ id: doc.id, ...data });
                sessionArr.push(temp as Session);
            });
            store.dispatch(setQueriedSessions(sessionArr));
            return "success";
        }
    } catch (e: any) {
        console.error(e);
        return e.code;
    }
};

export const checkInQueueListener = async () => {
    const db = getFirestore();
    const q = query(collection(db, "checkInQueue"), orderBy("time", "asc"));
    const unSub = onSnapshot(q, (querySnapshot) => {
        const queue: IPendingCheckInUser[] = [];
        querySnapshot.forEach((doc) => {
            const temp = fromDB(doc.data());
            queue.push(temp as IPendingCheckInUser);
        });
        // store.dispatch(setTodaySessions(temp));
    });
    allUnSubs.push(unSub);
};

export const staffListener = async () => {
    const q = query(collection(getFirestore(), staffs));
    const unSub = onSnapshot(q, (querySnapshot) => {
        const allStaff: Staff[] = [];
        querySnapshot.forEach((doc) => {
            allStaff.push({ id: doc.id, ...doc.data() } as Staff);
        });
        store.dispatch(setStaffList(allStaff));
    });
    allUnSubs.push(unSub);
};
export const coachListener = async () => {
    const q = query(collection(getFirestore(), coaches), orderBy("coachType", "asc"));
    const unSub = onSnapshot(q, (querySnapshot) => {
        const allCoach: Coach[] = [];
        querySnapshot.forEach((doc) => {
            const data = fromDB(doc.data());
            allCoach.push({ id: doc.id, ...data } as Coach);
        });
        store.dispatch(setCoachList(allCoach));
    });
    allUnSubs.push(unSub);
};

export const sessionsListener = async () => {
    const todayStart = Timestamp.fromDate(dayjs().startOf("D").subtract(1, "second").toDate());
    const todayEnd = Timestamp.fromDate(dayjs().endOf("D").add(1, "second").toDate());
    const q = query(
        collection(getFirestore(), sessions),
        where("start", ">=", todayStart),
        where("start", "<=", todayEnd),
        orderBy("start", "asc"),
        orderBy("end", "asc")
    );
    const unSub = onSnapshot(q, (querySnapshot) => {
        const sessions: Session[] = [];
        querySnapshot.forEach((doc) => {
            const testing = fromDB(doc.data());
            sessions.push({ id: doc.id, ...testing } as Session);
        });
        store.dispatch(setLoadedSessions(sessions));
        store.dispatch(setSessionsIsInit(true));
    });
    allUnSubs.push(unSub);
};

export const shopItemListener = async () => {
    const q = query(collection(getFirestore(), shopItems), orderBy("name", "asc"));
    const unSub = onSnapshot(q, async (sss) => {
        const shopItems: ShopItem[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            shopItems.push({ id, ...data } as ShopItem);
        });
        store.dispatch(setAllShopItems(shopItems));
    });
    allUnSubs.push(unSub);
};

export const shopsListener = async () => {
    const q = query(collection(getFirestore(), shops), orderBy("name", "asc"));
    const unSub = onSnapshot(q, async (sss) => {
        const shops: Shop[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            shops.push({ id, ...ss.data() } as Shop);
        });
        store.dispatch(setAllShops(shops));
    });
    allUnSubs.push(unSub);
};

export const setNewShopFB = async (shop: Shop) => {
    const db = getFirestore();
    const { id, ...temp } = shop;
    try {
        if (shop.id === "new") {
            await addDoc(collection(db, shops), { ...temp });
            return "success";
        } else {
            await updateDoc(doc(db, `${shops}/${shop.id}`), { name: shop.name });
            return "edited";
        }
    } catch (e: any) {
        console.error(e);
        return e.code;
    }
};

export const setShopActive = async (shopId: string, isActive: boolean) => {
    const db = getFirestore();
    try {
        await updateDoc(doc(db, `${shops}/${shopId}`), { isActive });
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};

export const querySessions = async () => {
    const todayStart = Timestamp.fromDate(dayjs().startOf("D").subtract(1, "second").toDate());
    const todayEnd = Timestamp.fromDate(dayjs().endOf("D").add(1, "second").toDate());

    const q = query(
        collection(getFirestore(), sessions),
        where("start", ">=", todayStart),
        where("start", "<=", todayEnd),
        orderBy("start", "asc"),
        orderBy("end", "asc")
    );
    const temp: Session[] = [];

    const qss = await getDocs(q);

    qss.forEach((doc) => {
        const data = fromDB(doc.data());
        temp.push(data as Session);
    });

    // console.log(temp);
    store.dispatch(setLoadedSessions(temp));

    return "success";
};

export const verifyUser = async (customer: Customer, sessionId: string) => {
    const db = getFirestore();
    await updateDoc(doc(db, customers, customer.id), {
        isVerified: true,
    });
    const modifiedCustomer = toDB(customer);

    // Should run cloud function
    await setDoc(doc(db, `${sessions}/${sessionId}`), { checkedInUsers: arrayRemove(modifiedCustomer) }, { merge: true });

    await setDoc(
        doc(db, `${sessions}/${sessionId}`),
        {
            checkedInUsers: arrayUnion({
                ...modifiedCustomer,
                isVerified: true,
            }),
        },
        { merge: true }
    );
};

export const checkUserIn = async () => {
    const db = getFirestore();
    const user = store.getState().users.checkInUser;

    if (user) {
        try {
            await addDoc(collection(db, "checkInRecords"), {
                userId: user.id,
                checkInTime: toFirebaseTime(new Date()),
            }).then(async () => {
                await removeUserFromQueue(user.id);
            });
        } catch (error) {
            console.error(error);
        }
    }
};

export const removeUserFromQueue = async (id: string) => {
    const db = getFirestore();
    try {
        await deleteDoc(doc(db, "checkInQueue", id));
        return "success";
    } catch (e: any) {
        console.error(e);
        return e.code;
    }
};
export const removeUserFromSession = async (session: Session, userId: string) => {
    const checkedInUser = toDB(session.checkedInUsers.find((i) => i.id === userId)!);
    const signedWaiver = toDB(session.waivers.find((i) => i.userId === userId));

    const db = getFirestore();
    try {
        await setDoc(
            doc(db, `${sessions}/${session.id}`),
            {
                checkedInUsers: arrayRemove(checkedInUser),
                waivers: arrayRemove(signedWaiver),
            },
            { merge: true }
        );
    } catch (error) {
        console.error(error);
    }
};

export const generateSession = async () => {
    const db = getFirestore();
    let user: User;
    const q = query(collection(db, customers), where("email", "==", "shinjishinji1129@gmail.com"));
    const unSub = onSnapshot(q, async (querySnapshot) => {
        if (querySnapshot.empty) {
            return "User not found";
        }
        querySnapshot.forEach((doc) => {
            const data = doc.data();
            const id = doc.id;
            user = { id, ...data } as User;
        });
        const today = new Date().toLocaleDateString();
        const todayNum = new Date();
        await addDoc(collection(db, sessions), {
            bookingNumber: randomBookingNumber(),
            bookingUser: user,
            cancelledAt: null,
            checkedInUsers: [],
            confirmedAt: toFirebaseTime(todayNum),
            createdAt: toFirebaseTime(todayNum),
            end: toFirebaseTime(new Date(today + ", 08:00:00 PM")),
            start: toFirebaseTime(new Date(today + ", 07:00:00 PM")),
            gears: [],
            groupClass: null,
            isCheckedIn: false,
            lockedAt: null,
            pax: 2,
            playType: "snow",
            refundedAt: null,
            sessionDate: toFirebaseTime(new Date(today)),
            sessionType: "single",
            status: "confirmed",
            totalCredits: 600,
            waivers: [],
        });
    });
    allUnSubs.push(unSub);
};

export const randomBookingNumber = () => {
    const digitOfNumber = 4;
    const numberLimit = 1000;
    return Math.floor(Math.random() * numberLimit)
        .toString()
        .padStart(digitOfNumber, "0");
};

export const setCircletData = async (nfcId: string, circletObj: ICircletData) => {
    const db = getFirestore();

    try {
        await setDoc(doc(db, `circlet/${nfcId}`), circletObj);
        return "success";
    } catch (e: any) {
        console.error(e);
        return e.code;
    }
};

export const setSessionIsCheckedIn = async (sessionId: string, boolean: boolean) => {
    const db = getFirestore();
    try {
        await updateDoc(doc(db, `${sessions}/${sessionId}`), {
            isCheckedIn: boolean,
        });
    } catch (e) {
        console.error(e);
    }
};

export const setStaffIsEnabled = async (staffID: string, enabled: boolean) => {
    store.dispatch(setIsLoading(true));
    const db = getFirestore();
    try {
        await updateDoc(doc(db, `${staffs}/${staffID}`), {
            enabled,
        });
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};
export const setCoachIsEnabled = async (coachID: string, enabled: boolean) => {
    store.dispatch(setIsLoading(true));
    const db = getFirestore();
    try {
        await updateDoc(doc(db, `${coaches}/${coachID}`), {
            enabled,
        });
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

export const updateStaffDetails = async (staffID: string, staff: Staff) => {
    store.dispatch(setIsLoading(true));
    const db = getFirestore();
    const { id, ...temp } = staff;
    try {
        await updateDoc(doc(db, `${staffs}/${staffID}`), { ...temp });
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};
export const updateCoachDetails = async (coachID: string, coach: Coach) => {
    store.dispatch(setIsLoading(true));
    const db = getFirestore();
    const { id, ...temp } = coach;
    try {
        await updateDoc(doc(db, `${coaches}/${coachID}`), { ...temp });
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

export const saveGearSettings = async (gear: Gear) => {
    store.dispatch(setIsLoading(true));
    const db = getFirestore();
    const { id, ...content } = gear;
    try {
        await updateDoc(doc(db, `${gears}/${id}`), { ...content });
        return "success";
    } catch (e: any) {
        console.error(e);
        return e.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

export const createCoachSehedule = async () => {};

// functions moved from pos booking
export const getCoachScheduleByDate = async (date: Date) => {
    // convert date to start and end timestamps
    const targetDate = {
        start: Timestamp.fromDate(dayjs.tz(date).startOf("day").toDate()),
        end: Timestamp.fromDate(dayjs.tz(date).endOf("day").toDate()),
    };
    const qq = query(collection(getFirestore(), `coachSchedules`), where("date", ">=", targetDate.start), where("date", "<=", targetDate.end));
    const res = await getDocs(qq);
    const schedules: CoachSchedule[] = [];
    res.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        schedules.push({ id, ...data } as CoachSchedule);
    });
    return schedules;
};

export const getMonthCounterById = async (id: string) => {
    const ss = await getDoc(doc(getFirestore(), `counters/${id}`));
    if (ss.exists()) {
        const data = fromDB(ss.data());
        return {
            id,
            ...data,
        } as MonthCounters;
    } else {
        return null;
    }
};

const capListener = async () => {
    const q = query(collection(getFirestore(), `capacities`), limit(1));
    const sss = await getDocs(q);
    if (sss.size !== 1) {
        return;
    }
    const docRef = doc(getFirestore(), `capacities/${sss.docs[0].id}`);
    const unSub = onSnapshot(docRef, (ss) => {
        if (!ss.exists()) {
            // console.log(`cap doc doesn't exist`);
            return;
        }
        store.dispatch(
            setCapacities({
                ...ss.data(),
            } as VenueTypes.Capacities)
        );
    });
    allUnSubs.push(unSub);
};

export const gearListener = async () => {
    const q = query(collection(getFirestore(), `gears`));
    const unSub = onSnapshot(q, (sss) => {
        const gears: GearTypes.Gear[] = [];
        sss.forEach((ss) => {
            gears.push({
                id: ss.id,
                ...ss.data(),
            } as GearTypes.Gear);
        });
        store.dispatch(setAllGears(gears));
    });
    allUnSubs.push(unSub);
};

export const topUpPackagesListener = async () => {
    const q = query(collection(getFirestore(), topUpPackages), orderBy("price", "asc"));
    const unSub = onSnapshot(q, (sss) => {
        const packages: TopUpPackages[] = [];
        sss.forEach((ss) => {
            packages.push({
                id: ss.id,
                ...ss.data(),
            } as TopUpPackages);
        });
        store.dispatch(setPackages(packages));
    });
    allUnSubs.push(unSub);
};

// export const monthCountersListener = async () => {
//     const month = dayjs.tz(Date.now()).startOf("month").toDate();
//     const q = query(collection(getFirestore(), `counters`), where(`month`, `>=`, Timestamp.fromDate(month)));
//     const unSub = onSnapshot(q, async (sss) => {
//         sss.forEach((ss) => {
//             const data = fromDB(ss.data());
//             const monthCounter = {
//                 id: ss.id,
//                 ...data,
//             } as MonthCounters;
//             // console.log(monthCounter)
//             store.dispatch(setMonthCountersByMonth(monthCounter));
//         });
//     });
//     allUnSubs.push(unSub);
// };

export const getCounterByDate = async (date: Date) => {
    const monthStart = dayjs.tz(date).startOf("month").toDate();
    const monthEnd = dayjs.tz(date).endOf("month").toDate();
    const q = query(
        collection(getFirestore(), `counters`),
        where(`month`, `>=`, Timestamp.fromDate(monthStart)),
        where(`month`, `<=`, Timestamp.fromDate(monthEnd)),
        limit(1)
    );
    const sss = await getDocs(q);
    let counter: MonthCounters[] = [];
    sss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        counter.push({ id, ...data } as MonthCounters);
    });
    if (counter.length !== 1) {
        return null;
    }
    return counter[0];
};

export const getCounterByDateV2 = async (date: Date) => {
    const dateStart = dayjs.tz(date).startOf("date").toDate();
    const dateEnd = dayjs.tz(date).endOf("date").toDate();
    const q = query(
        collection(getFirestore(), `sessions`),
        where(`sessionDate`, `>=`, Timestamp.fromDate(dateStart)),
        where(`sessionDate`, `<=`, Timestamp.fromDate(dateEnd))
    );
    const sessions: MonthSession[] = [];
    const sss = await getDocs(q);
    sss.forEach((ss) => {
        const data = fromDB(ss.data());
        const session = { id: ss.id, ...data } as Session;
        const { id, sessionType, start, end, pax, playType, gears, groupClass, privateCoach, status } = session;
        if (status === "confirmed" || status === "locked") {
            sessions.push({
                id,
                sessionType,
                start,
                end,
                pax,
                playType,
                gears,
                groupClass,
                privateCoach,
            } as MonthSession);
        }
    });

    const tempCounters: MonthCounters = {
        id: dayjs.tz(date).format(`YYYYMMDD`),
        sessions,
        month: dateStart,
    };

    console.log(`sessions for date: ${dayjs.tz(date).format(`YYYY-MM-DD`)}`, tempCounters);

    return tempCounters;
};

export const createLockedSession = async (session: Session) => {
    const { id, lockedAt, status, ...data } = session;
    const payload = toDB({
        lockedAt: dayjs.tz(Date.now()).add(15, `minutes`).toDate(),
        status: `locked`,
        ...data,
    });

    // const sessionMouthCounters: MonthCounters[] = selectMonthCounters(store.getState());
    // const counterId = sessionMouthCounters.find((x) => dayjs.tz(session.sessionDate).startOf("month").isSame(dayjs.tz(x.month).startOf("month")))?.id;
    // if (!counterId) {
    //     return {
    //         message: "Failed to start your booking, please try again later.",
    //     };
    // }

    // const counterRef = doc(getFirestore(), `counters/${counterId}`);

    // V2 get counters from sessions docs
    const counters = await getCounterByDateV2(session.sessionDate);

    const singleCoachSchedules: CoachTypes.CoachSchedule[] = await getCoachScheduleByDate(session.sessionDate);
    const allRecurringCoachSchedules: CoachTypes.CoachSchedule[] = await getRecurringCoachSchedules();
    // const recurringCoachSchedulesByDay = allRecurringCoachSchedules.filter(
    //     (x) => x.daysOfWeek?.filter((y) => y.index === dayjs.tz(session.sessionDate).get("day")).length === 1
    // );
    const recurringCoachSchedulesByDay = allRecurringCoachSchedules.filter((x) => {
        const isRightDay = x.daysOfWeek?.filter((y) => y.index === dayjs.tz(session.sessionDate).get("day")).length === 1;
        const isValidDate = x.endDate ? x.endDate.valueOf() >= session.sessionDate.valueOf() : true;
        const isValidStartDate = x.startDate ? x.startDate.valueOf() <= session.sessionDate.valueOf() : true;
        return isRightDay && isValidDate && isValidStartDate;
    });

    // get coach schedules for use later
    const coachSchedules = singleCoachSchedules.concat(recurringCoachSchedulesByDay);
    const groupClassSlots: GroupClassTypes.GroupClassSlot[] = await getGroupClassSlotsByDate(dayjs.tz(session.sessionDate).startOf("day").toDate());
    const recurringSlots: GroupClassTypes.GroupClassSlot[] = await getRGroupClassSlotsByDay(dayjs.tz(session.sessionDate).get("day"));
    const newSessionId = uuidv4();
    const newSessionRef = doc(getFirestore(), `sessions/${newSessionId}`);
    // const { sessionType, start, end, pax, playType, gears, groupClass } = session;
    // const newCounterSession: MonthSession = {
    //     id: newSessionId,
    //     sessionType,
    //     start,
    //     end,
    //     pax,
    //     playType,
    //     gears,
    //     groupClass,
    // };

    // use transaction to create the session
    try {
        await runTransaction(getFirestore(), async (txn) => {
            // const counterSs = await txn.get(counterRef);
            // if (!counterSs.exists()) {
            //     throw new Error(`Something's not right... Please try again later.`);
            // }
            // const data = fromDB(counterSs.data());
            // const counter: MonthCounters = {
            //     id: counterSs.id,
            //     ...data,
            // } as MonthCounters;

            const finalCheck = checkAvailability(session, counters, coachSchedules, groupClassSlots, recurringSlots, false);
            if (finalCheck.result) {
                // create the session doc
                txn.set(newSessionRef, payload);
                // txn.update(counterSs.ref, {
                //     sessions: arrayUnion(toDB(newCounterSession)),
                // });
            } else {
                throw new Error(`${finalCheck.message}`);
            }
        });
    } catch (error: any) {
        return {
            message: error.message,
        };
    }

    store.dispatch(setTempSession(defaultSingleSession));
    return {
        message: "success",
        sessionId: newSessionId,
    };
};

export const cancelLockedSession = async (session: Session) => {
    // const monthCounter: MonthCounters | undefined = selectMonthCountersByMonth(session.sessionDate)(store.getState());
    // if (!monthCounter) {
    //     return "Network issue, Please retry later.";
    // }

    // const counterSession: MonthSession | undefined = monthCounter.sessions.find((x) => x.id === session.id);
    // if (!counterSession) {
    //     return "Network issue, Please retry later.";
    // }

    // const convertedCounter = toDB(counterSession);

    const batch = writeBatch(getFirestore());
    const docRef = doc(getFirestore(), `sessions/${session.id}`);
    // const counterRef = doc(getFirestore(), `counters/${monthCounter.id}`);

    batch.delete(docRef);
    // batch.update(counterRef, {
    //     sessions: arrayRemove(convertedCounter),
    // });

    await batch.commit();

    return "success";
};

export const getSessionById = async (id: string) => {
    const res = await getDoc(doc(getFirestore(), `sessions/${id}`));
    if (!res.exists()) {
        return null;
    }
    const data = fromDB(res.data());
    return {
        id,
        ...data,
    } as Session;
};

export const paySession = async (session: Session, customerId: string, useStaffCredit: boolean = false) => {
    const genCode = async () => {
        let code = randomize(`0000`, 4);
        let isUnique = false;

        while (!isUnique) {
            const q = query(collection(getFirestore(), `sessions`), where(`bookingNumber`, `==`, String(code)));
            const codeDocs = await getDocs(q);
            if (codeDocs.size === 0) {
                isUnique = true;
            } else {
                // console.log('code is not unique, checking same day')
                let hasDupToday: Session[] = [];
                codeDocs.forEach((ss) => {
                    const data = fromDB(ss.data());
                    const temp: Session = {
                        id: ss.id,
                        ...data,
                    } as Session;
                    const sessionDate = dayjs.tz(temp.sessionDate).valueOf();
                    const todayStart = dayjs.tz(Date.now()).startOf("day").valueOf();
                    const todayEnd = dayjs.tz(Date.now()).endOf("day").valueOf();
                    if (sessionDate >= todayStart && sessionDate <= todayEnd) {
                        hasDupToday.push(temp);
                    }
                });
                if (!hasDupToday.length) {
                    isUnique = true;
                } else {
                    // console.log('code is not unique even today, regen')
                    code = randomize(`0000`, 4);
                }
            }
        }

        return code;
    };

    const newCode = await genCode();

    // update session: confirm date, cancelled date
    const payload = {
        bookingNumber: String(newCode),
        status: `confirmed`,
        confirmedAt: dayjs.tz(Date.now()).toDate(),
        cancelledAt: null,
        lockedAt: null,
    };

    let customerPay = session.totalCredits;
    let staffPay = 0;
    const authStaff = store.getState().authUser.authUser;
    if (useStaffCredit && authStaff) {
        const staffBalance = authStaff.wallet.balance;
        staffPay = Math.min(staffBalance, session.totalCredits);
        customerPay = session.totalCredits - staffPay;
    }

    try {
        await setDoc(doc(getFirestore(), `sessions/${session.id}`), toDB(payload), {
            merge: true,
        });
        let usedCouponId = session.usedCoupon ? [session.usedCoupon.id] : [];
        await setDoc(
            doc(getFirestore(), `customers/${customerId}`),
            {
                wallet: {
                    balance: increment(-customerPay),
                    spent: increment(0),
                },
                usedCouponId: arrayUnion(...usedCouponId),
            },
            { merge: true }
        );
        if (useStaffCredit && authStaff) {
            await setDoc(
                doc(getFirestore(), `staffs/${authStaff.id}`),
                {
                    wallet: {
                        balance: increment(-staffPay),
                        spent: increment(staffPay),
                    },
                },
                { merge: true }
            );
        }

        if (session.usedCoupon) {
            if (!session.usedCoupon.canReuse) {
                await setDoc(
                    doc(getFirestore(), `coupons/${session.usedCoupon.id}`),
                    {
                        qty: increment(-1),
                    },
                    { merge: true }
                );
            }
        }

        return "success";
    } catch (error) {
        return error;
    }
};

// end of pos moved functions

// Coach Schedule Function
export const getCoachSchedulesByRange = async (from: Date, to: Date) => {
    const fromTS = Timestamp.fromDate(from);
    const toTS = Timestamp.fromDate(to);

    const q = query(collection(getFirestore(), `coachSchedules`), where("date", ">=", fromTS), where("date", "<=", toTS));
    const rq = query(collection(getFirestore(), `coachSchedules`), where("isRecurring", "==", true));
    const schedules: CoachSchedule[] = [];
    const sss = await getDocs(q);
    const rsss = await getDocs(rq);
    sss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        schedules.push({ id, ...data } as CoachSchedule);
    });
    rsss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        schedules.push({ id, ...data } as CoachSchedule);
    });
    return schedules;
};

export const getRecurringCoachSchedules = async () => {
    const rq = query(collection(getFirestore(), `coachSchedules`), where("isRecurring", "==", true));
    const schedules: CoachSchedule[] = [];
    const rsss = await getDocs(rq);
    rsss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        schedules.push({ id, ...data } as CoachSchedule);
    });
    return schedules;
};

export const createNewCoachSchedule = async (schedule: CoachTypes.CoachSchedule) => {
    const { id, ...data } = schedule;
    const payload = toDB(data);
    await addDoc(collection(getFirestore(), `coachSchedules`), payload);
    return "success";
};

export const updateCoachSchedule = async (schedule: CoachTypes.CoachSchedule) => {
    const { id, ...data } = schedule;
    const payload = toDB(data);

    await setDoc(doc(getFirestore(), `coachSchedules/${id}`), payload, { merge: true });
    return "success";
};

export const deleteCoachSchedule = async (id: string) => {
    await deleteDoc(doc(getFirestore(), `coachSchedules/${id}`));
    return "success";
};

export const getCustomerStorage = async (customerEmail: string) => {
    const db = getFirestore();
    const q = query(collection(db, storage), where("customerEmail", "==", customerEmail), orderBy("storageNum", "desc"));
    const res = await getDocs(q);
    const userStorage: GearStorage[] = [];
    res.forEach((doc) => {
        const data = fromDB(doc.data());
        userStorage.push({ ...data, id: doc.id } as GearStorage);
    });
    return userStorage;
};

export const getNewStorageNumber = async () => {
    const db = getFirestore();
    const q = query(collection(db, storage), orderBy("storageNum", "desc"), limit(1));
    const res = await getDocs(q);
    if (res.empty) {
        return (1).toString().padStart(3, "0");
    }
    let temp: any[] = [];
    res.forEach((doc) => {
        const data = fromDB(doc.data());
        temp.push(data);
    });
    return (parseInt(temp[0].storageNum) + 1).toString().padStart(3, "0");
};

export const removeStorageById = async (id: string) => {
    store.dispatch(setIsLoading(true));
    const db = getFirestore();
    try {
        await deleteDoc(doc(db, `${storage}/${id}`));
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

// export const getNewStorageNumber = async () => {
// const allStorageNumber = await getNewStorageNumber();

// const ranNumber = () => {
//     const ran = Math.floor(Math.random() * 1000);
//     const newStorage = ran.toString().padStart(3, "0");
//     if (!!allStorageNumber.find((i) => i === newStorage)) {
//         ranNumber();
//     } else {
//         return newStorage;
//     }
// };
// return ranNumber();
// };

export const addStorageToUser = async (id: string, email: string, price: number) => {
    store.dispatch(setIsLoading(true));
    const db = getFirestore();
    const storageNum = await getNewStorageNumber();
    const today = new Date(new Date().toLocaleDateString("en-CA"));
    try {
        await addDoc(collection(db, storage), {
            customerEmail: email,
            customerId: id,
            storageNum,
            validFrom: toFirebaseTime(today),
            validTo: toFirebaseTime(new Date(today.valueOf() + 31449600000)), //364 days
            price,
            createdAt: new Date(),
        });
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

export const topUpUserByStaff = async (customer: Customer, payment: PaymentMethod, exactAmount: number, topupPackage?: TopUpPackages) => {
    store.dispatch(setIsLoading(true));
    const staff = store.getState().authUser.authUser!;
    const baseObj = {
        createdAt: new Date(),
        orderReference: `topup_${customer.id}_${new Date().valueOf()}`,
        authUser: customer,
        staff,
        paymentStatus: `AUTHORIZED`,
        paymentMethod: payment,
        requestId: "",
        responseCode: "",
    };
    const payload: Omit<TopupOrder, "id"> = topupPackage ? { ...baseObj, topupPackage } : { ...baseObj, exactAmount };
    const db = getFirestore();
    try {
        await addDoc(collection(db, `${topUpRecords}`), payload);
        await setDoc(
            doc(db, `${customers}/${customer.id}`),
            {
                wallet: {
                    balance: topupPackage ? increment(topupPackage.receivedCredits) : increment(exactAmount),
                    spent: topupPackage ? increment(topupPackage.receivedCredits) : increment(exactAmount),
                },
            },
            { merge: true }
        );
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

export const getTopUpRecords = async (fromDate: Date, toDate: Date) => {
    store.dispatch(setIsLoading(true));
    const q = query(
        collection(getFirestore(), topUpRecords),
        where("createdAt", ">=", Timestamp.fromDate(fromDate)),
        where("createdAt", "<=", Timestamp.fromDate(toDate))
    );
    try {
        const sss = await getDocs(q);
        let records: TopupOrder[] = [];
        if (sss.empty) {
            return;
        } else {
            sss.forEach((ss) => {
                records.push({
                    id: ss.id,
                    ...fromDB(ss.data()),
                } as TopupOrder);
            });
            return records;
        }
    } catch (e: any) {
        console.error(e.code);
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

export const voidTopupRecord = async (id: string) => {
    await setDoc(
        doc(getFirestore(), `topUpRecords/${id}`),
        {
            paymentStatus: `voided`,
        },
        { merge: true }
    );
    return;
};

// functions for venue caps
export const updateCaps = async (caps: VenueTypes.Capacities) => {
    const payload = toDB(caps);
    const q = query(collection(getFirestore(), `capacities`), limit(1));
    const sss = await getDocs(q);
    if (sss.size !== 1) {
        return;
    }
    const docRef = doc(getFirestore(), `capacities/${sss.docs[0].id}`);
    await updateDoc(docRef, payload);
    return "success";
};

export const addNewTopUpPackage = async (tuPackage: TopUpPackages) => {
    const { id, ...temp } = tuPackage;
    try {
        if (id === "new") {
            await addDoc(collection(getFirestore(), topUpPackages), temp);
        } else {
            await updateDoc(doc(getFirestore(), `${topUpPackages}/${id}`), { ...temp });
        }
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};

export const removePackage = async (id: string) => {
    const db = getFirestore();
    try {
        await deleteDoc(doc(db, `${topUpPackages}/${id}`));
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};

// Functions for FAQ
export const faqListener = async () => {
    const q = query(collection(getFirestore(), `faqCategories`));
    const qq = query(collection(getFirestore(), `faqQuestions`));
    const unSub = onSnapshot(q, async (sss) => {
        let faqCats: FaqCat[] = [];
        sss.forEach((ss) => {
            const data = fromDB(ss.data());
            const id = ss.id;
            faqCats.push({
                id,
                ...data,
            } as FaqCat);
        });
        store.dispatch(setAllFaqCat(faqCats));
        store.dispatch(setFaqIsInit(true));
    });

    const unSub2 = onSnapshot(qq, async (sss) => {
        let faqQs: QNA[] = [];
        sss.forEach((ss) => {
            const data = fromDB(ss.data());
            const id = ss.id;
            faqQs.push({
                id,
                ...data,
            } as QNA);
        });
        store.dispatch(setAllFaqQuestion(faqQs));
        store.dispatch(setFaqIsInit(true));
    });
    allUnSubs.push(unSub);
    allUnSubs.push(unSub2);
};

export const setFaqCat = async (cat: FaqCat) => {
    let docRef;
    const { id, ...payload } = cat;
    if (id === "new") {
        docRef = doc(collection(getFirestore(), "faqCategories"));
    } else {
        docRef = doc(getFirestore(), `faqCategories/${id}`);
    }
    await setDoc(docRef, payload);
    return "success";
};

export const setFaqQuestion = async (quesion: QNA) => {
    let docRef;
    const { id, ...payload } = quesion;
    if (id === "new") {
        docRef = doc(collection(getFirestore(), "faqQuestions"));
    } else {
        docRef = doc(getFirestore(), `faqQuestions/${id}`);
    }
    await setDoc(docRef, payload);
    return "success";
};

export const deleteFaqCat = async (id: string) => {
    await deleteDoc(doc(getFirestore(), `faqCategories/${id}`));
    return "success";
};

export const deleteFaqQuestion = async (id: string) => {
    await deleteDoc(doc(getFirestore(), `faqQuestions/${id}`));
    return "success";
};

export const getCircletDataByID = async (id: string) => {
    const db = getFirestore();
    try {
        const res = await getDoc(doc(db, `circlet/${id}`));
        const circletData = fromDB(res.data()) as ICircletData;

        const sessionRes = await getDoc(doc(db, `${sessions}/${circletData.sessionId}`));
        const session = fromDB(sessionRes.data()) as Session;

        const customerRes = await getDoc(doc(db, `${customers}/${circletData.customerId}`));
        const customer = fromDB(customerRes.data()) as Customer;

        return { customer, session };
    } catch (e: any) {
        console.error(e);
        return e.code;
    }
};

// functions for maintenance
export const getMaintenencesByRange = async (start: Date, end: Date) => {
    const qq = query(
        collection(getFirestore(), `maintenences`),
        where("start", ">=", Timestamp.fromDate(start)),
        where("start", "<=", Timestamp.fromDate(end))
    );

    const sss = await getDocs(qq);
    const results: VenueTypes.MaintenanceSession[] = [];
    sss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        results.push({ id, ...data } as VenueTypes.MaintenanceSession);
    });

    return results;
};

export const getMaintenencesByDate = async (date: Date) => {
    const qq = query(
        collection(getFirestore(), `maintenences`),
        where("start", "<=", Timestamp.fromDate(dayjs.tz(date).startOf("day").add(1, "day").toDate())),
        where("start", ">=", Timestamp.fromDate(dayjs.tz(date).subtract(3, "months").toDate()))
    );

    const sss = await getDocs(qq);
    const results: VenueTypes.MaintenanceSession[] = [];
    sss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        results.push({ id, ...data } as VenueTypes.MaintenanceSession);
    });

    return results;
};

export const checkMaintenenceSession = async (session: VenueTypes.MaintenanceSession) => {
    const isIn60Days = dayjs.tz(session.start).diff(dayjs(), "days") <= 60;
    if (!isIn60Days) {
        await setMaintenence(session);
        return "success";
    }

    let effectedPlayTypes: PlayType[] = [];
    session.venue.forEach((v) => {
        const playType: PlayType = v.playType;
        if (!effectedPlayTypes.includes(playType)) {
            effectedPlayTypes.push(playType);
        }
    });

    if (!session.start || !session.end) {
        return "success";
    }

    // get sessions
    const q = query(
        collection(getFirestore(), `sessions`),
        where("start", ">=", Timestamp.fromDate(session.start)),
        where("start", "<=", Timestamp.fromDate(session.end))
    );

    const sss = await getDocs(q);
    let effectedSessions: Session[] = [];
    sss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        const item = { id, ...data } as Session;
        const playType: PlayType = item.playType === "snow" ? "ski" : item.playType === "sb" ? "ski" : item.playType;
        const isPlayType = playType === "all" ? true : effectedPlayTypes.includes(playType);
        const isConfirmed = item.confirmedAt !== null;
        const isCancelled = item.cancelledAt !== null;
        isPlayType && isConfirmed && !isCancelled && effectedSessions.push(item);
    });

    if (effectedSessions.length <= 0) {
        await setMaintenence(session);
        return "success";
    }

    const csv = getMonthSessionCsv(effectedSessions);

    return {
        message: "warning",
        count: effectedSessions.length,
        csv: csv,
    };
};

export const setMaintenence = async (session: VenueTypes.MaintenanceSession) => {
    let docRef;
    const { id, ...payload } = session;
    console.log(`main doc:`, payload);
    // return 'success';
    if (id === "new") {
        docRef = doc(collection(getFirestore(), "maintenences"));
    } else {
        docRef = doc(getFirestore(), `maintenences/${id}`);
    }
    await setDoc(docRef, payload);
    return "success";
};

export const deleteMaintenence = async (sessionId: string) => {
    await deleteDoc(doc(getFirestore(), `maintenences/${sessionId}`));
    return "success";
};

export const checkPrivateSession = async (start: Date, end: Date) => {
    const monthCounter = store.getState().sessions.monthCounters;
    const thisMonth = monthCounter.find((x) => x.month.getMonth() === new Date().getMonth());
    if (thisMonth) {
        const duration = (end.getHours() - start.getHours()) * 2;
        let accessArray: IAccess[] = [];

        for (let i = 0; i < duration; i++) {
            const thisStart = dayjs.tz(start).add(i * 30, "minutes");
            const thisEnd = thisStart.add(30, "minutes");
            if (
                thisMonth.sessions &&
                thisMonth.sessions.some((session) => {
                    return (
                        ((session.start && thisStart.valueOf() >= session.start.valueOf() && thisStart.valueOf() <= session.end!.valueOf()) ||
                            (session.start && thisEnd.valueOf() >= session.start.valueOf() && thisEnd.valueOf() <= session.end!.valueOf())) &&
                        session.sessionType === "privateBelt"
                    );
                })
            ) {
                accessArray.push({ area: ["snow2"], start: thisStart.toDate(), end: thisEnd.toDate() });
            } else {
                accessArray.push({ area: ["snow1", "snow2"], start: thisStart.toDate(), end: thisEnd.toDate() });
            }
        }
        return accessArray;
    } else {
        return false;
    }
};

// Customer Classes
export const CustomerClassesListener = async () => {
    const q = query(collection(getFirestore(), `customerClasses`));

    const unSub = onSnapshot(q, async (sss) => {
        const customerClasses: CustomerClass[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            customerClasses.push({ id, ...data } as CustomerClass);
        });
        store.dispatch(setCustomerClasses(customerClasses));
    });
    allUnSubs.push(unSub);
};

export const saveCustomerClasses = async (classes: CustomerClass[]) => {
    const db = getFirestore();
    const batch = writeBatch(db);
    classes.forEach((cl) => {
        const { id, ...data } = cl;
        if (id === `new`) {
            addDoc(collection(db, `customerClasses`), { ...data });
        } else {
            batch.update(doc(db, `customerClasses/${id}`), { ...data });
        }
    });
    await batch.commit();
};

export const deleteCustomerClass = async (id: string) => {
    await deleteDoc(doc(getFirestore(), `customerClasses/${id}`));
    return "success";
};

export const setSessionToCxl = async (session: Session, refundCredit: number) => {
    const db = getFirestore();
    const thisMonth = store.getState().sessions.monthCounters.find((i) => i.month.getMonth() === session.sessionDate.getMonth());
    const thisCount = thisMonth?.sessions.find((i) => i.id === session.id);

    try {
        await updateDoc(doc(db, `${sessions}/${session.id}`), {
            cancelledAt: new Date(),
            status: "cancelled",
        });
        if (!!refundCredit) {
            await setDoc(
                doc(db, `${customers}/${session.bookingUser!.id}`),
                {
                    wallet: {
                        balance: increment(refundCredit),
                        spent: increment(0),
                    },
                },
                { merge: true }
            );
        }
        if (!!thisCount) {
            await updateDoc(doc(db, `${counters}/${thisMonth?.id}`), {
                sessions: arrayRemove(thisCount),
            });
        }
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};

export const updateShopItem = async (item: ShopItem, newPic?: File | null) => {
    const db = getFirestore();
    const imgLink = newPic ? await uploadImage(newPic) : item.image;
    const temp = {
        ...item,
        image: imgLink,
        color: item.color ?? null,
        size: !!item.size ? item.size.toUpperCase() : null,
    };
    const { id, ...data } = temp;

    try {
        if (id === "new") {
            await addDoc(collection(db, `${shopItems}`), { ...data });
        } else {
            await updateDoc(doc(db, `${shopItems}/${id}`), { ...data });
        }
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};
export const activateShopItem = async (itemId: string, isActive: boolean) => {
    const db = getFirestore();
    try {
        await updateDoc(doc(db, `${shopItems}/${itemId}`), { isActive });
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};
export const setShopItem = async (barcode: string) => {
    const db = getFirestore();
    try {
        const q = query(collection(db, shopItems), where(`barcode`, `==`, barcode));
        const sss = await getDocs(q);
        if (sss.empty) {
            throw new Error("Item not found");
            // return "Item not found";
        } else {
            sss.forEach((ss) => {
                const id = ss.id;
                const data = ss.data();
                store.dispatch(setToShopBasket({ id, ...data } as ShopItem));
            });
        }
        // return "success";
    } catch (error: any) {
        console.error(error);
        throw error;
    }
};

export const getItemByBarcode = async (barcode: string) => {
    const db = getFirestore();
    try {
        const q = query(collection(db, shopItems), where(`barcode`, `==`, barcode));
        const sss = await getDocs(q);
        if (sss.empty) {
            return "Item not found";
        } else {
            sss.forEach((ss) => {
                const id = ss.id;
                const data = ss.data();
                store.dispatch(setToShopBasket({ id, ...data } as ShopItem));
            });
        }
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};

export const addShopOrder = async (data: ShopOrder) => {
    const db = getFirestore();
    const { id, ...order } = data;
    try {
        //id is always "new" for now
        await addDoc(collection(db, shopOrders), order);
        order.item.forEach(async (i) => {
            await setDoc(doc(db, `${shopItems}/${i.id}`), { stock: increment(-i.qty) }, { merge: true });
        });
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};

export const refundOrder = async (refundData: RefundShopOrder) => {
    const db = getFirestore();
    const { id, ...data } = refundData;

    try {
        if (refundData.id === "new") {
            const newDoc = await addDoc(collection(db, shopRefundOrders), data);

            await setDoc(
                doc(db, `${shopOrders}/${refundData.originalOrder.id}`),
                { refundOrder: arrayUnion({ id: newDoc.id, ...data }) },
                { merge: true }
            );
            if (refundData.restoreItem) {
                for (let i = 0; i < refundData.refundItems.length; i++) {
                    await setDoc(
                        doc(db, `${shopItems}/${refundData.refundItems[i].id}`),
                        { stock: increment(refundData.refundItems[i].qty) },
                        { merge: true }
                    );
                }
            }
        }
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code;
    }
};

export const shopOrderListener = async () => {
    const q = query(collection(getFirestore(), shopOrders), orderBy("orderNo", "asc"));
    const unSub = onSnapshot(q, async (sss) => {
        let orders: ShopOrder[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            orders.push({ id, ...data } as ShopOrder);
        });
        store.dispatch(setShopOrders(orders));
    });
    allUnSubs.push(unSub);
};
export const shopRefundOrderListener = async () => {
    const q = query(collection(getFirestore(), shopRefundOrders), orderBy("originalOrder.orderNo", "asc"));
    const unSub = onSnapshot(q, async (sss) => {
        let orders: RefundShopOrder[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            orders.push({ id, ...data } as RefundShopOrder);
        });
        store.dispatch(setRefundShopOrders(orders));
    });
    allUnSubs.push(unSub);
};

// functions for coupons
export const saveCoupon = async (coupon: Coupon) => {
    const db = getFirestore();
    const { id, ...data } = coupon;
    if (id === "new") {
        await setDoc(doc(db, `coupons`, data.code), data, { merge: true });
    } else {
        await setDoc(doc(db, `coupons/${id}`), data, { merge: true });
    }
    return "success";
};

export const deleteCoupon = async (id: string) => {
    await deleteDoc(doc(getFirestore(), `coupons/${id}`));
    return;
};

export const couponsListener = async () => {
    const q = query(collection(getFirestore(), `coupons`));
    const unSub = onSnapshot(q, async (sss) => {
        let coupons: Coupon[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            coupons.push({ id, ...data } as Coupon);
        });
        store.dispatch(setAllCoupons(coupons));
        store.dispatch(setCouponsIsInit(true));
    });
    allUnSubs.push(unSub);
};

export const getCouponByCode = async (couponCode: string) => {
    const q = query(collection(getFirestore(), `coupons`), where("code", "==", couponCode), limit(1));
    const sss = await getDocs(q);
    let coupon: Coupon | null = null;
    if (sss.empty) {
        return null;
    }
    sss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        coupon = { id, ...data } as Coupon;
    });
    return coupon;
};

// cafe funcs
export const saveCafeCat = async (cat: CafeCat) => {
    const db = getFirestore();
    const { id, ...data } = cat;
    if (id === "new") {
        await addDoc(collection(db, `cafeCats`), data);
    } else {
        await setDoc(doc(db, `cafeCats/${id}`), data, { merge: true });
    }
    return "success";
};
export const deleteCafeCat = async (id: string) => {
    await deleteDoc(doc(getFirestore(), `cafeCats/${id}`));
    return;
};
export const saveCafeCatsOrder = async (cats: CafeCat[]) => {
    const db = getFirestore();
    const batch = writeBatch(db);
    cats.forEach((c) => {
        const { id, index } = c;
        batch.set(doc(db, `cafeCats/${id}`), { index: index }, { merge: true });
    });
    await batch.commit();
    return;
};
export const cafeListener = async () => {
    const q = query(collection(getFirestore(), `cafeCats`));
    const itemsQ = query(collection(getFirestore(), `cafeItems`));
    const orderQ = query(
        collection(getFirestore(), `cafeOrders`),
        where("createdAt", ">=", Timestamp.fromDate(dayjs.tz(dayjs()).subtract(3, "hours").toDate()))
    );
    const unSub = onSnapshot(q, async (sss) => {
        let cats: CafeCat[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            cats.push({ id, ...data } as CafeCat);
        });
        store.dispatch(setAllCafeCats(cats));
        store.dispatch(setCafeIsInit(true));
    });
    const unSub2 = onSnapshot(itemsQ, async (sss) => {
        let items: CafeItem[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            items.push({ id, ...data } as CafeItem);
        });
        store.dispatch(setAllCafeItems(items));
        store.dispatch(setCafeIsInit(true));
    });
    const unSub3 = onSnapshot(orderQ, async (sss) => {
        let orders: CafeOrder[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            orders.push({ id, ...data } as CafeOrder);
        });
        store.dispatch(setCafeOrderQueue(orders));
        store.dispatch(setCafeIsInit(true));
    });
    allUnSubs.push(unSub);
    allUnSubs.push(unSub2);
    allUnSubs.push(unSub3);
};
export const createNewCafeItem = async (newItem: CafeItem) => {
    const { id, ...data } = newItem;
    await addDoc(collection(getFirestore(), `cafeItems`), data);
    return "success";
};

// upload img
export const uploadCafeItemImg = async (file: any, itemId: string) => {
    // console.log(file);

    if (!file.type.includes(`image`)) {
        console.log(`file is not image, return`);
        return;
    }

    const storage = getStorage();
    const storageRef = ref(storage, `${contentImages}/${file.name}`);

    const uploadRes = await uploadBytes(storageRef, file);
    const ImgUrl = await getDownloadURL(uploadRes.ref);

    // update the supply img
    const db = getFirestore();
    const itemRef = doc(db, `cafeItems/${itemId}`);
    await setDoc(itemRef, { images: ImgUrl }, { merge: true });
    return "success";
};

export const updateCafeItem = async (item: CafeItem) => {
    const { id, ...data } = item;
    await setDoc(doc(getFirestore(), `cafeItems/${id}`), data, { merge: true });
    return;
};

export const updateCafeItemStock = async (id: string, outOfStock: boolean) => {
    await setDoc(doc(getFirestore(), `cafeItems/${id}`), { outOfStock: outOfStock }, { merge: true });
    return;
};

export const deleteCafeItem = async (id: string) => {
    await deleteDoc(doc(getFirestore(), `cafeItems/${id}`));
    return;
};

export const saveCafeItemsOrder = async (items: CafeItem[]) => {
    const db = getFirestore();
    const batch = writeBatch(db);
    items.forEach((c) => {
        const { id, index } = c;
        batch.set(doc(db, `cafeItems/${id}`), { index: index }, { merge: true });
    });
    await batch.commit();
    return;
};

export const createCafeOrder = async (order: CafeOrder, orderNumber: string = `000`) => {
    const genCode = async () => {
        const code = randomize(`0`, 3);
        const q = query(collection(getFirestore(), `cafeOrders`), where(`orderNumber`, `==`, String(code)));
        const codeDocs = await getDocs(q);
        if (codeDocs.size === 0) {
            return code;
        }
        let hasDupToday: CafeOrder[] = [];
        codeDocs.forEach(async (ss) => {
            const data = fromDB(ss.data());
            const temp: CafeOrder = {
                id: ss.id,
                ...data,
            } as CafeOrder;
            const sessionDate = dayjs.tz(temp.orderDate).valueOf();
            const todayStart = dayjs.tz(Date.now()).startOf("day").valueOf();
            const todayEnd = dayjs.tz(Date.now()).endOf("day").valueOf();
            if (sessionDate >= todayStart && sessionDate <= todayEnd) {
                hasDupToday.push(temp);
            }
        });
        if (hasDupToday.length === 0) {
            return code;
        }
        await genCode();
    };
    const newCode = orderNumber ? orderNumber : await genCode();

    const orderStaff = store.getState().authUser.authUser;

    let tempOrder: CafeOrder = Object.assign(
        { ...order },
        {
            // update order number
            orderNumber: String(newCode),

            // update createAt to latest
            createdAt: dayjs().toDate(),

            // update total from the items
            total: [...order.items].reduce((p, v) => p + v.price, 0),

            status: "preparing",

            orderStaff,
        }
    );

    const { id, ...data } = tempOrder;

    // add Doc
    await addDoc(collection(getFirestore(), `cafeOrders`), data);

    // return
    return { newCode };
};

export const updateCafeOrderStatus = async (orderId: string, status: CafeOrderStatus) => {
    await setDoc(doc(getFirestore(), `cafeOrders/${orderId}`), { status }, { merge: true });
    return;
};

// update customer Partner status
export const updateCustomerPartnerFlag = async (userId: string, newStatus: boolean) => {
    await setDoc(
        doc(getFirestore(), `customers/${userId}`),
        {
            isPartner: newStatus,
        },
        { merge: true }
    );
    return;
};

// functions for homepage content
export const uploadSlideImage = async (file: any) => {
    if (!file.type.includes(`image`) && !file.type.includes(`video`)) {
        console.log(`file is not image, return`);
        return;
    }

    const storage = getStorage();
    const folder = file.type.includes(`video`) ? `videos` : contentImages;
    const storageRef = ref(storage, `${folder}/${file.name}`);

    const uploadRes = await uploadBytes(storageRef, file);
    const ImgUrl = await getDownloadURL(uploadRes.ref);

    return ImgUrl;
};

export const saveHomepageSlidesOrder = async (slides: HomePageSlide[]) => {
    const db = getFirestore();
    const batch = writeBatch(db);
    slides.forEach((c) => {
        const { id, index } = c;
        batch.set(doc(db, `homepageSlides/${id}`), { index: index }, { merge: true });
    });
    await batch.commit();
    return;
};

export const deleteHomepageSlide = async (id: string) => {
    await deleteDoc(doc(getFirestore(), `homepageSlides/${id}`));
    return;
};

export const deleteMediaByUrl = async (url: string) => {
    const decodedUrl = decodeURIComponent(url);
    console.log("decodedUrl", decodedUrl);
    const trimmedUrl = decodedUrl.split("?")[0];
    console.log("trimmedUrl", trimmedUrl);
    const splittedUrl = trimmedUrl.split("/");
    const folder = splittedUrl[splittedUrl.length - 2];
    const fileName = splittedUrl[splittedUrl.length - 1];
    console.log("folder is: ", folder);
    console.log("file name is: ", fileName);
    const storage = getStorage();
    const fileRef = ref(storage, `${folder}/${fileName}`);
    await deleteObject(fileRef);
    return;
};

export const updateHomepageSlide = async (slide: HomePageSlide) => {
    const { id, ...data } = slide;
    if (id === "new") {
        await addDoc(collection(getFirestore(), `homepageSlides`), data);
        return;
    } else {
        await setDoc(doc(getFirestore(), `homepageSlides/${id}`), data, { merge: true });
        return;
    }
};

export const homepageSlidesListener = async () => {
    const qq = query(collection(getFirestore(), `homepageSlides`));
    const unSub = onSnapshot(qq, async (sss) => {
        let slides: HomePageSlide[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            slides.push({ id, ...data } as HomePageSlide);
        });
        store.dispatch(setHomepageSlides(slides));
    });
    allUnSubs.push(unSub);
};

// Group Class functions
export const setGroupClassPreset = async (preset: GroupClassTypes.GroupClassPreset) => {
    const { id, ...payload } = preset;
    if (id === "new") {
        await addDoc(collection(getFirestore(), `groupClassPresets`), payload);
        return;
    } else {
        await setDoc(doc(getFirestore(), `groupClassPresets/${id}`), payload, { merge: true });
        return;
    }
};

export const deleteGroupClassPreset = async (id: string) => {
    await deleteDoc(doc(getFirestore(), `groupClassPresets/${id}`));
    return;
};

export const groupClassPresetsListener = async () => {
    const q = query(collection(getFirestore(), `groupClassPresets`));

    const unSub = onSnapshot(q, async (sss) => {
        const groupClassPresets: GroupClassTypes.GroupClassPreset[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            groupClassPresets.push({ id, ...data } as GroupClassTypes.GroupClassPreset);
        });
        store.dispatch(setGroupClassPresets(groupClassPresets));
    });
    allUnSubs.push(unSub);
};

export const setGroupClassSlot = async (item: GroupClassTypes.GroupClassSlot) => {
    const { id, ...payload } = item;
    if (id === "new") {
        await addDoc(collection(getFirestore(), `groupClassSlots`), payload);
        return;
    } else {
        await setDoc(doc(getFirestore(), `groupClassSlots/${id}`), payload, { merge: true });
        return;
    }
};

export const getGroupClassSlotsByDate = async (date: Date) => {
    const dateStart = dayjs.tz(date).startOf("day").toDate();
    const dateEnd = dayjs.tz(date).endOf("day").toDate();
    const q = query(
        collection(getFirestore(), `groupClassSlots`),
        where("date", ">=", Timestamp.fromDate(dateStart)),
        where("date", "<=", Timestamp.fromDate(dateEnd))
    );
    const sss = await getDocs(q);
    const slots: GroupClassTypes.GroupClassSlot[] = [];
    sss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        slots.push({ id, ...data } as GroupClassTypes.GroupClassSlot);
    });
    return slots;
};

export const getRGroupClassSlotsByDay = async (day: number) => {
    // console.log(`getRGroupClassSlotsByDay, day = `, day);
    const q = query(collection(getFirestore(), `groupClassSlots`), where("dayOfWeek.index", "==", day));
    const sss = await getDocs(q);
    const slots: GroupClassTypes.GroupClassSlot[] = [];
    sss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        slots.push({ id, ...data } as GroupClassTypes.GroupClassSlot);
    });
    return slots;
};

export const getGroupClassSlotsSinceToday = async () => {
    const q = query(collection(getFirestore(), `groupClassSlots`), where("date", ">=", Timestamp.fromDate(new Date())));
    const sss = await getDocs(q);
    const slots: GroupClassTypes.GroupClassSlot[] = [];
    sss.forEach((ss) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        slots.push({ id, ...data } as GroupClassTypes.GroupClassSlot);
    });
    return slots;
};

export const getGroupClassSlotsByRange = async (from: Date, to: Date) => {
    const q = query(
        collection(getFirestore(), `groupClassSlots`),
        where("date", ">=", Timestamp.fromDate(from)),
        where("date", "<=", Timestamp.fromDate(to))
    );
    const rq = query(collection(getFirestore(), `groupClassSlots`), where("isRecurring", "==", true));

    const sss = await getDocs(q);
    const rsss = await getDocs(rq);

    const slots: GroupClassTypes.GroupClassSlot[] = [];

    const addSlot = (ss: QueryDocumentSnapshot<DocumentData>) => {
        const id = ss.id;
        const data = fromDB(ss.data());
        slots.push({ id, ...data } as GroupClassTypes.GroupClassSlot);
    };

    sss.forEach((ss) => addSlot(ss));
    rsss.forEach((ss) => addSlot(ss));

    return slots;
};

export const deleteGroupClassSlot = async (id: string) => {
    await deleteDoc(doc(getFirestore(), `groupClassSlots/${id}`));
    return;
};

// get customer by id
export const getCustomerById = async (id: string) => {
    const ss = await getDoc(doc(getFirestore(), `customers/${id}`));
    if (!ss.exists()) {
        return undefined;
    }
    const data = fromDB(ss.data());
    return { id, ...data } as Customer;
};

//Pricing
export const pricingListener = async () => {
    const q = query(collection(getFirestore(), `pricing`), limit(1));
    const unSub = onSnapshot(q, async (sss) => {
        let pricing: Price[] = [];
        sss.forEach((ss) => {
            const data = fromDB(ss.data());
            pricing.push({ ...data } as Price);
        });
        if (pricing.length !== 1) {
            return;
        }
        store.dispatch(setPricing(pricing[0]));
        store.dispatch(setPricingIsInit(true));
    });
    allUnSubs.push(unSub);
};

export const updatePricing = async (prices: Price) => {
    const q = query(collection(getFirestore(), `pricing`), limit(1));
    let id: string = "";
    const sss = await getDocs(q);
    sss.forEach((ss) => {
        id = ss.id;
    });

    await setDoc(doc(getFirestore(), `pricing/${id}`), prices, { merge: true });
    return;
};

// func for page content
export const pageContentsListener = async () => {
    const q = query(collection(getFirestore(), `pageContents`));
    const unSub = onSnapshot(q, async (sss) => {
        const pageContents: PageContent[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            pageContents.push({ id, ...data } as PageContent);
        });
        store.dispatch(setAllPageContents(pageContents));
    });
    allUnSubs.push(unSub);
};

export const saveAllPageContents = async (payload: PageContent[]) => {
    let errorPage: string[] = [];

    for (const p of payload) {
        const { id, ...data } = p;
        try {
            await setDoc(doc(getFirestore(), `pageContents/${id}`), data);
            console.log("saved 1");
        } catch (error) {
            errorPage.push(data.title.en);
            console.log(error);
        }
    }

    return errorPage;
};

// functions for media library
export const listAllMediaFiles = async () => {
    const storage = getStorage();
    const imgRef = ref(storage, contentImages);
    const vidRef = ref(storage, `videos`);
    const fileRef = ref(storage, `files`);
    const imgRes = await listAll(imgRef);
    const vidRes = await listAll(vidRef);
    const fileRes = await listAll(fileRef);
    const imgList: MediaListItem[] = [];
    const vidList: MediaListItem[] = [];
    const fileList: MediaListItem[] = [];
    const makeList = (items: StorageReference[], list: MediaListItem[], folder = contentImages) => {
        items.forEach((x) => {
            list.push({
                name: x.name,
                folder,
                path: x.fullPath,
            } as MediaListItem);
        });
    };
    makeList(imgRes.items, imgList);
    makeList(vidRes.items, vidList, `videos`);
    makeList(fileRes.items, fileList, `files`);

    return [...imgList, ...vidList, ...fileList] as MediaListItem[];
};

export const getFileByPath = async (path: string) => {
    const storage = getStorage();
    const fileRef = ref(storage, path);
    const url = await getDownloadURL(fileRef);
    return url;
};

// func for other contents
export const otherContentsListener = async () => {
    const q = query(collection(getFirestore(), `otherContents`));
    const unSub = onSnapshot(q, async (sss) => {
        const contents: OtherContent[] = [];
        sss.forEach((ss) => {
            const id = ss.id;
            const data = fromDB(ss.data());
            contents.push({ id, ...data } as OtherContent);
        });
        store.dispatch(setAllOtherContents(contents));
    });
    allUnSubs.push(unSub);
};

export const saveAllOtherContents = async (payload: OtherContent[]) => {
    payload.forEach(async (p) => {
        const { id, ...data } = p;
        await setDoc(doc(getFirestore(), `otherContents/${id}`), data);
    });
    return;
};

// function to add gears on existing session
export const sessionAddMoreGears = async (selectedGears: BookingGear[], session: Session, paymentMethod: PaymentMethod, total: number) => {
    // const sessionMouthCounters: MonthCounters[] = selectMonthCounters(store.getState());
    // const counterId = sessionMouthCounters.find((x) => dayjs.tz(session.sessionDate).startOf("month").isSame(dayjs.tz(x.month).startOf("month")))?.id;
    // if (!counterId) {
    //     return {
    //         message: "Failed to start your booking, please try again later.",
    //     };
    // }
    if (isNaN(total)) {
        return {
            message: "Failed to add more gear, the total amount is invalid, please try again.",
        };
    }
    // const counterRef = doc(getFirestore(), `counters/${counterId}`);
    const sessionRef = doc(getFirestore(), `sessions/${session.id}`);
    const newGearList = [...session.gears].concat(selectedGears);

    const addGearDocId = uuidv4();
    const addGearDocRef = doc(getFirestore(), `addGearRecords/${addGearDocId}`);
    const addGearDocData = {
        gears: [...selectedGears],
        session: session,
        paymentMethod: paymentMethod,
        date: dayjs.tz(dayjs()).toDate(),
    };

    const bookingUserRef = doc(getFirestore(), `customers/${session.bookingUser?.id}`);

    const counter = await getCounterByDateV2(session.sessionDate);

    // use transaction to create the session
    try {
        await runTransaction(getFirestore(), async (txn) => {
            // const counterSs = await txn.get(counterRef);
            // if (!counterSs.exists()) {
            //     throw new Error(`Something's not right... Please try again later.`);
            // }
            // const data = fromDB(counterSs.data());
            // const counter: MonthCounters = {
            //     id: counterSs.id,
            //     ...data,
            // } as MonthCounters;
            // let counterSessions = [...counter.sessions];
            // const idx = counterSessions.findIndex((x) => x.id === session.id);
            // if (idx >= 0) {
            //     Object.assign(counterSessions[idx], { gears: [...counterSessions[idx].gears].concat(selectedGears) });
            // }

            const userDoc = await txn.get(bookingUserRef);
            const user = { id: userDoc.id, ...fromDB(userDoc.data()) } as Customer;
            const wallet = user.wallet;

            // check gears still available?
            const finalCheck = checkGearOnly(session, counter, selectedGears);
            if (paymentMethod === "credit") {
                if (wallet.balance < total) {
                    throw new Error(`The customer's wallet does not have enough balance to book these gears.`);
                }
            }
            if (finalCheck.result) {
                // update existing session
                txn.update(sessionRef, { gears: newGearList });

                // create addGearsDoc
                txn.set(addGearDocRef, addGearDocData);
                if (paymentMethod === "credit") {
                    txn.update(bookingUserRef, {
                        wallet: {
                            balance: wallet.balance - total,
                            spent: wallet.spent,
                        },
                    });
                }
                // txn.update(counterRef, { sessions: counterSessions });
            } else {
                throw new Error(`${finalCheck.message}`);
            }
        });
    } catch (error: any) {
        return {
            message: error.message,
        };
    }

    return {
        message: "success",
    };
};

export const updateSessionCoach = async (coach: Coach, session: Session) => {
    // const sessionMouthCounters: MonthCounters[] = selectMonthCounters(store.getState());
    // const counterId = sessionMouthCounters.find((x) => dayjs.tz(session.sessionDate).startOf("month").isSame(dayjs.tz(x.month).startOf("month")))?.id;
    // if (!counterId) {
    //     return "failed";
    // }
    const sessionRef = doc(getFirestore(), `sessions/${session.id}`);
    // const counterRef = doc(getFirestore(), `counters/${counterId}`);

    try {
        await runTransaction(getFirestore(), async (txn) => {
            // const counterSs = await txn.get(counterRef);
            // if (!counterSs.exists()) {
            //     throw new Error(`Something's not right... Please try again later.`);
            // }
            // const data = fromDB(counterSs.data());
            // const counter: MonthCounters = {
            //     id: counterSs.id,
            //     ...data,
            // } as MonthCounters;
            // let counterSessions = [...counter.sessions];
            // const idx = counterSessions.findIndex((x) => x.id === session.id);
            // if (idx >= 0) {
            //     Object.assign(counterSessions[idx], {
            //         privateCoach: coach,
            //     });
            // }

            txn.set(
                sessionRef,
                {
                    privateCoach: coach,
                },
                { merge: true }
            );
            // txn.update(counterRef, { sessions: counterSessions });
        });
    } catch (error: any) {
        return {
            message: error.message,
        };
    }
    return "success";
};

export const removeSessionCoach = async (session: Session) => {
    // const sessionMouthCounters: MonthCounters[] = selectMonthCounters(store.getState());
    // const counterId = sessionMouthCounters.find((x) => dayjs.tz(session.sessionDate).startOf("month").isSame(dayjs.tz(x.month).startOf("month")))?.id;
    // if (!counterId) {
    //     return "failed";
    // }
    const sessionRef = doc(getFirestore(), `sessions/${session.id}`);
    // const counterRef = doc(getFirestore(), `counters/${counterId}`);

    try {
        await runTransaction(getFirestore(), async (txn) => {
            // const counterSs = await txn.get(counterRef);
            // if (!counterSs.exists()) {
            //     throw new Error(`Something's not right... Please try again later.`);
            // }
            // const data = fromDB(counterSs.data());
            // const counter: MonthCounters = {
            //     id: counterSs.id,
            //     ...data,
            // } as MonthCounters;
            // let counterSessions = [...counter.sessions];
            // const idx = counterSessions.findIndex((x) => x.id === session.id);
            // if (idx >= 0) {
            //     delete counterSessions[idx].privateCoach
            // }

            txn.set(
                sessionRef,
                {
                    privateCoach: deleteField(),
                },
                { merge: true }
            );
            // txn.update(counterRef, { sessions: counterSessions });
        });
    } catch (error: any) {
        return {
            message: error.message,
        };
    }
    return "success";
};

export const editCustomerBalance = async (user: Customer, newBalance: number) => {
    await setDoc(
        doc(getFirestore(), `customers/${user.id}`),
        {
            wallet: {
                balance: newBalance,
                spent: increment(0),
            },
        },
        { merge: true }
    );
    return;
};

export const updateCustomerInfo = async (customer: Customer) => {
    await setDoc(
        doc(getFirestore(), `customers/${customer.id}`),
        {
            realName: customer.realName,
            phone: customer.phone,
        },
        { merge: true }
    );
    return;
};

export const disableUser = async (id: string) => {
    store.dispatch(setIsLoading(true));
    const db = getFirestore();
    try {
        await updateDoc(doc(db, `customers/${id}`), { enabled: false });
        return "success";
    } catch (error: any) {
        console.error(error);
        return error.code as string;
    } finally {
        store.dispatch(setIsLoading(false));
    }
};

export const GlobalListener = async () => {
    sessionsListener();
    staffListener();
    coachListener();
    capListener();
    // monthCountersListener();
    gearListener();
    topUpPackagesListener();
    faqListener();
    CustomerClassesListener();
    shopItemListener();
    couponsListener();
    cafeListener();
    shopsListener();
    shopOrderListener();
    shopRefundOrderListener();
    homepageSlidesListener();
    groupClassPresetsListener();
    pricingListener();
    pageContentsListener();
    otherContentsListener();
};

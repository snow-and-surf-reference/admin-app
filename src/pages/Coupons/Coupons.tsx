import {
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonPage,
    IonSegment,
    IonSegmentButton
} from "@ionic/react";
import * as SessionTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import { deleteCoupon } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAllCoupons, selectCouponsIsInit } from "app/slices/sessionsSlice";
import Header from "components/Global/Header";
import LoadingCard from "components/Global/LoadingCard";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { addOutline, createOutline, trashOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import CouponModal from "./CouponModal";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault("Asia/Hong_Kong");

const newCoupon: SessionTypes.Coupon = {
    id: "new",
    code: "",
    canReuse: true,
    oncePerUser: true,
    qty: 0,
    discount: 10,
    unit: "perc",
    isForever: false,
    validUntil: dayjs.tz(dayjs()).toDate(),
};

const Coupons: React.FC = () => {
    const [selectedCoupon, setSelectedCoupon] = useState<SessionTypes.Coupon | null>(null);
    const [deleteCouponId, setDeleteCouponId] = useState<string | null>(null);
    const [deleting, setDeleting] = useState("idle");
    const [segment, setSegment] = useState("valid");
    const coupons: SessionTypes.Coupon[] = useAppSelector(selectAllCoupons);
    const isInit: boolean = useAppSelector(selectCouponsIsInit);

    // effect to filter valid/invalid coupons
    useEffect(() => {}, [segment, coupons]);

    const handleDelete = async () => {
        console.log(deleteCouponId);
        if (!deleteCouponId) {
            return;
        }
        setDeleting("deleting");
        await deleteCoupon(deleteCouponId);
        setDeleting("deleted");
    };

    return (
        <IonPage>
            <Header title="Coupons Management" />
            <IonContent>
                <IonCard color={"light"}>
                    <IonCardContent>
                        <IonItem lines="none" color={"light"}>
                            <IonLabel>
                                <h3>Existing Coupons</h3>
                            </IonLabel>
                            <IonButton slot="end" shape="round" size="default" color={"danger"} onClick={() => setSelectedCoupon({ ...newCoupon })}>
                                <IonIcon icon={addOutline} slot="start" />
                                <IonLabel>New Coupon</IonLabel>
                            </IonButton>
                        </IonItem>
                        <br />
                        <IonSegment
                            value={segment}
                            onIonChange={(e) => {
                                setSegment(e.detail.value!);
                            }}
                        >
                            <IonSegmentButton value="valid">Valid</IonSegmentButton>
                            <IonSegmentButton value="invalid">Expired / Invalid</IonSegmentButton>
                        </IonSegment>
                    </IonCardContent>
                </IonCard>
                {isInit ? (
                    <IonCard color={"light"}>
                        <IonList inset>
                            {coupons.filter((cs) => {
                                            const isDateValid = cs.isForever ? true : cs.validUntil.valueOf() >= Date.now();
                                            const hasStock = cs.canReuse ? true : cs.qty > 0;

                                            if (segment === "valid") {
                                                return isDateValid && hasStock;
                                            }
                                            return !isDateValid || !hasStock;
                                        }).length > 0 ? (
                                <>
                                    {coupons
                                        .filter((cs) => {
                                            const isDateValid = cs.isForever ? true : cs.validUntil.valueOf() >= Date.now();
                                            const hasStock = cs.canReuse ? true : cs.qty > 0;

                                            if (segment === "valid") {
                                                return isDateValid && hasStock;
                                            }
                                            return !isDateValid || !hasStock;
                                        })
                                        .map((x) => (
                                            <IonItem key={`coupon-${x.id}`}>
                                                <IonLabel>
                                                    <h3>{x.code}</h3>
                                                    <p>Discount: {`${x.discount}${x.unit === "perc" ? "%" : " Credits"}`}</p>
                                                    <p>
                                                        Valid Until: {`${x.isForever ? "Forever" : dayjs.tz(x.validUntil).format(`DD MMM YYYY`)}`}{" "}
                                                        {!x.isForever && x.validUntil.valueOf() < Date.now() ? `(Expired)` : ``}
                                                    </p>
                                                    <p>QTY: {`${x.canReuse ? `Infinite` : `${x.qty > 0 ? x.qty : `Out of Stock`}`}`}</p>
                                                </IonLabel>
                                                <IonButtons slot="end">
                                                    <IonButton
                                                        color={"secondary"}
                                                        fill="solid"
                                                        onClick={() => {
                                                            setSelectedCoupon({ ...x });
                                                        }}
                                                    >
                                                        <IonIcon slot="start" icon={createOutline} size="small" />
                                                        <IonLabel>Edit</IonLabel>
                                                    </IonButton>
                                                    <IonButton
                                                        color={"danger"}
                                                        fill="solid"
                                                        onClick={() => {
                                                            setDeleteCouponId(x.id);
                                                        }}
                                                    >
                                                        <IonIcon slot="start" icon={trashOutline} size="small" />
                                                        <IonLabel>Delete</IonLabel>
                                                    </IonButton>
                                                </IonButtons>
                                            </IonItem>
                                        ))}
                                </>
                            ) : (
                                <IonItem>
                                    <IonLabel color={"danger"}>No {`${segment.charAt(0).toUpperCase() + segment.slice(1)}`} Coupons found.</IonLabel>
                                </IonItem>
                            )}
                        </IonList>
                    </IonCard>
                ) : (
                    <LoadingCard />
                )}
            </IonContent>

            <CouponModal isOpen={!!selectedCoupon} onDismiss={() => setSelectedCoupon(null)} coupon={selectedCoupon} />

            <IonAlert
                isOpen={!!deleteCouponId}
                header={"Delete Coupon"}
                message={`Are you sure? This cannot be undone.`}
                onDidDismiss={() => {
                    setDeleteCouponId(null);
                }}
                buttons={[
                    {
                        text: "Delete",
                        role: "confirm",
                        handler: () => {
                            handleDelete();
                        },
                    },
                    {
                        text: "Cancel",
                        role: "cancel",
                        handler: () => setDeleteCouponId(null),
                    },
                ]}
            />
            <IonLoading isOpen={deleting === "deleting"} message={`Deleting Coupon`} />
            <IonAlert
                isOpen={deleting === "deleted"}
                onDidDismiss={() => {
                    setDeleting("idle");
                    setDeleteCouponId(null);
                }}
                header={`Coupon Deleted`}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            setDeleting("idle");
                            setDeleteCouponId(null);
                        },
                    },
                ]}
            />
        </IonPage>
    );
};

export default Coupons;

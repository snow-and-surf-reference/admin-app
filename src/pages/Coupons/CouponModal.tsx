import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardHeader,
    IonCardSubtitle,
    IonContent,
    IonDatetime,
    IonDatetimeButton,
    IonFooter,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonPopover,
    IonSelect,
    IonSelectOption,
    IonTitle,
    IonToggle,
    IonToolbar,
} from "@ionic/react";
import { checkmarkCircle, closeOutline } from "ionicons/icons";
import * as SessionTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import randomize from "randomatic";
import { useEffect, useState } from "react";
import dayjs from "dayjs";
import { saveCoupon } from "app/firebase";

interface Props {
    isOpen: boolean;
    onDismiss: () => void;
    coupon: SessionTypes.Coupon | null;
}

const CouponModal: React.FC<Props> = (props) => {
    const { isOpen, onDismiss, coupon } = props;
    const [couponClone, setCouponClone] = useState(coupon);
    const [disableSubmit, setDisableSubmit] = useState(true);
    const [saving, setSaving] = useState("idle");

    useEffect(() => {
        if (!coupon) {
            return;
        }
        setCouponClone(coupon);
    }, [coupon]);

    // useEffect(() => console.log(couponClone), [couponClone]);

    useEffect(() => {
        if (couponClone && couponClone.code && couponClone.discount) {
            setDisableSubmit(false);
        } else {
            setDisableSubmit(true);
        }
    }, [couponClone]);

    const handleSubmit = async () => {
        if (!couponClone) {
            return;
        }
        setSaving("saving");
        await saveCoupon(couponClone);
        setSaving("saved");
    };

    return (
        <IonModal isOpen={isOpen} onDidDismiss={() => onDismiss()}>
            {couponClone ? (
                <>
                    <IonHeader>
                        <IonToolbar>
                            <IonButton slot="start" fill="clear" color={"meduim"} onClick={() => onDismiss()}>
                                <IonIcon icon={closeOutline} slot="start" />
                                <IonLabel>Cancel</IonLabel>
                            </IonButton>
                            <IonTitle>{couponClone.id === "new" ? `New Coupon` : `Edit Coupon`}</IonTitle>
                        </IonToolbar>
                    </IonHeader>
                    <IonContent>
                        <IonCard color={"light"}>
                            <IonList inset>
                                <IonItem>
                                    <IonLabel position="stacked" color={"medium"}>
                                        Coupon Code
                                    </IonLabel>
                                    <IonInput
                                        type="text"
                                        placeholder="e.g. NEWYEAR23"
                                        clearInput
                                        value={couponClone.code}
                                        onIonChange={(e) => {
                                            setCouponClone((cc) =>
                                                cc
                                                    ? Object.assign(
                                                          { ...cc },
                                                          {
                                                              code: e.detail.value!,
                                                          }
                                                      )
                                                    : cc
                                            );
                                        }}
                                    />
                                    <IonButton
                                        slot="end"
                                        onClick={() => {
                                            const newCode = `${randomize(`A0`, 8)}`;
                                            setCouponClone((cc) =>
                                                cc
                                                    ? Object.assign(
                                                          { ...cc },
                                                          {
                                                              code: newCode,
                                                          }
                                                      )
                                                    : cc
                                            );
                                        }}
                                    >
                                        Auto Gen
                                    </IonButton>
                                </IonItem>

                                <IonItem>
                                    <IonLabel position="stacked" color={"medium"}>
                                        Discount Amount
                                    </IonLabel>
                                    <IonInput
                                        type="number"
                                        placeholder="e.g. 10"
                                        clearInput
                                        value={couponClone.discount}
                                        onIonChange={(e) => {
                                            setCouponClone((cc) =>
                                                cc
                                                    ? Object.assign(
                                                          { ...cc },
                                                          {
                                                              discount: Number(e.detail.value!),
                                                          }
                                                      )
                                                    : cc
                                            );
                                        }}
                                    />
                                </IonItem>

                                <IonItem>
                                    <IonLabel color={"medium"}>Discount Unit</IonLabel>
                                    <IonSelect
                                        className="ion-text-end"
                                        value={couponClone.unit}
                                        onIonChange={(e) => {
                                            setCouponClone((cc) =>
                                                cc
                                                    ? Object.assign(
                                                          { ...cc },
                                                          {
                                                              unit: e.detail.value!,
                                                          }
                                                      )
                                                    : cc
                                            );
                                        }}
                                    >
                                        <IonSelectOption value={`perc`}>{`Percentage (%)`}</IonSelectOption>
                                        <IonSelectOption value={`credit`}>{`Credits`}</IonSelectOption>
                                    </IonSelect>
                                </IonItem>
                            </IonList>
                        </IonCard>

                        <IonCard color={`light`}>
                            <IonList inset>
                                <IonItem>
                                    <IonLabel>Forever Valid?</IonLabel>
                                    <IonToggle
                                        checked={couponClone.isForever}
                                        onIonChange={(e) => {
                                            setCouponClone((cc) =>
                                                cc
                                                    ? Object.assign(
                                                          { ...cc },
                                                          {
                                                              isForever: e.detail.checked,
                                                          }
                                                      )
                                                    : cc
                                            );
                                        }}
                                    />
                                </IonItem>
                                {couponClone.isForever ? null : (
                                    <IonItem>
                                        <IonLabel>Valid Until</IonLabel>
                                        <IonDatetimeButton datetime="validDatePicker"></IonDatetimeButton>
                                        <IonPopover keepContentsMounted>
                                            <IonDatetime
                                                id="validDatePicker"
                                                presentation="date"
                                                value={dayjs.tz(couponClone.validUntil).format(`YYYY-MM-DD`)}
                                                min={dayjs.tz(Date.now()).subtract(3, 'months').format()}
                                                max={dayjs.tz(Date.now()).add(3, 'years').format()}
                                                onIonChange={(e) => {
                                                    const newValue = dayjs(String(e.detail.value!)).format(`YYYY-MM-DD`);
                                                    const newDate = dayjs.tz(`${newValue}T00:00:00`).toDate();
                                                    setCouponClone((cc) =>
                                                        cc
                                                            ? Object.assign(
                                                                  { ...cc },
                                                                  {
                                                                      validUntil: newDate,
                                                                  }
                                                              )
                                                            : cc
                                                    );
                                                }}
                                            ></IonDatetime>
                                        </IonPopover>
                                    </IonItem>
                                )}
                            </IonList>
                        </IonCard>

                        <IonCard color={"light"}>
                            <IonCardHeader>
                                <IonCardSubtitle>Usage</IonCardSubtitle>
                            </IonCardHeader>
                            <IonList inset>
                                <IonItem>
                                    <IonLabel>Reusable Code?</IonLabel>
                                    <IonToggle
                                        slot="end"
                                        checked={couponClone.canReuse}
                                        onIonChange={(e) => {
                                            setCouponClone((cc) =>
                                                cc
                                                    ? Object.assign(
                                                          { ...cc },
                                                          {
                                                              canReuse: e.detail.checked,
                                                              qty: 0,
                                                          }
                                                      )
                                                    : cc
                                            );
                                        }}
                                    />
                                </IonItem>
                                {!couponClone.canReuse && (
                                    <>
                                        <IonItem>
                                            <IonLabel position="stacked" color={"medium"}>
                                                QTY
                                            </IonLabel>
                                            <IonInput
                                                type="number"
                                                placeholder="e.g. 10"
                                                value={couponClone.qty}
                                                onIonChange={(e) => {
                                                    setCouponClone((cc) =>
                                                        cc
                                                            ? Object.assign(
                                                                  { ...cc },
                                                                  {
                                                                      qty: Number(e.detail.value!),
                                                                  }
                                                              )
                                                            : cc
                                                    );
                                                }}
                                            />
                                        </IonItem>
                                    </>
                                )}
                                <IonItem>
                                    <IonLabel>Use Once Per Customer</IonLabel>
                                    <IonToggle
                                        slot="end"
                                        checked={couponClone.oncePerUser}
                                        onIonChange={(e) => {
                                            setCouponClone((cc) =>
                                                cc
                                                    ? Object.assign(
                                                          { ...cc },
                                                          {
                                                              oncePerUser: e.detail.checked,
                                                          }
                                                      )
                                                    : cc
                                            );
                                        }}
                                    />
                                </IonItem>
                            </IonList>
                        </IonCard>
                    </IonContent>
                    <IonFooter>
                        <IonToolbar color={disableSubmit ? "light" : "warning"} className="ion-text-center">
                            <IonButton
                                color={disableSubmit ? "light" : "warning"}
                                expand="block"
                                disabled={disableSubmit}
                                onClick={() => {
                                    handleSubmit();
                                }}
                            >
                                <IonIcon icon={checkmarkCircle} slot="start" />
                                <IonLabel>Save Changes</IonLabel>
                            </IonButton>
                        </IonToolbar>
                    </IonFooter>
                    <IonLoading isOpen={saving === "saving"} message={`Saving changes`} />
                    <IonAlert
                        isOpen={saving === "saved"}
                        onDidDismiss={() => {
                            setSaving("idle");
                            onDismiss();
                        }}
                        header={`Coupon Saved`}
                        buttons={[
                            {
                                text: "OK",
                                handler: () => {
                                    setSaving("idle");
                                    onDismiss();
                                },
                            },
                        ]}
                    />
                </>
            ) : null}
        </IonModal>
    );
};

export default CouponModal;

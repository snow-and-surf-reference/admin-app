import { IonAlert, IonButton, IonCard, IonContent, IonFooter, IonInput, IonItem, IonLabel, IonLoading, IonPage, IonToolbar } from "@ionic/react";
import { updatePricing } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectPricing, selectPricingIsInit } from "app/slices/pricingSlice";
import Header from "components/Global/Header";
import LoadingCard from "components/Global/LoadingCard";
import { useEffect, useState } from "react";

export type PriceUnit = "pax" | "hour" | "session" | "day" | "storage" | "item";

export interface UnitPrice {
    units: PriceUnit[];
    price: number;
}

export interface Price {
    enterance: UnitPrice;
    group_class: UnitPrice;
    personal_training: UnitPrice;
    default_snow_belt: UnitPrice;
    default_surf_lane: UnitPrice;
    default_snow_venue: UnitPrice;
    default_surf_venue: UnitPrice;
    default_full_park: UnitPrice;
    storage: UnitPrice;
    gear_standard: UnitPrice;
    gear_premium: UnitPrice;
}

const Pricing: React.FC = () => {
    const pricesRaw = useAppSelector(selectPricing);
    const isInit = useAppSelector(selectPricingIsInit);
    const [prices, setPrices] = useState<Price | null>(null);
    const [disable, setDisable] = useState(true);
    const [saving, setSaving] = useState("idle");

    useEffect(() => {
        if (!pricesRaw) {
            return;
        }
        setPrices(pricesRaw);
    }, [pricesRaw]);

    useEffect(() => {
        let canSubmit = true;
        Object.entries({ ...prices }).forEach((x) => {
            const price = x[1];
            if (price && price.price <= 0) {
                canSubmit = false;
            }
        });
        setDisable(!canSubmit);
    }, [prices]);

    const handleSubmit = async () => {
        if (!prices) {
            return;
        }
        setSaving("saving");
        await updatePricing(prices);
        setSaving("saved");
    };

    return (
        <IonPage>
            <Header title="Pricing Management" />
            <IonContent color={"light"}>
                {!isInit || !prices ? (
                    <LoadingCard />
                ) : (
                    <>
                        {Object.entries({ ...prices })
                            .sort((a, b) => a[0].localeCompare(b[0]))
                            .map((x) => {
                                const key = x[0];
                                const price = x[1];
                                return (
                                    <IonCard key={`${key}`}>
                                        <IonItem lines="none">
                                            <IonLabel
                                                style={{
                                                    textTransform: "capitalize",
                                                }}
                                                position="stacked"
                                            >
                                                <h3>{key.replaceAll(`_`, ` `)} (HKD)</h3>
                                                <p>Per {price.units.join(` | `)}</p>
                                            </IonLabel>
                                            <IonInput
                                                type="number"
                                                value={price.price}
                                                onIonChange={(e) => {
                                                    setPrices((p) =>
                                                        p
                                                            ? Object.assign(
                                                                  { ...p },
                                                                  {
                                                                      [`${key}`]: Object.assign(
                                                                          { ...price },
                                                                          {
                                                                              price: Number(e.detail.value),
                                                                          }
                                                                      ),
                                                                  }
                                                              )
                                                            : p
                                                    );
                                                }}
                                            />
                                        </IonItem>
                                    </IonCard>
                                );
                            })}
                    </>
                )}
            </IonContent>
            <IonFooter>
                <IonToolbar color={disable ? "light" : "warning"}>
                    <IonButton expand="block" disabled={disable} color={disable ? "light" : "warning"} onClick={() => handleSubmit()}>
                        <IonLabel>Save Change</IonLabel>
                    </IonButton>
                </IonToolbar>
            </IonFooter>
            <IonLoading isOpen={saving === "saving"} message={`Saving Changes`} spinner={"dots"} />
            <IonAlert
                isOpen={saving === "saved"}
                header={`All Prices are updated`}
                onDidDismiss={() => setSaving("idle")}
                buttons={[
                    {
                        text: "OK",
                        handler: () => setSaving("idle"),
                    },
                ]}
            />
        </IonPage>
    );
};

export default Pricing;

import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonCol,
    IonContent,
    IonFooter,
    IonGrid,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonPage,
    IonRow,
    IonToolbar,
} from "@ionic/react";
import { OtherContent } from "@tsanghoilun/snow-n-surf-interface/types/page";
import { saveAllOtherContents } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAllOtherContents } from "app/slices/otherContentsSlice";
import Header from "components/Global/Header";
import LoadingCard from "components/Global/LoadingCard";
import { checkmarkOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

const toolBarOptions = [
    ["bold", "italic", "underline", "strike"], // toggled buttons
    ["blockquote", "code-block", "image"],

    [{ header: 1 }, { header: 2 }], // custom button values
    [{ list: "ordered" }, { list: "bullet" }],
    [{ script: "sub" }, { script: "super" }], // superscript/subscript
    [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
    [{ direction: "rtl" }], // text direction

    [{ size: ["small", false, "large", "huge"] }], // custom dropdown
    [{ header: [1, 2, 3, 4, 5, 6, false] }],

    [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    [{ font: [] }],
    [{ align: [] }],

    ["clean"], // remove formatting button
];

const OtherContents: React.FC = () => {
    const contentsRaw = useAppSelector(selectAllOtherContents);
    const [otherContents, setOtherContents] = useState<OtherContent[]>(contentsRaw);
    const [saving, setSaving] = useState("idle");
    const [disableSubmit, setDisableSubmit] = useState(true);

    useEffect(() => {
        setOtherContents(contentsRaw);
    }, [contentsRaw]);

    // useEffect(() => console.log(otherContents), [otherContents]);

    // effect to check if can save
    useEffect(() => {
        let canSubmit = true;
        [...otherContents].forEach((p) => {
            if (!p.title.en || !p.content.en) {
                canSubmit = false;
            }
        });
        setDisableSubmit(!canSubmit);
    }, [otherContents]);

    const handleSubmit = async () => {
        if (!otherContents) {
            return;
        }
        setSaving("saving");
        await saveAllOtherContents(otherContents);
        setSaving("saved");
    };

    return (
        <IonPage>
            <Header title="Other Contents" />
            <IonContent>
                {!otherContents.length ? (
                    <LoadingCard />
                ) : (
                    otherContents.map((x) => (
                        <IonCard key={`other-content-item-${x.id}`} color={"light"}>
                            <IonCardContent>
                                <IonList inset>
                                    <IonItem>
                                        <IonLabel position="stacked">
                                            <p>Title (English)</p>
                                        </IonLabel>
                                        <IonInput
                                            type="text"
                                            value={x.title.en}
                                            onIonChange={(e) => {
                                                console.log('en title change')
                                                const tempItem = Object.assign(
                                                    { ...x },
                                                    {
                                                        title: Object.assign(
                                                            { ...x.title },
                                                            {
                                                                en: e.detail.value!,
                                                            }
                                                        ),
                                                    }
                                                );
                                                const idx = otherContents.findIndex((p) => p.id === x.id);
                                                let newArray = [...otherContents];
                                                newArray.splice(idx, 1, tempItem);
                                                setOtherContents(newArray);
                                            }}
                                        />
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel position="stacked">
                                            <p>標題 (中文)</p>
                                        </IonLabel>
                                        <IonInput
                                            type="text"
                                            value={x.title.zh}
                                            onIonChange={(e) => {
                                                const tempItem = Object.assign(
                                                    { ...x },
                                                    {
                                                        title: Object.assign(
                                                            { ...x.title },
                                                            {
                                                                zh: e.detail.value!,
                                                            }
                                                        ),
                                                    }
                                                );
                                                const idx = otherContents.findIndex((p) => p.id === x.id);
                                                let newArray = [...otherContents];
                                                newArray.splice(idx, 1, tempItem);
                                                setOtherContents(newArray);
                                            }}
                                        />
                                    </IonItem>
                                </IonList>

                                <IonGrid color="white">
                                    <IonRow>
                                        <IonCol>
                                            <IonLabel>
                                                <h3>{`Content (EN)`}</h3>
                                            </IonLabel>
                                        </IonCol>
                                    </IonRow>
                                    <IonRow color="white">
                                        <IonCol color="white">
                                            <ReactQuill
                                                theme="snow"
                                                className="faq-editor"
                                                modules={{
                                                    toolbar: toolBarOptions,
                                                }}
                                                value={(x.content.en)}
                                                onChange={(value) => {
                                                    const tempItem = Object.assign(
                                                        { ...x },
                                                        {
                                                            content: Object.assign(
                                                                { ...x.content },
                                                                {
                                                                    en: (value),
                                                                }
                                                            ),
                                                        }
                                                    );
                                                    const idx = otherContents.findIndex((p) => p.id === x.id);
                                                    let newArray = [...otherContents];
                                                    newArray.splice(idx, 1, tempItem);
                                                    setOtherContents(newArray);
                                                }}
                                            />
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>

                                <IonGrid color="white">
                                    <IonRow>
                                        <IonCol>
                                            <IonLabel>
                                                <h3>{`內容 (中文)`}</h3>
                                            </IonLabel>
                                        </IonCol>
                                    </IonRow>
                                    <IonRow color="white">
                                        <IonCol color="white">
                                            <ReactQuill
                                                theme="snow"
                                                className="faq-editor"
                                                modules={{
                                                    toolbar: toolBarOptions,
                                                }}
                                                value={x.content.zh}
                                                onChange={(value) => {
                                                    const tempItem = Object.assign(
                                                        { ...x },
                                                        {
                                                            content: Object.assign(
                                                                { ...x.content },
                                                                {
                                                                    zh: value,
                                                                }
                                                            ),
                                                        }
                                                    );
                                                    const idx = otherContents.findIndex((p) => p.id === x.id);
                                                    
                                                    let newArray = [...otherContents];
                                                    newArray.splice(idx, 1, tempItem);
                                                    setOtherContents(newArray);
                                                }}
                                            />
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>
                            </IonCardContent>
                        </IonCard>
                    ))
                )}
            </IonContent>
            <IonFooter>
                <IonToolbar>
                    <IonButton color={disableSubmit ? "medium" : "warning"} disabled={disableSubmit} slot="end" onClick={() => handleSubmit()}>
                        <IonIcon icon={checkmarkOutline} slot="start" />
                        <IonLabel>Save</IonLabel>
                    </IonButton>
                </IonToolbar>
            </IonFooter>
            <IonLoading isOpen={saving === "saving"} message={`Saving Updates`} />
            <IonAlert
                isOpen={saving === "saved"}
                onDidDismiss={() => setSaving("idle")}
                header={`Your Changes is Saved.`}
                buttons={[
                    {
                        text: "OK",
                        role: "confirm",
                        handler: () => {
                            setSaving("idle");
                        },
                    },
                ]}
            />
        </IonPage>
    );
};

export default OtherContents;

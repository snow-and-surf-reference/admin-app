import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonCol,
    IonContent,
    IonFooter,
    IonGrid,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonPage,
    IonRow,
    IonSpinner,
    IonToolbar,
} from "@ionic/react";
import { PageContent } from "@tsanghoilun/snow-n-surf-interface/types/page";
import { deleteMediaByUrl, saveAllPageContents, uploadSlideImage } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAllPageContents } from "app/slices/pageContentsSlice";
import Header from "components/Global/Header";
import LoadingCard from "components/Global/LoadingCard";
import MediaLibraryModal from "components/MediaLibrary/MediaLibraryModal";
import { checkmarkOutline, closeCircleOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import Dropzone from "react-dropzone";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import IMG_PLACEHOLDER from "../../assets/images/img-placeholder.svg";

const toolBarOptions = [
    ["bold", "italic", "underline", "strike"], // toggled buttons
    ["blockquote", "code-block", "link", "image"],

    [{ header: 1 }, { header: 2 }], // custom button values
    [{ list: "ordered" }, { list: "bullet" }],
    [{ script: "sub" }, { script: "super" }], // superscript/subscript
    [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
    [{ direction: "rtl" }], // text direction

    [{ size: ["small", false, "large", "huge"] }], // custom dropdown
    [{ header: [1, 2, 3, 4, 5, 6, false] }],

    [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    [{ font: [] }],
    [{ align: [] }],

    ["clean"], // remove formatting button
];

const PageContents: React.FC = () => {
    const pageContentsRaw = useAppSelector(selectAllPageContents);
    const [pageContents, setPageContents] = useState<PageContent[]>(pageContentsRaw);
    const [uploading, setUploading] = useState(false);
    const [saving, setSaving] = useState("idle");
    const [overSizedPage, setOverSizedPage] = useState<string[]>([]);
    const [errorPages, setErrorPages] = useState<string[]>([]);
    const [disableSubmit, setDisableSubmit] = useState(true);
    const [showMediaLibrary, setShowMediaLibrary] = useState(false);

    useEffect(() => {
        setPageContents(pageContentsRaw);
    }, [pageContentsRaw]);

    // effect to check if can save
    useEffect(() => {
        let canSubmit = true;
        pageContents.forEach((p) => {
            if (!p.title.en || !p.content.en) {
                canSubmit = false;
            }
        });
        setDisableSubmit(!canSubmit);
    }, [pageContents]);

    const handleSubmit = async () => {
        if (!pageContents) {
            return;
        }
        let newOverSize: string[] = [];
        pageContents.forEach(x => {
            const dataString = JSON.stringify(x.content);
            const size = new TextEncoder().encode(dataString).length;
            console.log(`Content: ${x.title.en}, size: ${size}`);
            if (size > 1500000) {
                newOverSize.push(x.title.en);
            }
        });
        if (newOverSize.length) {
            setOverSizedPage(newOverSize);
            return
        }
        setSaving("saving");
        const res = await saveAllPageContents(pageContents);
        if (res.length > 0) {
            setErrorPages(res);
            setSaving('savedWithError');
            return
        }
        setSaving("saved");
    };

    return (
        <IonPage>
            <Header title="Page Contents Management" />
            <IonContent>
                {!pageContents.length ? (
                    <LoadingCard />
                ) : (
                    pageContents.map((x) => (
                        <IonCard key={`page-content-${x.id}`} color={"light"}>
                            <IonCardContent>
                                <IonList inset>
                                    <IonItem>
                                        <IonLabel position="stacked">
                                            <p>{`Page Title (EN)`}</p>
                                        </IonLabel>
                                        <IonInput
                                            type="text"
                                            value={x.title.en}
                                            onIonChange={(e) => {
                                                const tempItem = Object.assign(
                                                    { ...x },
                                                    {
                                                        title: Object.assign(
                                                            { ...x.title },
                                                            {
                                                                en: e.detail.value!,
                                                            }
                                                        ),
                                                    }
                                                );
                                                const idx = pageContents.findIndex((p) => p.id === x.id);
                                                let newArray = [...pageContents];
                                                newArray.splice(idx, 1, tempItem);
                                                setPageContents(newArray);
                                            }}
                                        />
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel position="stacked">
                                            <p>{`頁面標題 (中文)`}</p>
                                        </IonLabel>
                                        <IonInput
                                            type="text"
                                            value={x.title.zh}
                                            onIonChange={(e) => {
                                                const tempItem = Object.assign(
                                                    { ...x },
                                                    {
                                                        title: Object.assign(
                                                            { ...x.title },
                                                            {
                                                                zh: e.detail.value!,
                                                            }
                                                        ),
                                                    }
                                                );
                                                const idx = pageContents.findIndex((p) => p.id === x.id);
                                                let newArray = [...pageContents];
                                                newArray.splice(idx, 1, tempItem);
                                                setPageContents(newArray);
                                            }}
                                        />
                                    </IonItem>
                                </IonList>

                                <IonGrid slot="content" color="white">
                                    <IonRow>
                                        <IonCol>
                                            <IonLabel>
                                                <h3>{`Content (EN)`}</h3>
                                            </IonLabel>
                                        </IonCol>
                                    </IonRow>
                                    <IonRow color="white">
                                        <IonCol color="white">
                                            <ReactQuill
                                                theme="snow"
                                                className="faq-editor"
                                                modules={{
                                                    toolbar: toolBarOptions,
                                                }}
                                                value={x.content.en}
                                                onChange={(value) => {
                                                    if (value === x.content.en) { return };
                                                    const tempItem = Object.assign(
                                                        { ...x },
                                                        {
                                                            content: Object.assign(
                                                                { ...x.content },
                                                                {
                                                                    en: value,
                                                                }
                                                            ),
                                                        }
                                                    );
                                                    const idx = pageContents.findIndex((p) => p.id === x.id);
                                                    let newArray = [...pageContents];
                                                    newArray.splice(idx, 1, tempItem);
                                                    setPageContents(newArray);
                                                }}
                                            />
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>

                                <IonGrid slot="content" color="white">
                                    <IonRow>
                                        <IonCol>
                                            <IonLabel>
                                                <h3>{`內容 (中文)`}</h3>
                                            </IonLabel>
                                        </IonCol>
                                    </IonRow>
                                    <IonRow color="white">
                                        <IonCol color="white">
                                            <ReactQuill
                                                theme="snow"
                                                className="faq-editor"
                                                modules={{
                                                    toolbar: toolBarOptions,
                                                }}
                                                value={x.content.zh}
                                                onChange={(value) => {
                                                    if (value === x.content.zh) { return };
                                                    console.log(value);
                                                    const tempItem = Object.assign(
                                                        { ...x },
                                                        {
                                                            content: Object.assign(
                                                                { ...x.content },
                                                                {
                                                                    zh: value,
                                                                }
                                                            ),
                                                        }
                                                    );
                                                    const idx = pageContents.findIndex((p) => p.id === x.id);
                                                    let newArray = [...pageContents];
                                                    newArray.splice(idx, 1, tempItem);
                                                    setPageContents(newArray);
                                                }}
                                            />
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>

                                <IonGrid>
                                    <IonRow>
                                        <IonCol>
                                            {x.mediaType === "videos" ? (
                                                <>
                                                    <video
                                                        style={{
                                                            width: `100%`,
                                                        }}
                                                    >
                                                        <source src={x.media} type="video/mp4"></source>
                                                    </video>
                                                </>
                                            ) : x.mediaType === "contentImages" ? (
                                                <img
                                                    alt="media-preview"
                                                    src={x.media ? x.media : IMG_PLACEHOLDER}
                                                    style={{
                                                        height: `96px`,
                                                    }}
                                                />
                                            ) : null}
                                        </IonCol>
                                        <IonCol>
                                            {x.media ? (
                                                <IonButton
                                                    onClick={async () => {
                                                        await deleteMediaByUrl(x.media);
                                                        const tempItem = Object.assign(
                                                            { ...x },
                                                            {
                                                                media: "",
                                                            }
                                                        );
                                                        const idx = pageContents.findIndex((p) => p.id === x.id);
                                                        let newArray = [...pageContents];
                                                        newArray.splice(idx, 1, tempItem);
                                                        setPageContents(newArray);
                                                        // setPageContents((s) => (s ? Object.assign({ ...s }, { media: "" }) : s));
                                                    }}
                                                >
                                                    <IonIcon icon={closeCircleOutline} slot="start" />
                                                    <IonLabel>Delete Banner Media</IonLabel>
                                                </IonButton>
                                            ) : (
                                                <Dropzone
                                                    disabled={uploading}
                                                    onDrop={async (acceptedFiles) => {
                                                        if (acceptedFiles.length <= 0) {
                                                            return;
                                                        }
                                                        setUploading(true);
                                                        const url = await uploadSlideImage(acceptedFiles[0]);
                                                        setUploading(false);
                                                        if (!url) {
                                                            return;
                                                        }
                                                        const decoded = decodeURIComponent(url);
                                                        const trimmed = decoded.split("?")[0];
                                                        const splitted = trimmed.split("/");
                                                        const folder = splitted[splitted.length - 2];
                                                        const tempItem = Object.assign(
                                                            { ...x },
                                                            {
                                                                media: url,
                                                                mediaType: folder,
                                                            }
                                                        );
                                                        const idx = pageContents.findIndex((p) => p.id === x.id);
                                                        let newArray = [...pageContents];
                                                        newArray.splice(idx, 1, tempItem);
                                                        setPageContents(newArray);
                                                    }}
                                                >
                                                    {({ getRootProps, getInputProps }) => (
                                                        <section>
                                                            <div
                                                                {...getRootProps()}
                                                                style={{
                                                                    border: `2px dashed #888`,
                                                                    padding: `1rem`,
                                                                    margin: `1rem`,
                                                                    borderRadius: `12px`,
                                                                    cursor: `pointer`,
                                                                    textAlign: `center`,
                                                                }}
                                                            >
                                                                {uploading ? (
                                                                    <IonSpinner name="dots" />
                                                                ) : (
                                                                    <>
                                                                        <input {...getInputProps()} />
                                                                        <p>Drop Image / Video Here...</p>
                                                                    </>
                                                                )}
                                                            </div>
                                                        </section>
                                                    )}
                                                </Dropzone>
                                            )}
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>
                            </IonCardContent>
                        </IonCard>
                    ))
                )}
            </IonContent>
            <IonFooter>
                <IonToolbar>
                    <IonButton color={disableSubmit ? "medium" : "warning"} disabled={disableSubmit} slot="end" onClick={() => handleSubmit()}>
                        <IonIcon icon={checkmarkOutline} slot="start" />
                        <IonLabel>Save</IonLabel>
                    </IonButton>
                </IonToolbar>
            </IonFooter>
            <IonLoading isOpen={saving === "saving"} message={`Saving Updates`} />
            <IonAlert
                isOpen={saving === "saved"}
                onDidDismiss={() => setSaving("idle")}
                header={`Your Changes is Saved.`}
                buttons={[
                    {
                        text: "OK",
                        role: "confirm",
                        handler: () => {
                            setSaving("idle");
                        },
                    },
                ]}
            />
            <IonAlert
                isOpen={overSizedPage.length > 0}
                onDidDismiss={() => setOverSizedPage([])}
                header={`Content too Large!`}
                message={`One or more content is larger than 800KB, please fix before saving.\n\n ${overSizedPage.join(`\n`)}`}
                buttons={[
                    {
                        text: "OK",
                        role: "confirm",
                        handler: () => {
                            setOverSizedPage([]);
                        },
                    },
                ]}
            />
            <IonAlert
                isOpen={errorPages.length > 0}
                onDidDismiss={() => setErrorPages([])}
                header={`Saved with Errors!`}
                message={`One or more content cannot be saved: ${errorPages.join(`, `)}`}
                buttons={[
                    {
                        text: "OK",
                        role: "confirm",
                        handler: () => {
                            setErrorPages([]);
                        },
                    },
                ]}
            />
            <MediaLibraryModal
                isOpen={showMediaLibrary}
                onDismiss={() => {
                    setShowMediaLibrary(false);
                }}
            />
        </IonPage>
    );
};

export default PageContents;

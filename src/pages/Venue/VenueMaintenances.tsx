import {
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonChip,
    IonContent,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonPage,
    IonSpinner
} from "@ionic/react";
import * as VenueTypes from "@tsanghoilun/snow-n-surf-interface/types/venue";
import { deleteMaintenence, getMaintenencesByRange } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAuthUser } from "app/slices/authSlice";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
import relativeTime from "dayjs/plugin/relativeTime";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { addOutline, alertCircleOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import MaintenenceModal from "./MaintenenceModal";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.extend(duration);
dayjs.extend(relativeTime);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const newMaintenenceSession: VenueTypes.MaintenanceSession = {
    id: `new`,
    start: dayjs.tz(dayjs()).startOf("day").toDate(),
    end: dayjs.tz(dayjs()).endOf("day").toDate(),
    venue: [],
    createdAt: dayjs().toDate(),
    staff: null,
};

interface SearchResults {
    status: "idle" | "loading";
    list: VenueTypes.MaintenanceSession[];
}

const VenueMaintenances: React.FC = () => {
    const [range, setRange] = useState({
        from: dayjs.tz(Date.now()).startOf("month"),
        to: dayjs.tz(Date.now()).endOf("month"),
    });

    const [results, setResults] = useState<SearchResults>({
        status: "loading",
        list: [],
    });

    const [defaultNewSession, setDefaultNewSession] = useState(newMaintenenceSession);
    const [selectedSession, setSelectedSession] = useState<VenueTypes.MaintenanceSession | null>(null);
    const [deleting, setDeleting] = useState<string>("idle");
    const [deleteSessionId, setDeleteSessionId] = useState<string | null>(null);
    const authStaff = useAppSelector(selectAuthUser);

    // update authUser in the session when auth user is ready
    useEffect(() => {
        if (!authStaff) {
            return;
        }
        setDefaultNewSession((s) =>
            s
                ? Object.assign(
                      { ...s },
                      {
                          staff: authStaff,
                      }
                  )
                : s
        );
    }, [authStaff]);

    useEffect(() => {
        const handleSearch = async () => {
            setResults((re) => Object.assign({ ...re }, { list: [] }));
            const res = await getMaintenencesByRange(range.from.toDate(), range.to.toDate());
            setResults({
                status: "idle",
                list: [...res],
            });
        };
        if (results.status === "loading") {
            handleSearch();
        }
    }, [results.status, range]);

    const handleDelete = async () => {
        setDeleting("deleting");
        if (!deleteSessionId) {
            return;
        }
        const res = await deleteMaintenence(deleteSessionId);
        if (res === "success") {
            setResults((rs) =>
                Object.assign(
                    { ...rs },
                    {
                        list: [...rs.list].filter((x) => x.id !== deleteSessionId),
                    }
                )
            );
            setDeleting("deleted");
        }
    };

    return (
        <IonPage>
            <Header title="Venue Maintenances" />
            <IonContent>
                <br />
                <IonCard color={"light"}>
                    <IonCardContent>
                        <IonItem lines="none" color={"light"}>
                            <IonLabel>
                                <h2>
                                    <b>Search Maintenence Sessions</b>
                                </h2>
                            </IonLabel>
                            <IonButtons slot="end">
                                <IonButton
                                    fill="solid"
                                    color={`warning`}
                                    size="large"
                                    shape="round"
                                    onClick={() => {
                                        setSelectedSession(defaultNewSession);
                                    }}
                                >
                                    <IonIcon icon={addOutline} slot="start" />
                                    <IonLabel className="ion-margin-end">New</IonLabel>
                                </IonButton>
                            </IonButtons>
                        </IonItem>
                        <IonList inset>
                            <IonItem>
                                <IonLabel>
                                    <p>From</p>
                                </IonLabel>
                                <IonInput
                                    type="date"
                                    value={range.from.format(`YYYY-MM-DD`)}
                                    onIonChange={(e) => {
                                        setRange((r) => Object.assign({ ...r }, { from: dayjs.tz(e.detail.value!) }));
                                    }}
                                />
                            </IonItem>
                            <IonItem>
                                <IonLabel>
                                    <p>To</p>
                                </IonLabel>
                                <IonInput
                                    type="date"
                                    value={range.to.format(`YYYY-MM-DD`)}
                                    onIonChange={(e) => {
                                        setRange((r) => Object.assign({ ...r }, { to: dayjs.tz(e.detail.value!) }));
                                    }}
                                />
                            </IonItem>
                        </IonList>
                        <IonButton
                            expand="block"
                            color={`primary`}
                            onClick={() => {
                                setResults((re) => Object.assign({ ...re }, { status: "loading" }));
                            }}
                        >
                            <IonLabel>Search</IonLabel>
                        </IonButton>
                    </IonCardContent>
                </IonCard>

                {results.status === "idle" ? (
                    results.list.length > 0 ? (
                        <IonCard color={`medium`}>
                            <IonCardContent>
                                <IonList inset>
                                    {results.list.map((x) => (
                                        <IonItem key={`schedule-item-${x.id}`}>
                                            <IonLabel>
                                                <h2>
                                                    {dayjs.tz(x.start).format(`DD MMM YY, HH:mm`)}
                                                    {` - `}
                                                    {dayjs.tz(x.end).format(`DD MMM YY, HH:mm`)}
                                                </h2>
                                                <p>{dayjs.duration(dayjs(x.end).diff(dayjs(x.start))).humanize()}</p>
                                                <p>
                                                    {x.venue.map((y) => (
                                                        <IonChip key={`item-${x.id}-venue-${y.venue.name}`}>{y.venue.name}</IonChip>
                                                    ))}
                                                </p>
                                            </IonLabel>
                                            <IonButtons slot="end">
                                                <IonButton
                                                    color={`secondary`}
                                                    fill="solid"
                                                    onClick={() => {
                                                        setSelectedSession({ ...x });
                                                    }}
                                                >
                                                    Edit
                                                </IonButton>
                                                <IonButton
                                                    color={`danger`}
                                                    fill="solid"
                                                    onClick={() => {
                                                        setDeleteSessionId(x.id);
                                                    }}
                                                >
                                                    Delete
                                                </IonButton>
                                            </IonButtons>
                                        </IonItem>
                                    ))}
                                </IonList>
                            </IonCardContent>
                        </IonCard>
                    ) : (
                        <IonCard>
                            <IonCardContent>
                                <IonItem lines="none">
                                    <IonIcon slot="start" icon={alertCircleOutline} color="danger" />
                                    <IonLabel color={"danger"}>No Maintenence found in the range you specified.</IonLabel>
                                </IonItem>
                            </IonCardContent>
                        </IonCard>
                    )
                ) : (
                    <IonCard color={`light`}>
                        <IonCardContent className="ion-text-center">
                            <IonSpinner name="dots" />
                        </IonCardContent>
                    </IonCard>
                )}
            </IonContent>

            <MaintenenceModal onDismiss={() => setSelectedSession(null)} session={selectedSession} />

            <IonAlert
                isOpen={!!deleteSessionId}
                header={`Are you sure?`}
                onDidDismiss={() => {
                    setDeleteSessionId(null)
                }}
                message={"Deleted maintenence will release the availabilities for booking again."}
                buttons={[
                    {
                        text: "OK",
                        role: "confirm",
                        handler: () => {
                            handleDelete();
                        },
                    },
                    {
                        text: "Back",
                        role: "cancel",
                        handler: () => {
                            setDeleteSessionId(null);
                        },
                    },
                ]}
            />
            <IonAlert
                isOpen={deleting === "deleted"}
                header="Maintenence Session Deleted"
                onDidDismiss={() => {
                    setDeleting("idle");
                    setDeleteSessionId(null);
                }}
                buttons={[
                    {
                        text: "OK",
                        role: "confirm",
                        handler: () => {
                            setDeleting("idle");
                            setDeleteSessionId(null);
                        },
                    },
                ]}
            />
        </IonPage>
    );
};

export default VenueMaintenances;

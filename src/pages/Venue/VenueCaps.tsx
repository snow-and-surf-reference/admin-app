import {
  IonAccordion,
  IonAccordionGroup,
  IonAlert,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent, IonGrid,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonLoading,
  IonPage,
  IonRow,
  IonToggle
} from "@ionic/react";
import * as VenueTypes from "@tsanghoilun/snow-n-surf-interface/types/venue";
import { updateCaps } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectCapacities } from "app/slices/venueSlice";
import Header from "components/Global/Header";
import LoadingCard from "components/Global/LoadingCard";
import dayjs from "dayjs";
import { addCircleOutline, removeCircleOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

const VenueCaps: React.FC = () => {
  const rawCaps: VenueTypes.Capacities | undefined = useAppSelector(selectCapacities);
  const [caps, setCaps] = useState(rawCaps);
  const [saving, setSaving] = useState('idle');

  useEffect(() => {
    if (!rawCaps) {
      return;
    }
    setCaps(rawCaps);
  }, [rawCaps]);

  const handleUpdate = async () => {
    setSaving('saving');
    if (!caps) { return };
    const res = await updateCaps(caps);
    if (res === 'success') {
      setSaving('saved');
    }
  }

  return (
    <IonPage>
      <Header title="Venue Capacity" />
      <IonContent color={"light"}>
        <br />
        {!caps ? (
          <LoadingCard />
        ) : (
          <>
            <IonCard>
              <IonCardContent>
                <IonAccordionGroup value={"hours"}>
                  <IonAccordion value="hours">
                    <IonItem slot="header">
                      <IonLabel>
                        <h2>
                          <b>Opening Hours</b>
                        </h2>
                      </IonLabel>
                    </IonItem>
                    <IonGrid slot={"content"}>
                      <IonRow>
                        <IonCol>
                          <IonList>
                            <IonListHeader>Weekdays</IonListHeader>
                            <IonItem>
                              <IonLabel>Open</IonLabel>
                              <IonInput
                                type="time"
                                value={`${caps.hours.weekday.open}:00`}
                                onIonChange={(e) => {
                                  setCaps(
                                    Object.assign(
                                      { ...caps },
                                      {
                                        hours: Object.assign(
                                          { ...caps.hours },
                                          {
                                            weekday: Object.assign(
                                              { ...caps.hours.weekday },
                                              {
                                                open: Number(dayjs(`${dayjs().format(`YYYY-MM-DD`)}T${e.detail.value!}`).format("HH")),
                                              }
                                            ),
                                          }
                                        ),
                                      }
                                    )
                                  );
                                }}
                              />
                            </IonItem>
                            <IonItem>
                              <IonLabel>Close</IonLabel>
                                <IonInput type="time" value={`${caps.hours.weekday.close}:00`}
                                onIonChange={(e) => {
                                  setCaps(
                                    Object.assign(
                                      { ...caps },
                                      {
                                        hours: Object.assign(
                                          { ...caps.hours },
                                          {
                                            weekday: Object.assign(
                                              { ...caps.hours.weekday },
                                              {
                                                close: Number(dayjs(`${dayjs().format(`YYYY-MM-DD`)}T${e.detail.value!}`).format("HH")),
                                              }
                                            ),
                                          }
                                        ),
                                      }
                                    )
                                  );
                                }}
                                />
                            </IonItem>
                          </IonList>
                        </IonCol>
                      </IonRow>
                      <IonRow>
                        <IonCol>
                          <IonList>
                            <IonListHeader>Weekend / PH</IonListHeader>
                            <IonItem>
                              <IonLabel>Open</IonLabel>
                                <IonInput type="time" value={`${caps.hours.weekend.open}:00`}
                                  onIonChange={(e) => {
                                    setCaps(
                                      Object.assign(
                                        { ...caps },
                                        {
                                          hours: Object.assign(
                                            { ...caps.hours },
                                            {
                                              weekend: Object.assign(
                                                { ...caps.hours.weekend },
                                                {
                                                  open: Number(dayjs(`${dayjs().format(`YYYY-MM-DD`)}T${e.detail.value!}`).format("HH")),
                                                }
                                              ),
                                            }
                                          ),
                                        }
                                      )
                                    );
                                  }}
                                />
                            </IonItem>
                            <IonItem>
                              <IonLabel>Close</IonLabel>
                                <IonInput type="time" value={`${caps.hours.weekend.close}:00`}
                                  onIonChange={(e) => {
                                    setCaps(
                                      Object.assign(
                                        { ...caps },
                                        {
                                          hours: Object.assign(
                                            { ...caps.hours },
                                            {
                                              weekend: Object.assign(
                                                { ...caps.hours.weekend },
                                                {
                                                  close: Number(dayjs(`${dayjs().format(`YYYY-MM-DD`)}T${e.detail.value!}`).format("HH")),
                                                }
                                              ),
                                            }
                                          ),
                                        }
                                      )
                                    );
                                  }}
                                />
                            </IonItem>
                          </IonList>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </IonAccordion>
                </IonAccordionGroup>
              </IonCardContent>
            </IonCard>

            <IonCard>
              <IonCardContent>
                <IonAccordionGroup value={"caps"}>
                  <IonAccordion value="caps">
                    <IonItem slot="header">
                      <h2>
                        <b>Venue Capacities</b>
                      </h2>
                    </IonItem>
                    <IonGrid slot="content">
                      <IonRow>
                        <IonCol>
                          <IonList>
                            {caps.capacities.map((x) => (
                              <IonItem key={`capacities-item-${x.venue.name}`}>
                                <IonLabel>{x.venue.name}</IonLabel>
                                <IonButtons slot="end">
                                  <IonButton
                                    disabled={x.max <= 1}
                                    onClick={() => {
                                      let tempCaps = { ...caps }
                                      let tempcap = [...tempCaps.capacities];
                                      let idx = tempcap.findIndex(y => y.venue.name === x.venue.name);
                                      console.log(idx);
                                      tempcap[idx] = Object.assign({ ...x }, { max: x.max - 1 })
                                      Object.assign(tempCaps, { capacities: tempcap });
                                      setCaps(tempCaps);
                                    }}
                                  >
                                    <IonIcon slot="icon-only" icon={removeCircleOutline} />
                                  </IonButton>
                                  <IonLabel className="ion-margin">{x.max}</IonLabel>
                                  <IonButton
                                    onClick={() => {
                                      let tempCaps = { ...caps }
                                      let tempcap = [...tempCaps.capacities];
                                      let idx = tempcap.findIndex(y => y.venue.name === x.venue.name);
                                      console.log(idx);
                                      tempcap[idx] = Object.assign({ ...x }, { max: x.max + 1 })
                                      Object.assign(tempCaps, { capacities: tempcap });
                                      setCaps(tempCaps);
                                    }}
                                  >
                                    <IonIcon slot="icon-only" icon={addCircleOutline} />
                                  </IonButton>
                                </IonButtons>
                              </IonItem>
                            ))}
                          </IonList>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </IonAccordion>
                </IonAccordionGroup>
              </IonCardContent>
              </IonCard>
              
              <IonCard>
                <IonCardContent>
                  <IonItem>
                    <IonLabel>Hide POS Booking</IonLabel>
                    <IonToggle checked={caps.hidePos}
                      onIonChange={(e) => {
                        setCaps(c => c ? Object.assign({ ...c }, { hidePos: e.detail.checked }) : c);
                      }}
                    />
                  </IonItem>
                </IonCardContent>
            </IonCard>

            <IonGrid>
              <IonRow>
                <IonCol className="ion-text-end">
                    <IonButton color={"warning"}
                      onClick={() => {
                        handleUpdate()
                      }}
                    >Save Changes</IonButton>
                </IonCol>
              </IonRow>
            </IonGrid>
          </>
        )}
      </IonContent>
      <IonLoading
        isOpen={saving === 'saving'}
        message='Updating Your Changes'
      />
      <IonAlert
        isOpen={saving === 'saved'}
        header='Venue Info Updated!'
        onDidDismiss={() => setSaving('idle')}
        buttons={[
          {
            text: 'OK',
            role: 'confirm',
            handler: () => setSaving('idle')
          }
        ]}
      />
    </IonPage>
  );
};

export default VenueCaps;

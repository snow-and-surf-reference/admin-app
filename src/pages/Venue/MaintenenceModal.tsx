import {
    IonAlert,
    IonButton,
    IonCard,
    IonCheckbox,
    IonContent,
    IonDatetime,
    IonDatetimeButton,
    IonFooter,
    IonHeader,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonPopover,
    IonTitle,
    IonToolbar
} from "@ionic/react";
import * as VenueTypes from "@tsanghoilun/snow-n-surf-interface/types/venue";
import { checkMaintenenceSession, setMaintenence } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectCapacities } from "app/slices/venueSlice";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { checkmarkOutline, closeOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

interface Props {
    session: VenueTypes.MaintenanceSession | null;
    onDismiss: () => void;
}

const MaintenenceModal: React.FC<Props> = (props) => {
    const { session, onDismiss } = props;
    const [sessionClone, setSessionClone] = useState<VenueTypes.MaintenanceSession | null>(session);
    const [saving, setSaving] = useState("idle");
    const [disableSaving, setDisableSaving] = useState(true);
    const allVenues = useAppSelector(selectCapacities);
    const [warningCsv, setWarningCsv] = useState<string | null>(null);
    const [csvCount, setCsvCount] = useState(0);

    useEffect(() => {
        setSessionClone(session);
    }, [session]);

    // useEffect(() => console.log(sessionClone), [sessionClone]);

    useEffect(() => {
        if (
            sessionClone &&
            sessionClone.start &&
            sessionClone.end &&
            sessionClone.end.valueOf() > sessionClone.start.valueOf() &&
            sessionClone.venue.length > 0
        ) {
            setDisableSaving(false);
        } else {
            setDisableSaving(true);
        }
    }, [sessionClone]);

    const handleSave = async () => {
        if (!sessionClone) {
            return;
        }
        setSaving("saving");
        const res = await checkMaintenenceSession(sessionClone);
        if (res === 'success') {
            setSaving("saved");
        } else {
            setWarningCsv(res.csv);
            setCsvCount(res.count);
            setSaving('warning');
        }
    };

    const handleSureSave = async () => {
        if (!sessionClone) {
            return;
        }
        setSaving("saving");
        await setMaintenence(sessionClone);
        setSaving("saved");   
    }

    return (
        <>
            <IonModal isOpen={session !== null} onDidDismiss={() => onDismiss()}>
                {!sessionClone ? null : (
                    <>
                        <IonHeader>
                            <IonToolbar>
                                <IonTitle>{sessionClone.id === "new" ? "New" : "Edit"} Maintenence Session</IonTitle>
                                <IonButton
                                    slot="start"
                                    fill="clear"
                                    onClick={() => {
                                        onDismiss();
                                    }}
                                >
                                    <IonIcon slot="start" icon={closeOutline} />
                                    <IonLabel>Cancel</IonLabel>
                                </IonButton>
                            </IonToolbar>
                        </IonHeader>
                        <IonContent>
                            <IonCard color={"light"}>
                                <IonList inset>
                                    <IonItem>
                                        <IonLabel>Start Time</IonLabel>
                                        <IonDatetimeButton datetime="startTimePicker"></IonDatetimeButton>
                                        <IonPopover keepContentsMounted={true}>
                                            <IonDatetime
                                                id="startTimePicker"
                                                value={dayjs.tz(sessionClone.start).format(`YYYY-MM-DDTHH:mm:ss`)}
                                                min={dayjs.tz(Date.now()).subtract(3, 'months').format()}
                                                max={dayjs.tz(Date.now()).add(3, 'years').format()}
                                                onIonChange={(e) => {
                                                    setSessionClone((sc) =>
                                                        sc
                                                            ? Object.assign(
                                                                  { ...sc },
                                                                  {
                                                                      start: dayjs
                                                                          .tz(`${dayjs(String(e.detail.value!)).format(`YYYY-MM-DDTHH:mm:ss`)}`)
                                                                          .toDate(),
                                                                  }
                                                              )
                                                            : sc
                                                    );
                                                }}
                                            ></IonDatetime>
                                        </IonPopover>
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel
                                            color={
                                                sessionClone.start && sessionClone.end && sessionClone.end.valueOf() <= sessionClone.start.valueOf()
                                                    ? "danger"
                                                    : "dark"
                                            }
                                        >
                                            End Time
                                        </IonLabel>
                                        <IonDatetimeButton datetime="endTimePicker"></IonDatetimeButton>
                                        <IonPopover keepContentsMounted={true}>
                                            <IonDatetime
                                                id="endTimePicker"
                                                value={dayjs.tz(sessionClone.end).format(`YYYY-MM-DDTHH:mm:ss`)}
                                                min={dayjs.tz(Date.now()).subtract(3, 'months').format()}
                                                max={dayjs.tz(Date.now()).add(3, 'years').format()}
                                                onIonChange={(e) => {
                                                    setSessionClone((sc) =>
                                                        sc
                                                            ? Object.assign(
                                                                  { ...sc },
                                                                  {
                                                                      end: dayjs
                                                                          .tz(`${dayjs(String(e.detail.value!)).format(`YYYY-MM-DDTHH:mm:ss`)}`)
                                                                          .toDate(),
                                                                  }
                                                              )
                                                            : sc
                                                    );
                                                }}
                                            ></IonDatetime>
                                        </IonPopover>
                                    </IonItem>
                                </IonList>
                            </IonCard>
                            <IonCard color={"light"}>
                                <IonItem color={"light"} lines="none">
                                    <IonLabel>
                                        <b>Venues</b>
                                        <p>Select The venues to be closed for maintenence.</p>
                                    </IonLabel>
                                </IonItem>
                                <IonList inset>
                                    {allVenues &&
                                        allVenues.capacities.map((x) => (
                                            <IonItem key={`venue-${x.venue.name}`}>
                                                <IonCheckbox
                                                    slot="start"
                                                    checked={sessionClone.venue.filter((y) => y.venue.name === x.venue.name).length > 0}
                                                    onIonChange={(e) => {
                                                        const tempItem = { ...x };
                                                        let tempVenues = [...sessionClone.venue];
                                                        const idx = tempVenues.findIndex((y) => y.venue.name === x.venue.name);
                                                        if (idx < 0) {
                                                            tempVenues.push(tempItem);
                                                        } else {
                                                            tempVenues = tempVenues.filter((z, index) => index !== idx);
                                                        }

                                                        setSessionClone((sc) =>
                                                            sc
                                                                ? Object.assign(
                                                                      { ...sc },
                                                                      {
                                                                          venue: tempVenues,
                                                                      }
                                                                  )
                                                                : sc
                                                        );
                                                    }}
                                                />
                                                <IonLabel>
                                                    <h3>
                                                        {x.playType} - <b>{x.venue.name}</b>
                                                    </h3>
                                                    <p>capacity: {x.max}</p>
                                                </IonLabel>
                                            </IonItem>
                                        ))}
                                </IonList>
                            </IonCard>
                        </IonContent>
                        <IonFooter>
                            <IonToolbar>
                                <IonButton
                                    expand="block"
                                    color={"dark"}
                                    disabled={disableSaving}
                                    onClick={() => handleSave()}
                                >
                                    <IonIcon icon={checkmarkOutline} slot="start" />
                                    <IonLabel>Save Changes</IonLabel>
                                </IonButton>
                            </IonToolbar>
                        </IonFooter>
                    </>
                )}
            </IonModal>
            <IonLoading isOpen={saving === "saving"} spinner="dots" message="Saving" />
            <IonAlert
                isOpen={saving === "saved"}
                header="Your changes is Saved."
                onDidDismiss={() => {
                    setSaving("idle");
                    onDismiss();
                }}
                buttons={[
                    {
                        text: "OK",
                        role: "confirm",
                        handler: () => {
                            setSaving("idle");
                            onDismiss();
                        },
                    },
                ]}
            />
            <IonAlert
                isOpen={saving === 'warning'}
                header='WARNING!'
                message={`There are seems to be ${csvCount} confirmed bookings within your maintenece time, please make sure to DOWNLOAD the .csv file provided here and contact the customers if you continue to save this maintenence session.`}
                buttons={[
                    {
                        text: 'Download CSV',
                        handler: () => {
                            if (!warningCsv) { return false };
                            const encodedUri = encodeURI(warningCsv);
                            window.open(encodedUri);
                            return false;
                        }
                    },
                    {
                        text: 'Confirm Create',
                        handler: () => {
                            handleSureSave()
                        }
                    },
                    {
                        text: 'Cancel',
                        handler: () => {
                            setSaving('idle')
                        }
                    }
                ]}
            />
        </>
    );
};

export default MaintenenceModal;

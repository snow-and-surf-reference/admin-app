import {
    IonButton,
    IonCard,
    IonCardContent,
    IonContent,
    IonFooter,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonTitle,
    IonToolbar,
} from "@ionic/react";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { updateCustomerInfo } from "app/firebase";
import { closeOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

interface Props {
    user: Customer | null;
    isOpen: boolean;
    onDismiss: () => void;
}

const EditUserModal: React.FC<Props> = (props) => {
    const { user, isOpen, onDismiss } = props;
    const [editingUser, setEditingUser] = useState<Customer | null>(null);
    const [saving, setSaving] = useState('idle');

    useEffect(() => {
        if (user) {
            setEditingUser(user);
        }
        return () => {
            setEditingUser(null);
        };
    }, [user]);

    const handleSubmit = async () => {
        if (!editingUser) {
            return
        }
        console.log('editing user', editingUser);
        setSaving('saving');
        await updateCustomerInfo(editingUser);
        setSaving('idle');
        setTimeout(() => {
            onDismiss()
        }, 300);
    }

    return (
        <>
            <IonModal
                isOpen={isOpen}
                onDidDismiss={() => {
                    onDismiss();
                }}
            >
                <IonHeader>
                    <IonToolbar>
                        <IonTitle>Edit User</IonTitle>
                        <IonButton slot="start" color={"danger"} fill='clear'
                            onClick={() => {
                                onDismiss()
                            }}
                        >
                            <IonIcon icon={closeOutline} slot="start" />
                            <IonLabel>Cancel</IonLabel>
                        </IonButton>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    {editingUser ? (
                        <>
                            <IonCard>
                                <IonCardContent>
                                    <IonItem lines="none">
                                        <IonLabel>
                                            <b>Member ID:</b> {editingUser.id}
                                        </IonLabel>
                                    </IonItem>
                                </IonCardContent>
                            </IonCard>
                            <IonCard color={"light"}>
                                <IonCardContent>
                                    <IonList inset>
                                        <IonItem>
                                            <IonLabel position="stacked">Real Name</IonLabel>
                                            <IonInput
                                                inputmode="numeric"
                                                value={editingUser.realName}
                                                onIonChange={(e) => {
                                                    setEditingUser(u => u ? Object.assign({ ...u }, {
                                                        realName: e.detail.value!
                                                    }) : u)
                                                }}
                                            ></IonInput>
                                        </IonItem>
                                        <IonItem>
                                            <IonLabel position="stacked">Phone</IonLabel>
                                            <IonInput
                                                inputmode="numeric"
                                                value={editingUser.phone}
                                                onIonChange={(e) => {
                                                    setEditingUser(u => u ? Object.assign({ ...u }, {
                                                        phone: e.detail.value!
                                                    }) : u)
                                                }}
                                            ></IonInput>
                                        </IonItem>
                                    </IonList>
                                </IonCardContent>
                            </IonCard>
                        </>
                    ) : null}
                </IonContent>
                <IonFooter>
                    <IonToolbar>
                        <IonButton expand="block" color={"warning"}
                            onClick={() => handleSubmit()}
                            disabled={!editingUser}
                        >
                            <IonLabel>Save Changes</IonLabel>
                        </IonButton>
                    </IonToolbar>
                </IonFooter>
            </IonModal>
            <IonLoading
                isOpen={saving === 'saving'}
                spinner="dots"
                message={`saving changes`}
            />
        </>
    );
};

export default EditUserModal;

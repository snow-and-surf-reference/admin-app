import {
    IonAccordion,
    IonAccordionGroup,
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonChip,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonImg,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonPage,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonSpinner,
    IonText,
    IonThumbnail,
    IonTitle,
    IonToolbar
} from "@ionic/react";
import { IAccess, ICircletData } from "@tsanghoilun/snow-n-surf-interface/types/circlet";
import { GearStorage } from "@tsanghoilun/snow-n-surf-interface/types/gear";
import { PlayType, Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Coach, Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import {
    checkPrivateSession,
    getBookingById, getCounterByDateV2,
    getCustomerStorage,
    removeUserFromSession,
    setCircletData,
    setSessionIsCheckedIn,
    updateSessionCoach,
    verifyUser
} from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAllCoaches } from "app/slices/coachSlice";
// import { ISession } from "app/models/sessions";
import * as SessionsTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import { setIsLoading } from "app/slices/globalSlice";
import { selectLoadedSessionById } from "app/slices/sessionsSlice";
import { setWristbandId } from "app/slices/wristbandSlice";
import { RootState } from "app/store";
import { singleAlertBtn } from "app/variables";
import Header from "components/Global/Header";
import LoadingCard from "components/Global/LoadingCard";
import "css/Sessions.css";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { fromDB } from "helpers/date";
import { addOutline, closeCircle, watchOutline } from "ionicons/icons";
import { checkAvailableCoachesByDateTime, checkAvailablePrivateCoach } from "pages/GroupClasses/checkAvailableCoaches";
import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";
import AddGearModal from "./AddGearModal";
import axios from 'axios';

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const SessionDetails: React.FC = () => {
    const [message, setMessage] = useState("");
    const { id } = useParams<{ id: string }>();
    const todaySession = useSelector(selectLoadedSessionById(id));
    const [viewUsr, setViewUsr] = useState({ image: "", realName: "" });
    const [reconfirmModal, setReconfirmModal] = useState(false);
    const [targetUser, setTargetUser] = useState<Customer | null>(null);
    const [confirmRemoveModal, setConfirmRemoveModal] = useState(false);
    const [removeUser, setRemoveUser] = useState<Customer | null>(null);
    const [session, setSession] = useState<Session | undefined | null>(todaySession);
    const [connectReader, setConnectReader] = useState(false);
    const [userStorage, setUserStorage] = useState<GearStorage[]>([]);
    const [validAccess, setValidAccess] = useState<IAccess[]>([]);
    const history = useHistory();
    const dispatch = useDispatch();
    const wristbandRedux = useSelector((state: RootState) => state.wristband);
    const wristbandId = wristbandRedux.wristbandId;
    const connectToScanner = wristbandRedux.connectToScanner;

    // coach state
    const allCoaches = useAppSelector(selectAllCoaches);
    const [availableCoaches, setAvailableCoaches] = useState<Coach[]>([]);
    const [selectedCoachId, setSelectedCoachId] = useState("");
    const [updatingCoaches, setUpdatingCoaches] = useState(false);

    // confirmation email resend
    const [confirmResend, setConfirmResend] = useState(false);
    const [resending, setResending] = useState('idle');
    const handleResendConfirmEmail = async () => {
        if(!session){
            setResending('noSession');
            return
        }
        setConfirmResend(false);
        setResending('resending');
        const res = await axios.post(`${process.env.REACT_APP_Firebase_apiUrl}/resendConfirmationEmail`, {
            session
        });
        if(res.status !== 200){
            setResending('failed');
        }else{
            setResending('success');
        }
    }

    useEffect(() => {
        if (!session || session.sessionType !== "privateClass" || !session.start) {
            return;
        }
        const updateAvailableCoaches = async () => {
            setUpdatingCoaches(true);
            if (!session.start || !session.end) {
                return;
            }
            if (session.playType === "ski" || session.playType === "sb") {
                const duration = dayjs.tz(session.end).subtract(1, "minute").diff(dayjs.tz(session.start), "hour");
                const coaches: Coach[] = await checkAvailableCoachesByDateTime(session.start, session.playType, duration);
                const counters: SessionsTypes.MonthCounters | null = await getCounterByDateV2(session.sessionDate);
                if (!counters) {
                    return;
                }
                const checkedCoaches = checkAvailablePrivateCoach(coaches, session, counters);
                setUpdatingCoaches(false);
                setAvailableCoaches(checkedCoaches);
            }
        };
        updateAvailableCoaches();
    }, [session, allCoaches]);

    // add gear modal state
    const [showAddGearModal, setShowAddGearModal] = useState(false);
    const [savingCoach, setSavingCoach] = useState("idle");

    const getBooking = useCallback(async () => {
        const res = await getBookingById(id);
        // console.log(`res.exists()`, res.exists());
        if (res.exists()) {
            const data = fromDB(res.data());
            setSession({ id, ...data } as Session);
        } else {
            setSession(null);
        }
    }, [id]);

    useEffect(() => {
        getBooking();
    }, [id, getBooking]);

    const handleUpdateCoach = async () => {
        if (!selectedCoachId) {
            return;
        }
        const selectedCoach = availableCoaches.find((x) => x.id === selectedCoachId);
        if (!selectedCoach || !session) {
            return;
        }
        setSavingCoach("saving");
        const res = await updateSessionCoach(selectedCoach, session);
        if (res === "success") {
            setSavingCoach("saved");
            getBooking();
        } else {
            setSavingCoach("idle");
        }
    };

    const alertButtons = [
        {
            text: "Cancel",
            role: "cancel",
            handler: () => setReconfirmModal(false),
        },
        {
            text: "Confirm",
            role: "confirm",
            handler: async () => {
                dispatch(setIsLoading(true));
                await verifyUser(targetUser!, session!.id);
                setReconfirmModal(false);
                setTargetUser(null);
                await getBooking();
                dispatch(setIsLoading(false));
            },
        },
    ];
    const removeButtons = [
        {
            text: "Cancel",
            role: "cancel",
            handler: () => setConfirmRemoveModal(false),
        },
        {
            text: "Confirm",
            role: "confirm",
            handler: async () => {
                dispatch(setIsLoading(true));
                await removeUserFromSession(session!, removeUser!.id);
                setConfirmRemoveModal(false);
                setRemoveUser(null);
                await getBooking();
                dispatch(setIsLoading(false));
            },
        },
    ];

    const handleWristband = useCallback(
        async (nfcId: string) => {
            if (!connectReader || !targetUser || !session || !validAccess.length) return;
            const circletObj: ICircletData = {
                customerId: targetUser.id,
                sessionId: session.id,
                access: validAccess,
            };
            await setCircletData(nfcId, circletObj).then((e) => {
                if (e === "success") {
                    setConnectReader(false);
                    setTargetUser(null);
                    dispatch(setWristbandId(""));
                } else {
                    setMessage(e);
                }
            });
        },
        [connectReader, session, targetUser, dispatch, validAccess]
    );

    const completeSession = async (boolean: boolean) => {
        dispatch(setIsLoading(true));
        try {
            await setSessionIsCheckedIn(session!.id, boolean).then((e) => {
                history.push(`/sessions/upcoming`);
            });
        } catch (error) {
            console.error(error);
        } finally {
            dispatch(setIsLoading(false));
        }
    };

    useEffect(() => {
        if (wristbandId) {
            handleWristband(wristbandId.trim());
        }
    }, [handleWristband, wristbandId]);

    const getCStorage = useCallback(async () => {
        const userGear: GearStorage[] = [];
        if (session?.checkedInUsers) {
            for (let i = 0; i < session.checkedInUsers.length; i++) {
                const res = await getCustomerStorage(session.checkedInUsers[i].email);
                for (let j = 0; j < res.length; j++) {
                    userGear.push(res[j]);
                }
            }
            setUserStorage(userGear);
        }
    }, [session]);

    const determineAccessArea = useCallback(async (session: Session) => {
        const snowArr: PlayType[] = ["sb", "ski", "snow"];
        if (session.playType === "surf") {
            setValidAccess([{ area: ["surf"], start: session.start!, end: session.end! }]);
        }
        if (session.playType === "all") {
            setValidAccess([{ area: ["snow1", "snow2", "surf"], start: session.start!, end: session.end! }]);
        }
        if (snowArr.includes(session.playType)) {
            if (session.sessionType === "privateBelt") {
                setValidAccess([{ area: ["snow1"], start: session.start!, end: session.end! }]);
            } else if (session.sessionType === "fullSnow") {
                setValidAccess([{ area: ["snow1", "snow2"], start: session.start!, end: session.end! }]);
            } else {
                const access = await checkPrivateSession(session.start!, session.end!);
                if (access) {
                    setValidAccess(access);
                }
            }
        }
    }, []);

    useEffect(() => {
        if (session) {
            getCStorage();
            determineAccessArea(session);
        } else {
            setUserStorage([]);
            setValidAccess([]);
        }
    }, [session, determineAccessArea, getCStorage]);

    return (
        <IonPage>
            <Header title={`Session Details`} back />
            <IonContent>
                {!session ? (
                    <IonCard>
                        <IonCardContent>
                            <IonSpinner name="dots" />
                        </IonCardContent>
                    </IonCard>
                ) : (
                    <>
                        <IonCard>
                            <IonCardContent>
                                <IonAccordionGroup value={`details`}>
                                    <IonAccordion value="details">
                                        <IonItem slot="header">
                                            Session #{session.bookingNumber}{" "}
                                            <IonChip color="tertiary" className="ion-margin-horizontal">
                                                {session.sessionType}
                                            </IonChip>
                                        </IonItem>
                                        <IonList slot="content">
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Date</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{dayjs(Number(session.start)).format(`DD MMM YY`)}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Time</p>
                                                </IonLabel>
                                                <IonLabel slot="end">
                                                    {dayjs(session.start).format(`HH:mm`)} - {dayjs(session.end).format(`HH:mm`)}
                                                </IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Session ID</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.id}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Player{session.pax > 1 ? `s` : ``}</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.pax}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Type</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.playType}</IonLabel>
                                            </IonItem>
                                        </IonList>
                                    </IonAccordion>
                                </IonAccordionGroup>
                            </IonCardContent>
                        </IonCard>

                        <IonCard>
                            <IonCardContent>
                                <UserAccordion user={session.bookingUser!} />
                            </IonCardContent>
                        </IonCard>

                        <IonCard>
                            <IonCardContent>
                                <IonAccordionGroup value={`gears`}>
                                    <IonAccordion value="gears">
                                        <IonItem slot="header">Gear Rentals</IonItem>
                                        <IonList slot="content">
                                            {session.gears.length <= 0 ? (
                                                <IonItem>
                                                    <IonLabel>No Rentals for this session.</IonLabel>
                                                </IonItem>
                                            ) : (
                                                session.gears.map((gear, idx) => (
                                                    <IonItem key={`gear-${idx}-id-${gear.gear.id}-size-${gear.size}-${gear.class}`}>
                                                        <IonText>
                                                            {`${gear.gear.name} ${gear.class.toUpperCase()} ${gear.size} (${gear.gear.unit.name})`}
                                                        </IonText>
                                                    </IonItem>
                                                ))
                                            )}
                                            <IonItem lines="none">
                                                <IonButton
                                                    onClick={() => {
                                                        setShowAddGearModal(true);
                                                    }}
                                                >
                                                    <IonIcon icon={addOutline} slot="start" />
                                                    <IonLabel>Add Gears</IonLabel>
                                                </IonButton>
                                            </IonItem>
                                        </IonList>
                                    </IonAccordion>
                                </IonAccordionGroup>
                            </IonCardContent>
                        </IonCard>

                        <IonCard>
                            <IonCardContent>
                                <IonAccordionGroup value={`players`}>
                                    <IonAccordion value="players">
                                        <IonItem slot="header">
                                            Checked In Users ({session.checkedInUsers.length}/{session.pax})
                                        </IonItem>
                                        <IonList slot="content">
                                            {session.checkedInUsers.length <= 0 ? (
                                                <IonItem>
                                                    <IonLabel>No Player checked in yet.</IonLabel>
                                                </IonItem>
                                            ) : (
                                                session.checkedInUsers.map((user) => (
                                                    <IonItem key={`session-${session.id}-player-${user.id}`}>
                                                        <IonThumbnail
                                                            style={{ cursor: "pointer" }}
                                                            slot="start"
                                                            onClick={() => setViewUsr({ image: user.realImage, realName: user.realName })}
                                                        >
                                                            <IonImg src={user.realImage} />
                                                        </IonThumbnail>
                                                        <IonLabel>
                                                            <h3>{user.realName + " " + user.phone}</h3>
                                                            <p>Member No. {user.memberId}</p>
                                                        </IonLabel>
                                                        <IonButtons slot="end">
                                                            {!user.isVerified ? (
                                                                <IonChip
                                                                    color={`danger`}
                                                                    onClick={() => {
                                                                        setTargetUser(user);
                                                                        setReconfirmModal(true);
                                                                    }}
                                                                >
                                                                    <IonIcon icon={closeCircle} />
                                                                    <IonLabel>Not Verified</IonLabel>
                                                                </IonChip>
                                                            ) : (
                                                                <IonChip
                                                                    color={`warning`}
                                                                    onClick={() => {
                                                                        if (connectToScanner) {
                                                                            setTargetUser(user);
                                                                            setConnectReader(true);
                                                                        } else {
                                                                            setMessage("Please connect to scanner");
                                                                        }
                                                                    }}
                                                                >
                                                                    <IonIcon icon={watchOutline} />
                                                                    <IonLabel>Give Wristband</IonLabel>
                                                                </IonChip>
                                                            )}
                                                            {!session.isCheckedIn && (
                                                                <IonChip
                                                                    color={`danger`}
                                                                    onClick={() => {
                                                                        setRemoveUser(user);
                                                                        setConfirmRemoveModal(true);
                                                                    }}
                                                                >
                                                                    <IonIcon icon={closeCircle} />
                                                                    <IonLabel>Remove from session</IonLabel>
                                                                </IonChip>
                                                            )}
                                                        </IonButtons>
                                                    </IonItem>
                                                ))
                                            )}
                                        </IonList>
                                    </IonAccordion>
                                </IonAccordionGroup>
                            </IonCardContent>
                        </IonCard>

                        {session.sessionType === "privateClass" ? (
                            updatingCoaches ? (
                                <LoadingCard />
                            ) : (
                                <IonCard>
                                    <IonCardContent>
                                        <IonAccordionGroup value={`ptCoach`}>
                                            <IonAccordion value="ptCoach">
                                                <IonItem slot="header">Private Class Coach</IonItem>
                                                <IonList slot="content">
                                                    {session.privateCoach ? (
                                                        <IonItem>
                                                            <IonLabel>
                                                                <h3>{session.privateCoach.usrname}</h3>
                                                            </IonLabel>
                                                        </IonItem>
                                                    ) : (
                                                        <>
                                                            <IonItem>
                                                                <IonLabel>Select Coach</IonLabel>
                                                                <IonSelect
                                                                    value={
                                                                        availableCoaches.find((x) => x.id === selectedCoachId)
                                                                            ? availableCoaches.find((x) => x.id === selectedCoachId)?.id
                                                                            : null
                                                                    }
                                                                    onIonChange={(e) => {
                                                                        console.log(`coach id`, e.detail.value);
                                                                        setSelectedCoachId(e.detail.value);
                                                                    }}
                                                                >
                                                                    <IonSelectOption value={null}>No Coach Assigned</IonSelectOption>
                                                                    {availableCoaches.map((x) => (
                                                                        <IonSelectOption key={`available-coach-option-${x.id}`} value={x.id}>
                                                                            {x.usrname}
                                                                        </IonSelectOption>
                                                                    ))}
                                                                </IonSelect>
                                                            </IonItem>
                                                            <IonItem lines="none">
                                                                <IonButton
                                                                    disabled={
                                                                        availableCoaches.find((x) => x.id === selectedCoachId) === undefined ||
                                                                        availableCoaches.find((x) => x.id === selectedCoachId) === null
                                                                    }
                                                                    onClick={() => {
                                                                        handleUpdateCoach();
                                                                    }}
                                                                >
                                                                    Update
                                                                </IonButton>
                                                            </IonItem>
                                                        </>
                                                    )}
                                                </IonList>
                                            </IonAccordion>
                                        </IonAccordionGroup>
                                    </IonCardContent>
                                </IonCard>
                            )
                        ) : null}

                        {!!userStorage.length && (
                            <IonCard>
                                <IonCardContent>
                                    <IonAccordionGroup value={`storage`}>
                                        <IonAccordion value="storage">
                                            <IonItem slot="header">User Storage</IonItem>
                                            <IonList slot="content">
                                                {userStorage.map((i, idx) => (
                                                    <IonItem key={idx}>{i.storageNum}</IonItem>
                                                ))}
                                            </IonList>
                                        </IonAccordion>
                                    </IonAccordionGroup>
                                </IonCardContent>
                            </IonCard>
                        )}
                        <IonRow className="ion-padding">
                            <IonCol>
                                <IonButton
                                    expand={`block`}
                                    onClick={() => {
                                        setConfirmResend(true);
                                    }}
                                >
                                    <IonLabel>Re-send Confirmation Email</IonLabel>
                                </IonButton>
                                {!session.isCheckedIn ? (
                                    <IonButton
                                        style={{ width: "100%" }}
                                        disabled={session.checkedInUsers.length < session.pax}
                                        onClick={() => completeSession(true)}
                                    >
                                        {"CONFIRM ALL GUEST(S) CHECKED IN"}
                                    </IonButton>
                                ) : (
                                    <IonButton
                                        style={{ width: "100%" }}
                                        disabled={session.checkedInUsers.length < session.pax}
                                        onClick={() => completeSession(false)}
                                    >
                                        {"Change booking back to 'PENDING'"}
                                    </IonButton>
                                )}
                            </IonCol>
                        </IonRow>
                    </>
                )}
            </IonContent>
            <IonAlert
                isOpen={confirmResend}
                onDidDismiss={() => {
                    setConfirmResend(false);
                }}
                header={`Confirm Resend?`}
                buttons={[
                    {
                        text: `Resend`,
                        handler: () => {
                            handleResendConfirmEmail()
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]}
            />
            <IonLoading
                isOpen={resending === 'resending'}
                spinner={'dots'}
                message={`Resending`}
            />
            <IonAlert
                isOpen={resending === 'noSession'}
                onDidDismiss={() => {
                    setResending('idle');
                }}
                header={`Resend Failed.`}
                message={`The session data seems to be missing, please refresh and retry.`}
                buttons={[
                    {
                        text: 'OK',
                        role: 'cancel'
                    }
                ]}
            />
            <IonAlert
                isOpen={resending === 'failed'}
                onDidDismiss={() => {
                    setResending('idle');
                }}
                header={`Resend Failed.`}
                message={`Something is wrong with the resending, please refresh and retry.`}
                buttons={[
                    {
                        text: 'OK',
                        role: 'cancel'
                    }
                ]}
            />
            <IonAlert
                isOpen={resending === 'success'}
                onDidDismiss={() => {
                    setResending('idle');
                }}
                header={`Resend Success.`}
                buttons={[
                    {
                        text: 'OK',
                        role: 'cancel'
                    }
                ]}
            />
            <IonModal isOpen={!!viewUsr.image || !!viewUsr.realName} onDidDismiss={() => setViewUsr({ image: "", realName: "" })}>
                <IonContent>
                    <IonGrid>
                        <IonItem>
                            <IonTitle>{"User real data"} </IonTitle>
                        </IonItem>
                        <IonRow>
                            <IonCol>
                                <IonImg src={viewUsr.image} />
                            </IonCol>
                        </IonRow>
                        <IonRow>
                            <IonCol style={{ textAlign: "center" }}>{viewUsr.realName}</IonCol>
                        </IonRow>
                        <IonRow>
                            <IonCol size="12">
                                <IonButton
                                    color="medium"
                                    size={"large"}
                                    style={{ width: "100%" }}
                                    onClick={() => setViewUsr({ image: "", realName: "" })}
                                >
                                    BACK
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonGrid>
                </IonContent>
            </IonModal>
            <IonModal isOpen={connectReader}>
                <IonHeader>
                    <IonToolbar>
                        <IonTitle>Wristband connector</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    <IonGrid className="qrModalContent">
                        <IonRow>
                            {!!validAccess.length && validAccess.some((i) => i.area.toString() === "snow2") && (
                                <>
                                    <IonCol size="12" className="ion-text-center">
                                        <IonLabel color="danger">Detected private belt session overlapping this session</IonLabel>
                                    </IonCol>
                                    {validAccess.map((i) => (
                                        <>
                                            <IonCol size="6">
                                                <IonLabel style={{ textTransform: "capitalize" }}>{`Access area: ${i.area}`}</IonLabel>
                                            </IonCol>
                                            <IonCol size="6" className="ion-text-end">
                                                <IonLabel>{`${dayjs.tz(i.start).format(`HH:mm`)} - ${dayjs.tz(i.end).format(`HH:mm`)}`}</IonLabel>
                                            </IonCol>
                                        </>
                                    ))}
                                </>
                            )}
                        </IonRow>
                        <IonRow className="ion-padding">
                            <IonCol size="12" className="flex-center">
                                <IonSpinner />
                            </IonCol>
                            <IonCol size="12" className="flex-center">
                                Please place the wristband on the scanner
                            </IonCol>
                        </IonRow>
                        <IonRow>
                            <IonCol size="12">
                                <IonButton color="medium" size={"large"} style={{ width: "100%" }} onClick={() => setConnectReader(false)}>
                                    BACK
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonGrid>
                </IonContent>
            </IonModal>
            <IonAlert
                isOpen={reconfirmModal}
                header="Alert"
                onDidDismiss={() => {}}
                message={`Confirm to veryify ${targetUser?.realName}?`}
                buttons={alertButtons}
            />
            <IonAlert
                isOpen={confirmRemoveModal}
                header="Alert"
                onDidDismiss={() => {}}
                message={`Confirm to remove ${removeUser?.realName}?`}
                buttons={removeButtons}
            />
            <AddGearModal
                isOpen={showAddGearModal}
                onDismiss={() => {
                    setShowAddGearModal(false);
                }}
                onSaved={() => {
                    setShowAddGearModal(false);
                    getBooking();
                }}
                sessionRaw={session}
            />
            <IonLoading isOpen={savingCoach === "saving"} spinner={"dots"} message={"Saving Coach"} />
            <IonAlert
                isOpen={savingCoach === "saved"}
                onDidDismiss={() => {
                    setSavingCoach("idle");
                }}
                header={`Coach Assigned`}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            setSavingCoach("idle");
                        },
                    },
                ]}
            />
            <IonAlert
                isOpen={!!message}
                header={`System Message`}
                message={message}
                onDidDismiss={() => setMessage("")}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
};

export default SessionDetails;

interface UserAccordionProps {
    headerText?: string;
    user: Customer;
}

export const UserAccordion = (props: UserAccordionProps) => {
    const { headerText = "Booking User" } = props;
    return (
        <IonAccordionGroup>
            <IonAccordion>
                <IonItem slot="header">{headerText}</IonItem>
                <IonList slot="content">
                    <IonItem>
                        <IonLabel>
                            <p>Name</p>
                        </IonLabel>
                        <IonLabel slot="end">{props.user.realName}</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel>
                            <p>Member ID</p>
                        </IonLabel>
                        <IonLabel slot="end">{props.user.memberId}</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel>
                            <p>Email</p>
                        </IonLabel>
                        <IonLabel slot="end">{props.user.email}</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel>
                            <p>Phone</p>
                        </IonLabel>
                        <IonLabel slot="end">{props.user.phone}</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel>
                            <p>ID</p>
                        </IonLabel>
                        <IonLabel slot="end">{props.user.id}</IonLabel>
                    </IonItem>
                </IonList>
            </IonAccordion>
        </IonAccordionGroup>
    );
};

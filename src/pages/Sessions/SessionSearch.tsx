import {
    IonAccordion,
    IonAccordionGroup,
    IonAlert,
    IonButton,
    IonCard,
    IonCol,
    IonContent,
    IonGrid,
    IonItem,
    IonPage,
    IonRow,
    IonSearchbar,
    IonSegment,
    IonSegmentButton,
} from "@ionic/react";
import { Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { getBookingByBookingNumber, getBookingByPhoneNumber } from "app/firebase";
import { setQueriedSessions } from "app/slices/sessionsSlice";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SessionRow } from "./UpcomingList";
import { singleAlertBtn } from "app/variables";

type SearchType = "Booking number" | "Phone";

function SessionSearch() {
    const [tab, setTab] = useState<SearchType>("Booking number");
    const [phone, setPhone] = useState("");
    const [bookingNumber, setBookingNumber] = useState("");
    const [message, setMessage] = useState("");
    const searchBar = "searchBar";
    const dispatch = useDispatch();
    const searchResult = useSelector((state: RootState) => state.sessions.queriedSessions);
    const handleSearch = async () => {
        if (tab === "Booking number") {
            const res = await getBookingByBookingNumber(bookingNumber);
            if (res !== "success") setMessage(res);
        }
        if (tab === "Phone") {
            const res = await getBookingByPhoneNumber(phone);
            if (res !== "success") setMessage(res);
        }
    };
    const handleClearSearch = () => {
        setPhone("");
        setBookingNumber("");
        dispatch(setQueriedSessions([]));
    };

    return (
        <IonPage>
            <Header title={`Search Session`} />
            <IonContent color="light">
                <IonCard>
                    <IonAccordionGroup value={searchBar}>
                        <IonAccordion value="searchBar">
                            <IonItem slot="header">{"SEARCH"}</IonItem>
                            <IonGrid slot="content">
                                <IonRow>
                                    <IonCol />
                                    <IonCol size="8">
                                        <IonSegment
                                            value={tab}
                                            onIonChange={(e) => {
                                                handleClearSearch();
                                                //@ts-ignore
                                                setTab(e.detail.value!);
                                            }}
                                        >
                                            <IonSegmentButton value="Booking number">Booking number</IonSegmentButton>
                                            <IonSegmentButton value="Phone">Booking user phone number</IonSegmentButton>
                                        </IonSegment>
                                    </IonCol>
                                    <IonCol />
                                </IonRow>
                                <br />
                                <IonRow>
                                    <IonCol>
                                        {tab === "Booking number" ? (
                                            <IonSearchbar
                                                animated
                                                placeholder={"Booking number"}
                                                type={"tel"}
                                                value={bookingNumber}
                                                onIonChange={(e) => setBookingNumber(e.detail.value!)}
                                            />
                                        ) : (
                                            <IonSearchbar
                                                animated
                                                placeholder={"Booking user's phone number"}
                                                type={"tel"}
                                                value={phone}
                                                onIonChange={(e) => setPhone(e.detail.value!)}
                                            />
                                        )}
                                    </IonCol>
                                </IonRow>
                                <IonRow>
                                    <IonCol size="6">
                                        <IonButton
                                            disabled={tab === "Booking number" ? !bookingNumber : !phone}
                                            style={{ width: "100%" }}
                                            onClick={handleSearch}
                                        >
                                            Search
                                        </IonButton>
                                    </IonCol>
                                    <IonCol size="6">
                                        <IonButton color="danger" style={{ width: "100%" }} onClick={handleClearSearch}>
                                            Clear All
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </IonAccordion>
                    </IonAccordionGroup>
                </IonCard>
                {searchResult.map((i: Session) => (
                    <SessionRow key={i.id} session={i} isQuery />
                ))}
            </IonContent>
            <IonAlert
                isOpen={!!message}
                header={`System Message`}
                message={message}
                onDidDismiss={() => setMessage("")}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default SessionSearch;

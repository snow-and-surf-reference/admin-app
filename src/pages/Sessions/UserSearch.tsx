import {
    IonAccordion,
    IonAccordionGroup,
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCheckbox,
    IonChip,
    IonCol,
    IonContent,
    IonFooter,
    IonGrid,
    IonHeader,
    IonIcon,
    IonImg,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonModal,
    IonPage,
    IonRadio,
    IonRow,
    IonSearchbar,
    IonSegment,
    IonSegmentButton,
    IonSpinner,
    IonTitle,
    IonToolbar,
    IonLoading
} from "@ionic/react";
import { GearStorage } from "@tsanghoilun/snow-n-surf-interface/types/gear";
import { PaymentMethod } from "@tsanghoilun/snow-n-surf-interface/types/global";
import { Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { TopupOrder, TopUpPackages } from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import {
    addStorageToUser,
    getCustomerStorage,
    getMainAccByMemberId,
    getMainAccByPhone,
    getSessionByCustomerId,
    getSubAccountsByEmailOrPhone,
    getTopupOrdersByCustomerId,
    getUserByEmail,
    removeStorageById,
    topUpUserByStaff,
    updateCustomerPartnerFlag,
    verifyUserEmailManually
} from "app/firebase";
import { useAppSelector } from "app/hooks";
import { useSearchParams } from "app/hooks/useSearchParams";
import { selectAuthUser } from "app/slices/authSlice";
import { setIsLoading } from "app/slices/globalSlice";
import { selectPricing } from "app/slices/pricingSlice";
import { setQueriedSessions } from "app/slices/sessionsSlice";
import { clearSearch, setCustomer, setMobileSearchResults, setParentAccount, setSubAccounts } from "app/slices/usersSlice";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import StorageWaiver from "components/StorageWaiver";
import "css/UserSearchModal.css";
import dayjs from "dayjs";
import { formatDate, formatTime } from "helpers/date";
import { personSharp } from "ionicons/icons";
import { useCallback, useEffect, useRef, useState } from "react";
import { CSVLink } from "react-csv";
import { Data } from "react-csv/components/CommonPropTypes";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import EditUserModal from "./EditUserModal";
import EditUserWalletModal from "./EditUserWalletModal";

type SearchType = "Phone number" | "Email" | "Member ID";
type ModalType = "topUp" | "disableUser" | "storage" | "subAcc";

function UserSearch() {
    const [tab, setTab] = useState<SearchType>("Phone number");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [memberID, setMemberID] = useState("");
    const [selected, setSelected] = useState<TopUpPackages | null | "custom">(null);
    const [payment, setPayment] = useState<PaymentMethod | "">("");
    const [openModal, setOpenModal] = useState<ModalType | null>(null);
    const [userStorage, setUserStorage] = useState<GearStorage[]>([]);
    const [storageDelTarget, setStorageDelTarget] = useState("");
    const [agreementModal, setAgreementModal] = useState(false);
    const [agreed, setAgreed] = useState(false);
    const [message, setMessage] = useState("");
    const [historyModal, setHistoryModal] = useState(false);
    const [customTU, setCustomTU] = useState<number | undefined>(undefined);
    const history = useHistory();
    const dispatch = useDispatch();
    const users = useSelector((state: RootState) => state.users);
    const searchResult = users.parentAccountUser;
    const mobileResults = users.mobileSearchResults;
    const subAccounts = useSelector((state: RootState) => state.users.subAccounts);
    const tuPackages = useSelector((state: RootState) => state.packages.packages);
    const userSessions = useSelector((state: RootState) => state.sessions.queriedSessions);
    const historyRef = useRef<any>(null);
    const authUser = useAppSelector(selectAuthUser);
    const pricesRaw = useAppSelector(selectPricing);
    const [editUser, setEditUser] = useState<Customer | null>(null);
    const [verifying, setVerifying] = useState('idle');

    // get search params (for V2 booking usage)
    const searchParams = useSearchParams();
    const [useV2, setUseV2] = useState(false);
    useEffect(() => {
        searchParams.forEach((v, n) => {
            // console.log(n,v);
            if (n === "test" && v === "2") {
                setUseV2(true);
            }
        });
    }, [searchParams]);

    // new stuff for edit balance modal
    const [showEditBalanceModal, setShowEditBalanceModal] = useState(false);

    // topup history modal
    const [showTopupHistoryModal, setShowTopupHistoryModal] = useState(false);
    const [userTopups, setUserTopups] = useState<TopupOrder[] | null>(null);

    const handleSearch = useCallback(async () => {
        if (tab === "Phone number") {
            const res = await getMainAccByPhone(phone);
            if (res !== "success") setMessage(res);
        }
        if (tab === "Email") {
            const res = await getUserByEmail(email, true);
            if (res !== "success") setMessage(res);
        }
        if (tab === "Member ID") {
            const res = await getMainAccByMemberId(memberID);
            if (res !== "success") setMessage(res);
        }
    }, [tab, email, memberID, phone]);

    const handleClearSearch = useCallback(() => {
        setEmail("");
        setPhone("");
        setMemberID("");
        dispatch(setCustomer(null));
        dispatch(setParentAccount(null));
        dispatch(setSubAccounts([]));
        dispatch(setMobileSearchResults(null));
    }, [dispatch]);

    const handleClearSubs = useCallback(() => {
        dispatch(setSubAccounts([]));
    }, [dispatch]);

    const handleCancel = () => {
        setSelected(null);
        setPayment("");
        setCustomTU(undefined);
        setOpenModal(null);
        setUserStorage([]);
        setAgreementModal(false);
        setAgreed(false);
        setHistoryModal(false);
        dispatch(setQueriedSessions([]));
    };

    const getStorage = useCallback(async () => {
        if (!searchResult) return;
        await getCustomerStorage(searchResult.email).then((e) => {
            setUserStorage(e);
        });
    }, [searchResult]);

    useEffect(() => {
        if (searchResult) {
            dispatch(setCustomer(searchResult));
            getSubAccountsByEmailOrPhone(searchResult.email, "email");
        }
        if (searchResult && openModal === "storage") {
            getStorage();
        }
    }, [searchResult, openModal, getStorage, dispatch]);

    const addStorage = useCallback(
        async (customerId: string, customerEmail: string) => {
            if (!pricesRaw) return;
            await addStorageToUser(customerId, customerEmail, pricesRaw.storage.price).then((e) => {
                if (agreementModal) {
                    setAgreementModal(false);
                    setOpenModal(null);
                }
                if (e !== "success") {
                    setMessage(e);
                }
            });
        },
        [agreementModal, pricesRaw]
    );

    const handleAddStorage = () => {
        if (!!userStorage.length && !!pricesRaw) {
            setMessage(`Please confirm storage fee of $${pricesRaw.storage.price} is received`);
        } else {
            setAgreementModal(true);
            setAgreed(false);
        }
    };

    const handleTopUp = useCallback(async () => {
        if (!payment || !searchResult || (selected === "custom" && !customTU) || !selected) return;
        await topUpUserByStaff(searchResult, payment, customTU ? customTU : 0, selected !== "custom" ? selected : undefined).then((e) => {
            if (e === "success") {
                setMessage("Top up successful");
                handleSearch();
            } else {
                setMessage(e);
            }
        });
    }, [searchResult, selected, payment, customTU, handleSearch]);

    const handleSearchSessions = useCallback(
        async (customer?: Customer) => {
            if (!searchResult && !customer) return;
            dispatch(setIsLoading(true));
            const res = await getSessionByCustomerId(searchResult ? searchResult.id : customer!.id);
            if (res === "success") {
                setHistoryModal(true);
            } else {
                setMessage(res);
            }
            dispatch(setIsLoading(false));
        },
        [dispatch, searchResult]
    );

    const printSessionHistory = useCallback((history: Session[]) => {
        let tempHistory: Data = [];
        history.forEach((i) => {
            const { createdAt, pax, bookingStaff, status, cancelledAt, start, end, gears, playType, totalCredits, sessionType } = i;
            tempHistory.push({
                playType,
                sessionType,
                sessionDate: formatDate(start!),
                time: `${formatTime(start!)} - ${formatTime(end!)}`,
                pax,
                gears: gears.length ? "YES" : "NO",
                totalCredits,
                bookedByStaff: bookingStaff ? bookingStaff.email : "NO",
                cancelled: cancelledAt ? formatDate(cancelledAt) : "NO",
                status,
                bookingDate: formatDate(createdAt),
            });
        });
        return tempHistory;
    }, []);

    const [disablePartnerClick, setDisablePartnerClick] = useState(false);
    const handleUpdatePartnerCustomer = async (userId: string, memberId: string, newStatus: boolean) => {
        setDisablePartnerClick(true);
        await updateCustomerPartnerFlag(userId, newStatus);
        await getMainAccByMemberId(memberId);
        setDisablePartnerClick(false);
    };

    useEffect(() => {
        return () => {
            // handleClearSearch();
            handleClearSubs();
        };
    }, [handleClearSubs]);

    const handleGetTopups = useCallback(
        async (customer?: Customer) => {
            if (!searchResult && !customer) return;
            dispatch(setIsLoading(true));
            const res = await getTopupOrdersByCustomerId(searchResult ? searchResult.id : customer!.id);
            if (res.topups) {
                console.log("topups:", res.topups);
                setUserTopups(res.topups);
                setShowTopupHistoryModal(true);
            } else {
                setMessage(res.message);
            }
            dispatch(setIsLoading(false));
        },
        [dispatch, searchResult]
    );

    const handleVerify = async () => {
        if(!searchResult){
            setVerifying('error');
            return
        };
        setVerifying('verifying');
        const res = await verifyUserEmailManually(searchResult.id);
        setVerifying(res);
        handleSearch();
    }

    return (
        <IonPage>
            <Header title={`Customer Search`} />
            <IonContent color="light">
                <IonCard>
                    <IonAccordionGroup value={"searchBar"}>
                        <IonAccordion value="searchBar">
                            <IonItem slot="header">{"SEARCH"}</IonItem>
                            <IonGrid slot="content">
                                <IonRow>
                                    <IonCol />
                                    <IonCol size="8">
                                        <IonSegment
                                            value={tab}
                                            onIonChange={(e) => {
                                                handleClearSearch();
                                                //@ts-ignore
                                                setTab(e.detail.value!);
                                            }}
                                        >
                                            <IonSegmentButton value="Phone number">Phone number</IonSegmentButton>
                                            <IonSegmentButton value="Email">Email</IonSegmentButton>
                                            <IonSegmentButton value="Member ID">Member ID</IonSegmentButton>
                                        </IonSegment>
                                    </IonCol>
                                    <IonCol />
                                </IonRow>
                                <br />
                                <IonRow>
                                    <IonCol>
                                        {tab === "Phone number" ? (
                                            <IonSearchbar
                                                animated
                                                placeholder={"Customer's phone number"}
                                                type={"tel"}
                                                value={phone}
                                                onIonChange={(e) => setPhone(e.detail.value!)}
                                                onKeyDown={(e) => {
                                                    if (e.key === "Enter") handleSearch();
                                                }}
                                            />
                                        ) : tab === "Email" ? (
                                            <IonSearchbar
                                                animated
                                                placeholder={"Customer's email address"}
                                                type={"email"}
                                                value={email}
                                                onIonChange={(e) => setEmail(e.detail.value!)}
                                                onKeyDown={(e) => {
                                                    if (e.key === "Enter") handleSearch();
                                                }}
                                            />
                                        ) : (
                                            <IonSearchbar
                                                animated
                                                placeholder={"Customer's member ID"}
                                                type={"text"}
                                                value={memberID}
                                                onIonChange={(e) => setMemberID(e.detail.value!)}
                                                onKeyDown={(e) => {
                                                    if (e.key === "Enter") handleSearch();
                                                }}
                                            />
                                        )}
                                    </IonCol>
                                </IonRow>
                                <IonRow>
                                    <IonCol size="6">
                                        <IonButton
                                            disabled={tab === "Phone number" ? !phone : tab === "Email" ? !email : !memberID}
                                            style={{ width: "100%" }}
                                            onClick={handleSearch}
                                        >
                                            Search
                                        </IonButton>
                                    </IonCol>
                                    <IonCol size="6">
                                        <IonButton color="danger" style={{ width: "100%" }} onClick={handleClearSearch}>
                                            Clear All
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </IonAccordion>
                    </IonAccordionGroup>
                </IonCard>
                {mobileResults &&
                    !!mobileResults.length &&
                    mobileResults.map((searchResult) => (
                        <IonCard key={searchResult.id}>
                            <IonGrid>
                                <IonRow>
                                    <IonCol
                                        size="4"
                                        style={{ display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}
                                    >
                                        {searchResult.realImage ? (
                                            <IonImg src={searchResult.realImage} style={{ width: "100%" }} />
                                        ) : (
                                            <IonIcon icon={personSharp} style={{ width: "100%", height: "100%" }} />
                                        )}
                                    </IonCol>
                                    <IonCol>
                                        <IonList inset>
                                            <IonItem>
                                                <IonLabel>Member ID</IonLabel>
                                                <IonLabel slot="end">{searchResult.memberId}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Real name</IonLabel>
                                                <IonLabel slot="end">{searchResult.realName}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Phone</IonLabel>
                                                <IonLabel slot="end">{searchResult.phone}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Email</IonLabel>
                                                <IonLabel slot="end">{searchResult.email}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Email Verified?</IonLabel>
                                                <IonChip
                                                    slot="end"
                                                    color={searchResult.emailVerified ? "success" : "danger"}
                                                    onClick={
                                                        searchResult.emailVerified
                                                            ? () => {}
                                                            : () => {
                                                                  dispatch(setParentAccount(searchResult));
                                                                  setVerifying("confirming");
                                                              }
                                                    }
                                                >
                                                    {searchResult.emailVerified ? "Verified" : "Not Verified"}
                                                </IonChip>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>DOB</IonLabel>
                                                <IonLabel slot="end">{formatDate(searchResult.dob)}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Current wallet credit</IonLabel>
                                                <IonLabel
                                                    slot="end"
                                                    color={
                                                        pricesRaw?.enterance.price && searchResult.wallet.balance < pricesRaw?.enterance.price
                                                            ? "danger"
                                                            : "dark"
                                                    }
                                                >
                                                    {searchResult.wallet.balance}
                                                </IonLabel>
                                                {authUser?.role === "admin" ? (
                                                    <IonButtons
                                                        slot="end"
                                                        onClick={() => {
                                                            setShowEditBalanceModal(true);
                                                        }}
                                                    >
                                                        <IonButton color={"danger"} size="small" fill="solid">
                                                            Edit Balance
                                                        </IonButton>
                                                    </IonButtons>
                                                ) : null}
                                                <EditUserWalletModal
                                                    isOpen={showEditBalanceModal}
                                                    authCustomer={searchResult}
                                                    onDismiss={() => {
                                                        setShowEditBalanceModal(false);
                                                        handleSearch();
                                                    }}
                                                />
                                            </IonItem>
                                            {authUser?.role === "admin" ? (
                                                <IonItem>
                                                    <IonLabel>Partner Customer?</IonLabel>
                                                    <IonLabel slot="end">{searchResult.isPartner ? "YES" : "NO"}</IonLabel>
                                                </IonItem>
                                            ) : null}
                                            {authUser?.role === "admin" || authUser?.role === "manager" ? (
                                                <>
                                                    <IonItem>
                                                        <IonButton
                                                            onClick={() => {
                                                                setEditUser(searchResult);
                                                            }}
                                                        >
                                                            <IonLabel>Edit User Info</IonLabel>
                                                        </IonButton>
                                                    </IonItem>
                                                    <EditUserModal
                                                        user={editUser}
                                                        isOpen={!!editUser}
                                                        onDismiss={() => {
                                                            setEditUser(null);
                                                            handleSearch();
                                                        }}
                                                    />
                                                </>
                                            ) : null}
                                        </IonList>
                                    </IonCol>
                                </IonRow>
                                {searchResult.emergencyContact && (
                                    <IonAccordionGroup value="emergencyContact">
                                        <IonAccordion value="">
                                            <IonItem slot="header">
                                                <IonLabel>Emergency Contact</IonLabel>
                                            </IonItem>
                                            <IonGrid slot="content">
                                                <IonItem>
                                                    <IonLabel>Name</IonLabel>
                                                    <IonLabel slot="end">{searchResult.emergencyContact.name}</IonLabel>
                                                </IonItem>
                                                <IonItem>
                                                    <IonLabel>Phone</IonLabel>
                                                    <IonLabel slot="end">{searchResult.emergencyContact.phone}</IonLabel>
                                                </IonItem>
                                            </IonGrid>
                                            {/* {JSON.stringify(searchResult)} */}
                                        </IonAccordion>
                                    </IonAccordionGroup>
                                )}
                                <IonRow>
                                    <IonCol>
                                        <IonButton
                                            expand="block"
                                            onClick={() => {
                                                dispatch(setParentAccount(searchResult));
                                                setOpenModal("topUp");
                                            }}
                                        >
                                            Top up
                                        </IonButton>
                                    </IonCol>
                                    <IonCol>
                                        <IonButton
                                            expand="block"
                                            onClick={() => {
                                                dispatch(setIsLoading(true));
                                                dispatch(setParentAccount(searchResult));
                                                setTimeout(() => {
                                                    dispatch(setIsLoading(false));
                                                    history.push(`/newBooking`);
                                                }, 600);
                                            }}
                                        >
                                            Book session
                                        </IonButton>
                                        {useV2 ? (
                                            <IonButton
                                                expand="block"
                                                onClick={() => {
                                                    dispatch(setIsLoading(true));
                                                    dispatch(setParentAccount(searchResult));
                                                    setTimeout(() => {
                                                        dispatch(setIsLoading(false));
                                                        history.push(`/newBookingV2`);
                                                    }, 600);
                                                }}
                                            >
                                                Book session V2 (DO NOT USE)
                                            </IonButton>
                                        ) : null}
                                    </IonCol>
                                    <IonCol>
                                        <IonButton
                                            expand="block"
                                            onClick={() => {
                                                dispatch(setIsLoading(true));
                                                dispatch(setParentAccount(searchResult));
                                                setTimeout(() => {
                                                    dispatch(setIsLoading(false));
                                                    setOpenModal("storage");
                                                });
                                            }}
                                        >
                                            Storage
                                        </IonButton>
                                    </IonCol>
                                    <IonCol size="12">
                                        <IonButton expand="block" color="tertiary" onClick={() => handleSearchSessions(searchResult)}>
                                            Session history
                                        </IonButton>
                                    </IonCol>
                                    <IonCol size="12">
                                        <IonButton expand="block" color="primary" onClick={() => handleGetTopups(searchResult)}>
                                            Topup history
                                        </IonButton>
                                    </IonCol>
                                    {/* <IonCol size="12">
                                        <IonButton
                                            expand="block"
                                            color={searchResult.enabled ? "danger" : "success"}
                                            onClick={() => {
                                                dispatch(setParentAccount(searchResult));
                                                setOpenModal("disableUser");
                                            }}
                                        >
                                            {searchResult.enabled ? "Disable user" : "Enable user"}
                                        </IonButton>
                                    </IonCol> */}
                                    {authUser && authUser.role === "admin" ? (
                                        <IonCol size="12">
                                            <IonButton
                                                expand="block"
                                                color={searchResult.isPartner ? `danger` : `warning`}
                                                disabled={disablePartnerClick}
                                                onClick={() => {
                                                    handleUpdatePartnerCustomer(
                                                        searchResult.id,
                                                        searchResult.memberId,
                                                        searchResult.isPartner ? false : true
                                                    );
                                                }}
                                            >
                                                {disablePartnerClick ? (
                                                    <IonSpinner />
                                                ) : searchResult.isPartner ? (
                                                    `Disable as Partner User`
                                                ) : (
                                                    `Enable as Partner User`
                                                )}
                                            </IonButton>
                                        </IonCol>
                                    ) : null}
                                </IonRow>
                            </IonGrid>
                        </IonCard>
                    ))}
                {!mobileResults && searchResult && (
                    <IonCard>
                        <IonGrid>
                            <IonRow>
                                <IonCol>
                                    <IonButton expand="block" disabled={!subAccounts || !subAccounts.length} onClick={() => setOpenModal("subAcc")}>
                                        {!subAccounts || !subAccounts.length ? "No sub account" : "View sub account"}
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonCard>
                )}
                {!mobileResults && searchResult && (
                    <IonCard>
                        <IonGrid>
                            <IonRow>
                                <IonCol size="4" style={{ display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
                                    {searchResult.realImage ? (
                                        <IonImg src={searchResult.realImage} style={{ width: "100%" }} />
                                    ) : (
                                        <IonIcon icon={personSharp} style={{ width: "100%", height: "100%" }} />
                                    )}
                                </IonCol>
                                <IonCol>
                                    <IonList inset>
                                        <IonItem>
                                            <IonLabel>Member ID</IonLabel>
                                            <IonLabel slot="end">{searchResult.memberId}</IonLabel>
                                        </IonItem>
                                        <IonItem>
                                            <IonLabel>Real name</IonLabel>
                                            <IonLabel slot="end">{searchResult.realName}</IonLabel>
                                        </IonItem>
                                        <IonItem>
                                            <IonLabel>Phone</IonLabel>
                                            <IonLabel slot="end">{searchResult.phone}</IonLabel>
                                        </IonItem>
                                        <IonItem>
                                            <IonLabel>Email</IonLabel>
                                            <IonLabel slot="end">{searchResult.email}</IonLabel>
                                        </IonItem>
                                        <IonItem>
                                            <IonLabel>Email Verified?</IonLabel>
                                            <IonChip
                                                slot='end'
                                                color={searchResult.emailVerified ? 'success' : 'danger'}
                                                onClick={searchResult.emailVerified ? () => {} : () => {
                                                    setVerifying('confirming')
                                                }}
                                            >{searchResult.emailVerified ? 'Verified' : 'Not Verified'}</IonChip>
                                        </IonItem>
                                        <IonItem>
                                            <IonLabel>DOB</IonLabel>
                                            <IonLabel slot="end">{formatDate(searchResult.dob)}</IonLabel>
                                        </IonItem>
                                        <IonItem>
                                            <IonLabel>Current wallet credit</IonLabel>
                                            <IonLabel
                                                slot="end"
                                                color={
                                                    pricesRaw?.enterance.price && searchResult.wallet.balance < pricesRaw?.enterance.price
                                                        ? "danger"
                                                        : "dark"
                                                }
                                            >
                                                {searchResult.wallet.balance}
                                            </IonLabel>
                                            {authUser?.role === "admin" ? (
                                                <IonButtons
                                                    slot="end"
                                                    onClick={() => {
                                                        setShowEditBalanceModal(true);
                                                    }}
                                                >
                                                    <IonButton color={"danger"} size="small" fill="solid">
                                                        Edit Balance
                                                    </IonButton>
                                                </IonButtons>
                                            ) : null}
                                            <EditUserWalletModal
                                                isOpen={showEditBalanceModal}
                                                authCustomer={searchResult}
                                                onDismiss={() => {
                                                    setShowEditBalanceModal(false);
                                                    handleSearch();
                                                }}
                                            />
                                        </IonItem>
                                        {authUser?.role === "admin" ? (
                                            <IonItem>
                                                <IonLabel>Partner Customer?</IonLabel>
                                                <IonLabel slot="end">{searchResult.isPartner ? "YES" : "NO"}</IonLabel>
                                            </IonItem>
                                        ) : null}
                                        {authUser?.role === "admin" || authUser?.role === "manager" ? (
                                            <>
                                                <IonItem>
                                                    <IonButton
                                                        onClick={() => {
                                                            setEditUser(searchResult);
                                                        }}
                                                    >
                                                        <IonLabel>Edit User Info</IonLabel>
                                                    </IonButton>
                                                </IonItem>
                                                <EditUserModal
                                                    user={editUser}
                                                    isOpen={!!editUser}
                                                    onDismiss={() => {
                                                        setEditUser(null);
                                                        handleSearch();
                                                    }}
                                                />
                                            </>
                                        ) : null}
                                    </IonList>
                                </IonCol>
                            </IonRow>
                            {searchResult.emergencyContact && (
                                <IonAccordionGroup value="emergencyContact">
                                    <IonAccordion value="">
                                        <IonItem slot="header">
                                            <IonLabel>Emergency Contact</IonLabel>
                                        </IonItem>
                                        <IonGrid slot="content">
                                            <IonItem>
                                                <IonLabel>Name</IonLabel>
                                                <IonLabel slot="end">{searchResult.emergencyContact.name}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Phone</IonLabel>
                                                <IonLabel slot="end">{searchResult.emergencyContact.phone}</IonLabel>
                                            </IonItem>
                                        </IonGrid>
                                        {/* {JSON.stringify(searchResult)} */}
                                    </IonAccordion>
                                </IonAccordionGroup>
                            )}
                            <IonRow>
                                <IonCol>
                                    <IonButton expand="block" onClick={() => setOpenModal("topUp")}>
                                        Top up
                                    </IonButton>
                                </IonCol>
                                <IonCol>
                                    <IonButton
                                        expand="block"
                                        onClick={() => {
                                            history.push(`/newBooking`);
                                        }}
                                    >
                                        Book session
                                    </IonButton>
                                    {useV2 ? (
                                        <IonButton
                                            expand="block"
                                            onClick={() => {
                                                history.push(`/newBookingV2`);
                                            }}
                                        >
                                            Book session V2 (DO NOT USE)
                                        </IonButton>
                                    ) : null}
                                </IonCol>
                                <IonCol>
                                    <IonButton expand="block" onClick={() => setOpenModal("storage")}>
                                        Storage
                                    </IonButton>
                                </IonCol>
                                <IonCol size="12">
                                    <IonButton expand="block" color="tertiary" onClick={() => handleSearchSessions()}>
                                        Session history
                                    </IonButton>
                                </IonCol>
                                <IonCol size="12">
                                    <IonButton expand="block" color="primary" onClick={() => handleGetTopups()}>
                                        Topup history
                                    </IonButton>
                                </IonCol>
                                <IonCol size="12">
                                    <IonButton
                                        expand="block"
                                        color={searchResult.enabled ? "danger" : "success"}
                                        onClick={() => setOpenModal("disableUser")}
                                    >
                                        {searchResult.enabled ? "Disable user" : "Enable user"}
                                    </IonButton>
                                </IonCol>
                                {authUser && authUser.role === "admin" ? (
                                    <IonCol size="12">
                                        <IonButton
                                            expand="block"
                                            color={searchResult.isPartner ? `danger` : `warning`}
                                            disabled={disablePartnerClick}
                                            onClick={() => {
                                                handleUpdatePartnerCustomer(
                                                    searchResult.id,
                                                    searchResult.memberId,
                                                    searchResult.isPartner ? false : true
                                                );
                                            }}
                                        >
                                            {disablePartnerClick ? (
                                                <IonSpinner />
                                            ) : searchResult.isPartner ? (
                                                `Disable as Partner User`
                                            ) : (
                                                `Enable as Partner User`
                                            )}
                                        </IonButton>
                                    </IonCol>
                                ) : null}
                            </IonRow>
                        </IonGrid>
                    </IonCard>
                )}
                <IonModal isOpen={!!openModal} onDidDismiss={handleCancel}>
                    {openModal === "topUp" ? (
                        <>
                            <ModalHeader text={"TOP UP"} />
                            <IonContent>
                                <IonGrid>
                                    <IonRow>
                                        <IonCol>
                                            <IonList>
                                                <IonListHeader style={{ alignItems: "center" }}>
                                                    <IonLabel className="m-0">PACKAGES</IonLabel>
                                                </IonListHeader>
                                                {tuPackages.map((i) => (
                                                    <IonItem
                                                        key={i.id}
                                                        onClick={() => {
                                                            setSelected(i);
                                                            setCustomTU(undefined);
                                                        }}
                                                        style={{ cursor: "pointer" }}
                                                        color={selected && selected !== "custom" && selected.id === i.id ? "success" : ""}
                                                    >
                                                        {`${i.receivedCredits} credit - (HKD$ ${i.price})`}
                                                    </IonItem>
                                                ))}
                                                <IonItem color={selected === "custom" ? "success" : ""} onClick={() => setSelected("custom")}>
                                                    <IonInput
                                                        placeholder="Or enter custom amount here"
                                                        value={customTU}
                                                        type="number"
                                                        onIonChange={(e) => setCustomTU(Number(e.detail.value!))}
                                                    />
                                                </IonItem>
                                            </IonList>
                                        </IonCol>
                                    </IonRow>
                                    <IonRow>
                                        <IonCol>
                                            <IonList>
                                                <IonListHeader>
                                                    <IonLabel className="m-0">PAYMENT METHOD</IonLabel>
                                                </IonListHeader>
                                                <IonItem
                                                    onClick={() => setPayment("cash")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "cash" ? "success" : ""}
                                                >
                                                    Cash
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("vm")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "vm" ? "success" : ""}
                                                >
                                                    Visa/Master
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("ae")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "ae" ? "success" : ""}
                                                >
                                                    American Express
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("fps")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "fps" ? "success" : ""}
                                                >
                                                    FPS
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("fpsQr")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "fpsQr" ? "success" : ""}
                                                >
                                                    FPS (QR Code)
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("payme")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "payme" ? "success" : ""}
                                                >
                                                    PayMe
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("alipay")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "alipay" ? "success" : ""}
                                                >
                                                    AliPay
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("wechat")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "wechat" ? "success" : ""}
                                                >
                                                    WeChat
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("unionPay")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "unionPay" ? "success" : ""}
                                                >
                                                    Union Pay
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("octopus")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "octopus" ? "success" : ""}
                                                >
                                                    八達通
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("ent")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "ent" ? "success" : ""}
                                                >
                                                    ENT
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("KKDay")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "KKDay" ? "success" : ""}
                                                >
                                                    KKDay
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("Klook")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "Klook" ? "success" : ""}
                                                >
                                                    Klook
                                                </IonItem>
                                                <IonItem
                                                    onClick={() => setPayment("deduct_credit")}
                                                    style={{ cursor: "pointer" }}
                                                    color={payment === "deduct_credit" ? "success" : ""}
                                                >
                                                    Deduct credit
                                                </IonItem>
                                            </IonList>
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>
                            </IonContent>
                            <IonFooter>
                                <IonRow>
                                    <IonCol>
                                        <IonButton expand="block" onClick={handleTopUp}>
                                            CONFIRM TOP UP
                                        </IonButton>
                                    </IonCol>
                                    <IonCol>
                                        <IonButton color="medium" expand="block" onClick={handleCancel}>
                                            CANCEL
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonFooter>
                        </>
                    ) : openModal === "disableUser" ? (
                        <>
                            <ModalHeader text={"Disable User"} />
                            <IonGrid className="flexBetween">
                                <IonRow />
                                <IonRow>
                                    <IonCol>Confirm to disable this user?</IonCol>
                                </IonRow>
                                <IonRow className="full-width">
                                    <IonCol>
                                        <IonButton expand="block">CONFIRM</IonButton>
                                    </IonCol>
                                    <IonCol>
                                        <IonButton color="medium" expand="block" onClick={handleCancel}>
                                            CANCEL
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </>
                    ) : openModal === "storage" ? (
                        <>
                            <ModalHeader text={"Customer storage"} />
                            <IonContent>
                                {userStorage.length ? (
                                    userStorage.map((i, idx) => (
                                        <IonCard key={i.validFrom.toString() + idx}>
                                            <IonCardHeader>
                                                <IonTitle>{"Storage #" + i.storageNum}</IonTitle>
                                            </IonCardHeader>
                                            <IonCardContent>
                                                <IonList>
                                                    <IonItem>
                                                        <IonLabel>Valid from:</IonLabel>
                                                        <IonLabel>{i.validFrom.toLocaleDateString("en-CA")}</IonLabel>
                                                    </IonItem>
                                                    <IonItem>
                                                        <IonLabel>Expiry date:</IonLabel>
                                                        <IonLabel>{i.validTo.toLocaleDateString("en-CA")}</IonLabel>
                                                    </IonItem>
                                                </IonList>
                                                <IonGrid>
                                                    <IonRow>
                                                        <IonCol>
                                                            <IonButton
                                                                color="danger"
                                                                expand="block"
                                                                onClick={() => {
                                                                    setStorageDelTarget(i.id);
                                                                    setMessage("Confirm to remove?");
                                                                }}
                                                            >
                                                                REMOVE STORAGE
                                                            </IonButton>
                                                        </IonCol>
                                                    </IonRow>
                                                </IonGrid>
                                            </IonCardContent>
                                        </IonCard>
                                    ))
                                ) : (
                                    <IonGrid>
                                        <IonRow>
                                            <IonCol className="ion-padding">No storage purchased</IonCol>
                                        </IonRow>
                                    </IonGrid>
                                )}
                                <IonGrid>
                                    <IonRow>
                                        <IonCol>
                                            <IonButton expand="block" onClick={handleAddStorage}>
                                                ADD STORAGE
                                            </IonButton>
                                            <IonButton expand="block" color="medium" onClick={handleCancel}>
                                                CANCEL
                                            </IonButton>
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>
                            </IonContent>
                        </>
                    ) : (
                        openModal === "subAcc" && (
                            <>
                                <ModalHeader text={"Sub Accounts"} />
                                <IonContent>
                                    {!!subAccounts &&
                                        !!subAccounts.length &&
                                        subAccounts.map((subAcc, idx) => (
                                            <IonCard key={subAcc.id}>
                                                <IonGrid>
                                                    <IonRow>
                                                        <IonCol size="4" className="flex-column-center">
                                                            {subAcc.realImage ? (
                                                                <IonImg src={subAcc.realImage} style={{ width: "100%" }} />
                                                            ) : (
                                                                <IonIcon icon={personSharp} style={{ width: "100%", height: "100%" }} />
                                                            )}
                                                        </IonCol>
                                                        <IonCol size="8">
                                                            <IonList inset>
                                                                <IonItem>
                                                                    <IonLabel>Member ID</IonLabel>
                                                                    <IonLabel slot="end">{subAcc.memberId}</IonLabel>
                                                                </IonItem>
                                                                <IonItem>
                                                                    <IonLabel>User name</IonLabel>
                                                                    <IonLabel slot="end">{subAcc.usrname}</IonLabel>
                                                                </IonItem>
                                                                <IonItem>
                                                                    <IonLabel>Real name</IonLabel>
                                                                    <IonLabel slot="end">{subAcc.realName || "N/A"}</IonLabel>
                                                                </IonItem>
                                                                <IonItem>
                                                                    <IonLabel>DOB</IonLabel>
                                                                    <IonLabel slot="end">{formatDate(subAcc.dob)}</IonLabel>
                                                                </IonItem>
                                                            </IonList>
                                                        </IonCol>
                                                    </IonRow>
                                                    {subAcc.emergencyContact && (
                                                        <IonAccordionGroup value="emergencyContact">
                                                            <IonAccordion value="">
                                                                <IonItem slot="header">
                                                                    <IonLabel>Emergency Contact</IonLabel>
                                                                </IonItem>
                                                                <IonGrid slot="content">
                                                                    <IonItem>
                                                                        <IonLabel>Name</IonLabel>
                                                                        <IonLabel slot="end">{subAcc.emergencyContact.name}</IonLabel>
                                                                    </IonItem>
                                                                    <IonItem>
                                                                        <IonLabel>Phone</IonLabel>
                                                                        <IonLabel slot="end">{subAcc.emergencyContact.phone}</IonLabel>
                                                                    </IonItem>
                                                                </IonGrid>
                                                            </IonAccordion>
                                                        </IonAccordionGroup>
                                                    )}
                                                </IonGrid>
                                            </IonCard>
                                        ))}
                                </IonContent>
                                <IonFooter>
                                    <IonGrid>
                                        <IonRow>
                                            <IonCol>
                                                <IonButton expand="block" color="medium" onClick={handleCancel}>
                                                    CANCEL
                                                </IonButton>
                                            </IonCol>
                                        </IonRow>
                                    </IonGrid>
                                </IonFooter>
                            </>
                        )
                    )}
                </IonModal>
                <IonModal isOpen={agreementModal} onDidDismiss={handleCancel}>
                    <IonContent>
                        <IonGrid>
                            <StorageWaiver />
                            <IonItem>
                                <IonCheckbox slot="start" color="success" checked={agreed} onIonChange={(e) => setAgreed(e.detail.checked)} />
                                <IonCol>By clicking here, I state that I have read and understood the terms and conditions</IonCol>
                            </IonItem>

                            <IonRow>
                                <IonCol size="12">
                                    <IonButton expand="block" disabled={!agreed} onClick={() => addStorage(searchResult!.id, searchResult!.email)}>
                                        Submit
                                    </IonButton>
                                    <IonButton expand="block" color="medium" onClick={handleCancel}>
                                        Cancel
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonContent>
                </IonModal>
                <IonModal isOpen={historyModal} onDidDismiss={handleCancel}>
                    <ModalHeader text={"Customer's session history"} />
                    <IonContent>
                        <IonList>
                            {userSessions.length ? (
                                userSessions.map((session) => (
                                    <IonItem
                                        key={session.id}
                                        button
                                        onClick={() => {
                                            handleCancel();
                                            setTimeout(() => {
                                                history.push(`/session/search/${session.id}`);
                                            }, 300);
                                        }}
                                    >
                                        <IonLabel>
                                            <h3>
                                                <b>
                                                    #{session.bookingNumber} - {session.sessionType}
                                                </b>
                                            </h3>
                                            <p>Status: <b>{session.cancelledAt ? `Cancelled` : session.lockedAt ? `Pending` : session.confirmedAt ? `Confirmed` : `Unknown`}</b></p>
                                            <p>
                                                <b>
                                                    {dayjs.tz(session.start).format(`HH:mm`)} - {dayjs.tz(session.end).format(`HH:mm`)}
                                                </b>
                                            </p>
                                            <p>
                                                <b>{dayjs.tz(session.sessionDate).format(`DD MMM YYYY`)}</b>
                                            </p>
                                            <p>
                                                Pax: {session.pax} | Type: {session.playType}
                                            </p>
                                        </IonLabel>
                                        <IonButtons slot="end">
                                            <IonChip>
                                                <IonLabel>Total: {session.totalCredits.toLocaleString()}</IonLabel>
                                            </IonChip>
                                        </IonButtons>
                                    </IonItem>
                                ))
                            ) : (
                                <IonRow>
                                    <IonCol className="ion-text-center">No records</IonCol>
                                </IonRow>
                            )}
                        </IonList>
                    </IonContent>
                    <IonFooter>
                        <IonRow>
                            <IonCol size="12">
                                {userSessions.length && (
                                    <IonButton onClick={() => historyRef.current.link.click()} expand="block">
                                        <CSVLink
                                            ref={historyRef}
                                            data={printSessionHistory(userSessions)}
                                            style={{ color: "#FFF", textDecoration: "none", pointerEvents: "none", width: "100%" }}
                                            filename={"Member " + searchResult?.memberId + " session records"}
                                        >
                                            DOWNLOAD USER HISTORY
                                        </CSVLink>
                                    </IonButton>
                                )}
                                <IonButton expand="block" color="medium" onClick={handleCancel}>
                                    Cancel
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonFooter>
                </IonModal>
                <IonModal
                    isOpen={showTopupHistoryModal}
                    onDidDismiss={() => {
                        setShowTopupHistoryModal(false);
                    }}
                >
                    <IonHeader>
                        <IonToolbar>
                            <IonTitle>Topup History</IonTitle>
                            <IonChip slot="end">
                                <IonLabel>
                                    Total:{" "}
                                    {!userTopups
                                        ? `NA`
                                        : `$` +
                                          userTopups
                                              .filter((tt) => tt.paymentStatus === "AUTHORIZED")
                                              .reduce((a, b) => a + (b.exactAmount ? b.exactAmount : b.topupPackage?.price || 0), 0)
                                              .toLocaleString()}
                                </IonLabel>
                            </IonChip>
                        </IonToolbar>
                    </IonHeader>
                    <IonContent>
                        {userTopups && userTopups.length ? (
                            <IonList>
                                {userTopups.map((tp) => (
                                    <IonItem key={`user-${tp.authUser.id}-topup-${tp.id}`}>
                                        <IonLabel>
                                            <h3>
                                                Topup ${tp.exactAmount ? tp.exactAmount.toLocaleString() : tp.topupPackage?.price.toLocaleString()}
                                            </h3>
                                            <p>
                                                <b>Date: {dayjs.tz(tp.createdAt).format(`HH:mm, DD MMM YY`)}</b>
                                            </p>
                                            <p>
                                                <b>Payment Method: {tp.paymentMethod}</b>
                                            </p>
                                            <p>Ref: {tp.orderReference}</p>
                                        </IonLabel>
                                        <IonButtons slot="end">
                                            <IonChip>{tp.paymentStatus}</IonChip>
                                        </IonButtons>
                                    </IonItem>
                                ))}
                            </IonList>
                        ) : null}
                    </IonContent>
                </IonModal>
                <IonAlert
                    isOpen={!!message}
                    onDidDismiss={() => {
                        setMessage("");
                    }}
                    header={`System Message`}
                    message={message}
                    buttons={
                        !!payment && !!selected
                            ? [
                                  {
                                      text: "OK",
                                      handler: () => {
                                          setMessage("");
                                          setSelected(null);
                                          setPayment("");
                                          setOpenModal(null);
                                      },
                                  },
                              ]
                            : openModal === "storage"
                            ? [
                                  {
                                      text: "OK",
                                      handler: async () => {
                                          if (!!storageDelTarget) {
                                              await removeStorageById(storageDelTarget).then((e) => {
                                                  if (e === "success") {
                                                      setStorageDelTarget("");
                                                  } else {
                                                      setMessage(e);
                                                  }
                                              });
                                          } else {
                                              if (searchResult) {
                                                  await addStorage(searchResult.id, searchResult.email);
                                              }
                                          }
                                          setMessage("");
                                          setOpenModal(null);
                                      },
                                  },
                                  {
                                      text: "CANCEL",
                                      handler: () => {
                                          setMessage("");
                                      },
                                  },
                              ]
                            : [
                                  {
                                      text: "CLOSE",
                                      handler: () => {
                                          if (message === "User not found") {
                                              dispatch(clearSearch());
                                              getStorage();
                                          }
                                          setMessage("");
                                          setSelected(null);
                                          setPayment("");
                                          setOpenModal(null);
                                      },
                                  },
                              ]
                    }
                />
                <IonLoading
                    isOpen={verifying === 'verifying'}
                    spinner={'dots'}
                />
                <IonAlert
                    isOpen={verifying === 'confirming'}
                    // onDidDismiss={() => setVerifying('idle')}
                    header={`Confirm to manually verify this user?`}
                    buttons={[
                        {
                            text: 'Confirm',
                            handler: () => {
                                handleVerify()
                            }
                        },
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            handler: () => setVerifying('idle')
                        }
                    ]}
                />
                <IonAlert
                    isOpen={verifying === 'error'}
                    onDidDismiss={() => setVerifying('idle')}
                    header={`Something is wrong when trying to manually verify, please try again.`}
                    buttons={[
                        {
                            text: 'OK',
                            role: 'cancel'
                        }
                    ]}
                />
                <IonAlert
                    isOpen={verifying === 'success'}
                    onDidDismiss={() => setVerifying('idle')}
                    header={`Email Verified`}
                    buttons={[
                        {
                            text: 'OK',
                            role: 'cancel'
                        }
                    ]}
                />
            </IonContent>
        </IonPage>
    );
}

export default UserSearch;

interface SearchBarRowProps {
    type: "email" | "tel";
    name: SearchType;
    placeholder: string;
    value: string;
    setValue: (args0: string) => void;
}

export const SearchBarRow = (props: SearchBarRowProps) => {
    return (
        <>
            <IonSearchbar
                animated
                placeholder={props.placeholder}
                type={props.type}
                value={props.value}
                onIonChange={(e) => props.setValue(e.detail.value! as string)}
            />
        </>
    );
};

interface ListItemProps {
    item: string | number;
}

export const ListItem = (props: ListItemProps) => {
    return (
        <IonItem>
            <IonLabel style={{ textTransform: "capitalize" }}>{props.item}</IonLabel>
            <IonRadio slot="start" value={props.item} />
        </IonItem>
    );
};

export const ModalHeader = ({ text }: { text: string }) => {
    return (
        <IonHeader>
            <IonToolbar>
                <IonTitle>{text}</IonTitle>
            </IonToolbar>
        </IonHeader>
    );
};

interface CustomerCardProps {
    customer: Customer;
}

export const CustomerCard = (props: CustomerCardProps) => {
    const { customer } = props;
    return (
        <IonCard>
            <IonGrid>
                <IonRow>
                    <IonCol size="4" style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                        <IonImg src={customer.realImage} style={{ width: "100%" }} />
                    </IonCol>
                    <IonCol>
                        <IonList inset>
                            <IonItem>
                                <IonLabel>Member ID</IonLabel>
                                <IonLabel slot="end">{customer.memberId}</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Real name</IonLabel>
                                <IonLabel slot="end">{customer.realName}</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Phone</IonLabel>
                                <IonLabel slot="end">{customer.phone}</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Email</IonLabel>
                                <IonLabel slot="end">{customer.email}</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>DOB</IonLabel>
                                <IonLabel slot="end">{formatDate(customer.dob)}</IonLabel>
                            </IonItem>
                        </IonList>
                    </IonCol>
                </IonRow>
                {customer.emergencyContact && (
                    <IonAccordionGroup value="emergencyContact">
                        <IonAccordion value="">
                            <IonItem slot="header">
                                <IonLabel>Emergency Contact</IonLabel>
                            </IonItem>
                            <IonGrid slot="content">
                                <IonItem>
                                    <IonLabel>Name</IonLabel>
                                    <IonLabel slot="end">{customer.emergencyContact.name}</IonLabel>
                                </IonItem>
                                <IonItem>
                                    <IonLabel>Phone</IonLabel>
                                    <IonLabel slot="end">{customer.emergencyContact.phone}</IonLabel>
                                </IonItem>
                            </IonGrid>
                        </IonAccordion>
                    </IonAccordionGroup>
                )}
            </IonGrid>
        </IonCard>
    );
};

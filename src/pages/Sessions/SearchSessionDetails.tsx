import {
    IonAccordion,
    IonAccordionGroup,
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonChip,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonImg,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonLoading,
    IonModal,
    IonPage,
    IonRadio,
    IonRadioGroup,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonSpinner,
    IonText,
    IonThumbnail,
    IonTitle
} from "@ionic/react";
import * as SessionsTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Coach } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { getBookingById, getCounterByDateV2, removeSessionCoach, setSessionToCxl, updateSessionCoach } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAllCoaches } from "app/slices/coachSlice";
import Header from "components/Global/Header";
import LoadingCard from "components/Global/LoadingCard";
import CancelPolicy from "components/Sessions/CancelPolicy";
import "css/Sessions.css";
import dayjs from "dayjs";
import { formatDate, fromDB } from "helpers/date";
import { addOutline } from "ionicons/icons";
import { checkAvailableCoachesByDateTime, checkAvailablePrivateCoach } from "pages/GroupClasses/checkAvailableCoaches";
import { useCallback, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import AddGearModal from "./AddGearModal";
import { UserAccordion } from "./SessionDetails";
import { ModalHeader } from "./UserSearch";
import axios from 'axios';

type RefundType = "100% refund" | "50% refund" | "No refund";

const SearchSessionDetails = () => {
    const { id } = useParams<{ id: string }>();
    // const queriedSession = useSelector(selectViewSession(id));
    const [session, setSession] = useState<Session | undefined | null>(null);
    const [cxlModal, setCxlModal] = useState(false);
    const [refundType, setRefundType] = useState<RefundType>("No refund");
    const [showAddGearModal, setShowAddGearModal] = useState(false);
    const history = useHistory();

    const [disableCancel, setDisableCancel] = useState(false);
    const [isCancelling, setIsCancelling] = useState(false);

    const getBooking = useCallback(async () => {
        const res = await getBookingById(id);
        if (res.exists()) {
            const data = fromDB(res.data());
            setSession({ id, ...data } as Session);
        } else {
            setSession(null);
        }
    }, [id]);

    useEffect(() => {
        getBooking();
    }, [id, getBooking]);

    const handleCxlBooking = useCallback(async () => {
        // NEED TO CHECK:
        // Paid by staff credit? How much?
        // Used voucher?
        const credit = refundType === "100% refund" ? session?.totalCredits : refundType === "50% refund" ? session?.totalCredits! / 2 : 0;
        if (session) {
            setIsCancelling(true);
            setDisableCancel(true);
            await setSessionToCxl(session, credit ? credit : 0)
                .then((e) => {
                    if (e === "success") {
                        setCxlModal(false);
                        history.goBack();
                    }
                })
                .finally(() => {
                    setIsCancelling(false);
                    setDisableCancel(false);
                });
        }
    }, [refundType, session, history]);

    // coach state
    const allCoaches = useAppSelector(selectAllCoaches);
    const [availableCoaches, setAvailableCoaches] = useState<Coach[]>([]);
    const [selectedCoachId, setSelectedCoachId] = useState("");
    const [updatingCoaches, setUpdatingCoaches] = useState(false);
    const [savingCoach, setSavingCoach] = useState("idle");

    useEffect(() => {
        if (!session || session.sessionType !== "privateClass" || !session.start) {
            return;
        }
        const updateAvailableCoaches = async () => {
            setUpdatingCoaches(true);
            if (!session.start || !session.end) {
                return;
            }
            if (session.playType === "ski" || session.playType === "sb") {
                const duration = dayjs.tz(session.end).diff(dayjs.tz(session.start), "hour");
                const coaches: Coach[] = await checkAvailableCoachesByDateTime(session.start, session.playType, duration);
                const counters: SessionsTypes.MonthCounters | null = await getCounterByDateV2(session.sessionDate);
                if (!counters) {
                    return;
                }
                const checkedCoaches = checkAvailablePrivateCoach(coaches, session, counters);
                setUpdatingCoaches(false);
                setAvailableCoaches(checkedCoaches);
            }
        };
        updateAvailableCoaches();
    }, [session, allCoaches]);

    const handleUpdateCoach = async () => {
        if (!selectedCoachId) {
            return;
        }
        const selectedCoach = availableCoaches.find((x) => x.id === selectedCoachId);
        if (!selectedCoach || !session) {
            return;
        }
        setSavingCoach("saving");
        const res = await updateSessionCoach(selectedCoach, session);
        if (res === "success") {
            setSavingCoach("saved");
            getBooking();
        } else {
            setSavingCoach("idle");
        }
    };

    const removeCoach = async () => {
        if (!session || !session?.privateCoach) {
            return;
        }
        setSavingCoach("saving");
        const res = await removeSessionCoach(session);
        if (res === "success") {
            setSavingCoach("saved");
            getBooking();
        } else {
            setSavingCoach("idle");
        }
    };

    // confirmation email resend
    const [confirmResend, setConfirmResend] = useState(false);
    const [resending, setResending] = useState('idle');
    const handleResendConfirmEmail = async () => {
        if(!session){
            setResending('noSession');
            return
        }
        setConfirmResend(false);
        setResending('resending');
        const res = await axios.post(`${process.env.REACT_APP_Firebase_apiUrl}/resendConfirmationEmail`, {
            session
        });
        if(res.status !== 200){
            setResending('failed');
        }else{
            setResending('success');
        }
    }

    return (
        <IonPage>
            <Header title={`Session Details`} back />
            <IonContent>
                {!session ? (
                    <>
                        <IonCard>
                            <IonCardContent>
                                <IonSpinner name="dots" />
                            </IonCardContent>
                        </IonCard>
                    </>
                ) : (
                    <>
                        <IonCard>
                            <IonCardContent>
                                <IonAccordionGroup value={`details`}>
                                    <IonAccordion value="details">
                                        <IonItem slot="header">
                                            Session #{session.bookingNumber}{" "}
                                            <IonChip color="tertiary" className="ion-margin-horizontal">
                                                {session.sessionType}
                                            </IonChip>
                                        </IonItem>
                                        <IonList slot="content">
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Date</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{dayjs(Number(session.start)).format(`DD MMM YY`)}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Time</p>
                                                </IonLabel>
                                                <IonLabel slot="end">
                                                    {dayjs(Number(session.start)).format(`HH:mm`)} - {dayjs(Number(session.end)).format(`HH:mm`)}
                                                </IonLabel>
                                            </IonItem>
                                            <IonItem
                                                button
                                                onClick={() => {
                                                    history.push(`/checkout/${session.id}`);
                                                }}
                                            >
                                                <IonLabel>
                                                    <p>Session ID</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.id}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Player{session.pax > 1 ? `s` : ``}</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.pax}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Type</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.playType}</IonLabel>
                                            </IonItem>
                                        </IonList>
                                    </IonAccordion>
                                </IonAccordionGroup>
                            </IonCardContent>
                        </IonCard>

                        <IonCard>
                            <IonCardContent>
                                <IonAccordionGroup>
                                    <IonAccordion>
                                        <IonItem slot="header">Booking User</IonItem>
                                        <IonList slot="content">
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Member ID</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.bookingUser?.memberId}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Name</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.bookingUser?.realName}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Email</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.bookingUser?.email}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>Phone</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.bookingUser?.phone}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>
                                                    <p>ID</p>
                                                </IonLabel>
                                                <IonLabel slot="end">{session.bookingUser?.id}</IonLabel>
                                            </IonItem>
                                        </IonList>
                                    </IonAccordion>
                                </IonAccordionGroup>
                            </IonCardContent>
                        </IonCard>

                        <IonCard>
                            <IonCardContent>
                                <IonAccordionGroup value={`gears`}>
                                    <IonAccordion value="gears">
                                        <IonItem slot="header">Gear Rentals</IonItem>
                                        <IonList slot="content">
                                            {session.gears.length <= 0 ? (
                                                <IonItem>
                                                    <IonLabel>No Rentals for this session.</IonLabel>
                                                </IonItem>
                                            ) : (
                                                session.gears.map((gear, idx) => (
                                                    <IonItem key={`gear-${idx}-id-${gear.gear.id}-size-${gear.size}-${gear.class}`}>
                                                        <IonText>
                                                            {`${gear.gear.name} ${gear.class.toUpperCase()} ${gear.size} (${gear.gear.unit.name})`}
                                                        </IonText>
                                                    </IonItem>
                                                ))
                                            )}
                                            <IonItem lines="none">
                                                <IonButton
                                                    onClick={() => {
                                                        setShowAddGearModal(true);
                                                    }}
                                                >
                                                    <IonIcon icon={addOutline} slot="start" />
                                                    <IonLabel>Add Gears</IonLabel>
                                                </IonButton>
                                            </IonItem>
                                        </IonList>
                                    </IonAccordion>
                                </IonAccordionGroup>
                            </IonCardContent>
                        </IonCard>

                        {session.sessionType === "privateClass" ? (
                            updatingCoaches ? (
                                <LoadingCard />
                            ) : (
                                <IonCard>
                                    <IonCardContent>
                                        <IonAccordionGroup value={`ptCoach`}>
                                            <IonAccordion value="ptCoach">
                                                <IonItem slot="header">Private Class Coach</IonItem>
                                                <IonList slot="content">
                                                    {session.privateCoach ? (
                                                        <IonItem>
                                                            <IonLabel>
                                                                <h3>{session.privateCoach.usrname}</h3>
                                                            </IonLabel>
                                                            <IonButtons slot="end">
                                                                <IonButton
                                                                    color={"danger"}
                                                                    onClick={() => {
                                                                        removeCoach();
                                                                    }}
                                                                >
                                                                    <IonLabel>Remove</IonLabel>
                                                                </IonButton>
                                                            </IonButtons>
                                                        </IonItem>
                                                    ) : (
                                                        <>
                                                            <IonItem>
                                                                <IonLabel>Select Coach</IonLabel>
                                                                <IonSelect
                                                                    value={
                                                                        availableCoaches.find((x) => x.id === selectedCoachId)
                                                                            ? availableCoaches.find((x) => x.id === selectedCoachId)?.id
                                                                            : null
                                                                    }
                                                                    onIonChange={(e) => {
                                                                        setSelectedCoachId(e.detail.value);
                                                                    }}
                                                                >
                                                                    <IonSelectOption value={null}>No Coach Assigned</IonSelectOption>
                                                                    {availableCoaches.map((x) => (
                                                                        <IonSelectOption key={`available-coach-option-${x.id}`} value={x.id}>
                                                                            {x.usrname}
                                                                        </IonSelectOption>
                                                                    ))}
                                                                </IonSelect>
                                                            </IonItem>
                                                            <IonItem lines="none">
                                                                <IonButton
                                                                    disabled={
                                                                        availableCoaches.find((x) => x.id === selectedCoachId) === undefined ||
                                                                        availableCoaches.find((x) => x.id === selectedCoachId) === null
                                                                    }
                                                                    onClick={() => {
                                                                        handleUpdateCoach();
                                                                    }}
                                                                >
                                                                    Update
                                                                </IonButton>
                                                            </IonItem>
                                                        </>
                                                    )}
                                                </IonList>
                                            </IonAccordion>
                                        </IonAccordionGroup>
                                    </IonCardContent>
                                </IonCard>
                            )
                        ) : null}

                        <IonCard>
                            <IonCardContent>
                                <IonAccordionGroup value={`players`}>
                                    <IonAccordion value="players">
                                        <IonItem slot="header">
                                            Checked In Users ({session.checkedInUsers.length}/{session.pax})
                                        </IonItem>
                                        <IonList slot="content">
                                            {session.checkedInUsers.length <= 0 ? (
                                                <IonItem>
                                                    <IonLabel>Booking was not checked in</IonLabel>
                                                </IonItem>
                                            ) : (
                                                session.checkedInUsers.map((user) => (
                                                    <IonItem key={`session-${session.id}-player-${user.id}`}>
                                                        <IonThumbnail slot="start">
                                                            <IonImg src={user.realImage} />
                                                        </IonThumbnail>
                                                        <IonLabel slot="start">{user.realName + " " + user.phone}</IonLabel>
                                                    </IonItem>
                                                ))
                                            )}
                                        </IonList>
                                    </IonAccordion>
                                </IonAccordionGroup>
                            </IonCardContent>
                        </IonCard>
                    </>
                )}
                <IonRow>
                    <IonCol>
                        <IonButton
                            expand={`block`}
                            onClick={() => {
                                // setConfirmResend(true);
                            }}
                        >
                            <IonLabel>Re-send Confirmation Email</IonLabel>
                        </IonButton>
                    </IonCol>
                </IonRow>
                {session?.status !== "cancelled" ? (
                    <IonRow>
                        {true && (
                            <IonCol className="ion-padding">
                                <IonButton expand="block" color="danger" onClick={() => setCxlModal(true)}>
                                    CANCEL BOOKING
                                </IonButton>
                            </IonCol>
                        )}
                    </IonRow>
                ) : (
                    <IonRow className="ion-padding">
                        <IonCol className="ion-text-center">
                            <IonTitle>{`Booking cancelled at ${formatDate(session.cancelledAt!)}`}</IonTitle>
                        </IonCol>
                    </IonRow>
                )}

                {session ? (
                    <IonModal isOpen={cxlModal} onDidDismiss={() => setCxlModal(false)}>
                        <ModalHeader text={"Cancel Booking"} />
                        <IonContent>
                            <IonList>
                                <IonListHeader>
                                    <IonLabel>{`Cancel booking #${session?.bookingNumber}`}</IonLabel>
                                </IonListHeader>
                                <IonItem>
                                    <IonHeader>Date</IonHeader>
                                    <IonLabel slot="end">
                                        {`${formatDate(session?.sessionDate!)} ${
                                            session?.sessionDate === new Date(new Date().toLocaleDateString()) ? "(TODAY)" : ""
                                        }`}
                                    </IonLabel>
                                </IonItem>
                                <IonItem>
                                    <IonHeader>Credit</IonHeader>
                                    <IonLabel slot="end">{session?.totalCredits}</IonLabel>
                                </IonItem>
                                <UserAccordion user={session?.bookingUser!} />
                            </IonList>
                            <IonAccordionGroup>
                                <IonAccordion>
                                    <IonItem slot="header">Cancel policy</IonItem>
                                    <IonList slot="content">
                                        <CancelPolicy />
                                    </IonList>
                                </IonAccordion>
                            </IonAccordionGroup>
                            <IonRadioGroup value={refundType} onIonChange={(e) => setRefundType(e.target.value)}>
                                <IonListHeader style={{ alignItems: "center" }}>
                                    <IonLabel className="m-0">Refund to user?</IonLabel>
                                </IonListHeader>
                                <IonItem lines="none" color={refundType === "100% refund" ? "success" : "light"}>
                                    <IonLabel>{`100% Refund (${session?.totalCredits} credit)`}</IonLabel>
                                    <IonRadio slot="start" value={"100% refund"} />
                                </IonItem>
                                <IonItem lines="none" color={refundType === "50% refund" ? "warning" : "light"}>
                                    <IonLabel>{`50% Refund (${session?.totalCredits! / 2} credit)`}</IonLabel>
                                    <IonRadio slot="start" value={"50% refund"} />
                                </IonItem>
                                <IonItem lines="none" color={refundType === "No refund" ? "danger" : "light"}>
                                    <IonLabel>{`No Refund (0 credit)`}</IonLabel>
                                    <IonRadio slot="start" value={"No refund"} />
                                </IonItem>
                            </IonRadioGroup>
                            <IonGrid>
                                <IonRow>
                                    <IonCol>
                                        <IonButton
                                            color="danger"
                                            expand="block"
                                            onClick={disableCancel ? () => {} : () => handleCxlBooking()}
                                            disabled={disableCancel}
                                        >
                                            CONFIRM CANCEL BOOKING
                                        </IonButton>
                                        <IonButton color="medium" expand="block" onClick={() => setCxlModal(false)}>
                                            BACK
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </IonContent>
                    </IonModal>
                ) : null}
            </IonContent>
            <IonLoading isOpen={isCancelling} />
            <AddGearModal
                isOpen={showAddGearModal}
                onDismiss={() => {
                    setShowAddGearModal(false);
                }}
                onSaved={() => {
                    setShowAddGearModal(false);
                    getBooking();
                }}
                sessionRaw={session}
            />
            <IonLoading isOpen={savingCoach === "saving"} spinner={"dots"} message={"Saving Coach"} />
            <IonAlert
                isOpen={savingCoach === "saved"}
                onDidDismiss={() => {
                    setSavingCoach("idle");
                }}
                header={`Coach Assigned`}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            setSavingCoach("idle");
                        },
                    },
                ]}
            />

<IonAlert
                isOpen={confirmResend}
                onDidDismiss={() => {
                    setConfirmResend(false);
                }}
                header={`Confirm Resend?`}
                buttons={[
                    {
                        text: `Resend`,
                        handler: () => {
                            handleResendConfirmEmail()
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]}
            />
            <IonLoading
                isOpen={resending === 'resending'}
                spinner={'dots'}
                message={`Resending`}
            />
            <IonAlert
                isOpen={resending === 'noSession'}
                onDidDismiss={() => {
                    setResending('idle');
                }}
                header={`Resend Failed.`}
                message={`The session data seems to be missing, please refresh and retry.`}
                buttons={[
                    {
                        text: 'OK',
                        role: 'cancel'
                    }
                ]}
            />
            <IonAlert
                isOpen={resending === 'failed'}
                onDidDismiss={() => {
                    setResending('idle');
                }}
                header={`Resend Failed.`}
                message={`Something is wrong with the resending, please refresh and retry.`}
                buttons={[
                    {
                        text: 'OK',
                        role: 'cancel'
                    }
                ]}
            />
            <IonAlert
                isOpen={resending === 'success'}
                onDidDismiss={() => {
                    setResending('idle');
                }}
                header={`Resend Success.`}
                buttons={[
                    {
                        text: 'OK',
                        role: 'cancel'
                    }
                ]}
            />
        </IonPage>
    );
};

export default SearchSessionDetails;

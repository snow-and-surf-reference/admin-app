import {
    IonAccordion,
    IonAccordionGroup,
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCol,
    IonContent,
    IonFooter,
    IonGrid,
    IonIcon,
    IonImg,
    IonItem,
    IonLabel,
    IonList,
    IonModal,
    IonPage,
    IonRow,
    IonSearchbar,
    IonSegment,
    IonSegmentButton,
    IonText,
    IonTitle,
} from "@ionic/react";
import { GearStorage } from "@tsanghoilun/snow-n-surf-interface/types/gear";
import { Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { getCustomerStorage, getSessionByCustomerId, getSubAccountsByEmailOrPhone, getUserByEmail, getUserByMemberIdType } from "app/firebase";
import { setIsLoading } from "app/slices/globalSlice";
import { setQueriedSessions } from "app/slices/sessionsSlice";
import { clearSearch, setParentAccount, setSubAccounts } from "app/slices/usersSlice";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import "css/UserSearchModal.css";
import { formatDate, formatTime } from "helpers/date";
import { personSharp } from "ionicons/icons";
import { useCallback, useEffect, useRef, useState } from "react";
import { CSVLink } from "react-csv";
import { Data } from "react-csv/components/CommonPropTypes";
import { useDispatch, useSelector } from "react-redux";
import { CustomerCard, ModalHeader } from "./UserSearch";

type SearchType = "Phone number" | "Email" | "Member ID";
type ModalType = "disableUser" | "storage" | "mainUser";

function SubAccountSearch() {
    const [tab, setTab] = useState<SearchType>("Phone number");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [memberID, setMemberID] = useState("");
    const [openModal, setOpenModal] = useState<ModalType | null>(null);
    const [message, setMessage] = useState("");
    const [userStorage, setUserStorage] = useState<GearStorage[]>([]);
    const [historyModal, setHistoryModal] = useState(false);
    const dispatch = useDispatch();
    const mainAcc = useSelector((state: RootState) => state.users.parentAccountUser);
    const searchResults = useSelector((state: RootState) => state.users.subAccounts);
    const userSessions = useSelector((state: RootState) => state.sessions.queriedSessions);
    const historyRef = useRef<any>(null);

    const handleSearch = async () => {
        if (tab === "Phone number") {
            const res = await getSubAccountsByEmailOrPhone(phone, "phone");
            if (res !== "success") setMessage(res);
        }
        if (tab === "Email") {
            const res = await getSubAccountsByEmailOrPhone(email, "email");
            if (res !== "success") setMessage(res);
        }
        if (tab === "Member ID") {
            const res = await getUserByMemberIdType(memberID, "sub");
            if (res !== "success") setMessage(res);
        }
    };
    const handleClearSearch = useCallback(() => {
        setEmail("");
        setPhone("");
        setMemberID("");
        dispatch(setParentAccount(null));
        dispatch(setSubAccounts([]));
        setUserStorage([]);
    }, [dispatch]);

    const handleCancel = () => {
        setOpenModal(null);
        setHistoryModal(false);
        dispatch(setQueriedSessions([]));
    };

    useEffect(() => {
        if (searchResults && searchResults.length && openModal === "storage") {
            getCustomerStorage(searchResults[0].email).then((e) => {
                setUserStorage(e);
            });
        }
    }, [searchResults, openModal]);

    const handleSearchSessions = useCallback(
        async (customerId: string) => {
            if (!searchResults || !searchResults.length) return;
            dispatch(setIsLoading(true));
            const res = await getSessionByCustomerId(customerId, true);
            if (res === "success") {
                setHistoryModal(true);
            } else {
                setMessage(res);
            }
            dispatch(setIsLoading(false));
        },
        [dispatch, searchResults]
    );

    const printSessionHistory = useCallback((history: Session[]) => {
        let tempHistory: Data = [];
        history.forEach((i) => {
            const { createdAt, pax, bookingStaff, status, cancelledAt, start, end, gears, playType, totalCredits, sessionType } = i;
            tempHistory.push({
                playType,
                sessionType,
                sessionDate: formatDate(start!),
                time: `${formatTime(start!)} - ${formatTime(end!)}`,
                pax,
                gears: gears.length ? "YES" : "NO",
                totalCredits,
                bookedByStaff: bookingStaff ? bookingStaff.email : "NO",
                cancelled: cancelledAt ? formatDate(cancelledAt) : "NO",
                status,
                bookingDate: formatDate(createdAt),
            });
        });
        return tempHistory;
    }, []);

    useEffect(() => {
        if (searchResults && !!searchResults.length) {
            getUserByEmail(searchResults[0].email, true);
        }
    }, [searchResults]);

    useEffect(() => {
        return () => {
            handleClearSearch();
        };
    }, [handleClearSearch]);

    return (
        <IonPage>
            <Header title={`Sub-account Search`} />
            <IonContent color="light">
                <IonCard>
                    <IonAccordionGroup value={"searchBar"}>
                        <IonAccordion value="searchBar">
                            <IonItem slot="header">{"SEARCH"}</IonItem>
                            <IonGrid slot="content">
                                <IonRow>
                                    <IonCol />
                                    <IonCol size="8">
                                        <IonSegment
                                            value={tab}
                                            onIonChange={(e) => {
                                                handleClearSearch();
                                                //@ts-ignore
                                                setTab(e.detail.value!);
                                            }}
                                        >
                                            <IonSegmentButton value="Phone number">Phone number</IonSegmentButton>
                                            <IonSegmentButton value="Email">Email</IonSegmentButton>
                                            <IonSegmentButton value="Member ID">Member ID</IonSegmentButton>
                                        </IonSegment>
                                    </IonCol>
                                    <IonCol />
                                </IonRow>
                                <br />
                                <IonRow>
                                    <IonCol>
                                        {tab === "Phone number" ? (
                                            <IonSearchbar
                                                animated
                                                placeholder={"Main account's phone number"}
                                                type={"tel"}
                                                value={phone}
                                                onIonChange={(e) => setPhone(e.detail.value!)}
                                                onKeyDown={(e) => {
                                                    if (e.key === "Enter") handleSearch();
                                                }}
                                            />
                                        ) : tab === "Email" ? (
                                            <IonSearchbar
                                                animated
                                                placeholder={"Main account's email address"}
                                                type={"email"}
                                                value={email}
                                                onIonChange={(e) => setEmail(e.detail.value!)}
                                                onKeyDown={(e) => {
                                                    if (e.key === "Enter") handleSearch();
                                                }}
                                            />
                                        ) : (
                                            <IonSearchbar
                                                animated
                                                placeholder={"Customer's member ID"}
                                                type={"text"}
                                                value={memberID}
                                                onIonChange={(e) => setMemberID(e.detail.value!)}
                                                onKeyDown={(e) => {
                                                    if (e.key === "Enter") handleSearch();
                                                }}
                                            />
                                        )}
                                    </IonCol>
                                </IonRow>
                                <IonRow>
                                    <IonCol size="6">
                                        <IonButton
                                            disabled={tab === "Phone number" ? !phone : tab === "Email" ? !email : !memberID}
                                            style={{ width: "100%" }}
                                            onClick={handleSearch}
                                        >
                                            Search
                                        </IonButton>
                                    </IonCol>
                                    <IonCol size="6">
                                        <IonButton color="danger" style={{ width: "100%" }} onClick={handleClearSearch}>
                                            Clear All
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </IonAccordion>
                    </IonAccordionGroup>
                </IonCard>
                {searchResults && !!searchResults.length && (
                    <IonCard>
                        <IonGrid>
                            <IonRow>
                                <IonCol>
                                    <IonButton expand="block" onClick={() => setOpenModal("storage")}>
                                        Main account storage
                                    </IonButton>
                                </IonCol>
                                <IonCol>
                                    <IonButton expand="block" onClick={() => setOpenModal("mainUser")}>
                                        View main account
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonCard>
                )}
                {searchResults &&
                    !!searchResults.length &&
                    searchResults.map((searchResult) => (
                        <IonCard key={searchResult.id}>
                            <IonGrid>
                                <IonRow>
                                    <IonCol size="4" className="flex-column-center">
                                        {searchResult.realImage ? (
                                            <IonImg src={searchResult.realImage} style={{ width: "100%" }} />
                                        ) : (
                                            <IonIcon icon={personSharp} style={{ width: "100%", height: "100%" }} />
                                        )}
                                    </IonCol>
                                    <IonCol>
                                        <IonList inset>
                                            <IonItem>
                                                <IonLabel>Member ID</IonLabel>
                                                <IonLabel slot="end">{searchResult.memberId}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>User name</IonLabel>
                                                <IonLabel slot="end">{searchResult.usrname}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>DOB</IonLabel>
                                                <IonLabel slot="end">{formatDate(searchResult.dob)}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Real name</IonLabel>
                                                <IonLabel slot="end">{searchResult.realName || "N/A"}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Main Account Phone</IonLabel>
                                                <IonLabel slot="end">{searchResult.phone}</IonLabel>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Main Account Email</IonLabel>
                                                <IonLabel slot="end">{searchResult.email}</IonLabel>
                                            </IonItem>
                                        </IonList>
                                    </IonCol>
                                </IonRow>
                                {searchResult.emergencyContact && (
                                    <IonAccordionGroup value="emergencyContact">
                                        <IonAccordion value="">
                                            <IonItem slot="header">
                                                <IonLabel>Emergency Contact</IonLabel>
                                            </IonItem>
                                            <IonGrid slot="content">
                                                <IonItem>
                                                    <IonLabel>Name</IonLabel>
                                                    <IonLabel slot="end">{searchResult.emergencyContact.name}</IonLabel>
                                                </IonItem>
                                                <IonItem>
                                                    <IonLabel>Phone</IonLabel>
                                                    <IonLabel slot="end">{searchResult.emergencyContact.phone}</IonLabel>
                                                </IonItem>
                                            </IonGrid>
                                        </IonAccordion>
                                    </IonAccordionGroup>
                                )}
                                <IonRow>
                                    <IonCol>
                                        <IonButton expand="block" color="tertiary" onClick={() => handleSearchSessions(searchResult.id)}>
                                            User history
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </IonCard>
                    ))}
                <IonModal isOpen={!!openModal} onDidDismiss={handleCancel}>
                    {openModal === "disableUser" && (
                        <>
                            <ModalHeader text={"Disable User"} />
                            <IonGrid className="flexBetween">
                                <IonRow />
                                <IonRow>
                                    <IonCol>Confirm to disable this user?</IonCol>
                                </IonRow>
                                <IonRow className="full-width">
                                    <IonCol>
                                        <IonButton expand="block">CONFIRM</IonButton>
                                    </IonCol>
                                    <IonCol>
                                        <IonButton color="medium" expand="block" onClick={handleCancel}>
                                            CANCEL
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </>
                    )}
                    {openModal === "storage" && (
                        <>
                            <ModalHeader text={"Customer storage"} />
                            <IonContent>
                                {userStorage.length ? (
                                    userStorage.map((i, idx) => (
                                        <IonCard key={i.validFrom.toString() + idx}>
                                            <IonCardHeader>
                                                <IonTitle>{"Storage #" + i.storageNum}</IonTitle>
                                            </IonCardHeader>
                                            <IonCardContent>
                                                <IonList>
                                                    <IonItem>
                                                        <IonLabel>Valid from:</IonLabel>
                                                        <IonLabel>{i.validFrom.toLocaleDateString("en-CA")}</IonLabel>
                                                    </IonItem>
                                                    <IonItem>
                                                        <IonLabel>Expiry date:</IonLabel>
                                                        <IonLabel>{i.validTo.toLocaleDateString("en-CA")}</IonLabel>
                                                    </IonItem>
                                                </IonList>
                                            </IonCardContent>
                                        </IonCard>
                                    ))
                                ) : (
                                    <IonGrid>
                                        <IonRow>
                                            <IonCol className="ion-padding">No storage purchased</IonCol>
                                        </IonRow>
                                    </IonGrid>
                                )}
                            </IonContent>
                            <IonFooter>
                                <IonRow>
                                    <IonCol>
                                        <IonButton color="medium" expand="block" onClick={handleCancel}>
                                            CANCEL
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonFooter>
                        </>
                    )}
                    {openModal === "mainUser" && (
                        <>
                            <ModalHeader text={"Main account"} />
                            <IonContent>{mainAcc && <CustomerCard customer={mainAcc} />}</IonContent>
                            <IonFooter>
                                <IonRow>
                                    <IonCol>
                                        <IonButton color="medium" expand="block" onClick={handleCancel}>
                                            CANCEL
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonFooter>
                        </>
                    )}
                </IonModal>
                <IonModal isOpen={historyModal} onDidDismiss={handleCancel}>
                    <ModalHeader text={"Customer's session history"} />
                    <IonContent>
                        <IonList>
                            {userSessions.length ? (
                                userSessions.map((session) => (
                                    <IonItem key={session.id}>
                                        <IonCol>
                                            <IonText style={{ textTransform: "capitalize" }}>{session.playType}</IonText>
                                        </IonCol>
                                        <IonCol>
                                            <IonText style={{ textTransform: "capitalize" }}>{session.sessionType}</IonText>
                                        </IonCol>
                                        <IonCol>
                                            <IonText>{formatDate(session.start!)}</IonText>
                                        </IonCol>
                                        <IonCol>
                                            <IonText>{`${formatTime(session.start!)} - ${formatTime(session.end!)}`}</IonText>
                                        </IonCol>
                                    </IonItem>
                                ))
                            ) : (
                                <IonRow>
                                    <IonCol className="ion-text-center">No records</IonCol>
                                </IonRow>
                            )}
                        </IonList>
                    </IonContent>
                    <IonFooter>
                        <IonRow>
                            <IonCol size="12">
                                {userSessions.length && (
                                    <IonButton onClick={() => historyRef.current.link.click()} expand="block">
                                        <CSVLink
                                            ref={historyRef}
                                            data={printSessionHistory(userSessions)}
                                            style={{ color: "#FFF", textDecoration: "none", pointerEvents: "none", width: "100%" }}
                                            filename={"Session records"}
                                        >
                                            DOWNLOAD USER HISTORY
                                        </CSVLink>
                                    </IonButton>
                                )}
                                <IonButton expand="block" color="medium" onClick={handleCancel}>
                                    Cancel
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonFooter>
                </IonModal>
                <IonAlert
                    isOpen={!!message}
                    onDidDismiss={() => {
                        setMessage("");
                    }}
                    header={`System Message`}
                    message={message}
                    buttons={[
                        {
                            text: "CLOSE",
                            handler: () => {
                                if (message === "User not found") {
                                    dispatch(clearSearch());
                                }
                                setMessage("");
                                setOpenModal(null);
                            },
                        },
                    ]}
                />
            </IonContent>
        </IonPage>
    );
}

export default SubAccountSearch;

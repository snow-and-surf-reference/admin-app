import {
    IonButton,
    IonCard,
    IonCardContent,
    IonChip,
    IonCol,
    IonContent,
    IonGrid,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonPage,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonText,
} from "@ionic/react";
import { Session, SessionType } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { getEntranceSessionsByDate } from "app/firebase";
import { setIsLoading } from "app/slices/globalSlice";
import { setQueriedSessions } from "app/slices/sessionsSlice";
import { RootState } from "app/store";
import { maxMonth, minMonth, thisMonth } from "app/variables";
import Header from "components/Global/Header";
import "css/RetailReport.css";
import dayjs from "dayjs";
import { diamondOutline } from "ionicons/icons";
import { useCallback, useEffect, useRef, useState } from "react";
import { CSVLink } from "react-csv";
import { Data } from "react-csv/components/CommonPropTypes";
import { useDispatch, useSelector } from "react-redux";
import EntranceReportItem from "./EntranceReportItem";

function EntranceReport() {
    const [isViewByDay, setIsViewByDay] = useState(false);
    const [viewDay, setViewDay] = useState(dayjs.tz(new Date()).format("YYYY-MM-DD"));
    const [viewMonth, setViewMonth] = useState(thisMonth);
    const queried = useSelector((state: RootState) => state.sessions.queriedSessions);
    const reportCsvRef = useRef<any>(null);
    const dispatch = useDispatch();

    const [filter, setFilter] = useState<SessionType | null>(null);
    const [availableTypes, setAvailableTypes] = useState<SessionType[]>([]);

    const handleQuery = useCallback(async () => {
        dispatch(setIsLoading(true));
        try {
            await getEntranceSessionsByDate(new Date(isViewByDay ? viewDay : viewMonth), !isViewByDay);
        } catch (error) {
            console.error(error);
        } finally {
            dispatch(setIsLoading(false));
        }
    }, [dispatch, isViewByDay, viewDay, viewMonth]);

    const recordFormat = useCallback((sessions: Session[]) => {
        if (!sessions.length) return;
        let tempRecord: Data = [];
        sessions.forEach((i) => {
            const { bookingNumber, sessionDate, start, end, bookingUser, pax, playType, sessionType, totalCredits, bookingStaff, privateCoach } = i;
            tempRecord.push({
                bookingNumber,
                date: dayjs.tz(sessionDate).format("YYYY-MM-DD"),
                time: dayjs.tz(start).format("HH:mm") + " - " + dayjs.tz(end).format("HH:mm"),
                guestId: bookingUser?.memberId,
                numberOfPersons: pax,
                bookingType: playType + " - " + sessionType,
                creditSpent: totalCredits,
                bookedByStaff: bookingStaff ? bookingStaff.email : "",
                coach: privateCoach ? privateCoach.usrname : null,
            });
        });
        return tempRecord;
    }, []);

    const downloadButton = useCallback(() => {
        if (!queried.length) {
            return (
                <IonButton disabled expand="block">
                    NO RECORD FOR DOWNLOAD
                </IonButton>
            );
        } else {
            const data = recordFormat(queried)!;
            const filename =
                (isViewByDay ? dayjs.tz(viewDay).format("YYYY-MM-DD") : dayjs.tz(viewMonth).format("YYYY-MMM")) + " - Park entrance record";
            return (
                <IonButton onClick={() => reportCsvRef.current.link.click()} expand="block">
                    <CSVLink
                        ref={reportCsvRef}
                        data={data}
                        style={{ color: "#FFF", textDecoration: "none", pointerEvents: "none", width: "100%" }}
                        filename={filename}
                    >
                        DOWNLOAD RECORDS
                    </CSVLink>
                </IonButton>
            );
        }
    }, [isViewByDay, viewDay, viewMonth, queried, recordFormat]);

    useEffect(() => {
        return () => {
            dispatch(setQueriedSessions([]));
        };
    }, [dispatch]);

    useEffect(() => {
        if (!queried.length) {
            setAvailableTypes([]);
            return;
        }

        let types: SessionType[] = [];
        queried.forEach((q) => {
            if (!types.includes(q.sessionType)) {
                types.push(q.sessionType);
            }
        });
        setAvailableTypes(types);
    }, [queried]);

    return (
        <IonPage>
            <Header title={`Park Entrance Report`} />
            <IonContent>
                <IonCard>
                    <IonCardContent>
                        <IonGrid>
                            <IonRow style={{ alignItems: "flex-end" }}>
                                <IonCol>
                                    <br />
                                    <IonText>{isViewByDay ? "Select Date" : "Select Month"}</IonText>
                                    {isViewByDay ? (
                                        <IonInput
                                            className="border"
                                            type="date"
                                            min={"2022-10-01"}
                                            value={viewDay}
                                            onIonChange={(e) => {
                                                setViewDay(e.detail.value!);
                                                dispatch(setQueriedSessions([]));
                                            }}
                                        />
                                    ) : (
                                        <IonInput
                                            className="border"
                                            type="month"
                                            min={minMonth > "2022-10" ? minMonth : "2022-10"}
                                            max={maxMonth}
                                            value={viewMonth}
                                            onIonChange={(e) => {
                                                setViewMonth(e.detail.value!);
                                                dispatch(setQueriedSessions([]));
                                            }}
                                        />
                                    )}
                                </IonCol>
                                <IonButton
                                    expand="block"
                                    onClick={() => {
                                        setIsViewByDay(!isViewByDay);
                                        dispatch(setQueriedSessions([]));
                                    }}
                                    color="tertiary"
                                >
                                    {isViewByDay ? "Change to view by month" : "Change to view by day"}
                                </IonButton>
                            </IonRow>
                            <IonRow>
                                <IonCol>
                                    <IonButton onClick={handleQuery} expand="block">
                                        SEARCH
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonCardContent>
                </IonCard>
                <IonGrid className="ion-padding">
                    {!queried.length ? (
                        <IonRow>
                            <IonCol className="ion-text-center">NO RECORD</IonCol>
                        </IonRow>
                    ) : (
                        <>
                            <IonCard color={"dark"}>
                                <IonCardContent>
                                    <IonList inset>
                                        <IonItem>
                                            <IonLabel>Filter by type:</IonLabel>
                                            <IonSelect
                                                value={filter}
                                                onIonChange={(e) => {
                                                    setFilter(e.detail.value!);
                                                }}
                                            >
                                                <IonSelectOption value={null}>Show All</IonSelectOption>
                                                {availableTypes.map((t) => (
                                                    <IonSelectOption key={`session-type-${t}`} value={t}>
                                                        {t}
                                                    </IonSelectOption>
                                                ))}
                                            </IonSelect>
                                        </IonItem>
                                    </IonList>
                                </IonCardContent>
                                </IonCard>
                                <IonItem>
                                    <IonLabel>Total:</IonLabel>
                                    <IonChip slot="end" color={'warning'}>
                                        <IonIcon icon={diamondOutline} />
                                        <IonLabel>
                                            {
                                                queried
                                                .filter((q) => {
                                                    if (!filter) {
                                                        return true;
                                                    }
                                                    const isTyped = filter === q.sessionType;
                                                    return isTyped;
                                                })
                                                    .reduce((p, c) => p + c.totalCredits, 0).toLocaleString()
                                            }
                                        </IonLabel>
                                    </IonChip>
                                </IonItem>
                            <IonList>
                                {queried
                                    .filter((q) => {
                                        if (!filter) {
                                            return true;
                                        }
                                        const isTyped = filter === q.sessionType;
                                        return isTyped;
                                    })
                                    .sort((a, b) => ((a.start?.valueOf() || 0) > (b.start?.valueOf() || 0) ? 1 : -1))
                                    .map((i) => (
                                        <EntranceReportItem session={i} key={`session-item-${i.id}`} />
                                    ))}
                            </IonList>
                            <IonRow>
                                <IonCol>{downloadButton()}</IonCol>
                            </IonRow>
                        </>
                    )}
                </IonGrid>
            </IonContent>
        </IonPage>
    );
}

export default EntranceReport;

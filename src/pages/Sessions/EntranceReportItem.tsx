import { IonButton, IonButtons, IonChip, IonIcon, IonItem, IonLabel } from "@ionic/react";
import { Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { diamondOutline } from "ionicons/icons";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault("Asia/Hong_Kong");

interface Props {
    session: Session;
}

const EntranceReportItem: React.FC<Props> = (props) => {
    const { session } = props;
    return (
        <>
            <IonItem button
                onClick={() => {
                    window.open(`/session/search/${session.id}`);
                }}
            >
                <IonLabel>
                    <h2>
                        <b># {session.bookingNumber}</b>
                    </h2>
                    <p>
                        {`${dayjs.tz(session.start).format(`HH:mm`)} - ${dayjs
                            .tz(session.end)
                            .format(`HH:mm`)} | ${dayjs.tz(session.start).format(`YYYY MMM DD`)}`}
                    </p>
                    <br />
                    <h4>{session.pax} PAX</h4>
                    <h4><b>{session.sessionType}</b>, {session.playType}</h4>
                    {
                        session.sessionType === 'privateClass' ? 
                            session.privateCoach ?
                                <h4>Coach: {session.privateCoach.usrname}</h4>
                                :
                                <IonButton
                                    onClick={() => {
                                        window.open(`/session/search/${session.id}`);
                                    }}
                                >Assign Coach</IonButton>
                            : null
                    }
                </IonLabel>
                <IonButtons slot="end">
                    <IonChip color={'warning'}>
                        <IonIcon icon={diamondOutline}/>
                        <IonLabel>{session.totalCredits.toLocaleString()}</IonLabel>
                    </IonChip>
                </IonButtons>
            </IonItem>
        </>
    );
};

export default EntranceReportItem;

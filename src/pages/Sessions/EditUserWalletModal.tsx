import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonContent,
    IonFooter,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonTitle,
    IonToolbar,
} from "@ionic/react";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { editCustomerBalance } from "app/firebase";
import { closeOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

interface Props {
    authCustomer: Customer | null;
    isOpen: boolean;
    onDismiss: () => void;
}

const EditUserWalletModal: React.FC<Props> = (props) => {
    const { authCustomer, isOpen, onDismiss } = props;
    const [balance, setBalance] = useState(0);
    const [saving, setSaving] = useState("idle");

    useEffect(() => {
        if (authCustomer) {
            setBalance(authCustomer.wallet.balance);
        }
    }, [authCustomer]);

    const handleSubmit = async () => {
        if (!authCustomer) {
            return;
        }
        setSaving("saving");
        await editCustomerBalance(authCustomer, balance);
        setSaving("saved");
    };

    return (
        <IonModal isOpen={isOpen} onDidDismiss={() => onDismiss()}>
            {authCustomer ? (
                <>
                    <IonHeader>
                        <IonToolbar>
                            <IonButton
                                fill="clear"
                                color={"danger"}
                                slot="start"
                                onClick={() => {
                                    onDismiss();
                                }}
                            >
                                <IonIcon icon={closeOutline} slot="start" />
                                <IonLabel>Cancel</IonLabel>
                            </IonButton>
                            <IonTitle>Edit Wallet Balance</IonTitle>
                        </IonToolbar>
                    </IonHeader>
                    <IonContent>
                        <IonCard color={"medium"}>
                            <IonCardContent>
                                <IonLabel>You are editing the credit balance for user: {authCustomer.email}</IonLabel>
                            </IonCardContent>
                        </IonCard>
                        <IonCard color={"light"}>
                            <IonCardContent>
                                <IonList inset>
                                    <IonItem>
                                        <IonLabel position="stacked">
                                            <p>New Balance</p>
                                        </IonLabel>
                                        <IonInput
                                            value={balance}
                                            type={"number"}
                                            onIonChange={(e) => {
                                                setBalance(Number(e.detail.value!));
                                            }}
                                        />
                                    </IonItem>
                                </IonList>
                            </IonCardContent>
                        </IonCard>
                    </IonContent>
                    <IonFooter>
                        <IonToolbar color={"warning"}>
                            <IonButton
                                color={"warning"}
                                expand="block"
                                onClick={() => {
                                    handleSubmit();
                                }}
                            >
                                <h3>Save</h3>
                            </IonButton>
                        </IonToolbar>
                    </IonFooter>
                    <IonLoading isOpen={saving === "saving"} message={"Saving new balance"} spinner={"dots"} />
                    <IonAlert
                        isOpen={saving === "saved"}
                        onDidDismiss={() => {
                            setSaving("idle");
                            onDismiss();
                        }}
                        header={`Balance Updated`}
                        buttons={[
                            {
                                text: "OK",
                                handler: () => {
                                    setSaving("idle");
                                    onDismiss();
                                },
                            },
                        ]}
                    />
                </>
            ) : null}
        </IonModal>
    );
};

export default EditUserWalletModal;

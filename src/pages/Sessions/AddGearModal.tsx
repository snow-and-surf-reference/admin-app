import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonCol,
    IonContent,
    IonFooter,
    IonGrid,
    IonHeader,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonRow,
    IonTitle,
    IonToolbar
} from "@ionic/react";
import * as GearTypes from "@tsanghoilun/snow-n-surf-interface/types/gear";
import { PaymentMethod } from "@tsanghoilun/snow-n-surf-interface/types/global";
import * as SessionTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { getCounterByDateV2, sessionAddMoreGears } from "app/firebase";
import { handleGearsStock } from "app/functions/handleSlots";
import { useAppSelector } from "app/hooks";
import { selectGears } from "app/slices/gearSlice";
import { selectPricing, selectPricingIsInit } from "app/slices/pricingSlice";
import { closeOutline, swapHorizontalOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

interface Props {
    isOpen: boolean;
    onDismiss: () => void;
    onSaved: () => void;
    sessionRaw: Session | null | undefined;
}

const AddGearModal: React.FC<Props> = (props) => {
    const { isOpen, onDismiss, sessionRaw, onSaved } = props;
    const [session, setSession] = useState<Session | null | undefined>(sessionRaw);
    const allGearsRaw = useAppSelector(selectGears);
    const [allGears, setAllGears] = useState(allGearsRaw);
    const [isPremium, setIsPremium] = useState(false);
    const [showGearSize, setShowGearSize] = useState("");
    const [selectedGears, setSelectedGears] = useState<SessionTypes.BookingGear[]>([]);
    const [newGearsTotal, setNewGearsTotal] = useState(0);
    const [showPaymentAlert, setShowPaymentAlert] = useState(false);
    const [disableSubmit, setDisableSubmit] = useState(false);
    const [saving, setSaving] = useState('idle');
    const [errorMessage, setErrorMessage] = useState('');

    // get global pricing
    const prices = useAppSelector(selectPricing);
    const pricesIsInit = useAppSelector(selectPricingIsInit);

    useEffect(() => {
        setSession(sessionRaw);
    }, [sessionRaw]);

    useEffect(() => {
        return () => {
            setSelectedGears([])
        }
    }, [])

    useEffect(() => {
        console.log('new gear total', newGearsTotal);
    }, [newGearsTotal]);

    // effect to update the gear availability
    useEffect(() => {
        if (!session) {
            return;
        }
        // const thisMonthCounter = monthCounters.find((x) => dayjs.tz(x.month).startOf("month").isSame(dayjs.tz(session.start).startOf("month")));
        // if (!thisMonthCounter || !session || !session.start || !session.end) {
        //     return;
        // }
        // const tempGears = handleGearsStock(allGearsRaw, thisMonthCounter, session);

        // setAllGears(tempGears);

        const getLatestData = async () => {
			if (!session || !session.start || !session.end) {
				return
			}
			const thisMonthCounter = await getCounterByDateV2(session.sessionDate);
			const tempGears = handleGearsStock(
				allGearsRaw,
				thisMonthCounter,
				session
			);
			setAllGears(tempGears);
		}

        getLatestData();
    }, [session, allGearsRaw]);
    
    // check can submit or not
    useEffect(() => {
        if (selectedGears.length) {
            setDisableSubmit(false)
        } else {
            setDisableSubmit(true)
        }
    }, [selectedGears])


    const addGear = (gear: GearTypes.Gear, size: number | string) => {
        if (!pricesIsInit || !prices) { return };

        if (!session) {
            return;
        }

        // create temp booking gear out of params
        const tempBookingGear: SessionTypes.BookingGear = {
            class: isPremium ? "premium" : "standard",
            gear,
            size,
        };
        const tempBookingGears: SessionTypes.BookingGear[] = [...selectedGears];
        tempBookingGears.push(tempBookingGear);

        let gearTotal = 0;
        const gearStandardPrice = prices.gear_standard.price;
        const gearPremiumPrice = prices.gear_premium.price;
        let allSelectedGears: SessionTypes.BookingGear[] = tempBookingGears.concat(session.gears);
        let bookedGears = {
            standard: {
                shoe: 0,
                board: 0
            },
            premium: {
                shoe: 0,
                board: 0
            }
        }
        allSelectedGears.forEach((sg) => {
            const usage = sg.gear.gearUsage;
            const gearClass = sg.class;
            bookedGears[gearClass][usage]++;
        });

        gearTotal = (
            Math.max(
                bookedGears['standard']['shoe'],
                bookedGears['standard']['board'],
            ) * gearStandardPrice
        ) + (
            Math.max(
                bookedGears['premium']['shoe'],
                bookedGears['premium']['board'],
            ) * gearPremiumPrice
        )

        setSelectedGears(tempBookingGears);
        setNewGearsTotal(gearTotal - session.subTotals.gears);
    };

    const removeSelectedGear = (idx: number) => {
        if (!pricesIsInit || !prices) { return };
        
        if (!session) {
            return;
        }

        let tempBookingGears: SessionTypes.BookingGear[] = [...selectedGears];
        tempBookingGears.splice(idx, 1);

        let gearTotal = 0;
        let allSelectedGears: SessionTypes.BookingGear[] = tempBookingGears.concat(session.gears);
        const gearStandardPrice = prices.gear_standard.price;
        const gearPremiumPrice = prices.gear_premium.price;
        let bookedGears = {
            standard: {
                shoe: 0,
                board: 0
            },
            premium: {
                shoe: 0,
                board: 0
            }
        }
        allSelectedGears.forEach((sg) => {
            const usage = sg.gear.gearUsage;
            const gearClass = sg.class;
            bookedGears[gearClass][usage]++;
        });

        gearTotal = (
            Math.max(
                bookedGears['standard']['shoe'],
                bookedGears['standard']['board'],
            ) * gearStandardPrice
        ) + (
            Math.max(
                bookedGears['premium']['shoe'],
                bookedGears['premium']['board'],
            ) * gearPremiumPrice
        )

        setSelectedGears(tempBookingGears);
        setNewGearsTotal(gearTotal - session.subTotals.gears);
    }

    const handleSubmit = async (finalPaymentType: PaymentMethod) => {
        if (!session) { return };

        setSaving('saving');

        const res = await sessionAddMoreGears(selectedGears, session, finalPaymentType, newGearsTotal);
        if (res.message && res.message === 'success') {
            setSaving('saved');
            return
        } else {
            setErrorMessage(res.message);
            setSaving('idle');
        }
    }

    return (
        <IonModal
            isOpen={isOpen}
            onDidDismiss={() => {
                setSelectedGears([]);
                onDismiss();
            }}
        >
            <IonHeader>
                <IonToolbar>
                    <IonButton
                        onClick={() => {
                            setSelectedGears([]);
                            onDismiss();
                        }}
                        fill="clear"
                    >
                        <IonIcon icon={closeOutline} slot="start" />
                        <IonLabel>Cancel</IonLabel>
                    </IonButton>
                    <IonTitle>Add Gears</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                {!session ? null : (
                    <IonGrid>
                        <IonRow>
                            <IonCol>
                                <IonCard color={isPremium ? `warning` : `light`}>
                                    <IonCardContent>
                                        <IonItem color={isPremium ? `warning` : `light`} lines="none">
                                            <IonLabel>
                                                <h2>
                                                    <b>{`Select ${isPremium ? `Premium` : `Standard`} Gears`}</b>
                                                </h2>
                                            </IonLabel>
                                            <IonButton
                                                size="small"
                                                shape="round"
                                                color={isPremium ? "medium" : `warning`}
                                                onClick={() => {
                                                    setIsPremium(!isPremium);
                                                }}
                                            >
                                                <IonIcon icon={swapHorizontalOutline} />
                                                <IonLabel className="ion-margin-start ion-margin-end">
                                                    {isPremium ? "Switch To Standard" : "Switch to Premium"}
                                                </IonLabel>
                                            </IonButton>
                                        </IonItem>
                                        <IonList inset>
                                            {allGears.map((x) => {
                                                return (
                                                    <IonItem key={`gear-item-${x.id}`}>
                                                        <IonLabel>{x.name}</IonLabel>
                                                        <IonButton
                                                            onClick={() => {
                                                                setShowGearSize(x.id);
                                                            }}
                                                        >
                                                            Select Size
                                                        </IonButton>
                                                        <IonModal
                                                            isOpen={showGearSize === x.id}
                                                            onDidDismiss={() => {
                                                                setShowGearSize("");
                                                            }}
                                                        >
                                                            <IonHeader>
                                                                <IonToolbar color={"dark"}>
                                                                    <IonTitle>{x.name}</IonTitle>
                                                                </IonToolbar>
                                                            </IonHeader>
                                                            <IonContent>
                                                                <IonList>
                                                                    {x.stock.map((y) => {
                                                                        return (
                                                                            <IonItem
                                                                                key={`gear-item-${x.id}-class-${y.class}-size-${y.size}`}
                                                                                button
                                                                                disabled={y.qty <= 0}
                                                                                onClick={() => {
                                                                                    addGear(x, y.size);
                                                                                    setShowGearSize("");
                                                                                }}
                                                                            >{`${y.size} (${x.unit.name}) - ${
                                                                                y.qty > 0 ? `${y.qty} Left` : "OUT OF STOCK"
                                                                            }`}</IonItem>
                                                                        );
                                                                    })}
                                                                </IonList>
                                                            </IonContent>
                                                        </IonModal>
                                                    </IonItem>
                                                );
                                            })}
                                        </IonList>
                                    </IonCardContent>
                                </IonCard>
                                
                                <IonCard color={"light"}>
                                    <IonCardContent>
                                        <IonItem color={"light"} lines="none">
                                            <IonLabel>
                                                <h2>
                                                    <b>New Selected Gears</b>
                                                </h2>
                                            </IonLabel>
                                        </IonItem>
                                        <IonList color="light">
                                            {selectedGears.length <= 0 ? (
                                                <IonItem color={"light"} lines="none">
                                                    No Gear Selected
                                                </IonItem>
                                            ) : (
                                                selectedGears.map((x, idx) => {
                                                    return (
                                                        <IonItem key={`session-gears-${idx}-item-${x.class}-${x.gear}-${x.size}`} color="light">
                                                            <IonLabel>
                                                                {`${x.class.toUpperCase()} ${x.gear.name} (${x.size} ${x.gear.unit.name})`}
                                                            </IonLabel>
                                                            <IonButton
                                                                slot="end"
                                                                fill="clear"
                                                                color={'danger'}
                                                                onClick={() => removeSelectedGear(idx)}
                                                            >
                                                                <IonIcon icon={closeOutline} slot='icon-only' />
                                                            </IonButton>
                                                        </IonItem>
                                                    );
                                                })
                                            )}
                                        </IonList>
                                    </IonCardContent>
                                </IonCard>

                                <IonCard color={"medium"}>
                                    <IonCardContent>
                                        <IonItem color={"medium"} lines="none">
                                            <IonLabel>
                                                <h2>
                                                    <b>Booked Gears</b>
                                                </h2>
                                            </IonLabel>
                                        </IonItem>
                                        <IonList color="medium">
                                            {session.gears.length <= 0 ? (
                                                <IonItem color={"medium"} lines="none">
                                                    No Gear Booked
                                                </IonItem>
                                            ) : (
                                                session.gears.map((x, idx) => {
                                                    return (
                                                        <IonItem key={`session-gears-${idx}-item-${x.class}-${x.gear}-${x.size}`} color="medium">
                                                            <IonLabel>
                                                                {`${x.class.toUpperCase()} ${x.gear.name} (${x.size} ${x.gear.unit.name})`}
                                                            </IonLabel>
                                                        </IonItem>
                                                    );
                                                })
                                            )}
                                        </IonList>
                                    </IonCardContent>
                                </IonCard>
                            </IonCol>
                        </IonRow>
                    </IonGrid>
                )}
            </IonContent>
            <IonFooter>
                <IonToolbar color={disableSubmit ? 'light' : 'warning'}>
                    <IonButton
                        expand="full"
                        color={disableSubmit ? 'light' : 'warning'}
                        disabled={disableSubmit}
                        onClick={() => {
                            setShowPaymentAlert(true);
                        }}
                    >
                        {`Confirm Payment (HKD $${newGearsTotal.toFixed(2)})`}
                    </IonButton>
                </IonToolbar>
            </IonFooter>
            <IonAlert
                isOpen={showPaymentAlert}
                onDidDismiss={() => {
                    setShowPaymentAlert(false)
                }}
                header={`Confirm Payment`}
                message={`Please select payment type`}
                buttons={[
                    {
                        text: 'By Credits',
                        handler: () => {
                            handleSubmit('credit')
                        }
                    },
                    {
                        text: 'By Cash',
                        handler: () => {
                            handleSubmit('cash')
                        }
                    },
                    {
                        text: 'By Visa/Master',
                        handler: () => {
                            handleSubmit('vm')
                        }
                    },
                    {
                        text: 'By AMEX',
                        handler: () => {
                            handleSubmit('ae')
                        }
                    },
                    {
                        text: 'By FPS',
                        handler: () => {
                            handleSubmit('fps')
                        }
                    },
                    {
                        text: 'By PayMe',
                        handler: () => {
                            handleSubmit('payme')
                        }
                    },
                    {
                        text: 'By AliPay',
                        handler: () => {
                            handleSubmit('alipay')
                        }
                    },
                    {
                        text: 'By WeChat',
                        handler: () => {
                            handleSubmit('wechat')
                        }
                    },
                    {
                        text: 'By Union Pay',
                        handler: () => {
                            handleSubmit('unionPay')
                        }
                    },
                    {
                        text: 'By 八達通',
                        handler: () => {
                            handleSubmit('octopus')
                        }
                    },
                    {
                        text: 'By ENT',
                        handler: () => {
                            handleSubmit('ent')
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]}
            />
            <IonLoading
                isOpen={saving === 'saving'}
                spinner={`dots`}
                message={`Saving`}
            />
            <IonAlert
                isOpen={saving === 'saved'}
                header={`Gears Added to your session`}
                buttons={[
                    {
                        text: 'OK',
                        handler: () => {
                            setSaving('idle');
                            onSaved()
                        }
                    }
                ]}
            />
            <IonAlert
                isOpen={errorMessage !== ''}
                onDidDismiss={() => {
                    setErrorMessage('')
                }}
                header={`There is an error!`}
                message={errorMessage}
                buttons={[
                    {
                        text: 'Back',
                        role: 'cancel',
                        handler: () => {
                            setErrorMessage('')
                        }
                    }
                ]}
            />
        </IonModal>
    );
};

export default AddGearModal;

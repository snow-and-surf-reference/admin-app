import {
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonChip,
    IonCol,
    IonContent,
    IonGrid,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonModal,
    IonPage,
    IonRow,
    IonSegment,
    IonSegmentButton,
    IonSpinner,
    IonText
} from "@ionic/react";
import { Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { selectLoadedSessions } from "app/slices/sessionsSlice";
import Header from "components/Global/Header";
import "css/Sessions.css";
import dayjs from "dayjs";
import { duration } from "helpers/date";
import { people, timer } from "ionicons/icons";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";

type TabType = "pending" | "checkedIn" | "today";

const UpcomingSessionsList: React.FC = () => {
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [scannedUID, setScannedUID] = useState("");
    const sessions = useSelector(selectLoadedSessions).filter((i) => i.status !== "cancelled");
    const [tab, setTab] = useState<TabType>("pending");

    useEffect(() => {
        if (scannedUID) {
            setTimeout(() => setScannedUID(""), 1500);
        }
    }, [scannedUID]);

    return (
        <IonPage>
            <Header title="Upcoming Sessions" />
            <IonContent fullscreen>
                <IonCard>
                    <IonCardContent>
                        <IonSegment
                            value={tab}
                            onIonChange={(e) => {
                                setTab(e.detail.value! as TabType);
                            }}
                        >
                            <IonSegmentButton value="pending">Pending</IonSegmentButton>
                            <IonSegmentButton value="checkedIn">Checked In</IonSegmentButton>
                            <IonSegmentButton value="today">All Today</IonSegmentButton>
                        </IonSegment>
                    </IonCardContent>
                </IonCard>

                {tab === "pending" ? (
                    <IonList inset>
                        {/* <IonButton onClick={async () => await generateSession()}>Generate a session</IonButton> */}
                        {sessions
                            .filter((i) => !!i.checkedInUsers.length && !i.isCheckedIn)
                            .map((i) => (
                                <SessionRow key={`pending-session-${i.bookingNumber}`} session={i} />
                            ))}
                    </IonList>
                ) : tab === "checkedIn" ? (
                    <IonList inset>
                        {sessions
                            .filter((i) => i.isCheckedIn)
                            .map((i) => (
                                <SessionRow key={i.id} session={i} />
                            ))}
                    </IonList>
                ) : (
                    tab === "today" && (
                        <IonList inset>
                            {sessions
                                .filter((i) => i.bookingNumber !== "temp")
                                .map((i) => (
                                    <SessionRow key={`today-session-${i.bookingNumber}`} session={i} />
                                ))}
                        </IonList>
                    )
                )}

                <IonModal isOpen={modalIsOpen} onDidDismiss={() => setModalIsOpen(false)}>
                    <IonGrid className="modalContent">
                        <IonRow />
                        <IonRow>
                            <IonCol
                                style={{
                                    display: "flex",
                                    flexDirection: "column",
                                    alignItems: "center",
                                }}
                            >
                                <IonText style={{ marginBottom: "8px" }}>{scannedUID ? "Scan success!" : "Please scan circlet"}</IonText>
                                {!scannedUID && <IonSpinner color="medium"></IonSpinner>}
                            </IonCol>
                        </IonRow>
                        <IonRow>
                            <IonCol className="buttonContainer" style={{ flexDirection: "column" }}>
                                <IonButton onClick={() => setScannedUID("12345")}>Force Complete Button</IonButton>
                                <IonButton onClick={() => setModalIsOpen(false)}>Close</IonButton>
                            </IonCol>
                        </IonRow>
                    </IonGrid>
                </IonModal>
            </IonContent>
        </IonPage>
    );
};

export default UpcomingSessionsList;

interface SessionRowProps {
    isQuery?: boolean;
    session: Session;
}

export const SessionRow = (props: SessionRowProps) => {
    const { isQuery = false } = props;
    const history = useHistory();
    const today = new Date().toLocaleDateString("en-CA");
    const sessionDate = props.session.sessionDate.toLocaleDateString("en-CA");
    return (
        <IonItem
            button
            detail
            onClick={() => {
                if (isQuery) {
                    history.push(`/session/search/${props.session.id}`);
                } else {
                    history.push(`/session/${props.session.id}`);
                }
            }}
            color={props.session.status === "cancelled" ? "medium" : "white"}
        >
            <IonChip slot="start">
                <IonLabel>{dayjs(Number(props.session.start)).format(`HH:mm`)}</IonLabel>
            </IonChip>
            <IonLabel>
                <h2>{`${props.session.bookingNumber} - ${props.session.sessionType}`}</h2>
                <p>
                    {dayjs(Number(props.session.start)).format(`HH:mmA`)} - {dayjs(Number(props.session.end)).format(`HH:mmA`)}
                </p>
            </IonLabel>
            {isQuery && (
                <IonChip color={sessionDate === today ? "success" : "danger"}>
                    <h6>{sessionDate === today ? "Today" : sessionDate}</h6>
                </IonChip>
            )}
            <IonButtons slot="end">
                {props.session.status === "cancelled" ? (
                    <IonChip>
                        <IonLabel>CANCELLED</IonLabel>
                    </IonChip>
                ) : (
                    <>
                        <IonChip color={`${props.session.checkedInUsers.length === props.session.pax ? "secondary" : "medium"}`}>
                            <IonIcon icon={people} />
                            <IonLabel>{`${props.session.checkedInUsers.length} / ${props.session.pax}`}</IonLabel>
                        </IonChip>
                        <IonChip>
                            <IonLabel style={{ textTransform: "capitalize" }}>{props.session.playType}</IonLabel>
                        </IonChip>
                        <IonChip color="warning">
                            <IonIcon icon={timer} />
                            <IonLabel>
                                {duration(props.session.start!, props.session.end!)}{" "}
                                {duration(props.session.start!, props.session.end!) > 1 ? "hours" : "hour"}
                                {/* {Math.floor(dayjs(Number(props.session.end)).diff(dayjs(Number(props.session.start)), "h", true))}{" "}
                                {`${Math.floor(dayjs(Number(props.session.end)).diff(dayjs(Number(props.session.start)), "h", true)) > 1 ? "hours" : "hour"}`} */}
                            </IonLabel>
                        </IonChip>
                    </>
                )}
            </IonButtons>
        </IonItem>
    );
};

import { IonAccordion, IonAccordionGroup, IonAlert, IonButton, IonButtons, IonCard, IonCardContent, IonIcon, IonItem, IonLabel, IonList, IonLoading } from "@ionic/react";
import { FaqCat } from "@tsanghoilun/snow-n-surf-interface/types/faq";
import { deleteFaqCat } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAllFaqCats, selectFaqIsInit } from "app/slices/faqSlice";
import LoadingCard from "components/Global/LoadingCard";
import { addCircleOutline, createOutline, removeCircleOutline } from "ionicons/icons";
import { useState } from "react";
import FaqNewCat from "./FaqNewCat";

const newCat: FaqCat = {
    id: "new",
    name: {
        zh: "",
        en: "",
    },
};

const FaqCatsList: React.FC = () => {
    const [showNewModal, setShowNewModal] = useState(false);
    const [selectedFaqCat, setSelectedFaqCat] = useState(newCat);
    const [deleting, setDeleting] = useState('');
    const faqIsInit = useAppSelector(selectFaqIsInit);
    const faqCats = useAppSelector(selectAllFaqCats);

    const handleDelete = async () => {
        if (deleting === '' || deleting === 'deleting' || deleting === 'deleted') { return };
        const taregetId = `${deleting}`;
        setDeleting('deleting');
        await deleteFaqCat(taregetId);
        setDeleting('');
    }

    return (
        <>
            {!faqIsInit ? (
                <LoadingCard />
            ) : (
                <>
                    <IonCard>
                        <IonCardContent>
                            <IonAccordionGroup>
                                <IonAccordion value="cat">
                                    <IonItem slot="header">
                                        <IonLabel>
                                            <b>FAQ Categories</b>
                                        </IonLabel>
                                    </IonItem>
                                    <IonList slot="content">
                                        {faqCats.map((x) => {
                                            return (
                                                <IonItem key={`faq-cat-${x.id}`}>
                                                    <IonLabel>
                                                        <h3>{x.name.en}</h3>
                                                        {x.name.zh !== "" ? <p>{x.name.zh}</p> : null}
                                                    </IonLabel>
                                                    <IonButtons slot="end">
                                                        <IonButton
                                                            fill="clear"
                                                            color={"secondary"}
                                                            onClick={() => {
                                                                setSelectedFaqCat(x);
                                                                setShowNewModal(true);
                                                            }}
                                                        >
                                                            <IonIcon icon={createOutline} slot="icon-only" />
                                                        </IonButton>
                                                        <IonButton fill="clear" color={"danger"}
                                                            onClick={() => {
                                                                setDeleting(x.id);
                                                            }}
                                                        >
                                                            <IonIcon icon={removeCircleOutline} slot="icon-only" />
                                                        </IonButton>
                                                    </IonButtons>
                                                </IonItem>
                                            );
                                        })}

                                            <IonItem button
                                                lines="none"
                                                onClick={() => {
                                                    setSelectedFaqCat(newCat);
                                                    setShowNewModal(true);
                                                }}
                                            >
                                            <IonIcon icon={addCircleOutline} slot="start" color="danger" />
                                            <IonLabel color="danger">Add Category</IonLabel>
                                        </IonItem>
                                    </IonList>
                                </IonAccordion>
                            </IonAccordionGroup>
                        </IonCardContent>
                    </IonCard>
                        <FaqNewCat isOpen={showNewModal} faqCat={selectedFaqCat} onDismiss={() => setShowNewModal(false)} />
                        <IonLoading
                            isOpen={deleting === 'deleting'}
                            message='Deleting'
                            spinner='dots'
                        />
                        <IonAlert
                            isOpen={deleting !== '' && deleting !== 'deleting' && deleting !== 'deleted'}
                            message='Are you sure?'
                            buttons={[
                                {
                                    text: 'Delete',
                                    role: 'confirm',
                                    cssClass: 'alert-delete',
                                    handler: () => {
                                        handleDelete()
                                    }
                                },
                                {
                                    text: 'Back',
                                    role: 'cancel',
                                    handler: () => {
                                        setDeleting('')
                                    }
                                }
                            ]}
                        />
                </>
            )}
        </>
    );
};

export default FaqCatsList;

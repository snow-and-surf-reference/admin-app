import {
    IonAccordion,
    IonAccordionGroup,
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonCol,
    IonGrid,
    IonIcon,
    IonItem,
    IonLabel, IonLoading,
    IonRow,
    IonSelect,
    IonSelectOption
} from "@ionic/react";
import { QNA } from "@tsanghoilun/snow-n-surf-interface/types/faq";
import { deleteFaqQuestion } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAllFaqCats, selectAllFaqQuestions, selectFaqIsInit } from "app/slices/faqSlice";
import LoadingCard from "components/Global/LoadingCard";
import { addCircleOutline, filterOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import FaqQuestionModal from "./FaqQuestionModal";

const FaqQuestionsList: React.FC = () => {
    const faqIsInit = useAppSelector(selectFaqIsInit);
    const faqCats = useAppSelector(selectAllFaqCats);
    const faqQuestions = useAppSelector(selectAllFaqQuestions);
    const [questionFilter, setQuestionFilter] = useState<string[]>([]);
    const [selectedQuestion, setSelectedQuestion] = useState<QNA | null>(null);
    const [newQuestion, setNewQuestion] = useState<QNA | null>(null);
    const [deleting, setDeleting] = useState("");

    useEffect(() => {
        if (newQuestion || faqCats.length <= 0) {
            return;
        }
        setNewQuestion({
            id: `new`,
            categoryId: faqCats[0].id,
            question: { zh: "", en: "" },
            answer: { zh: "", en: "" },
        });
    }, [faqCats, newQuestion]);

    const handleDelete = async () => {
        if (deleting === "" || deleting === "deleting" || deleting === "deleted") {
            return;
        }
        const taregetId = `${deleting}`;
        setDeleting("deleting");
        await deleteFaqQuestion(taregetId);
        setDeleting("");
    };

    return (
        <>
            {!faqIsInit ? (
                <LoadingCard />
            ) : (
                <>
                    <IonCard>
                        <IonCardContent>
                            <IonAccordionGroup value={`questions`}>
                                <IonAccordion value="questions">
                                    <IonItem slot="header">
                                        <IonLabel>
                                            <b>{`Questions & Answers`}</b>
                                        </IonLabel>
                                    </IonItem>
                                    <IonGrid slot="content">
                                        <IonRow>
                                            <IonCol size="12">
                                                <IonItem lines="none">
                                                    <IonIcon icon={filterOutline} slot="start" />
                                                    <IonLabel>{`${questionFilter.length > 0 ? `${questionFilter.length} filters` : "All"}`}</IonLabel>
                                                    <IonSelect
                                                        className="ion-text-end"
                                                        value={questionFilter}
                                                        multiple
                                                        onIonChange={(e) => {
                                                            setQuestionFilter(e.detail.value!);
                                                        }}
                                                    >
                                                        {faqCats.map((x) => {
                                                            return (
                                                                <IonSelectOption key={`faq-q-filter-option-${x.id}`} value={x.id}>
                                                                    {x.name.en}
                                                                </IonSelectOption>
                                                            );
                                                        })}
                                                    </IonSelect>
                                                </IonItem>
                                            </IonCol>
                                        </IonRow>
                                        <IonRow>
                                            <IonCol>
                                                <IonAccordionGroup>
                                                    {faqQuestions
                                                        .filter((q) => {
                                                            if (questionFilter.length <= 0) {
                                                                return true;
                                                            }
                                                            return questionFilter.includes(q.categoryId);
                                                        })
                                                        .map((x) => (
                                                            <IonAccordion value={`question-${x.id}`} key={`faq-question-${x.id}`}>
                                                                <IonItem slot="header">
                                                                    <IonLabel>
                                                                        <h3>
                                                                            Q: <b>{x.question.en}</b>
                                                                        </h3>
                                                                    </IonLabel>
                                                                </IonItem>
                                                                <div slot="content" className="ion-padding">
                                                                    <h3>
                                                                        <b>Answers:</b>
                                                                    </h3>
                                                                    <div dangerouslySetInnerHTML={{ __html: String(x.answer.en) }}></div>
                                                                    <div>
                                                                        <br />
                                                                        <IonButtons>
                                                                            <IonButton
                                                                                fill="solid"
                                                                                shape="round"
                                                                                color={`secondary`}
                                                                                onClick={() => {
                                                                                    setSelectedQuestion({ ...x });
                                                                                }}
                                                                            >
                                                                                <IonLabel className="ion-padding-start ion-padding-end">
                                                                                    Edit
                                                                                </IonLabel>
                                                                            </IonButton>
                                                                            <IonButton fill="solid" shape="round" color={`danger`}
                                                                                onClick={() => {
                                                                                    setDeleting(x.id)
                                                                                }}
                                                                            >
                                                                                <IonLabel className="ion-padding-start ion-padding-end">
                                                                                    Delete
                                                                                </IonLabel>
                                                                            </IonButton>
                                                                        </IonButtons>
                                                                    </div>
                                                                </div>
                                                            </IonAccordion>
                                                        ))}
                                                </IonAccordionGroup>
                                                <IonItem
                                                    lines="none"
                                                    button
                                                    onClick={() => {
                                                        setSelectedQuestion(newQuestion);
                                                    }}
                                                >
                                                    <IonIcon slot="start" icon={addCircleOutline} color={`danger`} />
                                                    <IonLabel color={`danger`}>Add New Question</IonLabel>
                                                </IonItem>
                                            </IonCol>
                                        </IonRow>
                                    </IonGrid>
                                </IonAccordion>
                            </IonAccordionGroup>
                        </IonCardContent>
                    </IonCard>
                    <FaqQuestionModal
                        isOpen={selectedQuestion !== null}
                        onDismiss={() => {
                            setSelectedQuestion(null);
                        }}
                        question={selectedQuestion}
                    />
                    <IonLoading isOpen={deleting === "deleting"} message="Deleting" spinner="dots" />
                    <IonAlert
                        isOpen={deleting !== "" && deleting !== "deleting" && deleting !== "deleted"}
                        message="Are you sure?"
                        buttons={[
                            {
                                text: "Delete",
                                role: "confirm",
                                cssClass: "alert-delete",
                                handler: () => {
                                    handleDelete();
                                },
                            },
                            {
                                text: "Back",
                                role: "cancel",
                                handler: () => {
                                    setDeleting("");
                                },
                            },
                        ]}
                    />
                </>
            )}
        </>
    );
};

export default FaqQuestionsList;

import { IonContent, IonPage } from "@ionic/react"
import Header from "components/Global/Header";
import FaqCatsList from "./FaqCatsList";
import FaqQuestionsList from "./FaqQuestionsList";


const FaqList: React.FC = () => {
    return (
        <IonPage>
            <Header title="FAQ" />
            <IonContent color={'light'}>
                <FaqCatsList />
                <FaqQuestionsList/>
            </IonContent>
        </IonPage>
    )
}

export default FaqList;
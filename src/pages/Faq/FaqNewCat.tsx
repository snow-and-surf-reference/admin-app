import {
    IonModal,
    IonHeader,
    IonToolbar,
    IonTitle,
    IonContent,
    IonCard,
    IonButton,
    IonIcon,
    IonLabel,
    IonItem,
    IonList,
    IonInput,
    IonFooter,
    IonAlert,
    IonLoading,
} from "@ionic/react";
import { FaqCat } from "@tsanghoilun/snow-n-surf-interface/types/faq";
import { setFaqCat } from "app/firebase";
import { closeOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

interface Props {
    isOpen: boolean;
    onDismiss: () => void;
    faqCat: FaqCat;
}

const newCat: FaqCat = {
    id: "new",
    name: {
        zh: "",
        en: "",
    },
};

const FaqNewCat: React.FC<Props> = (props) => {
    const { isOpen, onDismiss, faqCat } = props;
    const [cat, setCat] = useState(faqCat);
    const [saving, setSaving] = useState("idle");

    useEffect(() => {
        setCat(faqCat);
        return () => {
            setCat(newCat);
        };
    }, [faqCat]);

    const handleSubmit = async () => {
        setSaving("saving");
        await setFaqCat(cat);
        setSaving("saved");
        setCat(newCat);
        onDismiss();
    };

    return (
        <>
            <IonModal
                isOpen={isOpen}
                onDidDismiss={() => {
                    setCat(newCat);
                    onDismiss();
                }}
            >
                <IonHeader>
                    <IonToolbar>
                        <IonTitle>{cat.id === "new" ? `New Category` : `Edit Category`}</IonTitle>
                        <IonButton
                            slot="start"
                            onClick={() => {
                                setCat(newCat);
                                onDismiss();
                            }}
                            fill="clear"
                        >
                            <IonIcon icon={closeOutline} />
                            <IonLabel>Cancel</IonLabel>
                        </IonButton>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    <IonCard color={`light`}>
                        <IonItem color={"light"} lines="none">
                            <IonLabel>
                                <b>Category Title</b>
                            </IonLabel>
                        </IonItem>
                        <IonList inset>
                            <IonItem>
                                <IonInput
                                    type="text"
                                    placeholder="中文標題 (可選填)"
                                    value={cat.name.zh}
                                    onIonChange={(e) =>
                                        setCat((c) =>
                                            Object.assign(
                                                { ...c },
                                                {
                                                    name: Object.assign(
                                                        { ...c.name },
                                                        {
                                                            zh: e.detail.value! || "",
                                                        }
                                                    ),
                                                }
                                            )
                                        )
                                    }
                                />
                            </IonItem>
                            <IonItem>
                                <IonInput
                                    type="text"
                                    placeholder="English Title"
                                    value={cat.name.en}
                                    onIonChange={(e) =>
                                        setCat((c) =>
                                            Object.assign(
                                                { ...c },
                                                {
                                                    name: Object.assign(
                                                        { ...c.name },
                                                        {
                                                            en: e.detail.value! || "",
                                                        }
                                                    ),
                                                }
                                            )
                                        )
                                    }
                                />
                            </IonItem>
                        </IonList>
                    </IonCard>
                </IonContent>
                <IonFooter>
                    <IonToolbar color={cat.name.en === "" ? "light" : "warning"}>
                        <IonButton
                            expand="block"
                            color={cat.name.en === "" ? "light" : "warning"}
                            disabled={cat.name.en === ""}
                            onClick={() => handleSubmit()}
                        >
                            Save New Category
                        </IonButton>
                    </IonToolbar>
                </IonFooter>
            </IonModal>
            <IonLoading
                isOpen={saving === 'saving'}
                spinner='dots'
                message='Saving Changes'
            />
            <IonAlert
                isOpen={saving === 'saved'}
                onDidDismiss={() => {
                    setSaving('idle')
                }}
                header='Category Saved'
                buttons={[
                    {
                        text: 'OK',
                        role: 'confirm',
                        handler: () => {
                            setSaving('idle')
                        }
                    }
                ]}
            />
        </>
    );
};

export default FaqNewCat;

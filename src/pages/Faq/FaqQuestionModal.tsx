import {
    IonAccordion,
    IonAccordionGroup,
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonCol,
    IonContent,
    IonFooter,
    IonGrid,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonTitle,
    IonToolbar,
} from "@ionic/react";
import { QNA } from "@tsanghoilun/snow-n-surf-interface/types/faq";
import { setFaqQuestion } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAllFaqCats } from "app/slices/faqSlice";
import { closeOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

interface Props {
    isOpen: boolean;
    question: QNA | null;
    onDismiss: () => void;
}

const FaqQuestionModal: React.FC<Props> = (props) => {
    const { isOpen, question, onDismiss } = props;
    const [questionClone, setQuestionClone] = useState<QNA | null>(null);
    const [disableSubmit, setDisableSubmit] = useState(true);
    const [saving, setSaving] = useState('idle');
    const allcats = useAppSelector(selectAllFaqCats);

    useEffect(() => {
        // console.log('question changed on parent', question);
        if (question) {
            setQuestionClone({...question});
        }
    }, [question]);

    useEffect(() => {
        // console.log(`questionClone changed`, questionClone);
        if (!questionClone || !questionClone.question.en || !questionClone.answer.en) {
            setDisableSubmit(true);
        } else {
            setDisableSubmit(false);
        }
    }, [questionClone]);

    const handleSubmit = async () => {
        if (!questionClone) { return };
        setSaving('saving');
        await setFaqQuestion(questionClone);
        setSaving('saved');
        onDismiss()
    }

    return (
        <>
            <IonModal
                isOpen={isOpen}
                onDidDismiss={() => {
                    onDismiss();
                }}
            >
                <IonHeader>
                    <IonToolbar>
                        <IonButton slot="start" fill="clear"
                            onClick={() => {
                                onDismiss()
                            }}
                        >
                            <IonIcon icon={closeOutline} slot='start' />
                            <IonLabel>Cancel</IonLabel>
                        </IonButton>
                        <IonTitle>{questionClone && questionClone.id === 'new' ? `New` : `Edit`} Question</IonTitle>
                    </IonToolbar>
                </IonHeader>
                {!questionClone ? null : (
                    <>
                        <IonContent>
                            <IonCard color={`light`}>
                                <IonCardContent>
                                    <IonItem color={`light`} lines="none">
                                        <IonLabel>Question:</IonLabel>
                                    </IonItem>
                                    <IonList inset>
                                        <IonItem>
                                            <IonInput
                                                type="text"
                                                placeholder="中文問題(可選填)"
                                                value={questionClone.question.zh}
                                                onIonChange={(e) => {
                                                    setQuestionClone(
                                                        Object.assign(
                                                            { ...questionClone },
                                                            {
                                                                question: Object.assign(
                                                                    { ...questionClone.question },
                                                                    {
                                                                        zh: e.detail.value!,
                                                                    }
                                                                ),
                                                            }
                                                        )
                                                    );
                                                }}
                                            />
                                        </IonItem>
                                        <IonItem>
                                            <IonInput
                                                type="text"
                                                placeholder="English Question"
                                                value={questionClone.question.en}
                                                onIonChange={(e) => {
                                                    setQuestionClone(
                                                        Object.assign(
                                                            { ...questionClone },
                                                            {
                                                                question: Object.assign(
                                                                    { ...questionClone.question },
                                                                    {
                                                                        en: e.detail.value!,
                                                                    }
                                                                ),
                                                            }
                                                        )
                                                    );
                                                }}
                                            />
                                        </IonItem>
                                    </IonList>
                                </IonCardContent>
                            </IonCard>

                            <IonCard color={`light`}>
                                <IonItem lines="none" color={`light`}>
                                    <IonLabel>Category</IonLabel>
                                    <IonSelect
                                        value={questionClone.categoryId}
                                        onIonChange={(e) => {
                                            const tempCat = allcats.find((x) => x.id === e.detail.value!);
                                            console.log(tempCat);
                                            if (!tempCat) {
                                                return;
                                            }
                                            setQuestionClone(
                                                Object.assign(
                                                    { ...questionClone },
                                                    {
                                                        categoryId: tempCat.id,
                                                    }
                                                )
                                            );
                                        }}
                                    >
                                        {allcats.map((x) => (
                                            <IonSelectOption key={`q-${questionClone.id}-cat-option-${x.id}`} value={x.id}>
                                                {x.name.en}
                                            </IonSelectOption>
                                        ))}
                                    </IonSelect>
                                </IonItem>
                            </IonCard>
                            <IonCard color={`light`}>
                                <IonAccordionGroup>
                                    <IonAccordion value="question-en">
                                        <IonItem slot="header" color={`light`}>
                                            English Answer
                                        </IonItem>
                                        <IonGrid slot="content" color={`light`}>
                                            <IonRow color={`light`}>
                                                <IonCol color={`light`}>
                                                    <ReactQuill
                                                        theme="snow"
                                                        className="faq-editor"
                                                        value={questionClone.answer.en}
                                                        onChange={(value) => {
                                                            setQuestionClone(
                                                                Object.assign(
                                                                    { ...questionClone },
                                                                    {
                                                                        answer: Object.assign(
                                                                            { ...questionClone.answer },
                                                                            {
                                                                                en: String(value),
                                                                            }
                                                                        ),
                                                                    }
                                                                )
                                                            );
                                                        }}
                                                    />
                                                </IonCol>
                                            </IonRow>
                                        </IonGrid>
                                    </IonAccordion>
                                </IonAccordionGroup>
                            </IonCard>
                            <IonCard color={`light`}>
                                <IonAccordionGroup>
                                    <IonAccordion value="question-zh">
                                        <IonItem slot="header" color={`light`}>
                                            中文答案
                                        </IonItem>
                                        <IonGrid slot="content" color={`light`}>
                                            <IonRow color={`light`}>
                                                <IonCol color={`light`}>
                                                    <ReactQuill
                                                        theme="snow"
                                                        className="faq-editor"
                                                        value={questionClone.answer.zh}
                                                        onChange={(value) => {
                                                            setQuestionClone(
                                                                Object.assign(
                                                                    { ...questionClone },
                                                                    {
                                                                        answer: Object.assign(
                                                                            { ...questionClone.answer },
                                                                            {
                                                                                zh: String(value),
                                                                            }
                                                                        ),
                                                                    }
                                                                )
                                                            );
                                                        }}
                                                    />
                                                </IonCol>
                                            </IonRow>
                                        </IonGrid>
                                    </IonAccordion>
                                </IonAccordionGroup>
                            </IonCard>
                        </IonContent>
                    </>
                )}
                <IonFooter>
                    <IonToolbar color={disableSubmit ? 'medium' : 'warning'}>
                        <IonButton
                            expand="block"
                            color={disableSubmit ? 'medium' : 'warning'}
                            disabled={disableSubmit}
                            onClick={() => { 
                                handleSubmit()
                            }}
                        >
                            Save Changes
                        </IonButton>
                    </IonToolbar>
                </IonFooter>
            </IonModal>
            <IonLoading
                isOpen={saving === 'saving'}
                spinner='dots'
                message='Saving Changes'
            />
            <IonAlert
                isOpen={saving === 'saved'}
                onDidDismiss={() => {
                    setSaving('idle')
                }}
                header='Category Saved'
                buttons={[
                    {
                        text: 'OK',
                        role: 'confirm',
                        handler: () => {
                            setSaving('idle')
                        }
                    }
                ]}
            />
        </>
    );
};

export default FaqQuestionModal;

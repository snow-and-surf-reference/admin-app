import { IonAlert, IonButton, IonCol, IonContent, IonInput, IonItem, IonLabel, IonList, IonPage, IonRow } from "@ionic/react";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { createSubAccount } from "app/firebase";
import { setParentAccount } from "app/slices/usersSlice";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import "css/CheckIn.css";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";

type SubAccount = Omit<Customer, "id">;

const defaultSubAccount: SubAccount = {
    usrname: "",
    email: "",
    dob: new Date("1970-01-01, 00:00:00"),
    phone: "",
    isVerified: false,
    realName: "",
    realImage: "",
    createdAt: new Date(),
    enabled: true,
    wallet: { balance: 0, spent: 0 },
    emailVerified: false,
    isSub: false,
    memberId: "new",
};

function CreateSubAccount() {
    const [newUserData, setNewUserData] = useState<SubAccount>(defaultSubAccount);
    const [message, setMessage] = useState("");
    const dispatch = useDispatch();
    const parentAccount = useSelector((state: RootState) => state.users.parentAccountUser);
    const history = useHistory();

    const handleSubmit = async () => {
        await createSubAccount(newUserData).then((e) => {
            if (e === "success") {
                setMessage("Sub account created");
            } else {
                setMessage(e);
            }
            resetState();
        });
    };

    const resetState = () => {
        setNewUserData(defaultSubAccount);
    };

    useEffect(() => {
        if (parentAccount) {
            setNewUserData({
                usrname: "",
                email: parentAccount.email,
                dob: new Date("2000-01-01, 00:00:00"),
                phone: parentAccount.phone,
                isVerified: false,
                realName: "",
                realImage: "",
                createdAt: new Date(),
                enabled: true,
                wallet: { balance: 0, spent: 0 },
                emailVerified: true,
                isSub: true,
                memberId: "",
            });
        } else {
            history.push("/customers/sub-account");
        }
    }, [parentAccount, history]);

    return (
        <IonPage>
            <IonContent fullscreen>
                <Header
                    title={`Create Sub Account under ${parentAccount?.usrname}`}
                    back
                    extraFunction={() => {
                        dispatch(setParentAccount(null));
                    }}
                />
                <IonList>
                    <IonItem>
                        <IonLabel color="medium">Email address</IonLabel>
                        <IonInput type="text" required value={newUserData.email} disabled />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">{"Mobile (+852)"}</IonLabel>
                        <IonInput name="mobile" type="tel" required maxlength={8} value={newUserData.phone} disabled />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Username</IonLabel>
                        <IonInput
                            type="text"
                            required
                            value={newUserData.usrname}
                            onIonChange={(e) => {
                                setNewUserData({
                                    ...newUserData,
                                    usrname: e.detail.value!,
                                });
                            }}
                        />
                    </IonItem>

                    <IonItem>
                        <IonLabel color="medium">DOB</IonLabel>
                        <IonInput
                            type="date"
                            value={new Date(newUserData.dob).toLocaleDateString("en-CA")}
                            onIonChange={(e) => {
                                setNewUserData({
                                    ...newUserData,
                                    dob: new Date(e.detail.value! + ", 00:00:00"),
                                });
                            }}
                        />
                    </IonItem>
                    <IonRow>
                        <IonCol size="12">
                            <IonButton disabled={!newUserData.usrname} style={{ width: "100%" }} onClick={handleSubmit}>
                                Confirm
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonList>
            </IonContent>
            <IonAlert
                isOpen={!!message}
                onDidDismiss={() => {
                    setMessage("");
                }}
                header={`System Message`}
                message={message}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            dispatch(setParentAccount(null));
                            setMessage("");
                        },
                    },
                ]}
            />
        </IonPage>
    );
}

export default CreateSubAccount;

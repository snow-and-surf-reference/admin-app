import {
    IonAccordion,
    IonAccordionGroup,
    IonButton,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCol,
    IonContent,
    IonGrid,
    IonItem,
    IonLabel,
    IonModal,
    IonPage,
    IonRow,
    IonSearchbar,
    IonSegment,
    IonSegmentButton,
    IonSelect,
    IonSelectOption,
    IonText,
} from "@ionic/react";
import { Shop } from "@tsanghoilun/snow-n-surf-interface/types/shop";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import "css/ShopManagement.css";
import { ModalHeader } from "pages/Sessions/UserSearch";
import React, { useCallback, useState } from "react";
import BarcodeScannerComponent from "react-qr-barcode-scanner-17";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import ots from "media/ots.png";

const beep = new Audio("/barcodeBeep.mp3");
type SearchType = "item details" | "barcode";

function InventoryManagement() {
    const [tab, setTab] = useState<SearchType>("item details");
    const [searchShop, setSearchShop] = useState<Shop | null>(null);
    const [searchName, setSearchName] = useState("");
    const [searchSize, setSearchSize] = useState("");
    const [searchColor, setSearchColor] = useState("");
    const [searchBarcode, setSearchBarcode] = useState("");
    const [scannerModal, setScannerModal] = useState(false);
    const allShopItems = useSelector((state: RootState) => state.shop.shopItems);
    const history = useHistory();
    const allShops = useSelector((state: RootState) => state.shop.shops);

    const handleClearSearch = useCallback(() => {
        setSearchName("");
        setSearchColor("");
        setSearchSize("");
        setSearchBarcode("");
    }, []);

    return (
        <IonPage>
            <Header title={`Inventory Management`} />
            <IonContent>
                <IonGrid>
                    <IonRow>
                        <IonCol size="12">
                            <IonButton expand="block" color="tertiary" onClick={() => history.push("/inventory/management/stock")}>
                                VIEW STOCK
                            </IonButton>
                            <IonButton expand="block" onClick={() => history.push("/inventory/new-item")}>
                                ADD NEW ITEM
                            </IonButton>
                        </IonCol>
                    </IonRow>
                    <IonCard>
                        <IonAccordionGroup value="searchBar">
                            <IonAccordion value="searchBar">
                                <IonItem slot="header">SEARCH</IonItem>
                                <IonGrid slot="content">
                                    <IonRow>
                                        <IonCol />
                                        <IonCol size="8">
                                            <IonSegment
                                                value={tab}
                                                onIonChange={(e) => {
                                                    handleClearSearch();
                                                    //@ts-ignore
                                                    setTab(e.detail.value!);
                                                }}
                                            >
                                                <IonSegmentButton value="item details">Item details</IonSegmentButton>
                                                <IonSegmentButton value="barcode">Barcode</IonSegmentButton>
                                            </IonSegment>
                                        </IonCol>
                                        <IonCol />
                                    </IonRow>
                                    {tab === "item details" ? (
                                        <IonRow>
                                            <IonCol className="searchFields">
                                                <IonItem>
                                                    <IonSelect placeholder="Filter by shop" onIonChange={(e) => setSearchShop(e.detail.value)}>
                                                        {allShops
                                                            .filter((i) => i.isActive)
                                                            .map((i) => (
                                                                <IonSelectOption key={i.id} value={i}>
                                                                    {i.name}
                                                                </IonSelectOption>
                                                            ))}
                                                    </IonSelect>
                                                    <IonButton slot="end" disabled={!searchShop} onClick={() => setSearchShop(null)}>
                                                        CLEAR
                                                    </IonButton>
                                                </IonItem>
                                                {/* <br /> */}
                                                <IonSearchbar
                                                    animated
                                                    placeholder={"Item size"}
                                                    value={searchSize}
                                                    onIonChange={(e) => setSearchSize(e.detail.value!)}
                                                />
                                            </IonCol>
                                            <IonCol className="searchFields">
                                                <IonSearchbar
                                                    animated
                                                    placeholder={"Item name"}
                                                    value={searchName}
                                                    onIonChange={(e) => setSearchName(e.detail.value!)}
                                                />
                                                <IonSearchbar
                                                    animated
                                                    placeholder={"Item color"}
                                                    value={searchColor}
                                                    onIonChange={(e) => setSearchColor(e.detail.value!)}
                                                />
                                            </IonCol>
                                        </IonRow>
                                    ) : (
                                        <IonRow>
                                            <IonCol size="10">
                                                <IonSearchbar
                                                    animated
                                                    placeholder={"Item barcode"}
                                                    value={searchBarcode}
                                                    onIonChange={(e) => setSearchBarcode(e.detail.value!)}
                                                />
                                            </IonCol>
                                            <IonCol>
                                                <IonButton expand="block" onClick={() => setScannerModal(true)}>
                                                    Use scanner
                                                </IonButton>
                                            </IonCol>
                                        </IonRow>
                                    )}
                                    <IonRow>
                                        <IonCol size="12">
                                            <IonButton
                                                color="danger"
                                                expand="block"
                                                disabled={!searchName && !searchSize && !searchColor}
                                                onClick={handleClearSearch}
                                            >
                                                Clear search fields
                                            </IonButton>
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>
                            </IonAccordion>
                        </IonAccordionGroup>
                    </IonCard>
                    <IonRow>
                        {allShopItems
                            .filter((i) => i.isActive && allShops.find((shop) => shop.isActive && shop.id === i.shop.id))
                            .filter((i) => i.name.toLowerCase().includes(searchName.toLowerCase()))
                            .filter((i) => (!!searchSize ? i.size?.toUpperCase().includes(searchSize.toUpperCase()) : i))
                            .filter((i) => (!!searchColor ? i.color?.toLowerCase().includes(searchColor.toLowerCase()) : i))
                            .filter((i) => (!!searchBarcode ? i.barcode.includes(searchBarcode) : i))
                            .filter((i) => (!!searchShop && !!i.shop ? i.shop.id === searchShop.id : i))
                            .map((i) => (
                                <IonCol size="4" key={i.id}>
                                    <IonCard button onClick={() => history.push(`/inventory/management/${i.id}`)}>
                                        <IonCardHeader>
                                            <IonCardSubtitle color={!i.stock ? "danger" : "dark"}>{i.name}</IonCardSubtitle>
                                        </IonCardHeader>
                                        <IonCardContent>
                                            {i.image ? (
                                                <IonItem lines="none">
                                                    <IonCol className="imageContainer">
                                                        <img src={i.image} alt={i.image} />
                                                        {!i.stock && <img src={ots} alt={ots} className="ots" />}
                                                    </IonCol>
                                                </IonItem>
                                            ) : (
                                                <IonItem lines="none">
                                                    <IonCol className="imageContainer">
                                                        <IonText>No image</IonText>
                                                    </IonCol>
                                                </IonItem>
                                            )}
                                            <br />
                                            <IonRow>
                                                <IonCol>
                                                    <IonLabel>{`Color: ${i.color ? i.color : "N/A"}`}</IonLabel>
                                                </IonCol>
                                                <IonCol>
                                                    <IonLabel>{`Size: ${i.size ? i.size : "N/A"}`}</IonLabel>
                                                </IonCol>
                                            </IonRow>
                                            <IonRow>
                                                <IonCol>
                                                    <IonLabel>{`Price: ${i.price}`}</IonLabel>
                                                </IonCol>
                                                <IonCol>
                                                    <IonLabel>{`Stock: ${i.stock}`}</IonLabel>
                                                </IonCol>
                                            </IonRow>
                                        </IonCardContent>
                                    </IonCard>
                                </IonCol>
                            ))}
                    </IonRow>
                </IonGrid>
                <IonModal isOpen={scannerModal} onDidDismiss={() => setScannerModal(false)}>
                    <ModalHeader text={"Scan barcode"} />
                    <IonContent>
                        <IonGrid className="flex-column-between">
                            <IonRow>
                                <IonCol />
                                <IonCol size="8" className="ion-text-center">
                                    <IonText>Barcode scanner</IonText>
                                    <BarcodeScannerComponent
                                        delay={800}
                                        onUpdate={(err, result: any) => {
                                            if (result) {
                                                beep.play();
                                                setSearchBarcode(result.text);
                                                setScannerModal(false);
                                            }
                                        }}
                                    />
                                </IonCol>
                                <IonCol />
                            </IonRow>
                            <IonRow className="full-w">
                                <IonCol size="12">
                                    <IonButton expand="block" onClick={() => setScannerModal(false)}>
                                        CANCEL
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonContent>
                </IonModal>
            </IonContent>
        </IonPage>
    );
}

export default InventoryManagement;

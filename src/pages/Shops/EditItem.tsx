import {
    IonAlert,
    IonButton,
    IonCol,
    IonContent,
    IonGrid,
    IonImg,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonModal,
    IonPage,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonText,
    IonToggle,
} from "@ionic/react";
import { Shop, ShopItem } from "@tsanghoilun/snow-n-surf-interface/types/shop";
import { activateShopItem, updateShopItem } from "app/firebase";
import { selectShopItemsById } from "app/slices/shopItemSlice";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import { ModalHeader } from "pages/Sessions/UserSearch";
import { useEffect, useRef, useState } from "react";
import BarcodeScannerComponent from "react-qr-barcode-scanner-17";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";
import { ScanType } from "./CreateItem";
import { singleAlertBtn } from "app/variables";
import { setIsLoading } from "app/slices/globalSlice";

const beep = new Audio("/barcodeBeep.mp3");

function EditItem() {
    const { id } = useParams<{ id: string }>();
    const shopItem = useSelector(selectShopItemsById(id));
    const [newItem, setNewItem] = useState<ShopItem>(shopItem!);
    const [disabledFields, setDisabledFields] = useState({ size: !!shopItem!.size, color: !!shopItem!.color });
    const [message, setMessage] = useState("");
    const [tempFile, setTempFile] = useState<File | null>(null);
    const [delModal, setDelModal] = useState(false);
    const [editShop, setEditShop] = useState<Shop | null>(null);
    const [reBarcode, setReBarcode] = useState(shopItem?.barcode);
    const [scanType, setScanType] = useState<ScanType>(null);
    const updateRef = useRef<ScanType>(null);
    const photoRef = useRef<HTMLInputElement>(null);
    const history = useHistory();
    const allShops = useSelector((state: RootState) => state.shop.shops);
    const dispatch = useDispatch();

    useEffect(() => {
        if (shopItem) {
            setNewItem(shopItem);
            // setNewItem({ ...newItem, shop: shopItem.shop });
            setReBarcode(shopItem.barcode);
            if (!!shopItem.size) {
                setDisabledFields((d) => Object.assign({ ...d }, { size: !!shopItem.size }));
            }
            if (!!shopItem.color) {
                setDisabledFields((d) => Object.assign({ ...d }, { color: !!shopItem.color }));
            }
        }
    }, [shopItem]);

    const handleSubmit = async () => {
        dispatch(setIsLoading(true));
        try {
            const item = handleCreateItem();
            const res = !!tempFile ? await updateShopItem(item, tempFile) : await updateShopItem(item);
            if (res === "success") {
                setMessage("Item edited");
            } else {
                setMessage(res);
            }
        } catch (error) {
            console.error(error);
        } finally {
            dispatch(setIsLoading(false));
        }
    };

    const handleDelete = async () => {
        dispatch(setIsLoading(true));
        try {
            const res = await activateShopItem(id, !newItem.isActive);
            if (res === "success") {
                setMessage(newItem.isActive ? "Item removed" : "Item restored");
                history.goBack();
                setDelModal(false);
            }
        } catch (error) {
            console.error(error);
        } finally {
            dispatch(setIsLoading(false));
        }
    };

    const handleCreateItem = () => {
        let temp = { ...newItem };
        if (!!temp.color) {
            if (!temp.color.trim()) {
                temp = { ...temp, color: null };
            } else {
                temp = { ...temp, color: temp.color.trim() };
            }
        }
        if (!!temp.size) {
            if (!temp.size.trim()) {
                temp = { ...temp, size: null };
            } else {
                temp = { ...temp, size: temp.size.trim() };
            }
        }
        return temp;
    };

    useEffect(() => {
        updateRef.current = scanType;
    }, [scanType]);

    return (
        <IonPage>
            <IonContent fullscreen>
                <Header title="Edit Item" back />
                <IonList>
                    <IonItem>
                        <IonLabel color="medium">Shop</IonLabel>
                        {!editShop && <IonText>{newItem.shop.name}</IonText>}
                        <IonSelect
                            // placeholder="Re-select shop"
                            value={editShop}
                            onIonChange={(e) => {
                                setEditShop(e.detail.value);
                                setNewItem({ ...newItem, shop: e.detail.value });
                            }}
                        >
                            {allShops
                                .filter((i) => i.isActive)
                                .map((i) => (
                                    <IonSelectOption key={i.id} value={i}>
                                        {i.name}
                                    </IonSelectOption>
                                ))}
                        </IonSelect>
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Name</IonLabel>
                        <IonInput
                            type="text"
                            required
                            disabled={!newItem.isActive}
                            value={newItem.name}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    name: e.detail.value!,
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Size</IonLabel>
                        <IonInput
                            type="text"
                            required
                            disabled={!disabledFields.size || !newItem.isActive}
                            value={newItem.size}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    size: e.detail.value!,
                                });
                            }}
                        />
                        {newItem.isActive && (
                            <IonToggle
                                checked={disabledFields.size}
                                onIonChange={(e) => {
                                    setDisabledFields({ ...disabledFields, size: e.detail.checked });
                                    setNewItem({ ...newItem, size: null });
                                }}
                            />
                        )}
                    </IonItem>

                    <IonItem>
                        <IonLabel color="medium">Color</IonLabel>
                        <IonInput
                            type="text"
                            required
                            disabled={!disabledFields.color || !newItem.isActive}
                            value={newItem.color}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    color: e.detail.value!,
                                });
                            }}
                        />
                        {newItem.isActive && (
                            <IonToggle
                                checked={disabledFields.color}
                                onIonChange={(e) => {
                                    setDisabledFields({ ...disabledFields, color: e.detail.checked });
                                    setNewItem({ ...newItem, color: null });
                                }}
                            />
                        )}
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Price</IonLabel>
                        <IonInput
                            type="text"
                            required
                            disabled={!newItem.isActive}
                            value={newItem.price}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    price: isNaN(parseInt(e.detail.value!)) ? 0 : Number(parseInt(e.detail.value!)),
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Stock</IonLabel>
                        <IonInput
                            type="text"
                            required
                            disabled={!newItem.isActive}
                            value={newItem.stock}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    stock: isNaN(parseInt(e.detail.value!)) ? 0 : Number(parseInt(e.detail.value!)),
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Barcode</IonLabel>
                        <IonInput
                            type="text"
                            required
                            disabled={!newItem.isActive}
                            value={newItem.barcode}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    barcode: e.detail.value!,
                                });
                            }}
                        />
                        <IonButton
                            onClick={() => {
                                setScanType("barcode");
                            }}
                        >
                            SCAN
                        </IonButton>
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Reconfirm Barcode</IonLabel>
                        <IonInput
                            color={reBarcode !== newItem.barcode ? "danger" : "dark"}
                            type="text"
                            required
                            value={reBarcode}
                            onIonChange={(e) => {
                                setReBarcode(e.detail.value!);
                            }}
                        />
                    </IonItem>
                    <input
                        ref={photoRef}
                        type="file"
                        multiple={false}
                        accept="image/*"
                        style={{ display: "none" }}
                        onChange={async (e) => {
                            if (!e.target.files) return;
                            const url = URL.createObjectURL(e.target.files[0]);
                            setTempFile(e.target.files[0]);
                            setNewItem({ ...newItem, image: url });
                        }}
                    />
                    <IonItem>
                        <IonLabel color="medium">Image</IonLabel>
                        <IonCol style={{ alignItems: "center" }}>
                            {newItem.isActive && (
                                <IonButton expand="block" onClick={() => photoRef.current?.click()}>
                                    {newItem.image ? "Re-Choose photo" : "Choose photo"}
                                </IonButton>
                            )}
                            {newItem.isActive && newItem.image && (
                                <IonButton expand="block" color="danger" onClick={() => setNewItem({ ...newItem, image: "" })}>
                                    Remove photo
                                </IonButton>
                            )}

                            {newItem.image && (
                                <IonCol className="flex-center">
                                    <IonImg src={newItem.image} alt={newItem.image} style={{ maxWidth: "50%" }} />
                                </IonCol>
                            )}
                        </IonCol>
                    </IonItem>

                    <IonRow>
                        <IonCol size="12">
                            {newItem.isActive && (
                                <IonButton
                                    expand="block"
                                    disabled={JSON.stringify(shopItem) === JSON.stringify(newItem) || reBarcode !== newItem.barcode || !newItem.shop}
                                    onClick={handleSubmit}
                                >
                                    Save changes
                                </IonButton>
                            )}
                            {newItem.isActive ? (
                                <IonButton expand="block" color="danger" onClick={() => setDelModal(true)}>
                                    Delete item
                                </IonButton>
                            ) : (
                                <IonButton expand="block" color="success" onClick={() => setDelModal(true)}>
                                    Restore item
                                </IonButton>
                            )}
                        </IonCol>
                    </IonRow>
                </IonList>
                <IonModal isOpen={delModal} onDidDismiss={() => setDelModal(false)}>
                    <ModalHeader text={`Remove ${shopItem!.name}`} />
                    <IonContent>
                        <IonGrid className="modalContent">
                            <IonRow />
                            <IonRow>
                                <IonCol className="ion-text-center">{`Confirm to ${newItem.isActive ? "delete" : "restore"} item?`}</IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol>
                                    <IonButton expand="block" color={newItem.isActive ? "danger" : "success"} onClick={handleDelete}>
                                        {newItem.isActive ? "Remove" : "RESTORE"}
                                    </IonButton>
                                    <IonButton expand="block" color="medium" onClick={() => setDelModal(false)}>
                                        CANCEL
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonContent>
                </IonModal>
                <IonModal isOpen={scanType !== null}>
                    <ModalHeader text={scanType === "barcode" ? "Scan barcode" : "Reconfirm barcode"} />
                    {scanType !== null && (
                        <IonContent>
                            <IonGrid className="flex-column-between">
                                <IonRow>
                                    <IonCol />
                                    <IonCol size="8" className="ion-text-center">
                                        <IonText>Barcode scanner</IonText>
                                        {scanType === "reBarcode" && (
                                            <>
                                                <br />
                                                <br />
                                                <IonText>Reconfirm barcode</IonText>
                                            </>
                                        )}
                                        <BarcodeScannerComponent
                                            delay={800}
                                            onUpdate={(err, result: any) => {
                                                if (result) {
                                                    beep.play();
                                                    if (updateRef.current !== "reBarcode") {
                                                        setNewItem({ ...newItem, barcode: result.text });
                                                        setScanType("reBarcode");
                                                    } else {
                                                        setReBarcode(result.text);
                                                        setScanType(null);
                                                    }
                                                }
                                            }}
                                        />
                                    </IonCol>
                                    <IonCol />
                                </IonRow>
                                <IonRow className="full-w">
                                    <IonCol size="12">
                                        <IonButton expand="block" onClick={() => setScanType(null)}>
                                            CANCEL
                                        </IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                        </IonContent>
                    )}
                </IonModal>
            </IonContent>
            <IonAlert
                isOpen={!!message}
                header={`System Message`}
                message={message}
                onDidDismiss={() => setMessage("")}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default EditItem;

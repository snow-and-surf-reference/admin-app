import {
    IonAlert,
    IonButton,
    IonButtons,
    IonCardSubtitle,
    IonCardTitle,
    IonCheckbox,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonItemDivider,
    IonLabel,
    IonList,
    IonModal,
    IonPage,
    IonRow,
    IonText,
} from "@ionic/react";
import { BasketItem, RefundShopOrder, ShopOrder } from "@tsanghoilun/snow-n-surf-interface/types/shop";
import { refundOrder } from "app/firebase";
import { setBasketItemArray } from "app/slices/shopItemSlice";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import "css/Cashier.css";
import { formatDate, formatTime } from "helpers/date";
import { addCircleOutline, removeCircleOutline } from "ionicons/icons";
import { ModalHeader } from "pages/Sessions/UserSearch";
import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { singleAlertBtn } from "app/variables";

function RefundShopItems() {
    const [message, setMessage] = useState("");
    // const [isRefund, setIsRefund] = useState(false);
    const [orderNo, setOrderNo] = useState("");
    const [searchedOrder, setSearchedOrder] = useState<ShopOrder | null>(null);
    const [tempItemList, setTempItemList] = useState<BasketItem[]>([]);
    const [restoreItem, setRestoreItem] = useState(false);
    const [checkoutModal, setCheckoutModal] = useState(false);
    const [isByCC, setIsByCC] = useState(false);
    const [refundedOrder, setRefundedOrder] = useState<RefundShopOrder[]>([]);
    const [showRefundedOrder, setShowRefundedOrder] = useState(false);
    const history = useHistory();
    const dispatch = useDispatch();
    const allOrders = useSelector((state: RootState) => state.shop.shopOrders);
    const shopBasket = useSelector((state: RootState) => state.shop.shopBasket);

    const handleReset = useCallback(() => {
        setOrderNo("");
        dispatch(setBasketItemArray([]));
        setTempItemList([]);
        setSearchedOrder(null);
    }, [dispatch]);

    const handleQtyChange = (item: BasketItem, idx: number, type: "minus" | "add") => {
        let tempNum = 0;
        let upperLimit = 0;
        if (!!refundedOrder.length) {
            refundedOrder.forEach((i) => {
                const foundItem = i.refundItems.find((x) => x.id === item.id);
                if (!!foundItem) {
                    tempNum += foundItem.qty;
                }
            });
            upperLimit = searchedOrder!.item[idx].qty - tempNum;
        }

        const temp = tempItemList.slice();
        if (item.qty - 1 < 0 && type === "minus") {
            return;
        } else if (!!upperLimit && type === "add" && item.qty + 1 > upperLimit) {
            return;
        } else if (!upperLimit && type === "add" && item.qty + 1 > searchedOrder!.item[idx].qty) {
            return;
        } else {
            temp.splice(idx, 1, { ...item, qty: type === "minus" ? item.qty - 1 : item.qty + 1 });
            setTempItemList(temp);
        }
    };
    const handleSubmit = useCallback(async () => {
        if (!searchedOrder || !tempItemList.length) return;
        const refundItems = tempItemList.filter((i) => !!i.qty);
        if (!refundItems.length) return;

        const data: RefundShopOrder = {
            id: "new",
            customerEmail: searchedOrder.email ?? null,
            refundItems: refundItems,
            restoreItem,
            refundAmount: refundItems.reduce((a, b) => a + b.qty * b.price, 0),
            refundMethod: isByCC ? "cc" : "cash", //"cash" | "cc"
            createdAt: new Date(),
            originalOrder: searchedOrder,
        };
        const res = await refundOrder(data);
        if (res === "success") {
            setMessage("Success");
            history.go(0);
        } else {
            setMessage(res);
        }
    }, [searchedOrder, tempItemList, restoreItem, isByCC, history]);

    const handleSearchOrder = () => {
        const order = allOrders.find((i) => i.orderNo.includes(orderNo));
        if (!!order) {
            let temp: BasketItem[] = [];
            setSearchedOrder(order);
            if (!order.refundOrder) {
                dispatch(setBasketItemArray(order.item));
                for (let i = 0; i < order.item.length; i++) {
                    temp.push({ ...order.item[i], qty: 0 });
                }
                setTempItemList(temp);
            } else {
                setRefundedOrder(order.refundOrder);
                const refundOrderClone = order.refundOrder.slice();
                const orderItemClone = order.item.slice();
                for (let i = 0; i < refundOrderClone.length; i++) {
                    const refundItemsClone = refundOrderClone[i].refundItems.slice();
                    for (let k = 0; k < refundItemsClone.length; k++) {
                        for (let j = 0; j < orderItemClone.length; j++) {
                            if (orderItemClone[j].id === refundItemsClone[k].id) {
                                if (!(orderItemClone[j].qty - refundItemsClone[k].qty)) {
                                    orderItemClone.splice(j, 1);
                                } else {
                                    orderItemClone.splice(j, 1, { ...orderItemClone[j], qty: orderItemClone[j].qty - refundItemsClone[k].qty });
                                }
                            }
                        }
                    }
                }
                if (!!orderItemClone.length) {
                    dispatch(setBasketItemArray(orderItemClone));
                    for (let i = 0; i < orderItemClone.length; i++) {
                        temp.push({ ...orderItemClone[i], qty: 0 });
                    }
                    setTempItemList(temp);
                } else {
                    setMessage("All items were refunded in this order");
                    setSearchedOrder(null);
                }
            }
        } else {
            setMessage("Order not found");
        }
    };

    useEffect(() => {
        if (!!refundedOrder.length) {
            setShowRefundedOrder(true);
        }
    }, [refundedOrder]);

    useEffect(() => {
        return () => {
            dispatch(setBasketItemArray([]));
        };
    }, [dispatch]);

    return (
        <IonPage>
            <Header title={`Refund items`} />
            <IonContent>
                <IonGrid className="maxHeight">
                    <IonRow>
                        <IonCol>
                            <br />
                            <IonHeader>
                                <IonText>Input order number to proceed</IonText>
                            </IonHeader>
                            <br />
                            <IonInput
                                clearInput
                                value={orderNo}
                                onIonChange={(e) => setOrderNo(e.detail.value!)}
                                className="border"
                                placeholder="Order number"
                                onKeyDown={(e) => {
                                    if (e.key === "Enter") handleSearchOrder();
                                }}
                            />
                            <br />
                            <IonButton expand="block" disabled={!orderNo} onClick={handleSearchOrder}>
                                FIND ORDER
                            </IonButton>
                        </IonCol>
                    </IonRow>
                    <br />
                    <IonRow>
                        {!!shopBasket.length && !!tempItemList.length && searchedOrder && (
                            <IonCol>
                                <IonCardTitle>Order item list</IonCardTitle>
                                <IonRow className="ion-padding">
                                    <IonCol>
                                        <IonCardSubtitle>{"Order: " + searchedOrder?.orderNo}</IonCardSubtitle>
                                        <IonCardSubtitle>
                                            {"Time: " + formatDate(searchedOrder?.createdAt!) + " " + formatTime(searchedOrder?.createdAt!)}
                                        </IonCardSubtitle>
                                    </IonCol>
                                </IonRow>

                                {shopBasket.map((i, idx) => (
                                    <IonItem key={i.id + idx}>
                                        <IonLabel>{i.name}</IonLabel>
                                        <IonButtons slot="end">
                                            <IonButton
                                                onClick={() => {
                                                    handleQtyChange(tempItemList[idx], idx, "minus");
                                                }}
                                            >
                                                <IonIcon icon={removeCircleOutline} slot="icon-only" />
                                            </IonButton>
                                            <IonText className="ion-padding">{tempItemList[idx].qty + " / " + i.qty}</IonText>
                                            <IonButton onClick={() => handleQtyChange(tempItemList[idx], idx, "add")}>
                                                <IonIcon icon={addCircleOutline} slot="icon-only" />
                                            </IonButton>
                                        </IonButtons>
                                    </IonItem>
                                ))}
                                <IonItemDivider />
                                <IonItem>
                                    <IonCol>Refund amount</IonCol>
                                    <IonCol className="ion-text-end">{"$" + tempItemList.reduce((a, b) => a + b.qty * b.price, 0)}</IonCol>
                                </IonItem>
                            </IonCol>
                        )}
                    </IonRow>
                    {!!searchedOrder && (
                        <IonRow>
                            <IonCol>
                                <IonButton expand="block" color="medium" onClick={handleReset}>
                                    CANCEL / CLEAR FIELDS
                                </IonButton>
                            </IonCol>
                            <IonCol>
                                <IonButton
                                    expand="block"
                                    color="success"
                                    disabled={!tempItemList.length || tempItemList.every((i) => !i.qty)}
                                    onClick={() => setCheckoutModal(true)}
                                >
                                    PROCEED TO NEXT STEP
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    )}
                </IonGrid>
                <IonModal isOpen={checkoutModal} onDidDismiss={() => setCheckoutModal(false)}>
                    <ModalHeader text={"Restore item?"} />
                    <IonContent>
                        <IonGrid className="flex-column-between">
                            <IonRow />
                            <IonRow className="full-w">
                                <IonCol className="ion-text-center">
                                    <IonRow>
                                        <IonCol size="12">
                                            <IonText>{"Restore refunded item to inventory?"}</IonText>
                                        </IonCol>
                                        <IonCol className="flex-center">
                                            <IonCheckbox
                                                color="success"
                                                checked={restoreItem}
                                                onIonChange={(e) => setRestoreItem(e.detail.checked)}
                                            />
                                            <IonText className="ion-padding">YES</IonText>
                                        </IonCol>
                                        <IonCol className="flex-center">
                                            <IonCheckbox
                                                color="success"
                                                checked={!restoreItem}
                                                onIonChange={(e) => setRestoreItem(!e.detail.checked)}
                                            />
                                            <IonText className="ion-padding">NO</IonText>
                                        </IonCol>
                                    </IonRow>
                                </IonCol>
                            </IonRow>
                            <IonRow className="full-w">
                                <IonCol className="ion-text-center">
                                    <IonRow>
                                        <IonCol size="12">
                                            <IonText>{"Refund method"}</IonText>
                                        </IonCol>
                                        <IonCol className="flex-center">
                                            <IonCheckbox color="success" checked={!isByCC} onIonChange={(e) => setIsByCC(!e.detail.checked)} />
                                            <IonText className="ion-padding">CASH</IonText>
                                        </IonCol>
                                        <IonCol className="flex-center">
                                            <IonCheckbox color="success" checked={isByCC} onIonChange={(e) => setIsByCC(e.detail.checked)} />
                                            <IonText className="ion-padding">CREDIT CARD</IonText>
                                        </IonCol>
                                    </IonRow>
                                </IonCol>
                            </IonRow>
                            <IonRow className="full-w">
                                <IonCol>
                                    <IonButton expand="block" onClick={handleSubmit}>
                                        SUBMIT
                                    </IonButton>
                                    <IonButton expand="block" color="medium" onClick={() => setCheckoutModal(false)}>
                                        CANCEL
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonContent>
                </IonModal>
                <IonModal isOpen={showRefundedOrder} onDidDismiss={() => setShowRefundedOrder(false)}>
                    <ModalHeader text={"Order was refunded"} />
                    <IonContent>
                        <IonGrid className="flex-column-between">
                            <IonRow />
                            <IonRow className="full-w">
                                <IonCol className="ion-text-center">
                                    <IonRow>
                                        <IonCol size="12">
                                            <IonText>{"Previously refunded item:"}</IonText>
                                        </IonCol>
                                    </IonRow>
                                    <IonList>
                                        {refundedOrder &&
                                            refundedOrder.map((i) =>
                                                i.refundItems.map((j) => (
                                                    <IonItem key={i.id + j.id}>
                                                        <IonText slot="start">{formatDate(i.createdAt)}</IonText>
                                                        <IonText>{j.name}</IonText>
                                                        <IonText slot="end">{j.qty}</IonText>
                                                    </IonItem>
                                                ))
                                            )}
                                    </IonList>
                                    <br />
                                    {refundedOrder && <IonText>{"Refunded: $" + refundedOrder.reduce((a, b) => a + b.refundAmount, 0)}</IonText>}
                                </IonCol>
                            </IonRow>
                            <IonRow className="full-w">
                                <IonCol>
                                    <IonButton expand="block" color="medium" onClick={() => setShowRefundedOrder(false)}>
                                        EXIT
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonContent>
                </IonModal>
            </IonContent>
            <IonAlert
                isOpen={!!message}
                header={`System Message`}
                message={message}
                onDidDismiss={() => setMessage("")}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default RefundShopItems;

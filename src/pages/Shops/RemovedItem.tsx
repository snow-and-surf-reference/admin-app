import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCol,
    IonContent,
    IonGrid,
    IonImg,
    IonItem,
    IonLabel,
    IonPage,
    IonRow,
} from "@ionic/react";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";

function RemovedItem() {
    const allShopItems = useSelector((state: RootState) => state.shop.shopItems);
    const history = useHistory();

    return (
        <IonPage>
            <Header title={`Removed Item`} />
            <IonContent>
                <IonGrid>
                    <IonRow>
                        {!allShopItems.filter((i) => !i.isActive).length ? (
                            <IonCol>
                                <IonLabel>No removed item</IonLabel>
                            </IonCol>
                        ) : (
                            allShopItems
                                .filter((i) => !i.isActive)
                                .map((i) => (
                                    <IonCol size="4" key={i.id}>
                                        <IonCard button onClick={() => history.push(`/inventory/management/${i.id}`)}>
                                            <IonCardHeader>
                                                <IonCardSubtitle>{i.name}</IonCardSubtitle>
                                            </IonCardHeader>
                                            <IonCardContent>
                                                <IonItem lines="none">
                                                    <IonCol
                                                        style={{ height: "176px", display: "flex", justifyContent: "center", alignItems: "center" }}
                                                    >
                                                        <IonImg src={i.image} alt={i.image} style={{ maxWidth: "160px" }} />
                                                    </IonCol>
                                                </IonItem>
                                                <br />
                                                <IonRow>
                                                    <IonCol>
                                                        <IonLabel>{`Color: ${!!i.color ? i.color : "N/A"}`}</IonLabel>
                                                    </IonCol>
                                                    <IonCol>
                                                        <IonLabel>{`Size: ${!!i.size ? i.size : "N/A"}`}</IonLabel>
                                                    </IonCol>
                                                </IonRow>
                                                <IonRow>
                                                    <IonCol>
                                                        <IonLabel>{`Price: ${i.price}`}</IonLabel>
                                                    </IonCol>
                                                    <IonCol>
                                                        <IonLabel>{`Stock: ${i.stock}`}</IonLabel>
                                                    </IonCol>
                                                </IonRow>
                                            </IonCardContent>
                                        </IonCard>
                                    </IonCol>
                                ))
                        )}
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
}

export default RemovedItem;

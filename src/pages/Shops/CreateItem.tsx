import {
    IonAlert,
    IonButton,
    IonCol,
    IonContent,
    IonGrid,
    IonImg,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonModal,
    IonPage,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonText,
    IonToggle,
} from "@ionic/react";
import { ShopItem } from "@tsanghoilun/snow-n-surf-interface/types/shop";
import { updateShopItem } from "app/firebase";
import { setIsLoading } from "app/slices/globalSlice";
import { RootState } from "app/store";
import { singleAlertBtn } from "app/variables";
import Header from "components/Global/Header";
import { Html5QrcodeScanner, Html5QrcodeScanType } from "html5-qrcode";
import { ModalHeader } from "pages/Sessions/UserSearch";
import { useCallback, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const beep = new Audio("/barcodeBeep.mp3");

const defaultNewItem: ShopItem = {
    id: "new",
    name: "",
    size: null,
    color: null,
    price: 0,
    stock: 0,
    barcode: "",
    image: "",
    isActive: true,
    shop: { id: "", name: "", isActive: false },
};

export type ScanType = "barcode" | "reBarcode" | null;

function CreateItem() {
    const [message, setMessage] = useState("");
    const [newItem, setNewItem] = useState<ShopItem>(defaultNewItem);
    const [reBarcode, setReBarcode] = useState("");
    const [scanType, setScanType] = useState<ScanType>(null);
    const [disabledFields, setDisabledFields] = useState({ size: false, color: false });
    const [tempFile, setTempFile] = useState<File | null>(null);
    const readerRef = useRef<HTMLDivElement>(null);
    const updateRef = useRef<ScanType>(null);
    const scannerRef = useRef<Html5QrcodeScanner | null>(null);
    const photoRef = useRef<HTMLInputElement>(null);
    const allShops = useSelector((state: RootState) => state.shop.shops);
    const dispatch = useDispatch();

    const handleSubmit = async () => {
        dispatch(setIsLoading(true));
        try {
            const item = handleCreateItem();
            const res = !!tempFile ? await updateShopItem(item, tempFile) : await updateShopItem(item);
            if (res === "success") {
                setMessage("New item created");
            } else {
                setMessage(res);
            }
        } catch (error) {
            console.error(error);
        } finally {
            dispatch(setIsLoading(false));
        }
    };

    const handleCreateItem = () => {
        let temp = { ...newItem };
        if (!!temp.color) {
            if (!temp.color.trim()) {
                temp = { ...temp, color: null };
            } else {
                temp = { ...temp, color: temp.color.trim() };
            }
        }
        if (!!temp.size) {
            if (!temp.size.trim()) {
                temp = { ...temp, size: null };
            } else {
                temp = { ...temp, size: temp.size.trim() };
            }
        }
        return temp;
    };

    useEffect(() => {
        updateRef.current = scanType;
    }, [scanType]);

    const handleResetFields = useCallback(() => {
        setNewItem(defaultNewItem);
        setReBarcode("");
        setTempFile(null);
    }, []);

    const handleUpdateBarcode = useCallback((barcode: string) => {
        if (updateRef.current !== "reBarcode") {
            setNewItem((newItem) => ({ ...newItem, barcode: barcode }));
            setScanType("reBarcode");
        } else {
            setReBarcode(barcode);
            setScanType(null);
        }
        beep.volume = 0.4;
        beep.play();
    }, []);

    useEffect(() => {
        if (!scanType) return;
        // let html5QrcodeScanner: Html5QrcodeScanner | null = null;
        setTimeout(() => {
            if (readerRef.current) {
                const onScanSuccess = (decodedText: any) => {
                    handleUpdateBarcode(decodedText);
                };

                if (!scannerRef.current) {
                    scannerRef.current = new Html5QrcodeScanner(
                        "reader",
                        {
                            fps: 8,
                            qrbox: { width: 150, height: 100 },
                            useBarCodeDetectorIfSupported: true,
                            // videoConstraints: { width: 400, height: 400, facingMode: {ideal: ["environment", "user"]} },
                            videoConstraints: { width: 400, height: 400, facingMode: { ideal: "environment" } },
                            supportedScanTypes: [Html5QrcodeScanType.SCAN_TYPE_CAMERA],
                        },
                        false
                    );
                }
                scannerRef.current.render(onScanSuccess, () => {});
            }
        }, 500);
        return () => {
            if (scannerRef.current) {
                try {
                    scannerRef.current.pause();
                    scannerRef.current.clear();
                } catch (e) {
                    console.warn(e);
                }
            }
        };
    }, [handleUpdateBarcode, scanType]);

    return (
        <IonPage>
            <IonContent fullscreen>
                <Header title="Create New Item" back />
                <IonList>
                    <IonItem>
                        <IonLabel color="medium">Shop</IonLabel>
                        <IonSelect placeholder="Select shop" onIonChange={(e) => setNewItem({ ...newItem, shop: e.detail.value })}>
                            {allShops
                                .filter((i) => i.isActive)
                                .map((i) => (
                                    <IonSelectOption key={i.id} value={i}>
                                        {i.name}
                                    </IonSelectOption>
                                ))}
                        </IonSelect>
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Name</IonLabel>
                        <IonInput
                            type="text"
                            required
                            value={newItem.name}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    name: e.detail.value!,
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Size</IonLabel>
                        <IonInput
                            type="text"
                            required
                            disabled={!disabledFields.size}
                            value={newItem.size}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    size: e.detail.value!,
                                });
                            }}
                        />
                        <IonToggle
                            checked={disabledFields.size}
                            onIonChange={(e) => {
                                setDisabledFields({ ...disabledFields, size: e.detail.checked });
                                setNewItem({ ...newItem, size: null });
                            }}
                        />
                    </IonItem>

                    <IonItem>
                        <IonLabel color="medium">Color</IonLabel>
                        <IonInput
                            type="text"
                            required
                            disabled={!disabledFields.color}
                            value={newItem.color}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    color: e.detail.value!,
                                });
                            }}
                        />
                        <IonToggle
                            checked={disabledFields.color}
                            onIonChange={(e) => {
                                setDisabledFields({ ...disabledFields, color: e.detail.checked });
                                setNewItem({ ...newItem, color: null });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Price</IonLabel>
                        <IonInput
                            type="text"
                            required
                            value={newItem.price}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    price: isNaN(parseInt(e.detail.value!)) ? 0 : Number(parseInt(e.detail.value!)),
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Stock</IonLabel>
                        <IonInput
                            type="text"
                            required
                            value={newItem.stock}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    stock: isNaN(parseInt(e.detail.value!)) ? 0 : Number(parseInt(e.detail.value!)),
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Barcode</IonLabel>
                        <IonInput
                            type="text"
                            required
                            value={newItem.barcode}
                            onIonChange={(e) => {
                                setNewItem({
                                    ...newItem,
                                    barcode: e.detail.value!,
                                });
                            }}
                        />
                        <IonButton
                            onClick={() => {
                                setScanType("barcode");
                            }}
                        >
                            SCAN
                        </IonButton>
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Reconfirm Barcode</IonLabel>
                        <IonInput
                            color={reBarcode !== newItem.barcode ? "danger" : "dark"}
                            type="text"
                            required
                            value={reBarcode}
                            onIonChange={(e) => {
                                setReBarcode(e.detail.value!);
                            }}
                        />
                        <IonButton
                            onClick={() => {
                                setScanType("reBarcode");
                            }}
                        >
                            SCAN
                        </IonButton>
                    </IonItem>
                    <input
                        ref={photoRef}
                        type="file"
                        multiple={false}
                        accept="image/*"
                        style={{ display: "none" }}
                        onChange={async (e) => {
                            if (!e.target.files) return;
                            const url = URL.createObjectURL(e.target.files[0]);
                            setTempFile(e.target.files[0]);
                            setNewItem({ ...newItem, image: url });
                        }}
                    />
                    <IonItem>
                        <IonLabel color="medium">Image</IonLabel>
                        <IonCol style={{ alignItems: "center" }}>
                            <IonButton expand="block" onClick={() => photoRef.current?.click()}>
                                {newItem.image ? "Re-Choose photo" : "Choose photo"}
                            </IonButton>
                            {newItem.image && (
                                <IonCol className="flex-center">
                                    <IonImg src={newItem.image} alt={newItem.image} style={{ maxWidth: "50%" }} />
                                </IonCol>
                            )}
                        </IonCol>
                    </IonItem>

                    <IonRow>
                        <IonCol size="12">
                            <IonButton
                                expand="block"
                                disabled={
                                    !newItem.shop.id ||
                                    reBarcode !== newItem.barcode ||
                                    !reBarcode ||
                                    !newItem.barcode ||
                                    !newItem.name ||
                                    !newItem.price ||
                                    (!newItem.size && disabledFields.size) ||
                                    (!newItem.color && disabledFields.color)
                                }
                                onClick={handleSubmit}
                            >
                                Create item
                            </IonButton>
                            <IonButton
                                expand="block"
                                color="medium"
                                disabled={JSON.stringify(newItem) === JSON.stringify(defaultNewItem)}
                                onClick={handleResetFields}
                            >
                                Clear All Fields
                            </IonButton>
                        </IonCol>
                    </IonRow>
                    <IonModal isOpen={scanType !== null} onDidDismiss={() => setScanType(null)}>
                        <ModalHeader text={scanType === "barcode" ? "Scan barcode" : "Reconfirm barcode"} />
                        {scanType !== null && (
                            <IonContent>
                                <IonGrid className="flex-column-between">
                                    <IonRow>
                                        <IonCol className="ion-text-center" style={{ width: "100%" }}>
                                            <IonText className="ion-text-center">Barcode scanner</IonText>
                                            {scanType === "reBarcode" && (
                                                <>
                                                    <br />
                                                    <br />
                                                    <IonText>Reconfirm barcode</IonText>
                                                </>
                                            )}
                                            <div id="reader" ref={readerRef} style={{ width: "100%" }} />
                                            {/* <BarcodeScannerComponent
                                                delay={800}
                                                onUpdate={(err, result: any) => {
                                                    if (result) {
                                                        beep.play();
                                                        if (updateRef.current !== "reBarcode") {
                                                            setNewItem({ ...newItem, barcode: result.text });
                                                            setScanType("reBarcode");
                                                        } else {
                                                            setReBarcode(result.text);
                                                            setScanType(null);
                                                        }
                                                    }
                                                }}
                                            /> */}
                                        </IonCol>
                                    </IonRow>
                                    <IonRow className="full-w">
                                        <IonCol size="12">
                                            <IonButton expand="block" onClick={() => setScanType(null)}>
                                                CANCEL
                                            </IonButton>
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>
                            </IonContent>
                        )}
                    </IonModal>
                </IonList>
            </IonContent>
            <IonAlert
                isOpen={!!message}
                header={`System Message`}
                message={message}
                onDidDismiss={() => setMessage("")}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default CreateItem;

import {
    IonAccordion,
    IonAccordionGroup,
    IonButton,
    IonCard,
    IonCardContent,
    IonCardSubtitle,
    IonCardTitle,
    IonCol,
    IonContent,
    IonGrid,
    IonInput,
    IonItem,
    IonItemDivider,
    IonLabel,
    IonList,
    IonPage,
    IonRow,
    IonSegment,
    IonSegmentButton,
    IonSelect,
    IonSelectOption,
    IonText,
    IonTitle,
} from "@ionic/react";
import { RefundShopOrder, ShopOrder } from "@tsanghoilun/snow-n-surf-interface/types/shop";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import { formatDate, formatTime } from "helpers/date";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { CSVLink } from "react-csv";
import { Data } from "react-csv/components/CommonPropTypes";
import { useSelector } from "react-redux";
import "css/RetailReport.css";

type TabType = "day" | "month" | "order";

function RetailReport() {
    const [tab, setTab] = useState<TabType>("day");
    const [selectShop, setSelectShop] = useState("");
    const [viewDate, setViewDate] = useState(new Date());
    const [viewMonth, setViewMonth] = useState(new Date().getFullYear().toString() + "-" + (new Date().getMonth() + 1).toString().padStart(2, "0"));
    const [orderNo, setOrderNo] = useState("");
    const shop = useSelector((state: RootState) => state.shop);
    const allShops = shop.shops;
    const shopOrders = shop.shopOrders;
    const refundOrders = shop.refundOrders;
    const purchaseCsvRef = useRef<any>(null);
    const refundCsvRef = useRef<any>(null);

    useEffect(() => {
        setSelectShop("");
        setViewDate(new Date());
        setViewMonth(new Date().getFullYear().toString() + "-" + (new Date().getMonth() + 1).toString().padStart(2, "0"));
    }, [tab]);

    const getOrderByShopId = useCallback(
        (shopId: string) => {
            if (tab === "day") {
                return shopOrders.filter(
                    (order) =>
                        formatDate(order.createdAt) === formatDate(viewDate ? viewDate : new Date()) &&
                        !!order.item.filter((item) => item.shop.id === shopId).length
                );
            } else if (tab === "month") {
                const startOfMonth = dayjs.tz(new Date(viewMonth)).startOf("month").toDate();
                const endOfMonth = dayjs.tz(new Date(viewMonth)).endOf("month").toDate();
                return shopOrders.filter(
                    (order) =>
                        order.createdAt > startOfMonth &&
                        order.createdAt < endOfMonth &&
                        !!order.item.filter((item) => item.shop.id === shopId).length
                );
            } else {
                return [];
            }
        },
        [shopOrders, tab, viewDate, viewMonth]
    );

    const filterRefundOrderByShopId = useCallback(
        (shopId: string) => {
            if (tab === "day") {
                return refundOrders.filter(
                    (order) =>
                        formatDate(order.createdAt) === formatDate(viewDate ? viewDate : new Date()) &&
                        !!order.refundItems.filter((item) => item.shop.id === shopId).length
                );
            } else if (tab === "month") {
                const startOfMonth = dayjs.tz(new Date(viewMonth)).startOf("month").toDate();
                const endOfMonth = dayjs.tz(new Date(viewMonth)).endOf("month").toDate();
                return refundOrders.filter(
                    (order) =>
                        order.createdAt > startOfMonth &&
                        order.createdAt < endOfMonth &&
                        !!order.refundItems.filter((item) => item.shop.id === shopId).length
                );
            } else return [];
        },
        [refundOrders, tab, viewDate, viewMonth]
    );

    const getOrderByDay = useCallback(() => {
        return shopOrders.filter((i) =>
            viewDate ? i.createdAt > dayjs.tz(viewDate).startOf("D").toDate() && i.createdAt < dayjs.tz(viewDate).endOf("D").toDate() : i
        );
    }, [shopOrders, viewDate]);

    const getOrderByMonth = useCallback(() => {
        const startOfMonth = dayjs.tz(new Date(viewMonth)).startOf("month").toDate();
        const endOfMonth = dayjs.tz(new Date(viewMonth)).endOf("month").toDate();
        return shopOrders.filter((order) => order.createdAt > startOfMonth && order.createdAt < endOfMonth);
    }, [shopOrders, viewMonth]);

    const getRefundOrderByDay = useCallback(() => {
        return refundOrders.filter((i) =>
            viewDate ? i.createdAt > dayjs.tz(viewDate).startOf("D").toDate() && i.createdAt < dayjs.tz(viewDate).endOf("D").toDate() : i
        );
    }, [refundOrders, viewDate]);

    const getRefundOrderByMonth = useCallback(() => {
        const startOfMonth = dayjs.tz(new Date(viewMonth)).startOf("month").toDate();
        const endOfMonth = dayjs.tz(new Date(viewMonth)).endOf("month").toDate();
        return refundOrders.filter((order) => order.createdAt > startOfMonth && order.createdAt < endOfMonth);
    }, [refundOrders, viewMonth]);

    const getGrandTotal = useCallback(() => {
        if (tab === "day") {
            const todayOrders = shopOrders.filter((order) => formatDate(order.createdAt) === formatDate(viewDate ? viewDate : new Date()));
            const todayRefunds = refundOrders.filter((order) => formatDate(order.createdAt) === formatDate(viewDate ? viewDate : new Date()));
            return todayOrders.reduce((a, b) => a + b.totalAmount, 0) - todayRefunds.reduce((a, b) => a + b.refundAmount, 0);
        } else if (tab === "month") {
            const startOfMonth = dayjs.tz(new Date(viewMonth)).startOf("month").toDate();
            const endOfMonth = dayjs.tz(new Date(viewMonth)).endOf("month").toDate();
            const thisMonthOrders = shopOrders.filter((order) => order.createdAt > startOfMonth && order.createdAt < endOfMonth);
            const thisMonthRefunds = refundOrders.filter((order) => order.createdAt > startOfMonth && order.createdAt < endOfMonth);
            return thisMonthOrders.reduce((a, b) => a + b.totalAmount, 0) - thisMonthRefunds.reduce((a, b) => a + b.refundAmount, 0);
        }
        return 0;
    }, [refundOrders, shopOrders, tab, viewDate, viewMonth]);

    const getOrderTotal = useCallback((order: ShopOrder, shopId: string) => {
        return order.item.filter((item) => item.shop.id === shopId).reduce((a, b) => a + b.qty * b.price, 0);
    }, []);

    const getShopTotal = (shopId: string) => {
        return (
            getOrderByShopId(shopId).reduce((a, b) => a + b.item.filter((i) => i.shop.id === shopId).reduce((c, d) => c + d.qty * d.price, 0), 0) -
            filterRefundOrderByShopId(shopId).reduce(
                (a, b) => a + b.refundItems.filter((item) => item.shop.id === shopId).reduce((c, d) => c + d.qty * d.price, 0),
                0
            )
        );
    };

    const printPurchaseOrderFormat = useCallback((orders: ShopOrder[]) => {
        let tempOrder: Data = [];
        orders.forEach((i) => {
            const { refundOrder, item, createdAt, totalAmount, paymentMethod, orderNo, email } = i;
            tempOrder.push({
                orderNo,
                totalAmount,
                paymentMethod,
                item: item.map((i) => i.name + " x " + i.qty).toString(),
                refundOrder: refundOrder.length,
                email,
                createdAt,
            });
        });
        return tempOrder;
    }, []);

    const printRefundOrderFormat = useCallback((refunds: RefundShopOrder[]) => {
        let tempRefund: Data = [];
        refunds.forEach((i) => {
            const { originalOrder, refundItems, createdAt, refundAmount, restoreItem, refundMethod, customerEmail } = i;
            tempRefund.push({
                orderNo: originalOrder.orderNo,
                refundAmount,
                refundMethod,
                itemRestored: restoreItem ? "YES" : "NO",
                refundItems: refundItems.map((i) => i.name + " x " + i.qty).toString(),
                customerEmail,
                createdAt,
            });
        });
        return tempRefund;
    }, []);

    const downloadAllPurchaseOrders = useCallback(() => {
        const orders = tab === "day" ? getOrderByDay() : tab === "month" ? getOrderByMonth() : [];
        const tempOrder = printPurchaseOrderFormat(orders);

        return !!orders.length ? (
            <IonButton onClick={() => purchaseCsvRef.current.link.click()} expand="block">
                <CSVLink
                    ref={purchaseCsvRef}
                    data={tempOrder}
                    style={{ color: "#FFF", textDecoration: "none", pointerEvents: "none", width: "100%" }}
                    filename={tab === "day" ? formatDate(orders[0].createdAt) : viewMonth + " - PURCHASE"}
                >
                    DOWNLOAD PURCHASE ORDERS
                </CSVLink>
            </IonButton>
        ) : (
            <IonButton disabled expand="block">
                NO PURCHASE ORDER FOR DOWNLOAD
            </IonButton>
        );
    }, [getOrderByDay, getOrderByMonth, printPurchaseOrderFormat, tab, viewMonth]);

    const downloadAllRefundOrders = useCallback(() => {
        const refunds = tab === "day" ? getRefundOrderByDay() : tab === "month" ? getRefundOrderByMonth() : [];
        const tempRefund = printRefundOrderFormat(refunds);

        return !!refunds.length ? (
            <IonButton onClick={() => refundCsvRef.current.link.click()} expand="block">
                <CSVLink
                    ref={refundCsvRef}
                    data={tempRefund}
                    style={{ color: "#FFF", textDecoration: "none", pointerEvents: "none", width: "100%" }}
                    filename={tab === "day" ? formatDate(refunds[0].createdAt) : viewMonth + " - REFUND"}
                >
                    DOWNLOAD REFUND ORDERS
                </CSVLink>
            </IonButton>
        ) : (
            <IonButton disabled expand="block">
                NO REFUND ORDER FOR DOWNLOAD
            </IonButton>
        );
    }, [getRefundOrderByDay, getRefundOrderByMonth, printRefundOrderFormat, tab, viewMonth]);

    const getPurchaseOrdersByShops = useCallback(
        (shopId: string) => {
            const order = getOrderByShopId(shopId);
            let temp: ShopOrder[] = [];
            if (!!order.length) {
                for (let i = 0; i < order.length; i++) {
                    const tempOrder = { ...order[i] };
                    tempOrder.item = order[i].item.filter((item) => item.shop.id === shopId);
                    tempOrder.totalAmount = getOrderTotal(order[i], shopId);
                    temp.push(tempOrder);
                }
            }
            return temp;
        },
        [getOrderByShopId, getOrderTotal]
    );

    const getRefundOrdersByShops = useCallback(
        (shopId: string) => {
            const order = filterRefundOrderByShopId(shopId);
            let temp: RefundShopOrder[] = [];
            if (!!order.length) {
                for (let i = 0; i < order.length; i++) {
                    const tempOrder = { ...order[i] };
                    tempOrder.refundItems = order[i].refundItems.filter((item) => item.shop.id === shopId);
                    tempOrder.refundAmount = order[i].refundItems.filter((item) => item.shop.id === shopId).reduce((a, b) => a + b.qty * b.price, 0);
                    temp.push(tempOrder);
                }
            }
            return temp;
        },
        [filterRefundOrderByShopId]
    );

    const dlCompanyPurchaseOrder = useCallback(
        (shopId: string) => {
            const orders = getPurchaseOrdersByShops(shopId);
            return printPurchaseOrderFormat(orders);
        },
        [getPurchaseOrdersByShops, printPurchaseOrderFormat]
    );

    const dlCompanyRefundOrder = useCallback(
        (shopId: string) => {
            const orders = getRefundOrdersByShops(shopId);
            return printRefundOrderFormat(orders);
        },
        [getRefundOrdersByShops, printRefundOrderFormat]
    );

    return (
        <IonPage>
            <Header title={`Retail report`} />
            <IonContent>
                <IonCard>
                    <IonCardContent>
                        <IonSegment
                            value={tab}
                            onIonChange={(e) => {
                                setTab(e.detail.value! as TabType);
                            }}
                        >
                            <IonSegmentButton value="day">View by day</IonSegmentButton>
                            <IonSegmentButton value="month">View by month</IonSegmentButton>
                            <IonSegmentButton value="order">View all orders</IonSegmentButton>
                        </IonSegment>
                        <IonGrid>
                            <IonRow>
                                {tab === "day" ? (
                                    <>
                                        <IonCol size="6">
                                            <br />
                                            <IonText>Select Date</IonText>
                                            <IonInput
                                                className="border"
                                                type="date"
                                                value={formatDate(viewDate)}
                                                onIonChange={(e) => setViewDate(new Date(e.detail.value!))}
                                            />
                                        </IonCol>
                                    </>
                                ) : tab === "month" ? (
                                    <IonCol size="6">
                                        <br />
                                        <IonText>Select Month</IonText>
                                        <IonInput
                                            className="border"
                                            type="month"
                                            value={viewMonth}
                                            onIonChange={(e) => setViewMonth(e.detail.value!)}
                                        />
                                    </IonCol>
                                ) : (
                                    <IonCol size="6">
                                        <br />
                                        <IonText>Search Order</IonText>
                                        <IonInput
                                            className="border"
                                            placeholder="Input order number"
                                            value={orderNo}
                                            onIonChange={(e) => setOrderNo(e.detail.value!)}
                                        />
                                    </IonCol>
                                )}
                                {tab !== "order" ? (
                                    <IonCol size="6">
                                        <br />
                                        <IonText>Select Shop</IonText>
                                        <IonSelect
                                            className="border"
                                            value={selectShop}
                                            onIonChange={(e) => {
                                                setSelectShop(e.detail.value!);
                                            }}
                                        >
                                            <IonSelectOption value={""}>All</IonSelectOption>
                                            {allShops.map((shop) => (
                                                <IonSelectOption key={shop.id} value={shop.id}>
                                                    {shop.name}
                                                </IonSelectOption>
                                            ))}
                                        </IonSelect>
                                    </IonCol>
                                ) : (
                                    <IonCol size="6">
                                        <br />
                                        <IonText>Select Date</IonText>
                                        <IonInput
                                            className="border"
                                            type="date"
                                            value={formatDate(viewDate)}
                                            onIonChange={(e) => setViewDate(new Date(e.detail.value!))}
                                        />
                                    </IonCol>
                                )}
                            </IonRow>
                        </IonGrid>
                    </IonCardContent>
                </IonCard>

                {tab !== "order" ? (
                    <>
                        {allShops
                            .filter((i) => (selectShop ? i.id === selectShop : i))
                            .map((shop) => (
                                <IonItem key={shop.id}>
                                    <IonGrid>
                                        <br />
                                        <IonCardTitle className="ion-text-start">{shop.name}</IonCardTitle>
                                        {/* {getOrderByShopId(shop.id).length ? (
                                            getOrderByShopId(shop.id).map((order) => (
                                                <IonCard key={order.id}>
                                                    <IonCardContent>
                                                        <IonAccordionGroup value={`details`}>
                                                            <IonAccordion>
                                                                <IonItem slot="header">
                                                                    {tab === "month" && (
                                                                        <IonCardSubtitle>{formatDate(order.createdAt) + " - "}</IonCardSubtitle>
                                                                    )}
                                                                    <IonCardSubtitle>
                                                                        {order.orderNo + " - $" + getOrderTotal(order, shop.id)}
                                                                    </IonCardSubtitle>
                                                                </IonItem>
                                                                <IonList slot="content">
                                                                    {order.item
                                                                        .filter((item) => item.shop.id === shop.id)
                                                                        .map((item) => (
                                                                            <IonItem key={item.id + "purchase"}>
                                                                                <IonLabel>{item.name}</IonLabel>
                                                                                <IonLabel slot="end">{"$" + item.price}</IonLabel>
                                                                                <IonLabel slot="end">{item.qty + " unit"}</IonLabel>
                                                                            </IonItem>
                                                                        ))}
                                                                    <IonItem lines="none">
                                                                        <IonLabel>TOTAL</IonLabel>
                                                                        <IonLabel slot="end">{"$" + getOrderTotal(order, shop.id)}</IonLabel>
                                                                    </IonItem>
                                                                </IonList>
                                                            </IonAccordion>
                                                        </IonAccordionGroup>
                                                    </IonCardContent>
                                                </IonCard>
                                            ))
                                        ) : (
                                            <IonCard>
                                                <IonCardContent>NO PURCHASE ORDER</IonCardContent>
                                            </IonCard>
                                        )} */}
                                        {getPurchaseOrdersByShops(shop.id).length ? (
                                            getPurchaseOrdersByShops(shop.id).map((order) => (
                                                <IonCard key={order.id}>
                                                    <IonCardContent>
                                                        <IonAccordionGroup value={`details`}>
                                                            <IonAccordion>
                                                                <IonItem slot="header">
                                                                    {tab === "month" && (
                                                                        <IonCardSubtitle>{formatDate(order.createdAt) + " - "}</IonCardSubtitle>
                                                                    )}
                                                                    <IonCardSubtitle>{order.orderNo + " - $" + order.totalAmount}</IonCardSubtitle>
                                                                </IonItem>
                                                                <IonList slot="content">
                                                                    {order.item.map((item) => (
                                                                        <IonItem key={item.id + "purchase"}>
                                                                            <IonLabel>{item.name}</IonLabel>
                                                                            <IonLabel slot="end">{"$" + item.price}</IonLabel>
                                                                            <IonLabel slot="end">{item.qty + " unit"}</IonLabel>
                                                                        </IonItem>
                                                                    ))}
                                                                    <IonItem lines="none">
                                                                        <IonLabel>TOTAL</IonLabel>
                                                                        <IonLabel slot="end">{"$" + order.totalAmount}</IonLabel>
                                                                    </IonItem>
                                                                </IonList>
                                                            </IonAccordion>
                                                        </IonAccordionGroup>
                                                    </IonCardContent>
                                                </IonCard>
                                            ))
                                        ) : (
                                            <IonCard>
                                                <IonCardContent>NO PURCHASE ORDER</IonCardContent>
                                            </IonCard>
                                        )}
                                        {!!getRefundOrdersByShops(shop.id).length &&
                                            getRefundOrdersByShops(shop.id).map((order) => (
                                                <IonCard key={order.id}>
                                                    <IonCardContent>
                                                        <IonAccordionGroup value={`details`}>
                                                            <IonAccordion>
                                                                <IonItem slot="header">
                                                                    {tab === "month" && (
                                                                        <IonCardSubtitle color="danger">
                                                                            {formatDate(order.createdAt) + " - "}
                                                                        </IonCardSubtitle>
                                                                    )}
                                                                    <IonCardSubtitle color="danger">
                                                                        {order.originalOrder.orderNo + " - Refunded $" + order.refundAmount}
                                                                    </IonCardSubtitle>
                                                                </IonItem>
                                                                <IonList slot="content">
                                                                    {order.refundItems.map((item) => (
                                                                        <IonItem key={item.id + "refund"}>
                                                                            <IonLabel>{item.name}</IonLabel>
                                                                            <IonLabel slot="end">{"$" + item.price}</IonLabel>
                                                                            <IonLabel slot="end">{item.qty + " unit"}</IonLabel>
                                                                        </IonItem>
                                                                    ))}
                                                                    <IonItem lines="none">
                                                                        <IonLabel color="danger">REFUNDED</IonLabel>
                                                                        <IonLabel slot="end" color="danger">
                                                                            {"$" + order.refundAmount}
                                                                        </IonLabel>
                                                                    </IonItem>
                                                                </IonList>
                                                            </IonAccordion>
                                                        </IonAccordionGroup>
                                                    </IonCardContent>
                                                </IonCard>
                                            ))}
                                        <IonRow>
                                            <IonCol>
                                                <IonTitle className="ion-text-center" color={getShopTotal(shop.id) >= 0 ? "" : "danger"}>
                                                    {"Total: $"}
                                                    {getShopTotal(shop.id)}
                                                </IonTitle>
                                            </IonCol>
                                        </IonRow>
                                        <br />
                                        <IonRow>
                                            {!!getPurchaseOrdersByShops(shop.id).length ? (
                                                <CSVLink
                                                    data={dlCompanyPurchaseOrder(shop.id)}
                                                    className="dlButton"
                                                    filename={
                                                        tab === "day"
                                                            ? `${formatDate(getPurchaseOrdersByShops(shop.id)[0].createdAt)} - ${
                                                                  shop.name
                                                              } - PURCHASE`
                                                            : `${viewMonth} - ${shop.name} - PURCHASE`
                                                    }
                                                    style={{ marginRight: "4px" }}
                                                >
                                                    Download {shop.name} purchase orders
                                                </CSVLink>
                                            ) : (
                                                <IonCol className="dlButton disabled" style={{ marginRight: "4px" }}>
                                                    No purchase order for download
                                                </IonCol>
                                            )}
                                            {!!getRefundOrdersByShops(shop.id).length ? (
                                                <CSVLink
                                                    data={dlCompanyRefundOrder(shop.id)}
                                                    className="dlButton"
                                                    filename={
                                                        tab === "day"
                                                            ? `${formatDate(getRefundOrdersByShops(shop.id)[0].createdAt)} - ${shop.name} - REFUND`
                                                            : `${viewMonth} - ${shop.name} - REFUND`
                                                    }
                                                >
                                                    Download {shop.name} refund orders
                                                </CSVLink>
                                            ) : (
                                                <IonCol className="dlButton disabled">No refund order for download</IonCol>
                                            )}
                                        </IonRow>
                                    </IonGrid>
                                </IonItem>
                            ))}
                        <br />
                        {!selectShop && (
                            <IonRow className="ion-padding">
                                <IonCol>
                                    <IonTitle color={getGrandTotal() >= 0 ? "" : "danger"}>{`Grand Total: $${getGrandTotal()}`}</IonTitle>
                                </IonCol>
                            </IonRow>
                        )}
                        <br />
                        <br />
                    </>
                ) : getOrderByDay().filter((i) => i.orderNo.includes(orderNo)).length ? (
                    getOrderByDay()
                        .filter((i) => i.orderNo.includes(orderNo))
                        .map((order) => (
                            <IonCard key={order.id}>
                                <IonCardContent>
                                    <IonItem slot="header" lines="none">
                                        <IonCardSubtitle>{order.orderNo}</IonCardSubtitle>
                                    </IonItem>
                                    <IonList slot="content">
                                        {order.item.map((item) => (
                                            <IonItem key={item.id}>
                                                <IonLabel>{item.name}</IonLabel>
                                                <IonLabel slot="end">{item.qty}</IonLabel>
                                            </IonItem>
                                        ))}
                                        <IonItem lines="none">
                                            <IonLabel>TOTAL</IonLabel>
                                            <IonLabel slot="end">${order.item.reduce((a, b) => a + b.qty * b.price, 0)}</IonLabel>
                                        </IonItem>
                                    </IonList>
                                </IonCardContent>
                            </IonCard>
                        ))
                ) : (
                    <IonRow>
                        <br />
                        <IonCol>
                            <IonTitle>No item</IonTitle>
                        </IonCol>
                    </IonRow>
                )}
                {tab === "order" &&
                    !!getRefundOrderByDay().filter((i) => i.originalOrder.orderNo.includes(orderNo)).length &&
                    getRefundOrderByDay()
                        .filter((i) => i.originalOrder.orderNo.includes(orderNo))
                        .map((order) => (
                            <IonCard key={order.id}>
                                <IonCardContent>
                                    <IonItem slot="header" lines="none">
                                        <IonCardSubtitle color="danger">{"REFUND ORDER - " + order.originalOrder.orderNo}</IonCardSubtitle>
                                        <IonCardSubtitle slot="end" color="danger">
                                            {"REFUND DATE - " + formatDate(order.createdAt) + " " + formatTime(order.createdAt)}
                                        </IonCardSubtitle>
                                    </IonItem>
                                    <IonList slot="content">
                                        {order.refundItems.map((item) => (
                                            <IonItem key={item.id}>
                                                <IonLabel>{item.name}</IonLabel>
                                                <IonLabel slot="end">{item.qty}</IonLabel>
                                            </IonItem>
                                        ))}
                                        <IonItem lines="none">
                                            <IonLabel color="danger">REFUNDED</IonLabel>
                                            <IonLabel color="danger" slot="end">
                                                ${order.refundAmount}
                                            </IonLabel>
                                        </IonItem>
                                    </IonList>
                                </IonCardContent>
                            </IonCard>
                        ))}
                {tab !== "order" && <IonItemDivider />}
                {tab !== "order" && (
                    <IonRow className="ion-padding">
                        <IonCol className="ion-text-center">{downloadAllPurchaseOrders()}</IonCol>
                        <IonCol className="ion-text-center">{downloadAllRefundOrders()}</IonCol>
                    </IonRow>
                )}
            </IonContent>
        </IonPage>
    );
}

export default RetailReport;

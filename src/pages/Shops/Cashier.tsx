import {
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonCardSubtitle,
    IonCardTitle,
    IonCol,
    IonContent,
    IonGrid,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonPage,
    IonRadio,
    IonRadioGroup,
    IonRow,
    IonText,
} from "@ionic/react";
import { BasketItem, PaymentType, ShopOrder } from "@tsanghoilun/snow-n-surf-interface/types/shop";
import { addShopOrder, isFirebaseError, setShopItem } from "app/firebase";
import { setBasketItemArray } from "app/slices/shopItemSlice";
import { RootState } from "app/store";
import { getLastOrderNo, shopPaymentMethods } from "app/variables";
import Header from "components/Global/Header";
import SimpleModal from "components/Global/SimpleModal";
import "css/Cashier.css";
import { Html5QrcodeScanner, Html5QrcodeScanType } from "html5-qrcode";
import { addCircleOutline, removeCircleOutline } from "ionicons/icons";
import { useCallback, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const beep = new Audio("/barcodeBeep.mp3");

function Cashier() {
    const [message, setMessage] = useState("");
    // const [isRefund, setIsRefund] = useState(false);
    const [data, setData] = useState("");
    const [checkoutModal, setCheckoutModal] = useState(false);
    const [cancelModal, setCancelModal] = useState(false);
    const [emailInput, setEmailInput] = useState("");
    const [paymentMethod, setPaymentMethod] = useState<PaymentType | null>(null);
    const [barcodeInput, showBarcodeInput] = useState(false);
    const [barcode, setBarcode] = useState("");
    const [paymentModal, setPaymentModal] = useState(false);
    const scannerRef = useRef<Html5QrcodeScanner | null>(null);
    const readerRef = useRef<HTMLDivElement>(null);
    const dispatch = useDispatch();
    const shopBasket = useSelector((state: RootState) => state.shop.shopBasket);

    const handleReset = () => {
        setData("");
        setBarcode("");
        setEmailInput("");
        showBarcodeInput(false);
        dispatch(setBasketItemArray([]));
        setPaymentMethod(null);
    };

    const handleScan = useCallback(async () => {
        try {
            await setShopItem(barcode || data);
            setData("");

            setBarcode((barcode) => {
                if (barcode) {
                    showBarcodeInput(false);
                    return "";
                }
                return barcode;
            });
        } catch (e) {
            if (isFirebaseError(e)) {
                setMessage(e.code);
            } else if (e instanceof Error) {
                setMessage(e.message);
            } else {
                console.error(e);
            }
        }
    }, [barcode, data]);

    useEffect(() => {
        if (!data) return;
        handleScan();
    }, [data, handleScan]);

    const handleQtyChange = (item: BasketItem, idx: number, type: "minus" | "add") => {
        const temp = shopBasket.slice();
        if (item.qty - 1 === 0 && type === "minus") {
            temp.splice(idx, 1);
            dispatch(setBasketItemArray(temp));
        } else if (item.qty + 1 > item.stock && type === "add") {
            return;
        } else {
            temp.splice(idx, 1, { ...item, qty: type === "minus" ? item.qty - 1 : item.qty + 1 });
            dispatch(setBasketItemArray(temp));
        }
    };

    const handleSubmit = async () => {
        if (!paymentMethod) return;
        const newOrderNo = getLastOrderNo();
        const order: ShopOrder = {
            id: "new",
            item: shopBasket,
            orderNo: newOrderNo,
            email: !!emailInput ? emailInput : null,
            paymentMethod,
            totalAmount: shopBasket.reduce((i, j) => i + j.price * j.qty, 0),
            refundOrder: [],
            createdAt: new Date(),
        };
        const res = await addShopOrder(order);
        if (res === "success") {
            setMessage("Success");
            handleReset();
        } else {
            setMessage(res);
        }
    };

    useEffect(() => {
        return () => {
            dispatch(setBasketItemArray([]));
        };
    }, [dispatch]);

    const handleUpdateBarcode = useCallback((barcode: string) => {
        setData(barcode);
        beep.volume = 0.4;
        beep.play();
    }, []);

    useEffect(() => {
        // let html5QrcodeScanner: Html5QrcodeScanner | null = null;
        setTimeout(() => {
            if (readerRef.current) {
                if (!scannerRef.current) {
                    scannerRef.current = new Html5QrcodeScanner(
                        "reader",
                        {
                            fps: 8,
                            qrbox: { width: 150, height: 100 },
                            useBarCodeDetectorIfSupported: true,
                            // videoConstraints: { width: 400, height: 400, facingMode: {ideal: ["environment", "user"]} },
                            videoConstraints: { width: 400, height: 400, facingMode: { ideal: "environment" } },
                            supportedScanTypes: [Html5QrcodeScanType.SCAN_TYPE_CAMERA],
                        },
                        false
                    );
                }
                const onScanSuccess = (decodedText: string, decodedResult: any) => {
                    handleUpdateBarcode(decodedText);
                    if (scannerRef.current) {
                        scannerRef.current.pause();
                        setTimeout(() => {
                            if (scannerRef.current) scannerRef.current.resume();
                        }, 800);
                    }
                };

                scannerRef.current.render(onScanSuccess, () => {});
            }
        }, 500);
        return () => {
            if (scannerRef.current) {
                scannerRef.current.pause();
                scannerRef.current.clear();
            }
        };
    }, [handleUpdateBarcode]);

    return (
        <IonPage>
            <Header title={`Cashier`} />
            <IonContent>
                <IonGrid className="maxHeight">
                    <IonRow className="maxHeight">
                        <IonCol className="panelLeft">
                            <IonCardTitle>Scanned item list</IonCardTitle>
                            <br />
                            {!shopBasket.length ? (
                                <IonItem>
                                    <IonText className="ion-padding">No scanned item</IonText>
                                </IonItem>
                            ) : (
                                shopBasket.map((i, idx) => (
                                    <IonItem key={i.id + idx}>
                                        <IonLabel>{i.name}</IonLabel>
                                        <IonButtons slot="end">
                                            <IonButton
                                                onClick={() => {
                                                    handleQtyChange(i, idx, "minus");
                                                }}
                                            >
                                                <IonIcon icon={removeCircleOutline} slot="icon-only" />
                                            </IonButton>
                                            <IonLabel className="ion-padding">{i.qty}</IonLabel>
                                            <IonButton onClick={() => handleQtyChange(i, idx, "add")}>
                                                <IonIcon icon={addCircleOutline} slot="icon-only" />
                                            </IonButton>
                                        </IonButtons>
                                    </IonItem>
                                ))
                            )}
                        </IonCol>

                        <IonCol className="panelRight">
                            <IonCardSubtitle>BARCODE SCANNER</IonCardSubtitle>
                            <IonItem lines="none">
                                <IonCol>
                                    <div id="reader" ref={readerRef} style={{ width: "320px" }} />
                                    {/* <BarcodeScannerComponent
                                        delay={800}
                                        onUpdate={(err, result: any) => {
                                            if (result) {
                                                beep.play();
                                                setData(result.text);
                                            }
                                        }}
                                    /> */}
                                </IonCol>
                            </IonItem>
                            <IonCol>
                                <IonButton expand="block" onClick={() => showBarcodeInput(true)}>
                                    Manual Input
                                </IonButton>
                            </IonCol>
                            <IonGrid className="full-w">
                                <IonCard>
                                    <IonCardContent>
                                        <IonCardSubtitle>Subtotal</IonCardSubtitle>
                                        <IonRow>
                                            <IonCol>
                                                <IonText>{shopBasket.reduce((i, j) => i + j.qty, 0) + " item(s)"}</IonText>
                                            </IonCol>
                                            <IonCol className="ion-text-end">
                                                <IonText>{"$" + shopBasket.reduce((i, j) => i + j.price * j.qty, 0)}</IonText>
                                            </IonCol>
                                        </IonRow>
                                    </IonCardContent>
                                </IonCard>
                                <IonCard>
                                    <IonCardContent>
                                        <IonCardSubtitle>Receipt to customer's email:</IonCardSubtitle>
                                        <IonInput
                                            value={emailInput}
                                            onIonChange={(e) => setEmailInput(e.detail.value!)}
                                            placeholder={"Email address (Optional)"}
                                        />
                                    </IonCardContent>
                                </IonCard>
                                <IonCard button onClick={() => setPaymentModal(true)}>
                                    <IonCardContent>
                                        <IonCardSubtitle>
                                            <IonText>Payment method</IonText>
                                            {!paymentMethod && <div className="required">{" *REQUIRED"}</div>}
                                        </IonCardSubtitle>
                                        <IonItem lines="none">
                                            <IonText>
                                                {!!paymentMethod ? shopPaymentMethods.find((i) => i.type === paymentMethod)!.name : "Not selected"}
                                            </IonText>
                                        </IonItem>
                                    </IonCardContent>
                                </IonCard>
                            </IonGrid>
                            <IonRow className="full-w">
                                <IonCol>
                                    <IonButton expand="block" onClick={() => setCheckoutModal(true)} disabled={!shopBasket.length || !paymentMethod}>
                                        CHECKOUT
                                    </IonButton>
                                    <IonButton expand="block" color="danger" onClick={() => setCancelModal(true)}>
                                        CANCEL ORDER
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonCol>
                    </IonRow>
                </IonGrid>
                <SimpleModal
                    headerText={"Select payment"}
                    isOpen={paymentModal}
                    onCancel={() => {
                        setPaymentModal(false);
                    }}
                    onConfirm={() => {
                        setPaymentModal(false);
                    }}
                    bodyChild={
                        <IonCol>
                            <IonRadioGroup value={paymentMethod} onIonChange={(e) => setPaymentMethod(e.target.value)}>
                                {shopPaymentMethods.map((i) => (
                                    <IonItem lines="none" color={paymentMethod === i.type ? "success" : ""} key={i.type}>
                                        <IonText>{i.name}</IonText>
                                        <IonRadio slot="start" value={i.type} />
                                    </IonItem>
                                ))}
                            </IonRadioGroup>
                        </IonCol>
                    }
                />
                <SimpleModal
                    headerText={"Checkout"}
                    isOpen={checkoutModal}
                    onCancel={() => setCheckoutModal(false)}
                    onConfirm={() => {
                        setCheckoutModal(false);
                        handleSubmit();
                    }}
                    text={"Confirm to check out?"}
                />
                <SimpleModal
                    headerText={"Cancel Order"}
                    isOpen={cancelModal}
                    onCancel={() => setCancelModal(false)}
                    onConfirm={() => {
                        setCancelModal(false);
                        handleReset();
                    }}
                    text={"Confirm to cancel order?"}
                />
                <SimpleModal
                    headerText={"Manual Input Barcode"}
                    isOpen={barcodeInput}
                    onCancel={() => {
                        showBarcodeInput(false);
                        setBarcode("");
                    }}
                    onConfirm={() => handleScan()}
                    bodyChild={
                        <IonCol>
                            <IonLabel>Please input barcode value</IonLabel>
                            <IonInput
                                value={barcode}
                                onIonChange={(e) => setBarcode(e.detail.value!)}
                                placeholder={"Input here"}
                                style={{ border: "solid 1px #CCC", borderRadius: "4px" }}
                            />
                        </IonCol>
                    }
                />
            </IonContent>
            <IonAlert
                isOpen={!!message}
                header={`System Message`}
                message={message}
                onDidDismiss={() => setMessage("")}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            setMessage("");
                            if (data) {
                                setData("");
                            }
                        },
                    },
                ]}
            />
        </IonPage>
    );
}

export default Cashier;

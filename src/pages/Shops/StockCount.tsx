import { IonCard, IonCardContent, IonCardSubtitle, IonContent, IonItem, IonPage, IonText } from "@ionic/react";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";

function StockCount() {
    const allShops = useSelector((state: RootState) => state.shop.shops);
    const allProducts = useSelector((state: RootState) => state.shop.shopItems);
    const history = useHistory();

    return (
        <IonPage>
            <Header title={`Inventory Count`} back />
            <IonContent>
                {allShops
                    .filter((shop) => shop.isActive)
                    .map((i) => (
                        <IonCard key={i.id}>
                            <IonCardContent>
                                <IonCardSubtitle>{i.name}</IonCardSubtitle>
                                {allProducts
                                    .filter((j) => j.shop.id === i.id && j.isActive)
                                    .map((item) => (
                                        <IonItem key={item.id} onClick={() => history.push(`/inventory/management/${item.id}`)} button>
                                            <IonText>{item.name}</IonText>
                                            {!item.stock && (
                                                <IonText slot="end" color="danger">
                                                    EMPTY!
                                                </IonText>
                                            )}
                                            <IonText slot="end" color={!item.stock ? "danger" : ""}>
                                                {item.stock}
                                            </IonText>
                                        </IonItem>
                                    ))}
                            </IonCardContent>
                        </IonCard>
                    ))}
            </IonContent>
        </IonPage>
    );
}

export default StockCount;

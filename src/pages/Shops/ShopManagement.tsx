import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonCardSubtitle,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonInput,
    IonItem,
    IonModal,
    IonPage,
    IonRow,
    IonText,
    IonTitle,
} from "@ionic/react";
import { Shop } from "@tsanghoilun/snow-n-surf-interface/types/shop";
import { setNewShopFB, setShopActive } from "app/firebase";
import { RootState } from "app/store";
import { singleAlertBtn } from "app/variables";
import Header from "components/Global/Header";
import "css/ShopManagement.css";
import { ModalHeader } from "pages/Sessions/UserSearch";
import { useCallback, useEffect, useLayoutEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";

const defaultShop: Shop = { id: "new", name: "", isActive: true };

function ShopManagement({ viewRemoved = false }: { viewRemoved?: Boolean }) {
    const [targetShop, setTargetShop] = useState<Shop | null>(null);
    const [message, setMessage] = useState("");
    const [newShop, setNewShop] = useState<Shop>(defaultShop);
    const [currentShops, setCurrentShops] = useState<Shop[]>([]);
    const allShops = useSelector((state: RootState) => state.shop.shops);
    const focusRef = useRef<HTMLIonInputElement>(null);

    const handleReset = useCallback(() => {
        setNewShop(defaultShop);
        setTargetShop(null);
    }, []);

    const handleSubmit = useCallback(
        async (shop: Shop) => {
            if (!allShops.some((i) => i.name === shop.name)) {
                const res = await setNewShopFB(shop);
                if (res === "success") {
                    setMessage("New shop created created");
                    handleReset();
                } else if (res === "edited") {
                    setMessage("Shop edited");
                    handleReset();
                } else {
                    setMessage(res);
                }
            } else {
                setMessage("Duplicated shop name");
            }
        },
        [allShops, handleReset]
    );

    const handleDeleteShop = useCallback(
        async (id: string, isActive: boolean) => {
            const res = await setShopActive(id, isActive);
            if (res === "success") {
                setMessage(!viewRemoved ? "Shop deleted" : "Shop restored");
                handleReset();
            } else {
                setMessage(res);
            }
        },
        [handleReset, viewRemoved]
    );

    useLayoutEffect(() => {
        if (!!targetShop) {
            focusRef.current?.focus();
        }
    }, [targetShop]);

    useEffect(() => {
        setCurrentShops(allShops.filter((i) => (!viewRemoved ? i.isActive : !i.isActive)));
    }, [allShops, viewRemoved]);

    return (
        <IonPage>
            <Header title={`Shops Management`} />
            <IonContent>
                {!viewRemoved && (
                    <IonCard>
                        <IonRow>
                            <IonCardContent className="full-w">
                                <IonCardSubtitle>Input new shop name</IonCardSubtitle>
                                <IonInput
                                    placeholder="New shop name"
                                    value={newShop.name}
                                    className="ion-text-center"
                                    onIonChange={(e) => setNewShop({ ...newShop, name: e.detail.value! })}
                                    style={{ border: "solid 1px #CCC", width: "100%", borderRadius: "4px" }}
                                />
                                <br />
                                <IonButton expand="block" disabled={!newShop.name} onClick={() => handleSubmit(newShop)}>
                                    ADD SHOP
                                </IonButton>
                            </IonCardContent>
                        </IonRow>
                    </IonCard>
                )}
                <br />
                <IonHeader>
                    <IonTitle>{!viewRemoved ? "Shops" : "Removed Shops"}</IonTitle>
                </IonHeader>
                <IonGrid>
                    {!!currentShops.length ? (
                        currentShops.map((i) => (
                            <IonItem key={i.id}>
                                <IonText>{i.name}</IonText>
                                <IonButton slot="end" onClick={() => setTargetShop(i)}>
                                    SELECT
                                </IonButton>
                            </IonItem>
                        ))
                    ) : (
                        <IonItem>List is empty</IonItem>
                    )}
                    <br />
                </IonGrid>

                <IonModal isOpen={!!targetShop} onDidDismiss={() => setTargetShop(null)}>
                    <ModalHeader text={"Edit shop"} />
                    <IonContent>
                        <IonGrid className="flex-column-between">
                            <IonRow />
                            {!!targetShop && (
                                <>
                                    <IonRow className="full-w">
                                        <IonCol>
                                            <IonCardSubtitle>{!viewRemoved ? "Edit shop name" : "Shop name"}</IonCardSubtitle>
                                            <IonInput
                                                disabled={!!viewRemoved}
                                                className="ion-text-center"
                                                placeholder="New shop name"
                                                value={targetShop.name}
                                                onIonChange={(e) => setTargetShop({ ...targetShop, name: e.detail.value! })}
                                                style={{ border: "solid 1px #CCC", width: "100%", borderRadius: "4px" }}
                                            />
                                        </IonCol>
                                    </IonRow>
                                    <IonRow className="full-w">
                                        <IonCol>
                                            {!viewRemoved && (
                                                <IonButton
                                                    expand="block"
                                                    disabled={!targetShop.name || allShops.includes(targetShop)}
                                                    onClick={() => handleSubmit(targetShop)}
                                                >
                                                    SAVE CHANGES
                                                </IonButton>
                                            )}
                                            <IonButton
                                                color={!viewRemoved ? "danger" : "success"}
                                                expand="block"
                                                onClick={() => handleDeleteShop(targetShop.id, !targetShop.isActive)}
                                            >
                                                {!viewRemoved ? "DELETE SHOP" : "RESTORE SHOP"}
                                            </IonButton>
                                            <IonButton color="medium" expand="block" onClick={() => setTargetShop(null)}>
                                                CANCEL
                                            </IonButton>
                                        </IonCol>
                                    </IonRow>
                                </>
                            )}
                        </IonGrid>
                    </IonContent>
                </IonModal>
            </IonContent>
            <IonAlert
                isOpen={!!message}
                header={`System Message`}
                message={message}
                onDidDismiss={() => setMessage("")}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default ShopManagement;

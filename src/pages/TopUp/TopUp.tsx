import {
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardTitle,
    IonChip,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonItem,
    IonLabel,
    IonList,
    IonMenuButton,
    IonPage,
    IonRow,
    IonTitle,
    IonToolbar,
} from "@ionic/react";
import { RootState } from "app/store";
import "css/CheckIn.css";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";

function TopUp() {
    const packages = useSelector((state: RootState) => state.packages.packages);
    const customer = useSelector((state: RootState) => state.users.parentAccountUser);
    const history = useHistory();

    useEffect(() => {
        if (customer) {
            history.push("/customers/sub-account/create");
        }
    }, [customer, history]);

    return (
        <IonPage>
            <IonContent fullscreen>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuButton />
                        </IonButtons>
                        <IonTitle>{`Top Up Packages`}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonGrid>
                    <IonRow>
                        <IonCol>
                            <IonButton onClick={() => history.push("/top-up/management/new")}>CREATE NEW PACKAGE</IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
                {packages.map((i, idx) => (
                    <IonCard key={i.id + idx} button onClick={() => history.push(`/top-up/management/${i.id}`)}>
                        <IonCardHeader>
                            <IonCardTitle>
                                {"# " + (idx + 1)} {i.isPopular && <IonChip color="tertiary">Popular</IonChip>}
                            </IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent>
                            <IonList>
                                <IonItem>
                                    <IonLabel>{"init credits: " + i.initCredits}</IonLabel>
                                </IonItem>
                                <IonItem>
                                    <IonLabel>{"Received credits: " + i.receivedCredits}</IonLabel>
                                </IonItem>
                                <IonItem>
                                    <IonLabel>{"Price: " + i.price}</IonLabel>
                                </IonItem>
                            </IonList>
                        </IonCardContent>
                    </IonCard>
                ))}
            </IonContent>
        </IonPage>
    );
}

export default TopUp;

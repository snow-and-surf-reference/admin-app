import {
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonChip,
    IonCol,
    IonContent,
    IonGrid,
    IonInput,
    IonItem,
    IonLabel,
    IonLoading,
    IonPage,
    IonRow,
    IonSegment,
    IonSegmentButton,
    IonSelect,
    IonSelectOption,
    IonText,
} from "@ionic/react";
import { TopupOrder } from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import { getTopUpRecords, voidTopupRecord } from "app/firebase";
import Header from "components/Global/Header";
import "css/RetailReport.css";
import dayjs from "dayjs";
import { formatDate } from "helpers/date";
import { useCallback, useEffect, useRef, useState } from "react";
import { CSVLink } from "react-csv";
import { Data } from "react-csv/components/CommonPropTypes";

type TabType = "day" | "month" | "range";
type FilterType = "Staff" | "Customer" | "Default";

function TopUpReport() {
    const [tab, setTab] = useState<TabType>("day");
    const [filter, setFilter] = useState<FilterType>("Default");
    const [fromDate, setFromDate] = useState(new Date());
    const [toDate, setToDate] = useState(new Date());
    const [viewMonth, setViewMonth] = useState(dayjs.tz(new Date()).format("YYYY-MM"));
    const [rawRecords, setRawRecords] = useState<TopupOrder[]>([]);
    const [records, setRecords] = useState<TopupOrder[]>([]);
    const [selectVoid, setSelectVoid] = useState<string | null>(null);
    const reportCsvRef = useRef<any>(null);

    const getDayRecords = useCallback(async () => {
        let res: TopupOrder[] | undefined = [];
        if (tab === "month") {
            res = await getTopUpRecords(dayjs.tz(viewMonth).startOf("month").toDate(), dayjs.tz(viewMonth).endOf("month").toDate());
        }
        if (tab === "day") {
            res = await getTopUpRecords(dayjs.tz(fromDate).startOf("day").toDate(), dayjs.tz(fromDate).endOf("day").toDate());
        }
        if (tab === "range") {
            res = await getTopUpRecords(dayjs.tz(fromDate).startOf("day").toDate(), dayjs.tz(toDate).endOf("day").toDate());
        }
        setFilter("Default");
        setRawRecords(res ? res : []);
        setRecords(res ? res : []);
    }, [tab, viewMonth, fromDate, toDate]);

    useEffect(() => {
        setFromDate(new Date());
        setViewMonth(new Date().getFullYear().toString() + "-" + (new Date().getMonth() + 1).toString().padStart(2, "0"));
    }, [tab]);

    const recordFormat = useCallback((orders: TopupOrder[]) => {
        let tempRecord: Data = [];
        orders.forEach((i) => {
            const { createdAt, authUser, staff, paymentMethod, topupPackage, exactAmount, paymentStatus } = i;
            tempRecord.push({
                date: dayjs.tz(createdAt).format("YYYY-MM-DD"),
                customerMemberId: authUser.memberId,
                customerName: authUser.usrname,
                customerEmail: authUser.email,
                staffName: staff?.usrname,
                staffPhone: staff?.phone,
                receivedCredit: topupPackage ? topupPackage.receivedCredits : exactAmount,
                packagePrice: topupPackage ? topupPackage.price : "N/A",
                exactAmount: exactAmount ? exactAmount : "N/A",
                paymentMethod,
                status: paymentStatus,
            });
        });
        return tempRecord;
    }, []);

    const downloadButton = useCallback(() => {
        const tempRecord = recordFormat(records);
        const filename =
            (tab === "day" ? formatDate(fromDate) : tab === "month" ? viewMonth : `${formatDate(fromDate)} - ${formatDate(toDate)}`) +
            ` - Staff top up record ${filter === "Staff" ? "(filtered by staff)" : filter === "Customer" ? "(filtered by customer)" : ""}`;
        return !!records.length ? (
            <IonButton onClick={() => reportCsvRef.current.link.click()} expand="block">
                <CSVLink
                    ref={reportCsvRef}
                    data={tempRecord}
                    style={{ color: "#FFF", textDecoration: "none", pointerEvents: "none", width: "100%" }}
                    filename={filename}
                >
                    DOWNLOAD RECORDS
                </CSVLink>
            </IonButton>
        ) : (
            <IonButton disabled expand="block">
                NO RECORD FOR DOWNLOAD
            </IonButton>
        );
    }, [filter, fromDate, recordFormat, records, tab, toDate, viewMonth]);

    useEffect(() => {
        if (filter === "Default") {
            setRecords(rawRecords);
        }
        if (filter === "Customer") {
            setRecords(rawRecords.filter((i) => !i.staff));
        }
        if (filter === "Staff") {
            setRecords(rawRecords.filter((i) => !!i.staff));
        }
    }, [filter, rawRecords, records]);

    const handleVoidTopup = async () => {
        if (!selectVoid) {
            return;
        }
        const id = selectVoid;
        setSelectVoid("voiding");
        await voidTopupRecord(id);
        getDayRecords();
        setTimeout(() => {
            setSelectVoid(null);
        }, 1000);
    };

    return (
        <IonPage>
            <Header title={`Topup Report`} />
            <IonContent>
                <IonCard>
                    <IonCardContent>
                        <IonSegment
                            value={tab}
                            onIonChange={(e) => {
                                setTab(e.detail.value! as TabType);
                                setRecords([]);
                            }}
                        >
                            <IonSegmentButton value="day">View by day</IonSegmentButton>
                            <IonSegmentButton value="month">View by month</IonSegmentButton>
                            <IonSegmentButton value="range">View by range</IonSegmentButton>
                        </IonSegment>
                        <IonGrid>
                            <IonRow>
                                {tab === "day" ? (
                                    <>
                                        <IonCol size="12">
                                            <br />
                                            <IonText>Select Date</IonText>
                                            <IonInput
                                                className="border"
                                                type="date"
                                                value={dayjs.tz(fromDate).format("YYYY-MM-DD")}
                                                onIonChange={(e) => {
                                                    setFromDate(new Date(e.detail.value!));
                                                    setRecords([]);
                                                }}
                                            />
                                        </IonCol>
                                    </>
                                ) : tab === "month" ? (
                                    <IonCol size="12">
                                        <br />
                                        <IonText>Select Month</IonText>
                                        <IonInput
                                            className="border"
                                            type="month"
                                            value={viewMonth}
                                            onIonChange={(e) => {
                                                setViewMonth(e.detail.value!);
                                                setRecords([]);
                                            }}
                                        />
                                    </IonCol>
                                ) : (
                                    <>
                                        <IonCol size="6">
                                            <br />
                                            <IonText>From</IonText>
                                            <IonInput
                                                className="border"
                                                type="date"
                                                value={dayjs.tz(fromDate).format("YYYY-MM-DD")}
                                                onIonChange={(e) => setFromDate(new Date(e.detail.value!))}
                                            />
                                        </IonCol>
                                        <IonCol size="6">
                                            <br />
                                            <IonText>To</IonText>
                                            <IonInput
                                                className="border"
                                                type="date"
                                                value={dayjs.tz(toDate).format("YYYY-MM-DD")}
                                                onIonChange={(e) => {
                                                    if (new Date(e.detail.value!) < fromDate) {
                                                        setFromDate(new Date(e.detail.value!));
                                                    }
                                                    setToDate(new Date(e.detail.value!));
                                                }}
                                            />
                                        </IonCol>
                                    </>
                                )}
                            </IonRow>
                            <IonRow>
                                <IonCol>
                                    <IonButton onClick={getDayRecords} expand="block">
                                        SEARCH
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonCardContent>
                </IonCard>
                <IonGrid className="ion-padding">
                    {!!rawRecords.length && (
                        <IonRow className="ion-text-end">
                            <IonCol size="6">
                                <IonItem lines="none">
                                    <IonText className="ion-margin-horizontal">Filter:</IonText>
                                    <IonSelect placeholder="Filter" value={filter} onIonChange={(e) => setFilter(e.detail.value)}>
                                        <IonSelectOption value="Default">Default</IonSelectOption>
                                        <IonSelectOption value="Staff">By staff</IonSelectOption>
                                        <IonSelectOption value="Customer">By Customer</IonSelectOption>
                                    </IonSelect>
                                </IonItem>
                            </IonCol>
                        </IonRow>
                    )}
                    {!records.length ? (
                        <IonRow>
                            <IonCol className="ion-text-center">NO RECORD</IonCol>
                        </IonRow>
                    ) : (
                        <>
                            <IonRow>
                                <IonCol size="12" className="ion-text-center ion-padding">
                                    {tab === "day" ? (
                                        <IonText>{dayjs.tz(fromDate).format("YYYY-MM-DD")}</IonText>
                                    ) : tab === "month" ? (
                                        <IonText>{dayjs.tz(viewMonth).format("YYYY-MMM")}</IonText>
                                    ) : (
                                        <IonText>{`${dayjs.tz(fromDate).format("YYYY-MM-DD")} - ${dayjs.tz(toDate).format("YYYY-MM-DD")}`}</IonText>
                                    )}
                                </IonCol>
                            </IonRow>
                        </>
                    )}
                    <IonItem>
                        <IonLabel>Total:</IonLabel>
                        <IonChip color={'warning'}>
                            {`$`}
                            {
                                records.filter(r => r.paymentStatus === 'AUTHORIZED').reduce((p, c) => p + (c.exactAmount || c.topupPackage?.price || 0), 0).toLocaleString()
                            }
                        </IonChip>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Voided:</IonLabel>
                        <IonChip color={'danger'}>
                            {`$`}
                            {
                                records.filter(r => r.paymentStatus !== 'AUTHORIZED').reduce((p, c) => p + (c.exactAmount || c.topupPackage?.price || 0), 0).toLocaleString()
                            }
                        </IonChip>
                    </IonItem>
                    <br/><br/>
                    {records.map((i) => (
                        <IonItem key={`topup-record-results-${i.id}`}>
                            <IonLabel>
                                <h3>
                                    <b>${i.exactAmount?.toFixed(2) || i.topupPackage?.price.toFixed(2)}</b>
                                </h3>
                                <h4>{i.exactAmount ? `Exact amount - ${i.exactAmount}` : `Package - ${i.topupPackage?.receivedCredits}`}</h4>
                                <br />

                                <h4>{dayjs.tz(i.createdAt).format("h:mm A | DD MMM YY")}</h4>
                                <h4>
                                    Member ID: <b>{i.authUser.memberId}</b>
                                </h4>
                                <p>
                                    Topup by: {i.staff ? `Staff (${i.staff.usrname})` : `User`} | {i.paymentMethod}
                                </p>
                                <p>order reference: {i.orderReference}</p>
                            </IonLabel>
                            <IonButtons slot="end">
                                {i.paymentStatus === "AUTHORIZED" ? (
                                    <IonButton
                                        fill="solid"
                                        color={"danger"}
                                        onClick={() => {
                                            setSelectVoid(i.id);
                                        }}
                                    >
                                        <IonLabel>Void Order</IonLabel>
                                    </IonButton>
                                ) : (
                                    <IonButton disabled>Voided</IonButton>
                                )}
                            </IonButtons>
                        </IonItem>
                    ))}
                    {!!records.length && (
                        <IonRow>
                            <IonCol>{downloadButton()}</IonCol>
                        </IonRow>
                    )}
                </IonGrid>
            </IonContent>
            <IonAlert
                isOpen={!!selectVoid && selectVoid !== "voiding" && selectVoid !== `voided`}
                onDidDismiss={() => {
                    setSelectVoid(null);
                }}
                header={`*** WARNING ***`}
                message={`Voiding this topup only will mark the record as "voided", any refunding of money or deduction of the received credits are to be made manually by staffs. This action CANNOT be reversed.`}
                buttons={[
                    {
                        text: "Confirm",
                        handler: () => {
                            handleVoidTopup();
                        },
                    },
                    {
                        text: "Cancel",
                        role: "cancel",
                    },
                ]}
            />
            <IonLoading isOpen={selectVoid === "voiding"} spinner={`dots`} message={`Voiding`} />
        </IonPage>
    );
}

export default TopUpReport;

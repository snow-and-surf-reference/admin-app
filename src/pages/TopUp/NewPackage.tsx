import { IonAlert, IonButton, IonCol, IonContent, IonInput, IonItem, IonLabel, IonList, IonPage, IonRow, IonToggle } from "@ionic/react";
import { TopUpPackages } from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import { addNewTopUpPackage, removePackage } from "app/firebase";
import { getPackageById } from "app/slices/topUpPackagesSlice";
import { singleAlertBtn } from "app/variables";
import Header from "components/Global/Header";
import { useState } from "react";
import { useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";

const defaultNewPackage: TopUpPackages = {
    id: "new",
    initCredits: 0,
    receivedCredits: 0,
    price: 0,
    isPopular: false,
};

interface NewPackageProps {
    isEdit?: boolean;
}

function NewPackage({ isEdit = false }: NewPackageProps) {
    const { id } = useParams<{ id: string }>();
    const topUpPack = useSelector(getPackageById(id));
    const history = useHistory();
    const [message, setMessage] = useState("");
    const [newPackage, setNewPackage] = useState<TopUpPackages>(isEdit && !!topUpPack ? topUpPack : defaultNewPackage);

    const handleSubmit = async () => {
        await addNewTopUpPackage(newPackage).then((e) => {
            if (e === "success") {
                setMessage(isEdit ? "Saved" : "New package created");
                history.push("/top-up/management");
            } else {
                setMessage(e);
            }
        });
    };

    const handleRemove = async () => {
        if (isEdit) {
            await removePackage(newPackage.id).then((e) => {
                if (e === "success") {
                    setMessage("Package removed");
                    history.push("/top-up/management");
                } else {
                    setMessage(e);
                }
            });
        }
    };

    return (
        <IonPage>
            <IonContent fullscreen>
                <Header title={isEdit ? "Edit Package" : "New Packages"} back />
                <IonList>
                    <IonItem>
                        <IonLabel color="medium">Init credits</IonLabel>
                        <IonInput
                            type="tel"
                            required
                            value={newPackage.initCredits}
                            onIonChange={(e) => {
                                setNewPackage({
                                    ...newPackage,
                                    initCredits: isNaN(parseInt(e.detail.value!)) ? 0 : Number(e.detail.value!),
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">User receives credit</IonLabel>
                        <IonInput
                            type="tel"
                            required
                            value={newPackage.receivedCredits}
                            onIonChange={(e) => {
                                setNewPackage({
                                    ...newPackage,
                                    receivedCredits: isNaN(parseInt(e.detail.value!)) ? 0 : Number(e.detail.value!),
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Selling price</IonLabel>
                        <IonInput
                            type="tel"
                            required
                            value={newPackage.price}
                            onIonChange={(e) => {
                                setNewPackage({
                                    ...newPackage,
                                    price: isNaN(parseInt(e.detail.value!)) ? 0 : Number(e.detail.value!),
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Set as popular package</IonLabel>
                        <IonToggle
                            checked={newPackage.isPopular}
                            onIonChange={(e) => setNewPackage({ ...newPackage, isPopular: e.detail.checked })}
                        />
                    </IonItem>
                    <IonRow>
                        <IonCol size="12">
                            <IonButton expand="block" onClick={handleSubmit}>
                                {isEdit ? "Save changes" : "Create package"}
                            </IonButton>
                            {isEdit && (
                                <IonButton expand="block" color="danger" onClick={handleRemove}>
                                    REMOVE PACKAGE
                                </IonButton>
                            )}
                        </IonCol>
                    </IonRow>
                </IonList>
            </IonContent>
            <IonAlert
                isOpen={!!message}
                header={`System Message`}
                message={message}
                onDidDismiss={() => setMessage("")}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default NewPackage;

import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent, IonContent,
    IonFooter,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonPage,
    IonToolbar
} from "@ionic/react";
import * as UserTypes from "@tsanghoilun/snow-n-surf-interface/types/user";
import { deleteCustomerClass, saveCustomerClasses } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectCustomerClasses } from "app/slices/customerClassesSlice";
import Header from "components/Global/Header";
import LoadingCard from "components/Global/LoadingCard";
import { addCircleOutline, removeCircleOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

const newClassTemplate: UserTypes.CustomerClass = {
    id: "new",
    name: {
        en: "New Class",
        zh: "",
    },
    creditTier: 0,
    discount: 0,
};

const CustomerClasses: React.FC = () => {
    const rawClasses = useAppSelector(selectCustomerClasses);
    const [classes, setClasses] = useState(rawClasses);
    const [saving, setSaving] = useState("idle");
    const [deleting, setDeleting] = useState('idle');

    useEffect(() => {
        setClasses(rawClasses);
    }, [rawClasses]);

    useEffect(() => console.log(classes), [classes]);

    const handleSave = async () => {
        setSaving("saving");
        await saveCustomerClasses(classes);
        setSaving("saved");
    };

    const handleDelete = async (id: string) => {
        setDeleting('deleting');
        await deleteCustomerClass(id);
        setDeleting('deleted');
    }

    return (
        <IonPage>
            <Header title="Customer Classes" />
            <IonContent>
                {classes.length <= 0 ? (
                    <LoadingCard />
                ) : (
                    <>
                        {classes.map((x, idx) => (
                            <IonCard key={`customer-class-card-${x.id === `new` ? `${x.id}${idx}` : x.id}`} color="light">
                                <IonCardContent>
                                    <IonList inset>
                                        <IonItem>
                                            <IonLabel position="stacked" color={`medium`}>{`Name (English)`}</IonLabel>
                                            <IonInput
                                                type="text"
                                                value={x.name.en}
                                                onIonChange={(e) => {
                                                    let tempClasses = [...classes];
                                                    const idx = tempClasses.findIndex((y) => y.id === x.id);
                                                    let tempItem = Object.assign(
                                                        { ...x },
                                                        {
                                                            name: Object.assign(
                                                                { ...x.name },
                                                                {
                                                                    en: e.detail.value!,
                                                                }
                                                            ),
                                                        }
                                                    );
                                                    tempClasses[idx] = tempItem;
                                                    setClasses(tempClasses);
                                                }}
                                            />
                                        </IonItem>
                                        <IonItem>
                                            <IonLabel position="stacked" color={`medium`}>{`等級名稱 (中文)`}</IonLabel>
                                            <IonInput
                                                type="text"
                                                value={x.name.zh}
                                                onIonChange={(e) => {
                                                    let tempClasses = [...classes];
                                                    const idx = tempClasses.findIndex((y) => y.id === x.id);
                                                    let tempItem = Object.assign(
                                                        { ...x },
                                                        {
                                                            name: Object.assign(
                                                                { ...x.name },
                                                                {
                                                                    zh: e.detail.value!,
                                                                }
                                                            ),
                                                        }
                                                    );
                                                    tempClasses[idx] = tempItem;
                                                    setClasses(tempClasses);
                                                }}
                                            />
                                        </IonItem>
                                    </IonList>
                                    <IonList inset>
                                        <IonItem>
                                            <IonLabel>Spending Requirement</IonLabel>
                                            <IonInput
                                                type="number"
                                                className="ion-text-end"
                                                value={x.creditTier}
                                                onIonChange={(e) => {
                                                    let tempClasses = [...classes];
                                                    const idx = tempClasses.findIndex((y) => y.id === x.id);
                                                    let tempItem = Object.assign(
                                                        { ...x },
                                                        {
                                                            creditTier: Number(e.detail.value!),
                                                        }
                                                    );
                                                    tempClasses[idx] = tempItem;
                                                    setClasses(tempClasses);
                                                }}
                                            />
                                        </IonItem>
                                        <IonItem>
                                            <IonLabel>discount Amount</IonLabel>
                                            <IonInput
                                                type="number"
                                                className="ion-text-end"
                                                value={x.discount.toFixed(2)}
                                                min={0}
                                                max={1}
                                                step={`0.05`}
                                                onIonChange={(e) => {
                                                    let tempClasses = [...classes];
                                                    const idx = tempClasses.findIndex((y) => y.id === x.id);
                                                    let tempItem = Object.assign(
                                                        { ...x },
                                                        {
                                                            discount: parseFloat(e.detail.value!),
                                                        }
                                                    );
                                                    tempClasses[idx] = tempItem;
                                                    setClasses(tempClasses);
                                                }}
                                            />
                                        </IonItem>
                                    </IonList>
                                    <IonButton
                                        size="small"
                                        color={`danger`}
                                        onClick={x.id === 'new' ? () => {
                                            setClasses((c) => {
                                                const tempClasses = [...c];
                                                tempClasses.splice(idx, 1);
                                                return tempClasses;
                                            })
                                        } : () => {
                                            handleDelete(x.id);
                                        }}
                                    >
                                        <IonIcon icon={removeCircleOutline} slot="start" />
                                        <IonLabel>Remove</IonLabel>
                                    </IonButton>
                                </IonCardContent>
                            </IonCard>
                        ))}
                        {classes.filter((x) => x.id === "new").length <= 0 ? (
                            <IonButton
                                expand="block"
                                className="ion-margin-start ion-margin-end ion-margin-bottom"
                                onClick={() => {
                                    setClasses((c) => {
                                        const tempClasses = [...c];
                                        tempClasses.push(newClassTemplate);
                                        return tempClasses;
                                    });
                                }}
                            >
                                <IonIcon icon={addCircleOutline} slot="start" />
                                <IonLabel>Create New</IonLabel>
                            </IonButton>
                        ) : null}
                    </>
                )}
            </IonContent>
            <IonFooter>
                <IonToolbar color={classes === rawClasses ? `light` : `warning`}>
                    <IonButton
                        color={classes === rawClasses ? `light` : `warning`}
                        expand="block"
                        disabled={classes === rawClasses}
                        onClick={() => handleSave()}
                    >
                        Save Changes
                    </IonButton>
                </IonToolbar>
            </IonFooter>
            <IonLoading isOpen={saving === "saving"} spinner="dots" message="Saving Changes" />
            <IonLoading isOpen={deleting === "deleting"} spinner="dots" message="Deleting Class" />
            <IonAlert
                isOpen={saving === "saved"}
                onDidDismiss={() => {
                    setSaving("idle");
                }}
                header={`All Changes Saved.`}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            setSaving("idle");
                        },
                    },
                ]}
            />
            <IonAlert
                isOpen={deleting === "deleted"}
                onDidDismiss={() => {
                    setDeleting("idle");
                }}
                header={`Class Deleted.`}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            setDeleting("idle");
                        },
                    },
                ]}
            />
        </IonPage>
    );
};

export default CustomerClasses;

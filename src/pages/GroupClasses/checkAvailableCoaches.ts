import { CoachPlayType, CoachSchedule } from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { GroupClassSlot } from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import { MonthCounters } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Coach } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { getCoachScheduleByDate, getGroupClassSlotsByDate, getGroupClassSlotsSinceToday, getRecurringCoachSchedules, getRGroupClassSlotsByDay } from "app/firebase";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

export const checkAvailableCoachesByDateTime = async (date: Date, coachType: CoachPlayType, hour: number = 1) => {
    const start = dayjs.tz(date);
    const end = start.add(hour, `hour`);
    const startOfDate = dayjs.tz(start).startOf('day');
    const targetDayOfWeek = startOfDate.get('day');
    const allDaySchedules: CoachSchedule[] = await getCoachScheduleByDate(startOfDate.toDate());
    const rSchedules: CoachSchedule[] = await getRecurringCoachSchedules();
    const todaySlots: GroupClassSlot[] = await getGroupClassSlotsByDate(startOfDate.toDate());
    const rSlots: GroupClassSlot[] = await getRGroupClassSlotsByDay(targetDayOfWeek);

    if (allDaySchedules.length <= 0 && rSchedules.length <= 0) {
        return [];
    }
    const coaches: Coach[] = [];

    // check schedules of the day to get coaches that is available on the hour
    allDaySchedules.forEach(x => {
        const scheduleStart = dayjs.tz(`${dayjs.tz(x.date).format(`YYYY-MM-DD`)}T${x.start}`);
        const scheduleEnd = dayjs.tz(`${dayjs.tz(x.date).format(`YYYY-MM-DD`)}T${x.end}`);
            //.subtract(1, 'minute');
        if (scheduleStart.valueOf() <= start.valueOf() && scheduleEnd.valueOf() >= end.valueOf() && x.coach.coachType.includes(coachType)) {
            const idx = coaches.findIndex(c => c.id === x.id);
            if (idx < 0) {
                coaches.push({ ...x.coach });
            }
        }
    });

    // check recurring schedule to get coaches that is available on the day of week and hour
    rSchedules.forEach(x => {
        if (!x.daysOfWeek) { return };
        const isDayAvailable = x.daysOfWeek.filter(d => d.index === targetDayOfWeek).length === 1;
        if (!isDayAvailable) { return };
        const scheduleStart = dayjs.tz(`${dayjs.tz(startOfDate).format(`YYYY-MM-DD`)}T${x.start}`);
        const scheduleEnd = dayjs.tz(`${dayjs.tz(startOfDate).format(`YYYY-MM-DD`)}T${x.end}`).subtract(1, 'minute');
        const recurringStartDate = dayjs.tz(`${dayjs.tz(x.startDate || dayjs()).format(`YYYY-MM-DD`)}T00:00`);
        const recurringEndDate = dayjs.tz(`${dayjs.tz(x.endDate || dayjs()).format(`YYYY-MM-DD`)}T00:00`);
        const isValidDate = recurringStartDate.valueOf() <= startOfDate.valueOf() && recurringEndDate.valueOf() >= startOfDate.valueOf();
        if (scheduleStart.valueOf() <= start.valueOf() && scheduleEnd.valueOf() >= end.valueOf() && x.coach.coachType.includes(coachType) && isValidDate) {
            const idx = coaches.findIndex(c => c.id === x.id);
            if (idx < 0) {
                coaches.push({ ...x.coach });
            }
        }
    });

    // remove the coach if he/she is already booked on the same hour with other slots
    todaySlots.forEach(x => {
        const existStart = dayjs.tz(`${dayjs.tz(x.date).format(`YYYY-MM-DD`)}T${x.start}`);
        const existEnd = dayjs.tz(`${dayjs.tz(x.date).format(`YYYY-MM-DD`)}T${x.end}`).subtract(1, `minute`);
        if (
            (
                (existStart.valueOf() >= start.valueOf() && existStart.valueOf() <= end.valueOf())
                ||
                (existEnd.valueOf() >= start.valueOf() && existEnd.valueOf() <= end.valueOf())
            ) && x.coach
        ) {
            const coachId = x.coach.id;
            const idx = coaches.findIndex(c => c.id === coachId);
            if (idx >= 0) {
                coaches.splice(idx, 1);
            }
        }
    });

    // remove the coach if he/she is booked on recurring classes
    rSlots.forEach(x => {
        const existStart = dayjs.tz(`${dayjs.tz(startOfDate).format(`YYYY-MM-DD`)}T${x.start}`);
        const existEnd = dayjs.tz(`${dayjs.tz(startOfDate).format(`YYYY-MM-DD`)}T${x.end}`).subtract(1, `minute`);
        if (
            (
                (existStart.valueOf() >= start.valueOf() && existStart.valueOf() <= end.valueOf())
                ||
                (existEnd.valueOf() >= start.valueOf() && existEnd.valueOf() <= end.valueOf())
            ) && x.coach
        ) {
            const coachId = x.coach.id;
            const idx = coaches.findIndex(c => c.id === coachId);
            if (idx >= 0) {
                coaches.splice(idx, 1);
            }
        }
    })

    return coaches;
};

export const checkAvailableRCoaches = async (
    day: number,
    startTime: string, 
    coachType: CoachPlayType,
    startDate: Date = dayjs.tz(dayjs()).startOf('day').toDate(),
    endDate: Date = dayjs.tz(`2099-12-31T00:00`).toDate()
) => {
    const rSchedules: CoachSchedule[] = await getRecurringCoachSchedules();
    const today = dayjs.tz(dayjs());
    const start = dayjs.tz(`${today.format(`YYYY-MM-DD`)}T${startTime}`);
    const end = start.add(1, `hour`).subtract(1, 'minute');
    const futureSingleSlots: GroupClassSlot[] = await getGroupClassSlotsSinceToday();
    const rSlots: GroupClassSlot[] = await getRGroupClassSlotsByDay(day);
    if (rSchedules.length <= 0) {
        return [];
    }
    const coaches: Coach[] = [];
    rSchedules.forEach(x => {
        if (!x.daysOfWeek) { return };
        const isDayAvailable = x.daysOfWeek.filter(d => d.index === day).length === 1;
        if (!isDayAvailable) { return };
        if (x.endDate) {
            if (x.endDate.valueOf() < endDate.valueOf()) {
                return
            }
        }
        const scheduleStart = dayjs.tz(`${dayjs.tz(today).format(`YYYY-MM-DD`)}T${x.start}`);
        const scheduleEnd = dayjs.tz(`${dayjs.tz(today).format(`YYYY-MM-DD`)}T${x.end}`);
        const recurringStartDate = dayjs.tz(`${dayjs.tz(x.startDate || dayjs()).format(`YYYY-MM-DD`)}T00:00`);
        const recurringEndDate = dayjs.tz(`${dayjs.tz(x.endDate || dayjs()).format(`YYYY-MM-DD`)}T00:00`);
        const isValidDate = recurringStartDate.valueOf() <= startDate.valueOf() && recurringEndDate.valueOf() >= startDate.valueOf();
        if (scheduleStart.valueOf() <= start.valueOf() && scheduleEnd.valueOf() >= end.valueOf() && x.coach.coachType.includes(coachType) && isValidDate) {
            const idx = coaches.findIndex(c => c.id === x.id);
            if (idx < 0) {
                coaches.push({ ...x.coach });
            }
        }
    })

    // remove the coach if he/she is booked on recurring classes
    rSlots.forEach(x => {
        const existStart = dayjs.tz(`${dayjs.tz(today).format(`YYYY-MM-DD`)}T${x.start}`);
        const existEnd = dayjs.tz(`${dayjs.tz(today).format(`YYYY-MM-DD`)}T${x.end}`).subtract(1, `minute`);
        if (
            (
                (existStart.valueOf() >= start.valueOf() && existStart.valueOf() <= end.valueOf())
                ||
                (existEnd.valueOf() >= start.valueOf() && existEnd.valueOf() <= end.valueOf())
            ) && x.coach
        ) {
            const coachId = x.coach.id;
            const idx = coaches.findIndex(c => c.id === coachId);
            if (idx >= 0) {
                coaches.splice(idx, 1);
            }
        }
    })

    // check all future single, if it is same day of week and same hour, remove that coach
    futureSingleSlots.forEach(x => {
        const slotDay = dayjs.tz(x.date).get('day');
        if (slotDay !== day) { return };
        const existStart = dayjs.tz(`${dayjs.tz(today).format(`YYYY-MM-DD`)}T${x.start}`);
        const existEnd = dayjs.tz(`${dayjs.tz(today).format(`YYYY-MM-DD`)}T${x.end}`).subtract(1, `minute`);
        if (
            (
                (existStart.valueOf() >= start.valueOf() && existStart.valueOf() <= end.valueOf())
                ||
                (existEnd.valueOf() >= start.valueOf() && existEnd.valueOf() <= end.valueOf())
            ) && x.coach
        ) {
            const coachId = x.coach.id;
            const idx = coaches.findIndex(c => c.id === coachId);
            if (idx >= 0) {
                coaches.splice(idx, 1);
            }
        }
    })

    return coaches;
}

export const checkAvailablePrivateCoach = (
    coaches: Coach[],
    session: Session,
    counter: MonthCounters
) => {

    const matchedCounterSessions = [...counter.sessions].filter(s => {
        const isSessionType = s.sessionType === 'privateClass';
        if (!s.start || !s.end || !session.start || !session.end) { return null };
        const isTimed = (s.start?.valueOf() >= session.start?.valueOf() && s.start?.valueOf() < session.end?.valueOf()) || (s.end?.valueOf() > session.start?.valueOf() && s.end?.valueOf() <= session.end?.valueOf());
        const hasCoach = !!s.privateCoach;
        return isSessionType && isTimed && hasCoach;
    });

    const assignedCoaches: string[] = [];
    matchedCounterSessions.forEach(x => {
        if (x.privateCoach) {
            assignedCoaches.push(x.privateCoach.id);
        }
    })

    const availableCoaches = [...coaches].filter(x => !assignedCoaches.includes(x.id));

    return availableCoaches;
}
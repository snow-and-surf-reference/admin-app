import {
    IonAlert,
    IonButton, IonContent,
    IonFooter,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonLoading,
    IonModal,
    IonSelect,
    IonSelectOption,
    IonTitle,
    IonToolbar
} from "@ionic/react";
import * as GroupClassTypes from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import { setGroupClassPreset } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAuthUser } from "app/slices/authSlice";
import { checkmarkOutline, closeOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

interface Props {
    groupClassItem: GroupClassTypes.GroupClassPreset | null;
    setItem: (item: GroupClassTypes.GroupClassPreset | null) => void;
}

const EditGroupClassModel: React.FC<Props> = (props) => {
    const { groupClassItem, setItem } = props;
    const [disableSubmit, setDisableSubmit] = useState(true);
    const [saving, setSaving] = useState('idle');
    const authStaff = useAppSelector(selectAuthUser);

    useEffect(() => {
        if (groupClassItem && groupClassItem.name.en) {
            setDisableSubmit(false);
        } else {
            setDisableSubmit(true);
        }
    }, [groupClassItem]);

    const handleSubmit = async () => {
        if (!groupClassItem) { return };
        const tempPreset = { ...groupClassItem };
        if (!tempPreset.createdBy && authStaff) {
            Object.assign(tempPreset, {
                createdBy: authStaff
            })
        }
        setSaving('saving');
        await setGroupClassPreset(tempPreset);
        setSaving('saved');
    };

    return (
        <>
            <IonModal isOpen={!!groupClassItem} onDidDismiss={() => setItem(null)}>
                {!groupClassItem ? null : (
                    <>
                        <IonHeader>
                            <IonToolbar>
                                <IonButton slot="start" fill="clear" onClick={() => setItem(null)}>
                                    <IonIcon icon={closeOutline} slot="start" />
                                    <IonLabel>Cancel</IonLabel>
                                </IonButton>
                                <IonTitle>{groupClassItem.id === "new" ? `New Class Type` : `Edit Class Type`}</IonTitle>
                            </IonToolbar>
                        </IonHeader>
                        <IonContent color={"light"}>
                            <IonList inset>
                                <IonItem>
                                    <IonLabel>Class Type</IonLabel>
                                    <IonSelect
                                        value={groupClassItem.classType}
                                        onIonChange={(e) => {
                                            setItem(
                                                Object.assign(
                                                    { ...groupClassItem },
                                                    {
                                                        classType: e.detail.value!,
                                                    }
                                                )
                                            );
                                        }}
                                    >
                                        <IonSelectOption value={`ski`}>Ski</IonSelectOption>
                                        <IonSelectOption value={`sb`}>Snowboard</IonSelectOption>
                                    </IonSelect>
                                </IonItem>
                            </IonList>
                            <IonList inset>
                                <IonListHeader>
                                    <IonLabel>
                                        <p>Class Name</p>
                                    </IonLabel>
                                </IonListHeader>
                                <IonItem>
                                    <IonLabel position="stacked">English</IonLabel>
                                    <IonInput
                                        type="text"
                                        placeholder="e.g. Beginner Ski Class"
                                        value={groupClassItem.name.en}
                                        onIonChange={(e) => {
                                            setItem(
                                                Object.assign(
                                                    { ...groupClassItem },
                                                    {
                                                        name: Object.assign(
                                                            { ...groupClassItem.name },
                                                            {
                                                                en: e.detail.value!,
                                                            }
                                                        ),
                                                    }
                                                )
                                            );
                                        }}
                                    />
                                </IonItem>
                                <IonItem>
                                    <IonLabel position="stacked">中文</IonLabel>
                                    <IonInput
                                        type="text"
                                        placeholder="e.g. 初級滑雪班"
                                        value={groupClassItem.name.zh}
                                        onIonChange={(e) => {
                                            setItem(
                                                Object.assign(
                                                    { ...groupClassItem },
                                                    {
                                                        name: Object.assign(
                                                            { ...groupClassItem.name },
                                                            {
                                                                zh: e.detail.value!,
                                                            }
                                                        ),
                                                    }
                                                )
                                            );
                                        }}
                                    />
                                </IonItem>
                            </IonList>
                        </IonContent>
                        <IonFooter>
                            <IonToolbar color={"warning"}>
                                <IonButton expand="block" color={"warning"} disabled={disableSubmit} onClick={() => handleSubmit()}>
                                    <IonIcon icon={checkmarkOutline} slot="start" />
                                    <IonLabel>Save Change</IonLabel>
                                </IonButton>
                            </IonToolbar>
                        </IonFooter>
                    </>
                )}
            </IonModal>
            <IonLoading
                isOpen={saving === 'saving'}
                message={`Saving`}
                spinner={'dots'}
            />
            <IonAlert
                isOpen={saving === 'saved'}
                onDidDismiss={() => {
                    setSaving('idle');
                    setItem(null);
                }}
                header={`Class Type Saved`}
                buttons={[
                    {
                        text: 'OK',
                        handler: () => {
                            setSaving('idle');
                            setItem(null);
                        }
                    }
                ]}
            />
        </>
    );
};

export default EditGroupClassModel;

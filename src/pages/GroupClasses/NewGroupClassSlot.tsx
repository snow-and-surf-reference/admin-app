import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonContent,
    IonDatetime,
    IonDatetimeButton,
    IonFooter,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonPage,
    IonSegment,
    IonSegmentButton,
    IonSelect,
    IonSelectOption,
    IonSpinner,
    IonToolbar,
} from "@ionic/react";
import { GroupClassSlot } from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import { Coach } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { defaultDaysOfWeek } from "app/data/defaultDaysOfWeek";
import { setGroupClassSlot } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAuthUser } from "app/slices/authSlice";
import { selectGroupClassPresets } from "app/slices/groupClassesSlice";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { checkmarkOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { checkAvailableCoachesByDateTime, checkAvailableRCoaches } from "./checkAvailableCoaches";
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const NewGroupClassSlot: React.FC = () => {
    const history = useHistory();
    const [isRecurring, setIsRecurring] = useState(false);
    const [singleClassSlot, setSingleClassSlot] = useState<GroupClassSlot | null>(null);
    const [rClassSlot, setRClassSlot] = useState<GroupClassSlot | null>(null);
    const authStaff = useAppSelector(selectAuthUser);
    const presets = useAppSelector(selectGroupClassPresets);
    const [updatingCoaches, setUpdatingCoaches] = useState(true);
    const [coaches, setCoaches] = useState<Coach[]>([]);
    const [rCoaches, setRCoaches] = useState<Coach[]>([]);
    const [disableSubmit, setDisableSubmit] = useState(true);
    const [saving, setSaving] = useState("idle");

    // update default slot when various states are ready
    useEffect(() => {
        if (!authStaff || presets.length <= 0) {
            return;
        }
        const tempSingleSlot: GroupClassSlot = {
            id: "new",
            createdAt: new Date(),
            enable: true,
            start: `10:00`,
            end: `11:00`,
            isRecurring: false,
            coach: null,
            classPreset: presets[0],
            createdBy: authStaff,
            minPax: 1,
            maxPax: 4,
        };
        setSingleClassSlot(
            Object.assign(
                { ...tempSingleSlot },
                {
                    date: dayjs.tz(new Date()).add(7, "day").startOf("day").toDate(),
                }
            )
        );
        setRClassSlot(
            Object.assign(
                { ...tempSingleSlot },
                {
                    dayOfWeek: defaultDaysOfWeek[0],
                    isRecurring: true,
                    startDate: dayjs.tz(dayjs()).toDate(),
                    endDate: dayjs.tz(dayjs()).add(1, "month").toDate(),
                }
            )
        );
    }, [authStaff, presets]);

    // check available coach upon date time changes
    useEffect(() => {
        if (!singleClassSlot?.date || !singleClassSlot.classPreset.classType) {
            return;
        }

        setSingleClassSlot((s) =>
            s
                ? Object.assign(
                      { ...s },
                      {
                          coach: null,
                      }
                  )
                : s
        );

        const updateAvailableCoaches = async () => {
            setUpdatingCoaches(true);
            const targetStart = dayjs.tz(`${dayjs.tz(singleClassSlot.date).format(`YYYY-MM-DD`)}T${singleClassSlot.start}`).toDate();
            const coaches: Coach[] = await checkAvailableCoachesByDateTime(targetStart, singleClassSlot.classPreset.classType);
            setUpdatingCoaches(false);
            setCoaches(coaches);
        };
        updateAvailableCoaches();
    }, [singleClassSlot?.date, singleClassSlot?.start, singleClassSlot?.classPreset.classType]);

    // check available coach for recurring slots
    useEffect(() => {
        if (!rClassSlot?.dayOfWeek || !rClassSlot.classPreset.classType || !rClassSlot.start) {
            return;
        }

        setRClassSlot((s) =>
            s
                ? Object.assign(
                      { ...s },
                      {
                          coach: null,
                      }
                  )
                : s
        );

        const updateRCoaches = async () => {
            setUpdatingCoaches(true);
            if (!rClassSlot.dayOfWeek || !rClassSlot.dayOfWeek) {
                return;
            }

            const coaches: Coach[] = await checkAvailableRCoaches(
                rClassSlot.dayOfWeek.index,
                rClassSlot.start,
                rClassSlot.classPreset.classType,
                rClassSlot.startDate,
                rClassSlot.endDate
            );
            setUpdatingCoaches(false);
            setRCoaches(coaches);
        };
        updateRCoaches();
    }, [rClassSlot?.dayOfWeek, rClassSlot?.start, rClassSlot?.classPreset.classType, rClassSlot?.endDate, rClassSlot?.startDate]);

    // check can submit
    useEffect(() => {
        if (isRecurring) {
            if (!rClassSlot || !rClassSlot.classPreset || !rClassSlot.dayOfWeek || rClassSlot.start === "" || !rClassSlot.coach) {
                setDisableSubmit(true);
            } else {
                setDisableSubmit(false);
            }
        } else {
            if (!singleClassSlot || !singleClassSlot.classPreset || !singleClassSlot.date || singleClassSlot.start === "" || !singleClassSlot.coach) {
                setDisableSubmit(true);
            } else {
                setDisableSubmit(false);
            }
        }
    }, [rClassSlot, singleClassSlot, isRecurring]);

    const handleSubmit = async () => {
        if (!rClassSlot || !singleClassSlot) {
            return;
        }
        setSaving("saving");
        await setGroupClassSlot(isRecurring ? rClassSlot : singleClassSlot);
        setSaving("saved");
    };

    return (
        <IonPage>
            <Header title="New Class Slot" back />
            <IonContent color={"light"}>
                <IonCard>
                    <IonCardContent>
                        <IonItem lines="none">
                            <IonLabel>
                                <h3>
                                    <b>Slot Type</b>
                                </h3>
                            </IonLabel>
                        </IonItem>
                        <IonSegment
                            color={`dark`}
                            value={isRecurring ? "recurring" : "single"}
                            onIonChange={(e) => {
                                setIsRecurring(e.detail.value === "recurring");
                            }}
                        >
                            <IonSegmentButton value="single">Single</IonSegmentButton>
                            <IonSegmentButton value="recurring">Recurring</IonSegmentButton>
                        </IonSegment>
                    </IonCardContent>
                </IonCard>

                {!isRecurring ? (
                    !singleClassSlot ? null : (
                        <>
                            <IonCard>
                                <IonCardContent>
                                    <IonItem lines="none">
                                        <IonLabel>
                                            <h3>
                                                <b>Class Preset</b>
                                            </h3>
                                        </IonLabel>
                                        <IonSelect
                                            value={singleClassSlot.classPreset.id}
                                            onIonChange={(e) => {
                                                const id = e.detail.value!;
                                                const newPreset = presets.find((p) => p.id === id);
                                                if (!newPreset) {
                                                    return;
                                                }
                                                setSingleClassSlot(
                                                    Object.assign(
                                                        { ...singleClassSlot },
                                                        {
                                                            classPreset: { ...newPreset },
                                                        }
                                                    )
                                                );
                                            }}
                                        >
                                            {presets.map((x) => (
                                                <IonSelectOption key={`new-class-slot-preset-option-${x.id}`} value={x.id}>
                                                    {x.name.en}
                                                </IonSelectOption>
                                            ))}
                                        </IonSelect>
                                    </IonItem>
                                </IonCardContent>
                            </IonCard>
                            <IonCard>
                                <IonCardContent>
                                    <IonItem lines="none">
                                        <IonLabel>
                                            <h3>
                                                <b>{`Date & Time`}</b>
                                            </h3>
                                        </IonLabel>
                                    </IonItem>
                                    <IonList inset color="light">
                                        <IonItem color={"light"}>
                                            <IonLabel>Date</IonLabel>
                                            <IonInput
                                                type="date"
                                                className="ion-text-end"
                                                value={dayjs.tz(singleClassSlot.date).format(`YYYY-MM-DD`)}
                                                onIonChange={(e) => {
                                                    setSingleClassSlot(
                                                        Object.assign(
                                                            { ...singleClassSlot },
                                                            {
                                                                date: dayjs.tz(`${e.detail.value!}T00:00`).toDate(),
                                                            }
                                                        )
                                                    );
                                                }}
                                            />
                                        </IonItem>
                                        <IonItem color={"light"}>
                                            <IonLabel>Start Time</IonLabel>
                                            <IonInput
                                                type="time"
                                                className="ion-text-end"
                                                value={singleClassSlot.start}
                                                onIonChange={(e) => {
                                                    setSingleClassSlot(
                                                        Object.assign(
                                                            { ...singleClassSlot },
                                                            {
                                                                start: e.detail.value!,
                                                                end: dayjs(`${dayjs().format(`YYYY-MM-DD`)}T${e.detail.value!}`)
                                                                    .add(1, `hour`)
                                                                    .format(`HH:mm`),
                                                            }
                                                        )
                                                    );
                                                }}
                                            />
                                        </IonItem>
                                        <IonItem color={`light`}>
                                            <IonLabel>End Time (Read Only)</IonLabel>
                                            <IonInput
                                                className="ion-text-end"
                                                disabled
                                                value={dayjs(`${dayjs().format(`YYYY-MM-DD`)}T${singleClassSlot.end}`).format(`hh:mm A`)}
                                            />
                                        </IonItem>
                                    </IonList>
                                </IonCardContent>
                            </IonCard>

                            <IonCard>
                                <IonCardContent>
                                    <IonItem lines="none">
                                        {updatingCoaches ? (
                                            <>
                                                <IonSpinner name="dots" slot="start" />
                                                <IonLabel>{`Updating`}</IonLabel>
                                            </>
                                        ) : coaches.length <= 0 ? (
                                            <IonLabel>
                                                <h3>
                                                    <b>{`No Coach Available at the selected time`}</b>
                                                </h3>
                                            </IonLabel>
                                        ) : (
                                            <>
                                                <IonLabel>
                                                    <h3>
                                                        <b>{`Assign Coach`}</b>
                                                    </h3>
                                                </IonLabel>
                                                <IonSelect
                                                    placeholder="Select Coach"
                                                    value={singleClassSlot.coach?.id}
                                                    onIonChange={(e) => {
                                                        setSingleClassSlot(
                                                            Object.assign(
                                                                { ...singleClassSlot },
                                                                {
                                                                    coach: coaches.find((y) => y.id === e.detail.value!) || null,
                                                                }
                                                            )
                                                        );
                                                    }}
                                                >
                                                    {coaches.map((x) => (
                                                        <IonSelectOption key={`coach-selection-item-${x.id}`} value={x.id}>
                                                            {x.usrname}
                                                        </IonSelectOption>
                                                    ))}
                                                </IonSelect>
                                            </>
                                        )}
                                    </IonItem>
                                </IonCardContent>
                            </IonCard>

                            <IonCard>
                                <IonCardContent>
                                    <IonItem lines="none">
                                        <IonLabel>
                                            <h3>
                                                <b>Capacity</b>
                                            </h3>
                                        </IonLabel>
                                    </IonItem>
                                    <IonList inset color="light">
                                        <IonItem color={`light`}>
                                            <IonLabel>Minimum</IonLabel>
                                            <IonInput
                                                slot="end"
                                                className="ion-text-end"
                                                type="number"
                                                value={singleClassSlot.minPax}
                                                min={1}
                                                max={singleClassSlot.maxPax}
                                                onIonChange={(e) => {
                                                    setSingleClassSlot(
                                                        Object.assign(
                                                            { ...singleClassSlot },
                                                            {
                                                                minPax: Number(e.detail.value!),
                                                            }
                                                        )
                                                    );
                                                }}
                                            />
                                        </IonItem>
                                        <IonItem color={`light`}>
                                            <IonLabel>Maximum</IonLabel>
                                            <IonInput
                                                slot="end"
                                                className="ion-text-end"
                                                type="number"
                                                value={singleClassSlot.maxPax}
                                                min={singleClassSlot.minPax}
                                                onIonChange={(e) => {
                                                    setSingleClassSlot(
                                                        Object.assign(
                                                            { ...singleClassSlot },
                                                            {
                                                                maxPax: Number(e.detail.value!),
                                                            }
                                                        )
                                                    );
                                                }}
                                            />
                                        </IonItem>
                                    </IonList>
                                </IonCardContent>
                            </IonCard>
                        </>
                    )
                ) : !rClassSlot || !rClassSlot.dayOfWeek ? null : (
                    <>
                        <IonCard>
                            <IonCardContent>
                                <IonItem lines="none">
                                    <IonLabel>
                                        <h3>
                                            <b>Class Preset</b>
                                        </h3>
                                    </IonLabel>
                                    <IonSelect
                                        value={rClassSlot.classPreset.id}
                                        onIonChange={(e) => {
                                            const id = e.detail.value!;
                                            const newPreset = presets.find((p) => p.id === id);
                                            if (!newPreset) {
                                                return;
                                            }
                                            setRClassSlot(
                                                Object.assign(
                                                    { ...rClassSlot },
                                                    {
                                                        classPreset: { ...newPreset },
                                                    }
                                                )
                                            );
                                        }}
                                    >
                                        {presets.map((x) => (
                                            <IonSelectOption key={`new-class-slot-preset-option-${x.id}`} value={x.id}>
                                                {x.name.en}
                                            </IonSelectOption>
                                        ))}
                                    </IonSelect>
                                </IonItem>
                            </IonCardContent>
                        </IonCard>
                        <IonCard>
                            <IonCardContent>
                                <IonItem lines="none">
                                    <IonLabel>
                                        <h3>
                                            <b>{`Day & Time`}</b>
                                        </h3>
                                    </IonLabel>
                                </IonItem>
                                <IonList inset color="light">
                                    <IonItem color={"light"}>
                                        <IonLabel>Day of Week</IonLabel>
                                        <IonSelect
                                            className="ion-text-end"
                                            slot="end"
                                            value={rClassSlot.dayOfWeek.index}
                                            onIonChange={(e) => {
                                                const index = Number(e.detail.value!);
                                                const dayOfWeek = defaultDaysOfWeek.find((w) => w.index === index);
                                                if (!dayOfWeek) {
                                                    return;
                                                }
                                                setRClassSlot(
                                                    Object.assign(
                                                        { ...rClassSlot },
                                                        {
                                                            dayOfWeek,
                                                        }
                                                    )
                                                );
                                            }}
                                        >
                                            {defaultDaysOfWeek.map((x) => (
                                                <IonSelectOption key={`day-of-week-index-${x.index}`} value={x.index}>
                                                    {`Every ${x.day}`}
                                                </IonSelectOption>
                                            ))}
                                        </IonSelect>
                                    </IonItem>
                                    <IonItem color={"light"}>
                                        <IonLabel>Start Time</IonLabel>
                                        <IonInput
                                            type="time"
                                            className="ion-text-end"
                                            value={rClassSlot.start}
                                            onIonChange={(e) => {
                                                setRClassSlot(
                                                    Object.assign(
                                                        { ...rClassSlot },
                                                        {
                                                            start: e.detail.value!,
                                                            end: dayjs(`${dayjs().format(`YYYY-MM-DD`)}T${e.detail.value!}`)
                                                                .add(1, `hour`)
                                                                .format(`HH:mm`),
                                                        }
                                                    )
                                                );
                                            }}
                                        />
                                    </IonItem>
                                    <IonItem color={`light`}>
                                        <IonLabel>End Time (Read Only)</IonLabel>
                                        <IonInput
                                            className="ion-text-end"
                                            disabled
                                            value={dayjs(`${dayjs().format(`YYYY-MM-DD`)}T${rClassSlot.end}`).format(`hh:mm A`)}
                                        />
                                    </IonItem>
                                    <IonItem color={`light`}>
                                        <IonLabel>Recurrent From</IonLabel>
                                        <IonDatetimeButton datetime="startDatePicker"></IonDatetimeButton>
                                        <IonModal keepContentsMounted={true}>
                                            <IonDatetime
                                                id="startDatePicker"
                                                style={{
                                                    color: `black`,
                                                }}
                                                presentation="date"
                                                value={dayjs.tz(rClassSlot.startDate).format(`YYYY-MM-DD`)}
                                                min={dayjs.tz(Date.now()).subtract(3, "months").format()}
                                                max={dayjs.tz(Date.now()).add(3, "years").format()}
                                                onIonChange={(e) => {
                                                    const newValue = dayjs(String(e.detail.value!)).format(`YYYY-MM-DD`);
                                                    const newDate = dayjs.tz(`${newValue}T00:00`).toDate();
                                                    setRClassSlot(
                                                        Object.assign(
                                                            { ...rClassSlot },
                                                            {
                                                                startDate: newDate,
                                                            }
                                                        )
                                                    );
                                                }}
                                            ></IonDatetime>
                                        </IonModal>
                                    </IonItem>
                                    <IonItem color={`light`}>
                                        <IonLabel>Recurrent Until</IonLabel>
                                        <IonDatetimeButton datetime="endDatePicker"></IonDatetimeButton>
                                        <IonModal keepContentsMounted={true}>
                                            <IonDatetime
                                                id="endDatePicker"
                                                style={{
                                                    color: `black`,
                                                }}
                                                presentation="date"
                                                value={dayjs.tz(rClassSlot.endDate).format(`YYYY-MM-DD`)}
                                                min={dayjs.tz(Date.now()).subtract(3, "months").format()}
                                                max={dayjs.tz(Date.now()).add(3, "years").format()}
                                                onIonChange={(e) => {
                                                    const newValue = dayjs(String(e.detail.value!)).format(`YYYY-MM-DD`);
                                                    const newDate = dayjs.tz(`${newValue}T00:00`).toDate();
                                                    setRClassSlot(
                                                        Object.assign(
                                                            { ...rClassSlot },
                                                            {
                                                                endDate: newDate,
                                                            }
                                                        )
                                                    );
                                                }}
                                            ></IonDatetime>
                                        </IonModal>
                                    </IonItem>
                                </IonList>
                            </IonCardContent>
                        </IonCard>

                        <IonCard>
                            <IonCardContent>
                                <IonItem lines="none">
                                    {updatingCoaches ? (
                                        <>
                                            <IonSpinner name="dots" slot="start" />
                                            <IonLabel>{`Updating`}</IonLabel>
                                        </>
                                    ) : rCoaches.length <= 0 ? (
                                        <IonLabel>
                                            <h3>
                                                <b>{`No Coach Available at the selected time`}</b>
                                            </h3>
                                        </IonLabel>
                                    ) : (
                                        <>
                                            <IonLabel>
                                                <h3>
                                                    <b>{`Assign Coach`}</b>
                                                </h3>
                                            </IonLabel>
                                            <IonSelect
                                                placeholder="Select Coach"
                                                value={rClassSlot.coach?.id}
                                                onIonChange={(e) => {
                                                    setRClassSlot(
                                                        Object.assign(
                                                            { ...rClassSlot },
                                                            {
                                                                coach: rCoaches.find((y) => y.id === e.detail.value!) || null,
                                                            }
                                                        )
                                                    );
                                                }}
                                            >
                                                {rCoaches.map((x) => (
                                                    <IonSelectOption key={`coach-selection-item-${x.id}`} value={x.id}>
                                                        {x.usrname}
                                                    </IonSelectOption>
                                                ))}
                                            </IonSelect>
                                        </>
                                    )}
                                </IonItem>
                            </IonCardContent>
                        </IonCard>

                        <IonCard>
                            <IonCardContent>
                                <IonItem lines="none">
                                    <IonLabel>
                                        <h3>
                                            <b>Capacity</b>
                                        </h3>
                                    </IonLabel>
                                </IonItem>
                                <IonList inset color="light">
                                    <IonItem color={`light`}>
                                        <IonLabel>Minimum</IonLabel>
                                        <IonInput
                                            slot="end"
                                            className="ion-text-end"
                                            type="number"
                                            value={rClassSlot.minPax}
                                            min={1}
                                            max={rClassSlot.maxPax}
                                            onIonChange={(e) => {
                                                setRClassSlot(
                                                    Object.assign(
                                                        { ...rClassSlot },
                                                        {
                                                            minPax: Number(e.detail.value!),
                                                        }
                                                    )
                                                );
                                            }}
                                        />
                                    </IonItem>
                                    <IonItem color={`light`}>
                                        <IonLabel>Maximum</IonLabel>
                                        <IonInput
                                            slot="end"
                                            className="ion-text-end"
                                            type="number"
                                            value={rClassSlot.maxPax}
                                            min={rClassSlot.minPax}
                                            onIonChange={(e) => {
                                                setRClassSlot(
                                                    Object.assign(
                                                        { ...rClassSlot },
                                                        {
                                                            maxPax: Number(e.detail.value!),
                                                        }
                                                    )
                                                );
                                            }}
                                        />
                                    </IonItem>
                                </IonList>
                            </IonCardContent>
                        </IonCard>
                    </>
                )}
            </IonContent>
            <IonFooter>
                <IonToolbar color={disableSubmit ? `light` : `warning`}>
                    <IonButton
                        color={disableSubmit ? `light` : `warning`}
                        expand={"block"}
                        disabled={disableSubmit}
                        onClick={() => {
                            handleSubmit();
                        }}
                    >
                        <IonIcon icon={checkmarkOutline} slot="start" />
                        <IonLabel>Save</IonLabel>
                    </IonButton>
                </IonToolbar>
            </IonFooter>
            <IonLoading isOpen={saving === "saving"} spinner={"dots"} message={`Saving Slot`} />
            <IonAlert
                isOpen={saving === "saved"}
                onDidDismiss={() => {
                    setSaving("idle");
                    setSingleClassSlot(null);
                    setRClassSlot(null);
                    history.push(`/groupClasses/slots`);
                }}
                header={`New Class Saved.`}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            setSaving("idle");
                            setSingleClassSlot(null);
                            setRClassSlot(null);
                            history.push(`/groupClasses/slots`);
                        },
                    },
                ]}
            />
        </IonPage>
    );
};

export default NewGroupClassSlot;

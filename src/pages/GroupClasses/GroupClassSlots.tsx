import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonContent,
    IonDatetime,
    IonDatetimeButton,
    IonIcon, IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonPage,
    IonSegment,
    IonSegmentButton,
    IonSpinner
} from "@ionic/react";
import { GroupClassSlot } from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import { deleteGroupClassSlot, getGroupClassSlotsByRange } from "app/firebase";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { addOutline } from "ionicons/icons";
import { useCallback, useEffect, useState } from "react";
import { useHistory } from "react-router";
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

interface SearchResults {
    status: "idle" | "searching";
    list: GroupClassSlot[];
}

const GroupClassSlots: React.FC = () => {
    const history = useHistory();
    const [searchRange, setSearchRange] = useState({
        from: dayjs.tz(new Date()).startOf("month"),
        to: dayjs.tz(new Date()).endOf(`month`),
    });
    const [results, setResults] = useState<SearchResults>({
        status: "idle",
        list: [],
    });
    const [isRecurring, setIsRecurring] = useState(false);
    const [deleting, setDeleting] = useState("idle");
    const [deleteId, setDeleteId] = useState("");

    const doSearch = useCallback(
        async () => {
            setResults({
                status: "searching",
                list: [],
            });
            const newList = await getGroupClassSlotsByRange(searchRange.from.toDate(), searchRange.to.toDate());
            setResults({
                list: newList,
                status: `idle`,
            });
        },
      [searchRange],
    )

    useEffect(() => {
        doSearch();
    }, [doSearch]);

    const handleDelete = async () => {
        setDeleting("deleting");
        await deleteGroupClassSlot(deleteId);
        setDeleting("deleted");
    };

    return (
        <IonPage>
            <Header title="Group Class Slots" />
            <IonContent>
                <IonCard color={"light"}>
                    <IonCardContent>
                        <IonItem color={"light"} lines="none">
                            <IonLabel>
                                <h2>
                                    <b>Search Class Slots</b>
                                </h2>
                            </IonLabel>
                            <IonButton
                                slot="end"
                                size="default"
                                color={"warning"}
                                onClick={() => {
                                    history.push(`/groupClasses/newSlot`);
                                }}
                            >
                                <IonIcon icon={addOutline} slot="start" />
                                <IonLabel>New Class</IonLabel>
                            </IonButton>
                        </IonItem>
                        <IonList inset>
                            <IonItem>
                                <IonLabel>From</IonLabel>
                                <IonDatetimeButton datetime="from-picker"></IonDatetimeButton>
                                <IonModal keepContentsMounted={true}>
                                    <IonDatetime
                                        className="custom-pickers"
                                        id="from-picker"
                                        color={`primary`}
                                        presentation="date"
                                        value={searchRange.from.format(`YYYY-MM-DD`)}
                                        min={dayjs.tz(Date.now()).subtract(3, 'months').format()}
                                        max={dayjs.tz(Date.now()).add(3, 'years').format()}
                                        onIonChange={e => {
                                            setSearchRange(r => Object.assign({ ...r }, {
                                                from: dayjs.tz(`${e.detail.value!}T00:00`)
                                            }))
                                        }}
                                    ></IonDatetime>
                                </IonModal>
                            </IonItem>
                            <IonItem>
                                <IonLabel>To</IonLabel>
                                <IonDatetimeButton datetime="to-picker"></IonDatetimeButton>
                                <IonModal keepContentsMounted={true}>
                                    <IonDatetime
                                        className="custom-pickers"
                                        id="to-picker"
                                        color={`primary`}
                                        presentation="date"
                                        value={searchRange.to.format(`YYYY-MM-DD`)}
                                        min={dayjs.tz(Date.now()).subtract(3, 'months').format()}
                                        max={dayjs.tz(Date.now()).add(3, 'years').format()}
                                        onIonChange={e => {
                                            setSearchRange(r => Object.assign({ ...r }, {
                                                to: dayjs.tz(`${e.detail.value!}T00:00`)
                                            }))
                                        }}
                                    ></IonDatetime>
                                </IonModal>
                            </IonItem>
                        </IonList>
                        <IonButton expand="block" onClick={() => doSearch()}>
                            <IonLabel>Search</IonLabel>
                        </IonButton>
                    </IonCardContent>
                </IonCard>
                <IonCard color={"light"}>
                    <IonCardContent className={results.status === "searching" ? `ion-text-center` : ``}>
                        {results.status === "searching" ? null : (
                            <IonSegment
                                value={isRecurring ? "recurring" : "single"}
                                onIonChange={(e) => {
                                    setIsRecurring(e.detail.value === "recurring");
                                }}
                            >
                                <IonSegmentButton value="single">Single</IonSegmentButton>
                                <IonSegmentButton value="recurring">Recurring</IonSegmentButton>
                            </IonSegment>
                        )}
                        {results.status === "searching" ? (
                            <IonSpinner name="dots" />
                        ) : !isRecurring ? (
                            results.list.filter((x) => !x.isRecurring).length <= 0 ? (
                                <IonLabel color={`danger`}><br/>No Class Found on the selected range.</IonLabel>
                            ) : (
                                <IonList inset>
                                    {results.list
                                        .filter((x) => !x.isRecurring)
                                        .map((y) => (
                                            <IonItem key={`single-class-slot-item-${y.id}`}>
                                                <IonLabel>
                                                    <h3>
                                                        <b>{y.classPreset.name.en}</b>
                                                    </h3>
                                                    <p>{`${dayjs.tz(y.date).format(`DD MMM YYYY`)} | ${y.start} - ${y.end}`}</p>
                                                    <p>
                                                        {y.classPreset.classType === "ski" ? "Ski" : "Snowboard"} class by <b>{y.coach?.usrname}</b>
                                                    </p>
                                                </IonLabel>
                                                <IonButton
                                                    slot="end"
                                                    color={`danger`}
                                                    onClick={() => {
                                                        setDeleteId(y.id);
                                                    }}
                                                >
                                                    <IonLabel>Delete</IonLabel>
                                                </IonButton>
                                            </IonItem>
                                        ))}
                                </IonList>
                            )
                        ) : results.list.filter((x) => x.isRecurring).length <= 0 ? (
                            <IonLabel color={`danger`}><br/>No Class Found on the selected range.</IonLabel>
                        ) : (
                            <IonList inset>
                                {results.list
                                    .filter((x) => x.isRecurring)
                                    .map((y) => (
                                        <IonItem key={`recurring-class-slot-item-${y.id}`}>
                                            <IonLabel>
                                                <h3>
                                                    <b>{y.classPreset.name.en}</b>
                                                </h3>
                                                <p>{`Every ${y.dayOfWeek?.day} | ${y.start} - ${y.end}`}</p>
                                                {y.startDate ? <p>Valid From {dayjs.tz(y.startDate).format(`DD MMM YYYY`)}</p> : null}
                                                {y.endDate ? <p>Valid Until {dayjs.tz(y.endDate).format(`DD MMM YYYY`)}</p> : null}
                                                <p>
                                                    {y.classPreset.classType === "ski" ? "Ski" : "Snowboard"} class by <b>{y.coach?.usrname}</b>
                                                </p>
                                            </IonLabel>
                                            <IonButton
                                                slot="end"
                                                color={`danger`}
                                                onClick={() => {
                                                    setDeleteId(y.id);
                                                }}
                                            >
                                                <IonLabel>Delete</IonLabel>
                                            </IonButton>
                                        </IonItem>
                                    ))}
                            </IonList>
                        )}
                    </IonCardContent>
                </IonCard>
            </IonContent>
            <IonAlert
                isOpen={deleteId !== ""}
                header={`Are you sure?`}
                message={`This action cannot be undone.`}
                onDidDismiss={() => setDeleteId("")}
                buttons={[
                    {
                        text: "Delete",
                        handler: () => {
                            handleDelete();
                        },
                    },
                    {
                        text: "Cancel",
                        role: "cancel",
                        handler: () => {
                            setDeleteId("");
                        },
                    },
                ]}
            />
            <IonLoading spinner={"dots"} isOpen={deleting === "deleting"} message={`Deleting`} />
            <IonAlert
                isOpen={deleting === "deleted"}
                header={`Class slot deleted`}
                onDidDismiss={() => {
                    setDeleteId("");
                    setDeleting("idle");
                    doSearch();
                }}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            setDeleteId("");
                            setDeleting("idle");
                            doSearch();
                        },
                    },
                ]}
            />
        </IonPage>
    );
};

export default GroupClassSlots;

import { IonAlert, IonButton, IonButtons, IonContent, IonIcon, IonItem, IonLabel, IonList, IonLoading, IonPage } from "@ionic/react";
import Header from "components/Global/Header";
import { addOutline } from "ionicons/icons";
import * as GroupClassTypes from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import { Text_i18n } from "@tsanghoilun/snow-n-surf-interface/types/global";
import { useEffect, useState } from "react";
import EditGroupClassModel from "./EditGroupClassModel";
import { useAppSelector } from "app/hooks";
import { selectGroupClassPresets } from "app/slices/groupClassesSlice";
import { deleteGroupClassPreset } from "app/firebase";

const default18: Text_i18n = {
    en: "",
    zh: "",
};

const newClassType: GroupClassTypes.GroupClassPreset = {
    id: "new",
    name: { ...default18 },
    classType: "ski",
    createdAt: new Date(),
    createdBy: null,
};

const GroupClassTypesPage: React.FC = () => {
    const [selectedItem, setSelectedItem] = useState<GroupClassTypes.GroupClassPreset | null>(null);
    const [selectedDeleteId, setSelectedDeleteId] = useState("");
    const [deleting, setDeleting] = useState("idle");
    const presets = useAppSelector(selectGroupClassPresets);

    useEffect(() => {
        console.log(selectedItem);
    }, [selectedItem]);

    const handleDelete = async () => {
        const deleteId = selectedDeleteId;
        setDeleting('deleting');
        await deleteGroupClassPreset(deleteId);
        setDeleting('deleted');
    };

    return (
        <IonPage>
            <Header title="Group Class Presets" />
            <IonContent>
                <br />
                <IonList lines="full">
                    <IonItem>
                        <IonButton size="default" color={"warning"} onClick={() => setSelectedItem({ ...newClassType })}>
                            <IonIcon icon={addOutline} slot="start" />
                            <IonLabel>New Class Type</IonLabel>
                        </IonButton>
                    </IonItem>
                    {presets.map((x) => (
                        <IonItem key={`group-class-preset-item-${x.id}`}>
                            <IonLabel>
                                <h3>{`${x.name.en}${x.name.zh ? ` | ${x.name.zh}` : ``}`}</h3>
                                <p>Type: {x.classType === "sb" ? `Snowboard` : "Ski"}</p>
                            </IonLabel>
                            <IonButtons slot="end">
                                <IonButton
                                    color={"secondary"}
                                    onClick={() => {
                                        setSelectedItem({ ...x });
                                    }}
                                >
                                    <IonLabel>Edit</IonLabel>
                                </IonButton>
                                <IonButton
                                    color={"danger"}
                                    onClick={() => {
                                        setSelectedDeleteId(x.id);
                                    }}
                                >
                                    <IonLabel>Delete</IonLabel>
                                </IonButton>
                            </IonButtons>
                        </IonItem>
                    ))}
                </IonList>
            </IonContent>
            <EditGroupClassModel groupClassItem={selectedItem} setItem={setSelectedItem} />
            <IonAlert
                isOpen={selectedDeleteId !== ""}
                header={`Confirm to delete this preset?`}
                buttons={[
                    {
                        text: "Delete",
                        handler: () => handleDelete(),
                    },
                    {
                        text: `Cancel`,
                        role: "cancel",
                    },
                ]}
            />
            <IonLoading isOpen={deleting === "deleting"} message={`Deleting`} spinner={`dots`} />
            <IonAlert
                isOpen={deleting === "deleted"}
                onDidDismiss={() => {
                    setDeleting("idle");
                    setSelectedDeleteId("");
                }}
                header={`Preset Deleted`}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            setDeleting("idle");
                            setSelectedDeleteId("");
                        },
                    },
                ]}
            />
        </IonPage>
    );
};

export default GroupClassTypesPage;

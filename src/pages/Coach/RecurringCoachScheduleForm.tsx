import { IonCard, IonCardContent, IonCheckbox, IonCol, IonDatetime, IonDatetimeButton, IonGrid, IonInput, IonItem, IonLabel, IonList, IonListHeader, IonModal, IonRow } from "@ionic/react";
import { CoachSchedule } from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { DayOfWeek } from "@tsanghoilun/snow-n-surf-interface/types/global";
import { defaultDaysOfWeek } from "app/data/defaultDaysOfWeek";
import LoadingCard from "components/Global/LoadingCard";
import dayjs from "dayjs";

interface Props {
    schedule: CoachSchedule | null;
    setSchedule: (schedule: CoachSchedule) => void;
}

const RecurringCoachScheduleForm: React.FC<Props> = (props) => {
    const { schedule, setSchedule } = props;

    return !schedule ? (
        <LoadingCard />
    ) : (
        <IonCard>
            <IonCardContent>
                <IonItem lines="none">
                    <IonLabel>
                        <h2>
                            <b>{`Recurring Schedule`}</b>
                        </h2>
                    </IonLabel>
                </IonItem>
                <IonGrid>
                    <IonRow>
                        <IonCol>
                            <IonList inset color={`light`}>
                                <IonListHeader color={`light`}>
                                    <IonLabel>
                                        <p>Available Days</p>
                                    </IonLabel>
                                </IonListHeader>
                                {defaultDaysOfWeek.map((x) => (
                                    <IonItem key={`recurring-day-of-week-checkbox-item-${x.index}`} color={`light`}>
                                        <IonCheckbox
                                            slot="start"
                                            checked={schedule.daysOfWeek?.filter((y) => y.index === x.index).length === 1}
                                            onIonChange={(e) => {
                                                let tempDays = schedule.daysOfWeek;
                                                if (!tempDays) {
                                                    return;
                                                }

                                                const isExist = tempDays.filter((y) => y.index === x.index).length === 1;
                                                if (isExist) {
                                                    tempDays = tempDays.filter((y) => y.index !== x.index);
                                                } else {
                                                    const newDay: DayOfWeek = {
                                                        index: x.index,
                                                        day: x.day,
                                                    };
                                                    tempDays.push(newDay);
                                                }
                                                setSchedule(
                                                    Object.assign(
                                                        { ...schedule },
                                                        {
                                                            daysOfWeek: tempDays,
                                                        }
                                                    )
                                                );
                                            }}
                                        ></IonCheckbox>
                                        <IonLabel>{x.day.toLocaleUpperCase()}</IonLabel>
                                    </IonItem>
                                ))}
                            </IonList>
                        </IonCol>
                        <IonCol>
                            <IonList color="light" inset>
                                <IonItem color={"light"}>
                                    <IonLabel>Start Time</IonLabel>
                                    <IonInput
                                        type="time"
                                        value={schedule.start}
                                        onIonChange={(e) => {
                                            setSchedule(Object.assign({ ...schedule }, { start: e.detail.value! }));
                                        }}
                                    />
                                </IonItem>
                                <IonItem color={"light"}>
                                    <IonLabel>End Time</IonLabel>
                                    <IonInput
                                        type="time"
                                        value={schedule.end}
                                        onIonChange={(e) => {
                                            setSchedule(Object.assign({ ...schedule }, { end: e.detail.value! }));
                                        }}
                                    />
                                </IonItem>
                                <IonItem color={"light"}>
                                    <IonLabel>Start Date</IonLabel>
                                    {/* <IonInput
                                        type="date"
                                        value={dayjs.tz(schedule.startDate).format(`YYYY-MM-DD`)}
                                        onIonChange={(e) => {
                                            setSchedule(Object.assign({ ...schedule }, { startDate: dayjs.tz(`${e.detail.value!}T00:00`).toDate() }));
                                        }}
                                    /> */}
                                        <IonDatetimeButton datetime="startDatePicker" ></IonDatetimeButton>
                                        <IonModal keepContentsMounted={true}>
                                            <IonDatetime
                                                id='startDatePicker'
                                                style={{
                                                    color: `black`
                                                }}
                                                presentation="date"
                                                value={dayjs.tz(schedule.startDate).format(`YYYY-MM-DD`)}
                                                min={dayjs.tz(Date.now()).subtract(3, 'months').format()}
                                                max={dayjs.tz(Date.now()).add(3, 'years').format()}
                                                onIonChange={(e) => {
                                                    const newValue = dayjs(String(e.detail.value!)).format(`YYYY-MM-DD`);
                                                    const newDate = dayjs.tz(`${newValue}T00:00`).toDate();
                                                    setSchedule(Object.assign({ ...schedule }, { startDate: newDate }));
                                                }}
                                            ></IonDatetime>
                                        </IonModal>
                                </IonItem>
                                <IonItem color={"light"}>
                                    <IonLabel>Valid Until</IonLabel>
                                    <IonDatetimeButton datetime="endDatePicker" ></IonDatetimeButton>
                                        <IonModal keepContentsMounted={true}>
                                            <IonDatetime
                                                id='endDatePicker'
                                                style={{
                                                    color: `black`
                                                }}
                                                presentation="date"
                                                value={dayjs.tz(schedule.endDate).format(`YYYY-MM-DD`)}
                                                min={dayjs.tz(Date.now()).subtract(3, 'months').format()}
                                                max={dayjs.tz(Date.now()).add(3, 'years').format()}
                                                onIonChange={(e) => {
                                                    const newValue = dayjs(String(e.detail.value!)).format(`YYYY-MM-DD`);
                                                    const newDate = dayjs.tz(`${newValue}T00:00`).toDate();
                                                    setSchedule(Object.assign({ ...schedule }, { endDate: newDate }));
                                                }}
                                            ></IonDatetime>
                                        </IonModal>
                                </IonItem>
                            </IonList>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonCardContent>
        </IonCard>
    );
};

export default RecurringCoachScheduleForm;

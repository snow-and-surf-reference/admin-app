import { IonButton, IonCol, IonContent, IonFooter, IonGrid, IonInput, IonItem, IonList, IonListHeader, IonPage, IonRow } from "@ionic/react";
import { MonthSession } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Coach } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { getCounterByDate } from "app/firebase";
import { selectAllCoaches } from "app/slices/coachSlice";
import { setIsLoading } from "app/slices/globalSlice";
import { thisMonth, minMonth, maxMonth } from "app/variables";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import { useCallback, useEffect, useRef, useState } from "react";
import { CSVLink } from "react-csv";
import { Data } from "react-csv/components/CommonPropTypes";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";

function CoachReport() {
    const { id } = useParams<{ id: string }>();
    const coaches = useSelector(selectAllCoaches);
    const [coach, setCoach] = useState<Coach>(coaches.find((i) => i.id === id)!);
    const [viewMonth, setViewMonth] = useState(thisMonth);
    const [coachReport, setCoachReport] = useState<MonthSession[]>([]);
    const purchaseCsvRef = useRef<any>(null);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setIsLoading(true));
        getCounterByDate(dayjs.tz(viewMonth).toDate()).then((e) => {
            const temp = e?.sessions.filter((i) => i.privateCoach && i.privateCoach.id === id);
            setCoachReport(temp ? temp : []);
            dispatch(setIsLoading(false));
        });
    }, [dispatch, id, viewMonth]);

    useEffect(() => setCoach((c) => c), []);

    const downloadCoachReport = useCallback(() => {
        let tempReport: Data = [];
        coachReport.forEach((i) => {
            const { start, end, pax } = i;
            tempReport.push({
                date: dayjs.tz(start).format("YYYY-MM-DD"),
                start: dayjs.tz(start).format("HH:mm"),
                end: dayjs.tz(end).format("HH:mm"),
                hour: dayjs(i.end).hour() - dayjs(i.start).hour(),
                pax,
            });
        });
        return !!coachReport.length ? (
            <IonButton onClick={() => purchaseCsvRef.current.link.click()} expand="block">
                <CSVLink
                    ref={purchaseCsvRef}
                    data={tempReport}
                    style={{ color: "#FFF", textDecoration: "none", pointerEvents: "none", width: "100%" }}
                    filename={`${coach.usrname} - Private Class - ${viewMonth}`}
                >
                    DOWNLOAD REPORT
                </CSVLink>
            </IonButton>
        ) : (
            <IonButton disabled expand="block">
                NO REPORT FOR DOWNLOAD
            </IonButton>
        );
    }, [coach.usrname, coachReport, viewMonth]);

    return (
        <IonPage>
            <IonContent fullscreen>
                <Header title={"Coach Report"} back />
                <IonList>
                    <br />
                    <IonListHeader>{`${coach.usrname} - Private class`}</IonListHeader>
                    <br />
                    <IonItem style={{ overflowX: "auto", width: "unset" }}>
                        <IonInput
                            className="border"
                            type="month"
                            min={minMonth > "2022-10" ? minMonth : "2022-10"}
                            max={maxMonth}
                            value={viewMonth}
                            onIonChange={(e) => setViewMonth(e.detail.value!)}
                        />
                    </IonItem>
                    <br />
                </IonList>
                {!coachReport.length ? (
                    <IonGrid className="ion-padding">
                        <IonRow>
                            <IonCol>NO DATA</IonCol>
                        </IonRow>
                    </IonGrid>
                ) : (
                    <IonGrid className="ion-padding">
                        <IonRow>
                            <IonCol className="border br0">Date</IonCol>
                            <IonCol className="border br0">Start</IonCol>
                            <IonCol className="border br0">End</IonCol>
                            <IonCol className="border br0">Hour(s)</IonCol>
                            <IonCol className="border br0">Pax</IonCol>
                        </IonRow>
                        {coachReport.map((i) => (
                            <IonRow key={i.id}>
                                <IonCol className="border br0">{i.start?.toLocaleDateString("en-CA")}</IonCol>
                                <IonCol className="border br0">{dayjs.tz(i.start).format("HH:mm")}</IonCol>
                                <IonCol className="border br0">{dayjs.tz(i.end).format("HH:mm")}</IonCol>
                                <IonCol className="border br0">{dayjs(i.end).hour() - dayjs(i.start).hour()}</IonCol>
                                <IonCol className="border br0">{i.pax}</IonCol>
                            </IonRow>
                        ))}
                    </IonGrid>
                )}
            </IonContent>
            <IonFooter>
                <IonRow>
                    <IonCol className="ion-text-center">{downloadCoachReport()}</IonCol>
                </IonRow>
            </IonFooter>
        </IonPage>
    );
}

export default CoachReport;

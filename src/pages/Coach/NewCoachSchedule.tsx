import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonChip, IonContent, IonItem,
    IonLabel, IonLoading,
    IonPage, IonSegment,
    IonSegmentButton,
    IonSelect,
    IonSelectOption
} from "@ionic/react";
import * as CoachTypes from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { createNewCoachSchedule } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAuthUser } from "app/slices/authSlice";
import { selectAllCoaches } from "app/slices/coachSlice";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import RecurringCoachScheduleForm from "./RecurringCoachScheduleForm";
import SingleCoachScheduleForm from "./SingleCoachScheduleForm";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const NewCoachSchedule: React.FC = () => {
    const authStaff = useAppSelector(selectAuthUser);
    const coaches = useAppSelector(selectAllCoaches);
    const [isRecurring, setIsRecurring] = useState(false);
    const [schedule, setSchedule] = useState<CoachTypes.CoachSchedule | null>(null);
    const [rSchedules, setRSchedules] = useState<CoachTypes.CoachSchedule | null>(null);
    const [saving, setSaving] = useState("idle");
    const history = useHistory();

    useEffect(() => {
        console.log(`recurring schedule updated`, rSchedules);
    }, [rSchedules])

    useEffect(() => {
        if (coaches.length <= 0 || coaches.filter((x) => x.enabled).length <= 0) {
            return;
        }
        const tempSchedule: CoachTypes.CoachSchedule = {
            id: "new",
            date: dayjs.tz(Date.now()).startOf("day").add(1, "week").toDate(),
            start: `10:00`,
            end: `22:00`,
            isRecurring: false,
            enable: true,
            createdBy: authStaff,
            coach: { ...coaches.filter((x) => x.enabled)[0] },
            createdAt: dayjs.tz(Date.now()).toDate(),
        };
        setSchedule(tempSchedule);

        const tempRSchedule: CoachTypes.CoachSchedule = {
            id: "new",
            daysOfWeek: [],
            isRecurring: true,
            startDate: dayjs.tz(Date.now()).toDate(),
            endDate: dayjs.tz(Date.now()).add(1, 'month').toDate(),
            enable: true,
            start: `10:00`,
            end: `22:00`,
            createdBy: authStaff,
            coach: { ...coaches.filter((x) => x.enabled)[0] },
            createdAt: dayjs.tz(Date.now()).toDate(),
        };
        setRSchedules(tempRSchedule);
    }, [coaches, authStaff]);

    const handleSubmit = async () => {
        setSaving(`saving`);
        if (!schedule) {
            return;
        }
        const res = await createNewCoachSchedule(schedule);

        if (res === "success") {
            setSaving(`saved`);
        }
    };

    const handleRecurringSubmit = async () => {
        setSaving(`saving`);
        if (!rSchedules) {
            return;
        }
        const res = await createNewCoachSchedule(rSchedules);

        if (res === "success") {
            setSaving(`saved`);
        }
    };

    return (
        <IonPage>
            <Header title="New Coach Schedule" back />
            <IonContent color={`light`}>
                {!schedule ? null : (
                    <>
                        <br />
                        <IonCard>
                            <IonCardContent>
                                <IonItem lines="none">
                                    <IonLabel>
                                        <h2>
                                            <b>Select Coach</b>
                                        </h2>
                                        <p>
                                            {schedule.coach.coachType.map((x) => (
                                                <IonChip key={`coach-${schedule.coach.id}-type-${x}`}>{x === "sb" ? "Snowboard" : "Ski"}</IonChip>
                                            ))}
                                        </p>
                                    </IonLabel>
                                    <IonSelect
                                        className="ion-text-end"
                                        value={schedule.coach}
                                        interface="action-sheet"
                                        onIonChange={(e) => {
                                            setSchedule(Object.assign({ ...schedule }, { coach: e.detail.value! }));
                                            setRSchedules(rSchedules ? Object.assign({ ...rSchedules }, { coach: e.detail.value! }) : null);
                                        }}
                                    >
                                        {coaches
                                            .filter((x) => x.enabled)
                                            .map((c) => (
                                                <IonSelectOption key={`coach-${c.id}`} value={c.id === schedule.coach.id ? schedule.coach : c}>
                                                    {c.usrname}
                                                </IonSelectOption>
                                            ))}
                                    </IonSelect>
                                </IonItem>
                            </IonCardContent>
                        </IonCard>
                        <IonCard>
                            <IonCardContent>
                                <IonSegment value={isRecurring ? "recurring" : "single"}>
                                    <IonSegmentButton value="single" onClick={() => setIsRecurring(false)}>
                                        <IonLabel>Single</IonLabel>
                                    </IonSegmentButton>
                                    <IonSegmentButton value="recurring" onClick={() => setIsRecurring(true)}>
                                        <IonLabel>Recurring</IonLabel>
                                    </IonSegmentButton>
                                </IonSegment>
                            </IonCardContent>
                        </IonCard>
                        {!isRecurring ? (
                            <SingleCoachScheduleForm schedule={schedule} setSchedule={setSchedule} />
                        ) : (
                            <RecurringCoachScheduleForm schedule={rSchedules} setSchedule={setRSchedules} />
                        )}
                        <IonButton
                            expand="block"
                            className="ion-margin"
                            onClick={() => {
                                isRecurring ? handleRecurringSubmit() : handleSubmit();
                            }}
                        >
                            Submit
                        </IonButton>
                    </>
                )}
            </IonContent>
            <IonLoading isOpen={saving === "saving"} message={`Saving New Schedule...`} />
            <IonAlert
                isOpen={saving === "saved"}
                onDidDismiss={() => {
                    setSaving("idle");
                    history.push("/coach/schedule");
                }}
                header={`Saved!`}
                message={`New Schedule is saved.`}
                buttons={[
                    {
                        text: "OK",
                        role: "confirm",
                        handler: () => {
                            setSaving("idle");
                            history.push("/coach/schedule");
                        },
                    },
                ]}
            />
        </IonPage>
    );
};

export default NewCoachSchedule;

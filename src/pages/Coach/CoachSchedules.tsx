import {
  IonAlert,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonChip,
  IonContent,
  IonFooter,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonLoading,
  IonModal,
  IonPage,
  IonSegment,
  IonSegmentButton,
  IonSpinner,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import * as CoachTypes from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { defaultDaysOfWeek } from "app/data/defaultDaysOfWeek";
import { deleteCoachSchedule, getCoachSchedulesByRange, updateCoachSchedule } from "app/firebase";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { addOutline, alertCircleOutline, checkmarkCircle, checkmarkCircleOutline, chevronBackOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import RecurringCoachScheduleForm from "./RecurringCoachScheduleForm";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

interface SearchResults {
  status: "idle" | "loading";
  list: CoachTypes.CoachSchedule[];
}

const CoachSchedules: React.FC = () => {
  const history = useHistory();
  const [range, setRange] = useState({
    from: dayjs.tz(Date.now()).startOf("month"),
    to: dayjs.tz(Date.now()).endOf("month"),
  });
  const [results, setResults] = useState<SearchResults>({
    status: "idle",
    list: [],
  });

  const [selectedSchedule, setSelectedSchedule] = useState<CoachTypes.CoachSchedule | null>(null);
  const [selectedRSchedule, setSelectedRSchedule] = useState<CoachTypes.CoachSchedule | null>(null);
  const [saving, setSaving] = useState("idle");
  const [deleting, setDeleting] = useState("idle");
  const [scheduleToDeleteId, setScheduleToDeleteId] = useState<string | null>(null);
  const [segment, setSegment] = useState(`single`);

  const handleSubmit = async () => {
    setResults({
      status: "loading",
      list: [],
    });
    const res = await getCoachSchedulesByRange(range.from.toDate(), range.to.toDate());
    setResults({
      status: "idle",
      list: [...res],
    });
  };

  useEffect(() => {
    const doInitSearch = async () => {
      setResults({
        status: "loading",
        list: [],
      });
      const res = await getCoachSchedulesByRange(range.from.toDate(), range.to.toDate());
      setResults({
        status: "idle",
        list: [...res],
      });
    }
    doInitSearch();
  }, [range]);

  const handleEdit = async () => {
    if (!selectedSchedule) {
      return;
    }
    setSaving("saving");
    const res = await updateCoachSchedule(selectedSchedule);
    if (res === "success") {
      let tempResult = [...results.list];
      const idx = tempResult.findIndex((x) => x.id === selectedSchedule.id);
      if (idx >= 0) {
        tempResult[idx] = selectedSchedule;
        setResults((rs) =>
          Object.assign(
            { ...rs },
            {
              list: tempResult,
            }
          )
        );
      }
      setSaving("saved");
    }
  };

  const handleREdit = async () => {
    if (!selectedRSchedule) {
      return;
    }
    setSaving("saving");
    const res = await updateCoachSchedule(selectedRSchedule);
    if (res === "success") {
      let tempResult = [...results.list];
      const idx = tempResult.findIndex((x) => x.id === selectedRSchedule.id);
      if (idx >= 0) {
        tempResult[idx] = selectedRSchedule;
        setResults((rs) =>
          Object.assign(
            { ...rs },
            {
              list: tempResult,
            }
          )
        );
      }
      setSaving("saved");
    }
  };

  const handleDelete = async () => {
    setDeleting("deleting");
    if (!scheduleToDeleteId) {
      return;
    }
    const res = await deleteCoachSchedule(scheduleToDeleteId);
    if (res === "success") {
      setResults((rs) =>
        Object.assign(
          { ...rs },
          {
            list: [...rs.list].filter((x) => x.id !== scheduleToDeleteId),
          }
        )
      );
      setDeleting("deleted");
    }
  };

  return (
    <IonPage>
      <Header title="Coaches Schedules" />
      <IonContent>
        <br />
        <IonCard color={"light"}>
          <IonCardContent>
            <IonItem lines="none" color={"light"}>
              <IonLabel>
                <h2>
                  <b>Search Schedules</b>
                </h2>
              </IonLabel>
              <IonButtons slot="end">
                <IonButton
                  fill="solid"
                  color={`warning`}
                  size="large"
                  shape="round"
                  onClick={() => {
                    history.push(`/newCoachSchedule`);
                  }}
                >
                  <IonIcon icon={addOutline} slot="start" />
                  <IonLabel className="ion-margin-end ion-margin-start">New Schedule</IonLabel>
                </IonButton>
              </IonButtons>
            </IonItem>
            <IonList inset>
              <IonItem>
                <IonLabel>
                  <p>From</p>
                </IonLabel>
                <IonInput
                  type="date"
                  value={range.from.format(`YYYY-MM-DD`)}
                  onIonChange={(e) => {
                    setRange((r) => Object.assign({ ...r }, { from: dayjs.tz(e.detail.value!) }));
                  }}
                />
              </IonItem>
              <IonItem>
                <IonLabel>
                  <p>To</p>
                </IonLabel>
                <IonInput
                  type="date"
                  value={range.to.format(`YYYY-MM-DD`)}
                  onIonChange={(e) => {
                    setRange((r) => Object.assign({ ...r }, { to: dayjs.tz(e.detail.value!) }));
                  }}
                />
              </IonItem>
            </IonList>
            <IonButton
              expand="block"
              color={`primary`}
              onClick={() => {
                handleSubmit();
              }}
            >
              <IonLabel>Search</IonLabel>
            </IonButton>
          </IonCardContent>
        </IonCard>

        <IonCard>
          <IonCardContent>
            <IonSegment
              value={segment}
              onIonChange={e => setSegment(e.detail.value!)}
            >
              <IonSegmentButton value="single" >
                Single
              </IonSegmentButton>
              <IonSegmentButton value="recurring" >
                Recurring
              </IonSegmentButton>
            </IonSegment>
          </IonCardContent>
        </IonCard>

        {results.status === "idle" ? (
          results.list.length > 0 ? (
            <IonCard color={`medium`}>
              {
                segment === 'single' ?
              <IonCardContent>
                <IonList inset>
                  {[...results.list].filter(l => l.date).map((x) => (
                    <IonItem key={`schedule-item-${x.id}`}>
                      <IonLabel>
                        <h2>
                          <b>{x.coach.usrname}</b> | {x.coach.coachType.join(', ')}
                        </h2>
                        <p>
                          {dayjs.tz(x.date).format(`DD MMM YYYY`)}
                        </p>
                        <p>
                          {`${x.start} - ${x.end}`}
                        </p>
                      </IonLabel>
                      <IonButtons slot="end">
                        <IonButton
                          color={`secondary`}
                          fill="solid"
                          onClick={() => {
                            setSelectedSchedule({ ...x });
                          }}
                        >
                          Edit
                        </IonButton>
                        <IonButton
                          color={`danger`}
                          fill="solid"
                          onClick={() => {
                            setScheduleToDeleteId(x.id);
                          }}
                        >
                          Delete
                        </IonButton>
                      </IonButtons>
                    </IonItem>
                  ))}
                </IonList>
              </IonCardContent>
                  :
                  <IonCardContent>
                  <IonList inset>
                    {[...results.list].filter(l => l.daysOfWeek).map((x) => (
                      <IonItem key={`schedule-item-${x.id}`}>
                        <IonLabel>
                          <h2>
                            <b>{x.coach.usrname}</b> | {x.coach.coachType.join(', ')}
                          </h2>
                          <p>
                            {`${x.start} - ${x.end}`}
                          </p>
                          {x.startDate ? <p>Start Date: {dayjs.tz(x.startDate).format(`DD MMM YYYY`)}</p> : null}
                          {x.endDate ? <p>Valid Until: {dayjs.tz(x.endDate).format(`DD MMM YYYY`)}</p> : null}
                          <IonButtons>
                            {
                              defaultDaysOfWeek.map(d => (
                                <IonChip
                                  key={`coach-schedule-recurring-item-${x.id}-day-chip-${d.index}`}
                                  color={x.daysOfWeek && x.daysOfWeek?.filter(xd => xd.index === d.index).length > 0 ? `danger` : `light`}
                                >
                                  {
                                    x.daysOfWeek && x.daysOfWeek?.filter(xd => xd.index === d.index).length > 0 ?
                                      <IonIcon icon={checkmarkCircle} /> : null
                                  }
                                  <IonLabel>{d.day.substring(0, 3).toUpperCase()}</IonLabel>
                                </IonChip>
                              ))
                            }
                          </IonButtons>
                        </IonLabel>
                        <IonButtons slot="end">
                          <IonButton
                            color={`secondary`}
                            fill="solid"
                            onClick={() => {
                              setSelectedRSchedule({ ...x });
                            }}
                          >
                            Edit
                          </IonButton>
                          <IonButton
                            color={`danger`}
                            fill="solid"
                            onClick={() => {
                              setScheduleToDeleteId(x.id);
                            }}
                          >
                            Delete
                          </IonButton>
                        </IonButtons>
                      </IonItem>
                    ))}
                  </IonList>
                </IonCardContent>
              }
            </IonCard>
          ) : (
            <IonCard>
              <IonCardContent>
                <IonItem lines="none">
                  <IonIcon slot="start" icon={alertCircleOutline} color="danger" />
                  <IonLabel color={"danger"}>No Coach Schedule found in the range you specified.</IonLabel>
                </IonItem>
              </IonCardContent>
            </IonCard>
          )
        ) : (
          <IonCard color={`light`}>
            <IonCardContent className="ion-text-center">
              <IonSpinner name="dots" />
            </IonCardContent>
          </IonCard>
        )}
      </IonContent>

      <IonModal isOpen={!!selectedSchedule} onDidDismiss={() => setSelectedSchedule(null)}>
        <IonHeader>
          <IonToolbar color={"dark"}>
            <IonButton slot="start" onClick={() => setSelectedSchedule(null)}>
              <IonIcon icon={chevronBackOutline} slot="start" />
              <IonLabel>Back</IonLabel>
            </IonButton>
            <IonTitle>Edit Schedule</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          {selectedSchedule && (
            <IonList lines="full">
              <IonItem>
                <IonLabel>Coach</IonLabel>
                <IonInput type="text" className="ion-text-end" readonly value={selectedSchedule.coach.usrname} />
              </IonItem>
              <IonItem>
                <IonLabel>Date</IonLabel>
                <IonInput
                  type="date"
                  className="ion-text-end"
                  value={dayjs(`${dayjs.tz(selectedSchedule.date).format(`YYYY-MM-DD`)}T00:00:00`).format(`YYYY-MM-DD`)}
                />
              </IonItem>
              <IonItem>
                <IonLabel>Start</IonLabel>
                <IonInput
                  type="time"
                  className="ion-text-end"
                  value={selectedSchedule.start}
                  onIonChange={(e) => {
                    setSelectedSchedule(Object.assign({ ...selectedSchedule }, { start: e.detail.value! }));
                  }}
                />
              </IonItem>
              <IonItem>
                <IonLabel>End</IonLabel>
                <IonInput
                  type="time"
                  className="ion-text-end"
                  value={selectedSchedule.end}
                  onIonChange={(e) => {
                    setSelectedSchedule(Object.assign({ ...selectedSchedule }, { end: e.detail.value! }));
                  }}
                />
              </IonItem>
            </IonList>
          )}
        </IonContent>
        <IonFooter color="dark">
          <IonToolbar color="dark">
            <IonButton expand="block" onClick={() => handleEdit()}>
              <IonIcon icon={checkmarkCircleOutline} slot="start" />
              <IonLabel>Save Change</IonLabel>
            </IonButton>
          </IonToolbar>
        </IonFooter>
      </IonModal>

      <IonModal
        isOpen={!!selectedRSchedule}
        onDidDismiss={() => setSelectedRSchedule(null)}
      >
        <IonHeader>
          <IonToolbar color={"dark"}>
            <IonButton slot="start" onClick={() => setSelectedRSchedule(null)}>
              <IonIcon icon={chevronBackOutline} slot="start" />
              <IonLabel>Back</IonLabel>
            </IonButton>
            <IonTitle>Edit Schedule</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonCard>
            <IonItem>
              <IonLabel><p>Coach</p></IonLabel>
              <IonLabel slot="end">
                <h3>{selectedRSchedule?.coach.usrname}</h3>
              </IonLabel>
            </IonItem>
          </IonCard>
          <RecurringCoachScheduleForm
            schedule={selectedRSchedule}
            setSchedule={setSelectedRSchedule}
          />
        </IonContent>
        <IonFooter color="dark">
          <IonToolbar color="dark">
            <IonButton expand="block" onClick={() => handleREdit()}>
              <IonIcon icon={checkmarkCircleOutline} slot="start" />
              <IonLabel>Save Change</IonLabel>
            </IonButton>
          </IonToolbar>
        </IonFooter>
      </IonModal>

      <IonLoading isOpen={saving === "saving"} message={`Updating Schedule...`} />
      <IonAlert
        isOpen={saving === "saved"}
        onDidDismiss={() => {
          setSaving("idle");
          setSelectedSchedule(null);
          setSelectedRSchedule(null);
        }}
        header={`Updated!`}
        message={`Schedule is updated.`}
        buttons={[
          {
            text: "OK",
            role: "confirm",
            handler: () => {
              setSaving("idle");
              setSelectedSchedule(null);
              setSelectedRSchedule(null);
            },
          },
        ]}
      />

      <IonAlert
        isOpen={!!scheduleToDeleteId}
        header={`Are you sure?`}
        onDidDismiss={() => {
          // setScheduleToDeleteId(null)
        }}
        message={"Deleted schedules can cause booked class unfulfillable!"}
        buttons={[
          {
            text: "OK",
            role: "confirm",
            handler: () => {
              handleDelete();
            },
          },
          {
            text: "Back",
            role: "cancel",
            handler: () => {
              setScheduleToDeleteId(null);
            },
          },
        ]}
      />
      <IonAlert
        isOpen={deleting === "deleted"}
        header="Schedule Deleted"
        onDidDismiss={() => {
          setDeleting("idle");
          setScheduleToDeleteId(null);
        }}
        buttons={[
          {
            text: "OK",
            role: "confirm",
            handler: () => {
              setDeleting("idle");
              setScheduleToDeleteId(null);
            },
          },
        ]}
      />
    </IonPage>
  );
};

export default CoachSchedules;

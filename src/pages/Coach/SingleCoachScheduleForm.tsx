import { IonCard, IonCardContent, IonCol, IonDatetime, IonGrid, IonInput, IonItem, IonLabel, IonList, IonRow } from "@ionic/react";
import { CoachSchedule } from "@tsanghoilun/snow-n-surf-interface/types/coach";
import LoadingCard from "components/Global/LoadingCard";
import dayjs from "dayjs";

interface Props {
    schedule: CoachSchedule | null;
    setSchedule: (schedule: CoachSchedule) => void;
}

const SingleCoachScheduleForm: React.FC<Props> = (props) => {
    const { schedule, setSchedule } = props;

    return !schedule ? (
        <LoadingCard />
    ) : (
        <IonCard>
            <IonCardContent>
                <IonItem lines="none">
                    <IonLabel>
                        <h2>
                            <b>{`Schedule Date & Time`}</b>
                        </h2>
                    </IonLabel>
                </IonItem>
                <IonGrid>
                    <IonRow>
                        <IonCol size="6">
                            <IonDatetime
                                presentation="date"
                                value={dayjs.tz(schedule.date).format(`YYYY-MM-DD`)}
                                min={dayjs.tz(Date.now()).subtract(3, "months").format()}
                                max={dayjs.tz(Date.now()).add(3, "years").format()}
                                onIonChange={(e) => {
                                    const newValue = dayjs(String(e.detail.value!)).format(`YYYY-MM-DD`);
                                    const newDate = dayjs.tz(`${newValue}T00:00`).toDate();
                                    setSchedule(Object.assign({ ...schedule }, { date: newDate }));
                                }}
                            ></IonDatetime>
                        </IonCol>
                        <IonCol>
                            <IonList color="light" inset>
                                <IonItem color={"light"}>
                                    <IonLabel position="stacked">Date</IonLabel>
                                    <IonInput
                                        type="date"
                                        readonly
                                        value={dayjs(`${dayjs.tz(schedule.date).format(`YYYY-MM-DD`)}T00:00`).format(`YYYY-MM-DD`)}
                                    />
                                </IonItem>
                            </IonList>
                            <IonList color="light" inset>
                                <IonItem color={"light"}>
                                    <IonLabel>Start Time</IonLabel>
                                    <IonInput
                                        type="time"
                                        value={schedule.start}
                                        onIonChange={(e) => {
                                            setSchedule(Object.assign({ ...schedule }, { start: e.detail.value! }));
                                        }}
                                    />
                                </IonItem>
                                <IonItem color={"light"}>
                                    <IonLabel>End Time</IonLabel>
                                    <IonInput
                                        type="time"
                                        value={schedule.end}
                                        onIonChange={(e) => {
                                            setSchedule(Object.assign({ ...schedule }, { end: e.detail.value! }));
                                        }}
                                    />
                                </IonItem>
                            </IonList>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonCardContent>
        </IonCard>
    );
};

export default SingleCoachScheduleForm;

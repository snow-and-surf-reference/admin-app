import { IonAlert, IonButton, IonChip, IonCol, IonContent, IonGrid, IonInput, IonItem, IonLabel, IonList, IonPage, IonRow } from "@ionic/react";
import { CoachPlayType } from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { Coach } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { registerNewCoach } from "app/firebase";
import Header from "components/Global/Header";
import "css/CoachList.css";
import { useState } from "react";
import { singleAlertBtn } from "app/variables";

const defaultNewStaff: Coach = {
    id: "new",
    email: "",
    usrname: "",
    role: "coach",
    phone: "",
    createdAt: new Date(),
    enabled: true,
    coachType: [],
    pricingOffset: 0,
    allowance: 0,
    wallet: { balance: 0, spent: 0 },
};

function CreateCoach() {
    const [message, setMessage] = useState("");
    const [newStaff, setNewStaff] = useState<Coach>(defaultNewStaff);
    const [password, setPassword] = useState("");
    const [rePassword, setRePassword] = useState("");

    const regex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,24}$/;

    const handleSubmit = async () => {
        if (newStaff.coachType.length < 1) {
            setMessage("Please set coach type");
        } else if (password !== rePassword) {
            setMessage("Passwords does not match");
        } else if (!regex.test(password)) {
            setMessage(
                "Password must contain at least one capital letter, one small letter, one number, one special character and 8 character length"
            );
        } else {
            try {
                const res = await registerNewCoach(newStaff, password);
                if (res === "success") {
                    setMessage("New coach created");
                    setNewStaff(defaultNewStaff);
                    setPassword("");
                    setRePassword("");
                } else {
                    setMessage(res);
                }
            } catch (e) {
                console.error(e);
            }
        }
    };

    const handleSelectType = (type: CoachPlayType) => {
        if (newStaff.coachType.includes(type)) {
            const clone = newStaff.coachType.slice();
            clone.splice(newStaff.coachType.indexOf(type), 1);
            setNewStaff({ ...newStaff, coachType: clone });
        } else if (type === "sb") {
            setNewStaff({ ...newStaff, coachType: [type, ...newStaff.coachType] });
        } else {
            setNewStaff({ ...newStaff, coachType: [...newStaff.coachType, type] });
        }
    };

    return (
        <IonPage>
            <IonContent fullscreen>
                <Header title="Create New Coach" />
                <IonList>
                    <IonGrid className="ion-padding">
                        <IonRow>
                            <IonCol style={{ display: "flex", alignItems: "center" }}>
                                <IonLabel color="medium">Coach Type</IonLabel>
                            </IonCol>
                            <IonCol size="6" className="chipContainer">
                                <IonChip
                                    className="chip"
                                    color={newStaff.coachType.includes("sb") ? "tertiary" : "medium"}
                                    onClick={() => handleSelectType("sb")}
                                >
                                    <IonLabel>SNOW BOARD</IonLabel>
                                </IonChip>
                                <IonChip
                                    className="chip"
                                    color={newStaff.coachType.includes("ski") ? "secondary" : "medium"}
                                    onClick={() => handleSelectType("ski")}
                                >
                                    <IonLabel>SKI</IonLabel>
                                </IonChip>
                                {/* <IonChip className="chip" color={newStaff.coachType.includes("surf") ? "tertiary" : "medium"} onClick={() => handleSelectType("surf")}                  >
                  <IonLabel>SURF</IonLabel>
                </IonChip> */}
                            </IonCol>
                            <IonCol />
                        </IonRow>
                    </IonGrid>
                    <IonItem>
                        <IonLabel color="medium">Email address</IonLabel>
                        <IonInput
                            type="text"
                            required
                            value={newStaff.email}
                            onIonChange={(e) => {
                                setNewStaff({
                                    ...newStaff,
                                    email: e.detail.value!,
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Username</IonLabel>
                        <IonInput
                            type="text"
                            required
                            value={newStaff.usrname}
                            onIonChange={(e) => {
                                setNewStaff({
                                    ...newStaff,
                                    usrname: e.detail.value!,
                                });
                            }}
                        />
                    </IonItem>

                    <IonItem>
                        <IonLabel color="medium">Phone</IonLabel>
                        <IonInput
                            type="tel"
                            required
                            maxlength={8}
                            value={newStaff.phone}
                            onIonChange={(e) => {
                                setNewStaff({
                                    ...newStaff,
                                    phone: isNaN(parseInt(e.detail.value!)) ? "" : String(parseInt(e.detail.value!)),
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Pricing offset</IonLabel>
                        <IonInput
                            type="number"
                            required
                            value={newStaff.pricingOffset}
                            onIonChange={(e) => {
                                setNewStaff({
                                    ...newStaff,
                                    pricingOffset: Number(e.detail.value!),
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Password</IonLabel>
                        <IonInput
                            type="password"
                            required
                            value={password}
                            onIonChange={(e) => {
                                setPassword(e.detail.value!);
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Reconfirm Password</IonLabel>
                        <IonInput
                            type="password"
                            required
                            value={rePassword}
                            onIonChange={(e) => {
                                setRePassword(e.detail.value!);
                            }}
                        />
                    </IonItem>

                    <IonRow>
                        <IonCol size="12">
                            <IonButton expand="block" onClick={handleSubmit}>
                                Create coach
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonList>
            </IonContent>
            <IonAlert
                isOpen={!!message}
                onDidDismiss={() => {
                    setMessage("");
                }}
                header={`System Message`}
                message={message}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default CreateCoach;

import {
    IonAlert,
    IonButton,
    IonChip,
    IonCol,
    IonContent,
    IonGrid,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonModal,
    IonPage,
    IonRow,
    IonSearchbar,
} from "@ionic/react";
import { CoachPlayType } from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { PlayType } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Coach } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { setCoachIsEnabled, updateCoachDetails } from "app/firebase";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import { closeSharp } from "ionicons/icons";
import { ModalHeader } from "pages/Sessions/UserSearch";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import { singleAlertBtn } from "app/variables";

function CoachList({ viewRemoved = false }: { viewRemoved?: Boolean }) {
    const [message, setMessage] = useState("");
    const [viewStaff, setViewStaff] = useState<Coach | null>(null);
    const [editStaff, setEditStaff] = useState<Coach | null>(null);
    const [staffList, setStaffList] = useState<Coach[]>([]);
    const [searchField, setSearchField] = useState("");
    const allCoach = useSelector((state: RootState) => state.coaches.coachList);
    const history = useHistory();

    const handleUpdateCoach = async () => {
        await updateCoachDetails(viewStaff?.id!, viewStaff!).then((e) => {
            if (e === "success") {
                setViewStaff(null);
                setEditStaff(null);
            } else {
                setMessage(e);
            }
        });
    };

    const handleSetCoachEnabled = async () => {
        if (viewStaff && viewStaff.id) {
            await setCoachIsEnabled(viewStaff.id, !viewStaff.enabled).then((e) => {
                if (e === "success") {
                    setViewStaff(null);
                    setEditStaff(null);
                } else {
                    setMessage(e);
                }
            });
        }
    };

    const handleSelectType = (type: CoachPlayType) => {
        if (!viewStaff || !!viewRemoved) return;
        if (viewStaff.coachType.includes(type)) {
            let clone = viewStaff.coachType.slice();
            clone.splice(viewStaff.coachType.indexOf(type), 1);
            setViewStaff({ ...viewStaff, coachType: clone });
        } else if (type === "sb") {
            setViewStaff({ ...viewStaff, coachType: [type, ...viewStaff.coachType] });
        } else {
            setViewStaff({ ...viewStaff, coachType: [...viewStaff.coachType, type] });
        }
    };

    useEffect(() => {
        setStaffList(allCoach.filter((i) => (viewRemoved ? !i.enabled : i.enabled)));
    }, [allCoach, viewRemoved]);

    const handleRoute = async () => {
        setViewStaff(null);
        setEditStaff(null);
    };

    return (
        <IonPage>
            <IonContent fullscreen>
                <Header title={!viewRemoved ? "Coach list" : "Removed Coach"} />
                <IonSearchbar
                    placeholder={"Coach username / phone number"}
                    value={searchField}
                    onIonChange={(e) => setSearchField(e.detail.value!)}
                    clearIcon={closeSharp}
                />
                <IonList>
                    <IonListHeader>{!viewRemoved ? "ACTIVE ACCOUNTS" : "REMOVED ACCOUNTS"}</IonListHeader>
                    <br />
                    {staffList.length ? (
                        staffList
                            .filter((i) => i.usrname.includes(searchField) || i.phone.includes(searchField))
                            .map((i) => (
                                <CoachRow
                                    staff={i}
                                    key={i.id}
                                    onClick={() => {
                                        setViewStaff(i);
                                        setEditStaff(i);
                                    }}
                                />
                            ))
                    ) : (
                        <IonGrid>
                            <IonRow className="ion-padding">
                                <IonCol>List is empty</IonCol>
                            </IonRow>
                        </IonGrid>
                    )}
                </IonList>
            </IonContent>
            <IonModal
                isOpen={!!viewStaff}
                onDidDismiss={() => {
                    setViewStaff(null);
                    setEditStaff(null);
                }}
            >
                <ModalHeader text={"Viewing: " + viewStaff?.usrname!} />
                {viewStaff && (
                    <IonList>
                        <IonItem style={{ width: "100%" }}>
                            <IonLabel>Coach Type</IonLabel>
                            <IonCol>
                                <IonChip
                                    className="chip"
                                    color={viewStaff.coachType.includes("sb") ? "tertiary" : "light"}
                                    onClick={() => handleSelectType("sb")}
                                >
                                    <IonLabel>SNOW BOARD</IonLabel>
                                </IonChip>
                                <IonChip
                                    className="chip"
                                    color={viewStaff.coachType.includes("ski") ? "secondary" : "light"}
                                    onClick={() => handleSelectType("ski")}
                                >
                                    <IonLabel>SKI</IonLabel>
                                </IonChip>
                            </IonCol>
                        </IonItem>
                        <IonItem style={{ width: "100%" }}>
                            <IonLabel>Username</IonLabel>
                            <IonInput
                                disabled={!!viewRemoved}
                                slot="end"
                                type="text"
                                value={viewStaff.usrname}
                                onIonChange={(e) => setViewStaff({ ...viewStaff, usrname: e.detail.value! })}
                            />
                        </IonItem>
                        <IonItem>
                            <IonLabel>Phone</IonLabel>
                            <IonInput
                                disabled={!!viewRemoved}
                                slot="end"
                                type="tel"
                                value={viewStaff.phone}
                                onIonChange={(e) => setViewStaff({ ...viewStaff, phone: e.detail.value! })}
                            />
                        </IonItem>
                        <IonItem>
                            <IonLabel>Email</IonLabel>
                            <IonInput
                                disabled={!!viewRemoved}
                                slot="end"
                                type="text"
                                value={viewStaff.email}
                                onIonChange={(e) => setViewStaff({ ...viewStaff, email: e.detail.value! })}
                            />
                        </IonItem>
                        <IonItem>
                            <IonLabel>Price offset</IonLabel>
                            <IonInput
                                disabled={!!viewRemoved}
                                slot="end"
                                type="number"
                                value={viewStaff.pricingOffset}
                                onIonChange={(e) => setViewStaff({ ...viewStaff, pricingOffset: Number(e.detail.value) })}
                            />
                        </IonItem>
                        <IonItem>
                            <IonLabel>Allowance</IonLabel>
                            <IonInput
                                disabled={!!viewRemoved}
                                slot="end"
                                type="number"
                                value={viewStaff.allowance}
                                onIonChange={(e) => setViewStaff({ ...viewStaff, allowance: Number(e.detail.value) })}
                            />
                        </IonItem>
                        <IonItem>
                            <IonLabel>Status</IonLabel>
                            <IonLabel>{viewStaff?.enabled ? "Active" : " Disabled"}</IonLabel>
                        </IonItem>
                    </IonList>
                )}
                <IonGrid style={{ width: "100%" }}>
                    <IonRow>
                        <IonCol>
                            {!viewRemoved && (
                                <IonButton
                                    onClick={handleUpdateCoach}
                                    expand="block"
                                    disabled={JSON.stringify(viewStaff) === JSON.stringify(editStaff)}
                                >
                                    SAVE CHANGES
                                </IonButton>
                            )}

                            <IonButton
                                onClick={async () => {
                                    await handleRoute().then((i) => history.push(`/coach/report/${viewStaff?.id}`));
                                }}
                                color={"tertiary"}
                                expand="block"
                            >
                                VIEW REPORT
                            </IonButton>
                            <IonButton onClick={handleSetCoachEnabled} color={viewStaff?.enabled ? "danger" : "success"} expand="block">
                                {viewStaff?.enabled ? "DISABLE COACH" : "Set Coach to Active"}
                            </IonButton>
                            <IonButton onClick={() => setViewStaff(null)} color="medium" expand="block">
                                CLOSE
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonModal>
            <IonAlert
                isOpen={!!message}
                onDidDismiss={() => {
                    setMessage("");
                }}
                header={`System Message`}
                message={message}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default CoachList;

interface CoachRowProps {
    staff: Coach;
    onClick: () => void;
}

export const CoachRow = (props: CoachRowProps) => {
    const getColor = (type: PlayType) => {
        if (type === "sb") {
            return "tertiary";
        } else if (type === "ski") {
            return "secondary";
        } else if (type === "surf") {
            return "success";
        }
    };
    return (
        <IonItem button detail onClick={props.onClick}>
            <IonCol>
                {props.staff.coachType.map((i) => (
                    <IonChip key={props.staff.id + i} color={getColor(i)}>
                        {i}
                    </IonChip>
                ))}
            </IonCol>
            <IonCol>
                <IonLabel>{props.staff.usrname}</IonLabel>
            </IonCol>
            <IonCol>
                <IonLabel>
                    <h2>{props.staff.phone}</h2>
                </IonLabel>
            </IonCol>
            <IonCol>
                <IonLabel>
                    <h2>{props.staff.email}</h2>
                </IonLabel>
            </IonCol>

            {/* <IonButtons slot="end">
      <IonChip color={`${props.staff.checkedInUsers.length === props.staff.pax ? "secondary" : "medium"}`}>
        <IonIcon icon={people} />
        <IonLabel>{`${props.staff.checkedInUsers.length} / ${props.staff.pax}`}</IonLabel>
      </IonChip>
      <IonChip>
        <IonLabel style={{ textTransform: "capitalize" }}>{props.staff.playType}</IonLabel>
      </IonChip>
      <IonChip color="warning">
        <IonIcon icon={timer} />
        <IonLabel>
          {duration(props.staff.start!, props.staff.end!)} {duration(props.staff.start!, props.staff.end!) > 1 ? "hours" : "hour"}
        </IonLabel>
      </IonChip>
    </IonButtons>*/}
        </IonItem>
    );
};

import { IonCard, IonCol, IonContent, IonGrid, IonInput, IonItem, IonLabel, IonList, IonLoading, IonPage, IonRow } from "@ionic/react";
import { loginStaff } from "app/firebase";
import { useState } from "react";

const Login: React.FC = () => {
  const [values, setValues] = useState({
    email: "",
    password: "",
  });
  const [loading, setLoading] = useState(false);

  const handleSubmit = async () => {
    setLoading(true);
    const res = await loginStaff(values.email, values.password);
    if (res === 'success') {
      window.location.assign(`/sessions/upcoming`);
    } else {
      setLoading(false);
    }
  }

  return (
    <IonPage>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol
              sizeXs="0"
              sizeSm="0"
              sizeMd="1"
              sizeLg="3"
              sizeXl="3"
            ></IonCol>
            <IonCol
              sizeXs="12"
              sizeSm="12"
              sizeMd="10"
              sizeLg="6"
              sizeXl="6"
            >
              <IonCard>
                <IonList>
                  <IonItem>
                    <IonLabel>Email</IonLabel>
                    <IonInput type="email" value={values.email}
                      onIonChange={(e) => {
                        setValues(v => Object.assign({...v}, {email: e.detail.value!}))
                      }}
                    />
                  </IonItem>
                  <IonItem>
                    <IonLabel>Password</IonLabel>
                    <IonInput type="password" value={values.password}
                      onIonChange={(e) => {
                        setValues(v => Object.assign({...v}, {password: e.detail.value!}))
                      }}
                    />
                  </IonItem>
                  <IonItem button color={`warning`} onClick={() => handleSubmit()} >Submit</IonItem>
                </IonList>
              </IonCard>
            </IonCol>
            <IonCol
              sizeXs="0"
              sizeSm="0"
              sizeMd="1"
              sizeLg="3"
              sizeXl="3"
            ></IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
      <IonLoading isOpen={loading} spinner={'dots'} message={'Signing in'}/>
    </IonPage>
  );
};

export default Login;

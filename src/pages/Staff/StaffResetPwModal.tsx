import {
    IonAlert,
    IonButton,
    IonCard,
    IonCardContent,
    IonContent,
    IonFooter,
    IonHeader,
    IonIcon, IonLabel, IonLoading,
    IonModal,
    IonTitle,
    IonToolbar
} from "@ionic/react";
import { Staff } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { resetStaffPassword } from "app/firebase";
import { closeOutline } from "ionicons/icons";
import { useState } from "react";

interface Props {
    staff: Staff | null;
    isOpen: boolean;
    onDismiss: () => void;
}

const StaffResetPwModal: React.FC<Props> = (props) => {
    const { staff, isOpen, onDismiss } = props;
    const [saving, setSaving] = useState("idle");

    const handleSubmit = async () => {
        if (!staff) {
            return;
        }
        setSaving("saving");
        await resetStaffPassword(staff);
        setSaving("saved");
    };

    return (
        <IonModal isOpen={isOpen} onDidDismiss={() => onDismiss()}>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Reset Password for Staff</IonTitle>
                    <IonButton slot="start" fill="clear" onClick={() => onDismiss()}>
                        <IonIcon icon={closeOutline} slot={`start`} />
                        <IonLabel>Cancel</IonLabel>
                    </IonButton>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                {!staff ? null : (
                    <>
                        <br />
                        <IonCard>
                            <IonCardContent>
                                <IonLabel>We will send reset pw link to: {staff.email}</IonLabel>
                            </IonCardContent>
                        </IonCard>
                    </>
                )}
            </IonContent>
            <IonFooter color={"warning"}>
                <IonToolbar color={"warning"}>
                    <IonButton expand="block" color={"warning"} disabled={false} onClick={() => handleSubmit()}>
                        <IonLabel>Send Link</IonLabel>
                    </IonButton>
                </IonToolbar>
            </IonFooter>
            <IonLoading message={`Sending`} spinner={`dots`} isOpen={saving === "saving"} />
            <IonAlert
                header="Reset Link Sent"
                isOpen={saving === "saved"}
                onDidDismiss={() => {
                    setSaving("idle");
                    onDismiss();
                }}
                buttons={[
                    {
                        text: "OK",
                        handler: () => {
                            setSaving("idle");
                            onDismiss();
                        },
                    },
                ]}
            />
        </IonModal>
    );
};

export default StaffResetPwModal;

import {
    IonAlert,
    IonButton,
    IonCol,
    IonContent,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonPage,
    IonRow,
    IonSelect,
    IonSelectOption,
} from "@ionic/react";
import { RoleType, Staff } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { defaultRoles } from "app/data/defaultRoles";
import { registerNewStaff } from "app/firebase";
import { singleAlertBtn } from "app/variables";
import Header from "components/Global/Header";
import { useState } from "react";

const defaultNewStaff: Staff = {
    id: "new",
    email: "",
    usrname: "",
    role: "admin",
    phone: "",
    createdAt: new Date(),
    enabled: true,
    wallet: {
        balance: 0,
        spent: 0,
    },
    allowance: 0,
};

const Roles: RoleType[] = defaultRoles;

function CreateStaff() {
    const [headerMessage, setHeaderMessage] = useState("System Message");
    const [newStaff, setNewStaff] = useState<Staff>(defaultNewStaff);
    const [password, setPassword] = useState("");
    const [rePassword, setRePassword] = useState("");
    const [message, setMessage] = useState("");

    const regex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,24}$/;

    const handleSubmit = async () => {
        if (password !== rePassword) {
            setMessage("Passwords does not match");
        } else if (!regex.test(password)) {
            setMessage(
                "Password must contain at least one capital letter, one small letter, one number, one special character and 8 character length"
            );
        } else {
            try {
                const res = await registerNewStaff(newStaff, password);
                if (res === "success") {
                    setHeaderMessage("Welcome!");
                    setMessage("New staff created");
                    setNewStaff(defaultNewStaff);
                    setPassword("");
                    setRePassword("");
                } else {
                    setMessage(res);
                }
            } catch (e) {
                console.error(e);
            }
        }
    };

    return (
        <IonPage>
            <IonContent fullscreen>
                <Header title="Create New Staff" />
                <IonList>
                    <IonItem>
                        <IonLabel color="medium">Email address</IonLabel>
                        <IonInput
                            type="text"
                            required
                            value={newStaff.email}
                            onIonChange={(e) => {
                                setNewStaff({
                                    ...newStaff,
                                    email: e.detail.value!,
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Username</IonLabel>
                        <IonInput
                            type="text"
                            required
                            value={newStaff.usrname}
                            onIonChange={(e) => {
                                setNewStaff({
                                    ...newStaff,
                                    usrname: e.detail.value!,
                                });
                            }}
                        />
                    </IonItem>

                    <IonItem>
                        <IonLabel color="medium">Phone</IonLabel>
                        <IonInput
                            type="text"
                            required
                            value={newStaff.phone}
                            onIonChange={(e) => {
                                setNewStaff({
                                    ...newStaff,
                                    phone: isNaN(parseInt(e.detail.value!)) ? "" : String(parseInt(e.detail.value!)),
                                });
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Password</IonLabel>
                        <IonInput
                            type="password"
                            required
                            value={password}
                            onIonChange={(e) => {
                                setPassword(e.detail.value!);
                            }}
                        />
                    </IonItem>
                    <IonItem>
                        <IonLabel color="medium">Reconfirm Password</IonLabel>
                        <IonInput
                            type="password"
                            required
                            value={rePassword}
                            onIonChange={(e) => {
                                setRePassword(e.detail.value!);
                            }}
                        />
                    </IonItem>

                    <IonItem>
                        <IonLabel color="medium">Role</IonLabel>
                        <IonSelect
                            value={newStaff.role}
                            onIonChange={(e) => {
                                setNewStaff((ns) =>
                                    Object.assign(
                                        { ...ns },
                                        {
                                            role: e.detail.value!,
                                        }
                                    )
                                );
                            }}
                        >
                            {Roles.map((x) => (
                                <IonSelectOption key={`new-user-role-${x}`} value={x}>
                                    {x}
                                </IonSelectOption>
                            ))}
                        </IonSelect>
                    </IonItem>

                    <IonRow>
                        <IonCol size="12">
                            <IonButton expand="block" onClick={handleSubmit}>
                                Create staff
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonList>
            </IonContent>
            <IonAlert
                isOpen={!!message}
                header={headerMessage}
                message={message}
                onDidDismiss={() => setMessage("")}
                buttons={singleAlertBtn(() => {
                    setHeaderMessage("System Message");
                    setMessage("");
                })}
            />
        </IonPage>
    );
}

export default CreateStaff;

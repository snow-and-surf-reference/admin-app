import {
    IonAlert,
    IonButton,
    IonButtons,
    IonChip,
    IonCol,
    IonContent,
    IonGrid,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonModal,
    IonPage,
    IonRow,
    IonSearchbar,
    IonSelect,
    IonSelectOption,
} from "@ionic/react";
import { RoleType, Staff } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { defaultRoles } from "app/data/defaultRoles";
import { setStaffIsEnabled, updateStaffDetails } from "app/firebase";
import { RootState } from "app/store";
import { singleAlertBtn } from "app/variables";
import Header from "components/Global/Header";
import { closeSharp } from "ionicons/icons";
import { ModalHeader } from "pages/Sessions/UserSearch";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import StaffResetPwModal from "./StaffResetPwModal";

const Roles: RoleType[] = defaultRoles;

function StaffList({ viewRemoved = false }: { viewRemoved?: Boolean }) {
    const [viewStaff, setViewStaff] = useState<Staff | null>(null);
    const [editStaff, setEditStaff] = useState<Staff | null>(null);
    const [pwStaff, setPwStaff] = useState<Staff | null>(null);
    const [staffList, setStaffList] = useState<Staff[]>([]);
    const [searchField, setSearchField] = useState("");
    const [message, setMessage] = useState("");
    const allStaff = useSelector((state: RootState) => state.staffs.staffList);

    const handleUpdateStaff = async () => {
        console.log(viewStaff);
        await updateStaffDetails(viewStaff?.id!, viewStaff!).then((e) => {
            if (e === "success") {
                setViewStaff(null);
                setEditStaff(null);
            } else {
                setMessage(e);
            }
        });
    };

    const handleSetStaffEnabled = async () => {
        await setStaffIsEnabled(viewStaff?.id!, !viewStaff?.enabled).then((e) => {
            if (e === "success") {
                setViewStaff(null);
            } else {
                setMessage(e);
            }
        });
    };

    useEffect(() => {
        setStaffList(allStaff.filter((i) => (viewRemoved ? !i.enabled : i.enabled)));
    }, [allStaff, viewRemoved]);

    return (
        <IonPage>
            <IonContent fullscreen>
                <Header title={!viewRemoved ? "Staff list" : "Removed Staff"} />
                <IonSearchbar
                    placeholder={"Staff username / phone number"}
                    value={searchField}
                    onIonChange={(e) => setSearchField(e.detail.value!)}
                    clearIcon={closeSharp}
                />

                <IonList>
                    <IonListHeader>{!viewRemoved ? "ACTIVE ACCOUNTS" : "REMOVED ACCOUNTS"}</IonListHeader>
                    <br />
                    {staffList.length ? (
                        staffList
                            .filter((i) => i.usrname.includes(searchField) || i.phone.includes(searchField))
                            .map((i) => (
                                <StaffRow
                                    staff={i}
                                    key={i.id}
                                    onClick={() => {
                                        setViewStaff(i);
                                        setEditStaff(i);
                                    }}
                                />
                            ))
                    ) : (
                        <IonGrid>
                            <IonRow className="ion-padding">
                                <IonCol>List is empty</IonCol>
                            </IonRow>
                        </IonGrid>
                    )}
                </IonList>
            </IonContent>
            <IonModal
                isOpen={!!viewStaff}
                onDidDismiss={() => {
                    setViewStaff(null);
                    setEditStaff(null);
                }}
            >
                <ModalHeader text={"Viewing: " + viewStaff?.usrname!} />
                {viewStaff && (
                    <IonList>
                        <IonItem>
                            <IonLabel>Username</IonLabel>
                            <IonInput
                                disabled={!!viewRemoved}
                                slot="end"
                                type="text"
                                value={viewStaff.usrname}
                                onIonChange={(e) => setViewStaff({ ...viewStaff, usrname: e.detail.value! })}
                            />
                        </IonItem>
                        <IonItem>
                            <IonLabel>Phone</IonLabel>
                            <IonInput
                                disabled={!!viewRemoved}
                                slot="end"
                                type="tel"
                                value={viewStaff.phone}
                                onIonChange={(e) => setViewStaff({ ...viewStaff, phone: e.detail.value! })}
                            />
                        </IonItem>
                        <IonItem>
                            <IonLabel>Email</IonLabel>
                            <IonInput
                                disabled={!!viewRemoved}
                                slot="end"
                                type="text"
                                value={viewStaff.email}
                                onIonChange={(e) => setViewStaff({ ...viewStaff, email: e.detail.value! })}
                            />
                        </IonItem>
                        <IonItem>
                            <IonLabel>Credit Allowance</IonLabel>
                            <IonInput
                                disabled={!!viewRemoved}
                                slot="end"
                                type="number"
                                value={viewStaff.allowance ?? 0}
                                onIonChange={(e) => setViewStaff({ ...viewStaff, allowance: Number(e.detail.value!) })}
                            />
                        </IonItem>
                        <IonItem>
                            <IonLabel>Role</IonLabel>
                            <IonSelect
                                value={viewStaff.role}
                                onIonChange={(e) => {
                                    setViewStaff({ ...viewStaff, role: e.detail.value! });
                                }}
                            >
                                {Roles.map((x) => (
                                    <IonSelectOption key={`new-user-role-${x}`} value={x}>
                                        {x}
                                    </IonSelectOption>
                                ))}
                            </IonSelect>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Status</IonLabel>
                            <IonLabel>{viewStaff?.enabled ? "Active" : " Disabled"}</IonLabel>
                        </IonItem>
                    </IonList>
                )}
                <IonGrid style={{ width: "100%" }}>
                    <IonRow>
                        {!viewRemoved && (
                            <IonCol size="12">
                                <IonButton
                                    onClick={handleUpdateStaff}
                                    expand="block"
                                    disabled={JSON.stringify(viewStaff) === JSON.stringify(editStaff)}
                                >
                                    {"SAVE CHANGES"}
                                </IonButton>
                            </IonCol>
                        )}
                        <IonCol size="12">
                            <IonButton onClick={handleSetStaffEnabled} color={viewStaff?.enabled ? "danger" : "success"} expand="block">
                                {viewStaff?.enabled ? "DISABLE STAFF" : "Set Staff to Active"}
                            </IonButton>
                        </IonCol>
                        <IonCol size="12">
                            <IonButton
                                expand="block"
                                color={`tertiary`}
                                onClick={() => {
                                    setPwStaff(viewStaff);
                                }}
                            >
                                Reset Password
                            </IonButton>
                        </IonCol>
                        <IonCol>
                            <IonButton onClick={() => setViewStaff(null)} color="medium" expand="block">
                                CLOSE
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonModal>
            <StaffResetPwModal
                isOpen={!!pwStaff}
                staff={pwStaff}
                onDismiss={() => {
                    setPwStaff(null);
                }}
            />
            <IonAlert
                isOpen={!!message}
                header={`System Message`}
                message={message}
                onDidDismiss={() => setMessage("")}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default StaffList;

interface StaffRowProps {
    staff: Staff;
    onClick: () => void;
}

export const StaffRow = (props: StaffRowProps) => {
    return (
        <IonItem button detail onClick={props.onClick}>
            <IonLabel>
                <h3>{props.staff.usrname}</h3>
            </IonLabel>
            <IonLabel>
                <h2>{props.staff.phone}</h2>
            </IonLabel>
            <IonLabel>
                <h2>{props.staff.email}</h2>
            </IonLabel>
            <IonButtons slot="end">
                <IonChip>{props.staff.role}</IonChip>
            </IonButtons>
        </IonItem>
    );
};

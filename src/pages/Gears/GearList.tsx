import { IonContent, IonItem, IonLabel, IonList, IonPage } from "@ionic/react";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import "css/GearManagement.css";
import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";

function GearList() {
    const history = useHistory();
    const gears = useSelector((state: RootState) => state.gears);
    return (
        <IonPage>
            <Header title={`Gears`} />
            <IonContent color="light">
                <IonList>
                    {gears.gears.map((i) => (
                        <IonItem key={i.id} button onClick={() => history.push(`/gears/management/${i.id}`)}>
                            <IonLabel>{i.name}</IonLabel>
                        </IonItem>
                    ))}
                </IonList>
            </IonContent>
        </IonPage>
    );
}

export default GearList;

import {
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCol,
    IonContent,
    IonGrid,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonPage,
    IonRow,
    IonTitle,
} from "@ionic/react";
import { Gear, GearStock } from "@tsanghoilun/snow-n-surf-interface/types/gear";
import { saveGearSettings } from "app/firebase";
import { selectGearById, setAllGears } from "app/slices/gearSlice";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import { addCircleOutline, removeCircleOutline } from "ionicons/icons";
import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";
import { singleAlertBtn } from "app/variables";

function GearDetails() {
    const { id } = useParams<{ id: string }>();
    const rawGear = useSelector(selectGearById(id));
    const [message, setMessage] = useState("");
    const [gear, setGear] = useState<Gear | undefined>(undefined);
    const history = useHistory();
    const dispatch = useDispatch();
    const reduxGear = useSelector((state: RootState) => state.gears.gears);

    useEffect(() => {
        if (!!rawGear) {
            setGear(rawGear);
        }
    }, [rawGear]);

    const createNewSize = useCallback((size: number) => {
        return {
            class: "standard",
            qty: 0,
            size: size,
        } as GearStock;
    }, []);

    const handleSizeRangeChange = useCallback(
        (size: number, value: "min" | "max") => {
            setGear((prev) => {
                if (!prev) {
                    throw new Error();
                }
                const gear = JSON.parse(JSON.stringify(prev)) as typeof prev;
                if (value === "min") {
                    if (size < gear.unit.minSize!) {
                        gear.stock = Array.from(Array((gear.unit.minSize! - size) / gear.unit.step!).keys())
                            .map((i) => createNewSize(i + size))
                            .concat(gear.stock);
                    } else if (size > gear.unit.minSize!) {
                        gear.stock = gear.stock.filter((stock) => stock.size >= size);
                    }
                    gear.unit.minSize = size;
                } else {
                    if (size < gear.unit.maxSize!) {
                        gear.stock = gear.stock.filter((stock) => stock.size <= size);
                    } else if (size > gear.unit.maxSize!) {
                        gear.stock = gear.stock.concat(
                            Array.from(Array((size - gear.unit.maxSize!) / gear.unit.step!).keys()).map((i) => createNewSize(i + size))
                        );
                    }

                    gear.unit.maxSize = size;
                }
                return gear;
            });
        },
        [createNewSize]
    );

    const chgQty = useCallback(
        (idx: number, value: "inc" | "dec") => {
            const temp = gear!.stock.slice();
            temp.splice(idx, 1, {
                ...gear!.stock[idx],
                qty: value === "dec" ? Math.max(gear!.stock[idx].qty - 1, 0) : gear!.stock[idx].qty + 1,
            });
            setGear((prev) => ({ ...prev!, stock: temp }));
        },
        [gear]
    );

    const handleSubmit = async () => {
        if (gear) {
            await saveGearSettings(gear).then((e) => {
                if (e === "success") {
                    const temp = reduxGear.slice();
                    temp.splice(temp.indexOf(temp.find((i) => i.id === id)!), 1, gear);
                    dispatch(setAllGears([...temp]));
                    setMessage("Changes uploaded");
                } else {
                    setMessage(e);
                }
            });
        }
    };
    const handlePriceChange = (idx: number, price: number) => {
        if (!gear) return;
        const temp = gear.prices.slice();
        temp?.splice(idx, 1, {
            ...temp[idx],
            unitPrice: price,
        });
        setGear({ ...gear, prices: temp });
    };

    return (
        <IonPage>
            {gear ? (
                <>
                    <Header title={`${gear.name} Details`} back />
                    <IonContent>
                        <IonCard>
                            <IonCardHeader>
                                <IonTitle color="medium">General settings</IonTitle>
                            </IonCardHeader>
                            <IonCardContent>
                                <IonList>
                                    <IonItem>
                                        <IonLabel>Gear Name</IonLabel>
                                        <IonButtons slot="end">
                                            <IonInput
                                                type="text"
                                                className="ion-padding"
                                                value={gear.name}
                                                style={{ textAlign: "right" }}
                                                onIonChange={(e) => setGear({ ...gear, name: e.detail.value! })}
                                            />
                                        </IonButtons>
                                    </IonItem>
                                    {gear.prices.map((i, idx) => {
                                        if (i.gearClass === "premium") {
                                            return <></>;
                                        } else {
                                            return (
                                                <IonItem key={i.gearClass + idx}>
                                                    <IonLabel style={{ textTransform: "capitalize" }}>{i.gearClass + " gear price"}</IonLabel>
                                                    <IonButtons slot="end">
                                                        <IonInput
                                                            type="number"
                                                            className="ion-padding"
                                                            value={i.unitPrice}
                                                            style={{ textAlign: "right" }}
                                                            onIonChange={(e) => handlePriceChange(idx, Number(e.detail.value!))}
                                                        />
                                                    </IonButtons>
                                                </IonItem>
                                            );
                                        }
                                    })}
                                    <IonItem>
                                        <IonLabel>Gear Usage</IonLabel>
                                        <IonLabel slot="end">
                                            <b>{gear.gearUsage}</b>
                                        </IonLabel>
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel>Min size</IonLabel>
                                        <IonButtons slot="end">
                                            <IonButton
                                                onClick={() => {
                                                    handleSizeRangeChange(gear.unit.minSize! - gear.unit.step!, "min");
                                                }}
                                            >
                                                <IonIcon icon={removeCircleOutline} slot="icon-only" />
                                            </IonButton>
                                            <IonLabel className="ion-padding">{gear.unit.minSize}</IonLabel>
                                            <IonButton onClick={() => handleSizeRangeChange(gear.unit.minSize! + gear.unit.step!, "min")}>
                                                <IonIcon icon={addCircleOutline} slot="icon-only" />
                                            </IonButton>
                                        </IonButtons>
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel>Max size</IonLabel>
                                        <IonButtons slot="end">
                                            <IonButton
                                                onClick={() => {
                                                    handleSizeRangeChange(gear.unit.maxSize! - gear.unit.step!, "max");
                                                }}
                                            >
                                                <IonIcon icon={removeCircleOutline} slot="icon-only" />
                                            </IonButton>
                                            <IonLabel className="ion-padding">{gear.unit.maxSize}</IonLabel>
                                            <IonButton onClick={() => handleSizeRangeChange(gear.unit.maxSize! + gear.unit.step!, "max")}>
                                                <IonIcon icon={addCircleOutline} slot="icon-only" />
                                            </IonButton>
                                        </IonButtons>
                                    </IonItem>
                                </IonList>
                            </IonCardContent>
                        </IonCard>
                        <IonCard>
                            <IonList>
                                <IonItem>
                                    <IonCol>
                                        <IonTitle color="medium">Standard gears stocks</IonTitle>
                                    </IonCol>
                                </IonItem>
                                {Array((gear.unit.maxSize! - gear.unit.minSize! + gear.unit.step!) / gear.unit.step!)
                                    .fill(0)
                                    .map((i, idx) => (
                                        <IonItem key={gear.stock[idx].size}>
                                            <IonLabel>{gear.unit.minSize! + idx * gear.unit.step!}</IonLabel>
                                            <IonButtons slot="end">
                                                <IonButton
                                                    onClick={() => {
                                                        chgQty(idx, "dec");
                                                    }}
                                                >
                                                    <IonIcon icon={removeCircleOutline} slot="icon-only" />
                                                </IonButton>
                                                <IonLabel className="ion-padding">
                                                    {gear.stock.find(
                                                        (i) => i.class === "standard" && i.size === gear.unit.minSize! + idx * gear.unit.step!
                                                    )?.qty || 0}
                                                </IonLabel>
                                                <IonButton onClick={() => chgQty(idx, "inc")}>
                                                    <IonIcon icon={addCircleOutline} slot="icon-only" />
                                                </IonButton>
                                            </IonButtons>
                                        </IonItem>
                                    ))}
                            </IonList>
                        </IonCard>
                        <IonGrid>
                            <IonRow>
                                <IonCol size="12">
                                    <IonButton expand="block" disabled={JSON.stringify(rawGear) === JSON.stringify(gear)} onClick={handleSubmit}>
                                        SAVE CHANGES
                                    </IonButton>
                                    <IonButton expand="block" color="medium" onClick={() => history.goBack()}>
                                        CANCEL
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </IonContent>
                </>
            ) : (
                <></>
            )}
            <IonAlert
                isOpen={!!message}
                onDidDismiss={() => {
                    setMessage("");
                }}
                header={`System Message`}
                message={message}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default GearDetails;

import {
    IonAlert,
    IonButton,
    IonCard,
    IonContent,
    IonFooter,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonTitle,
    IonToggle,
    IonToolbar,
} from "@ionic/react";
import { CafeItem } from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { updateCafeItem } from "app/firebase";
import { checkmarkCircleOutline, closeOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

interface Props {
    isOpen: boolean;
    item: CafeItem | null;
    onDismiss: () => void;
}

const CafeItemEditModal: React.FC<Props> = (props) => {
    const { isOpen, item, onDismiss } = props;
    const [itemClone, setItemClone] = useState<CafeItem | null>(null);
    const [saving, setSaving] = useState("idle");

    useEffect(() => {
        setItemClone(item);
    }, [item]);

    const handleSave = async () => {
        if (!itemClone) { return };
        setSaving('saving');
        await updateCafeItem(itemClone);
        setSaving('saved');
    }

    return (
        <IonModal isOpen={isOpen} onDidDismiss={() => onDismiss()}>
            {itemClone ? (
                <>
                    <IonHeader>
                        <IonToolbar>
                            <IonButton slot="start" fill="clear" color={`medium`}
                                onClick={() => onDismiss()}
                            >
                                <IonIcon icon={closeOutline} slot="start" />
                                <IonLabel>Cancel</IonLabel>
                            </IonButton>
                            <IonTitle>
                                <IonLabel>
                                    <h3>Edit Item</h3>
                                    <p>{itemClone.name.en}{itemClone.name.zh ? ` | ${itemClone.name.zh}` : ''}</p>
                                </IonLabel>
                            </IonTitle>
                        </IonToolbar>
                    </IonHeader>
                    <IonContent>
                        <IonCard color={`light`}>
                            <IonList inset>
                                <IonItem>
                                    <IonLabel position="stacked">Name</IonLabel>
                                    <IonInput
                                        type="text"
                                        value={itemClone.name.en}
                                        onIonChange={(e) => {
                                            setItemClone(
                                                (c) =>
                                                    c &&
                                                    Object.assign(
                                                        { ...c },
                                                        {
                                                            name: Object.assign(
                                                                { ...c.name },
                                                                {
                                                                    en: e.detail.value!,
                                                                }
                                                            ),
                                                        }
                                                    )
                                            );
                                        }}
                                    />
                                </IonItem>
                                <IonItem>
                                    <IonLabel position="stacked">中文名稱</IonLabel>
                                    <IonInput
                                        type="text"
                                        value={itemClone.name.zh}
                                        onIonChange={(e) => {
                                            setItemClone(
                                                (c) =>
                                                    c &&
                                                    Object.assign(
                                                        { ...c },
                                                        {
                                                            name: Object.assign(
                                                                { ...c.name },
                                                                {
                                                                    zh: e.detail.value!,
                                                                }
                                                            ),
                                                        }
                                                    )
                                            );
                                        }}
                                    />
                                </IonItem>
                                <IonItem>
                                    <IonLabel position="stacked">Price (HKD)</IonLabel>
                                    <IonInput
                                        type="number"
                                        placeholder="12.34"
                                        value={itemClone.price}
                                        onIonChange={(e) => {
                                            setItemClone(
                                                (c) =>
                                                    c &&
                                                    Object.assign(
                                                        { ...c },
                                                        {
                                                            price: Number(e.detail.value!),
                                                        }
                                                    )
                                            );
                                        }}
                                    />
                                </IonItem>
                                <IonItem>
                                    <IonLabel>Out of Stock?</IonLabel>
                                    <IonToggle
                                        checked={itemClone.outOfStock}
                                        onIonChange={(e) => {
                                            setItemClone(
                                                (c) =>
                                                    c &&
                                                    Object.assign(
                                                        { ...c },
                                                        {
                                                            outOfStock: e.detail.checked,
                                                        }
                                                    )
                                            );
                                        }}
                                    />
                                </IonItem>
                            </IonList>
                        </IonCard>
                    </IonContent>
                    <IonFooter>
                        <IonToolbar color={`warning`}>
                            <IonButton expand="block" color={`warning`}
                                disabled={itemClone.name.en === ''}
                                onClick={() => handleSave()}
                            >
                                <IonIcon slot="start" icon={checkmarkCircleOutline} />
                                <IonLabel>Save changes</IonLabel>
                            </IonButton>
                        </IonToolbar>
                    </IonFooter>
                    <IonLoading isOpen={saving === "saving"} spinner="dots" message="Saving Changes" />
                    <IonAlert
                        isOpen={saving === "saved"}
                        onDidDismiss={() => {
                            setSaving("idle");
                            onDismiss();
                        }}
                        header="Item Updated"
                        buttons={[
                            {
                                text: "OK",
                                role: "confirm",
                                handler: () => {
                                    setSaving("idle");
                                    onDismiss();
                                },
                            },
                        ]}
                    />
                </>
            ) : null}
        </IonModal>
    );
};

export default CafeItemEditModal;

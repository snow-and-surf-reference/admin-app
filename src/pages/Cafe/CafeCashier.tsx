import { IonAlert, IonContent, IonLoading, IonPage } from "@ionic/react";
import { CafeCat, CafeItem, CafeOrder } from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { createCafeOrder } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAllCafeItems, selectCafeCats } from "app/slices/cafeSlice";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { useState } from "react";
import { BiTrash } from "react-icons/bi";
import "./CafeCashier.scss";
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const newOrder: CafeOrder = {
    id: 'new',
    createdAt: dayjs().toDate(),
    // phone: '',
    items: [],
    orderDate: dayjs.tz(dayjs()).startOf('day').toDate(),
    orderNumber: '000',
    paymentMethod: 'cash',
    total: 0,
    takeaway: true,
    status: 'pending'
}

const CafeCashier: React.FC = () => {
    const allCats: CafeCat[] = useAppSelector(selectCafeCats);
    const allItems: CafeItem[] = useAppSelector(selectAllCafeItems);
    const [selectedCat, setSelectedCat] = useState<CafeCat | null>(null);
    const [order, setOrder] = useState(newOrder);
    const [checkingOut, setCheckingOut] = useState('idle');
    const [orderNumber, setOrderNumber] = useState('');

    const handleCheckout = async (orderNumber: string) => {
        setCheckingOut('creating');
        // add order number to the order
        let tempOrder = { ...order };
        const newNumber = Number(orderNumber).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });
        Object.assign(tempOrder, { orderNumber: `C${newNumber}` });
        const res = await createCafeOrder(tempOrder, `C${newNumber}`);
        if (res.newCode) {
            setOrderNumber(res.newCode);
        }
        setCheckingOut('confirmed');
    }

    return (
        <IonPage>
            <Header title='Cafe Cashier'/>
            <IonContent>
                <div className="cafe-cashier-container">
                    <div className="panels">
                        <div className="menu-panel">
                            {!selectedCat ? (
                                <>
                                    {[...allCats]
                                        .sort((a, b) => a.index - b.index)
                                        .map((x) => (
                                            <div
                                                key={`cat-${x.id}`}
                                                className="category-btn"
                                                onClick={() => {
                                                    setSelectedCat({ ...x });
                                                }}
                                            >
                                                {x.name.zh ? x.name.zh : x.name.en}
                                            </div>
                                        ))}
                                </>
                            ) : (
                                    <>
                                        <div
                                                key={`item-back`}
                                                className="item-btn back-btn"
                                                onClick={() => {
                                                    setSelectedCat(null);
                                                }}
                                        >
                                        返回    
                                        </div>
                                    {[...allItems]
                                        .filter((g) => g.cat.id === selectedCat.id)
                                        .sort((a, b) => a.index - b.index)
                                        .map((x) => (
                                            <button
                                                key={`item-${x.id}`}
                                                className={`item-btn${x.outOfStock ? ' soldOut' : ''}`}
                                                disabled={x.outOfStock}
                                                onClick={() => {
                                                    setOrder(o => Object.assign({ ...o }, {
                                                        items: [...o.items, {...x}]
                                                    }))
                                                }}
                                            >
                                                {x.name.zh ? x.name.zh : x.name.en}
                                                {x.outOfStock ? <><br/><span className="soldout-text" >SOLD OUT</span></>: null}
                                            </button>
                                        ))}
                                </>
                            )}
                        </div>
                        <div className="selected-items">
                            <label>Selected Items:</label>
                            {
                                order.items.map((x, idx) => (
                                    <div
                                        key={`selected-item-${idx}-id-${x.id}`}
                                        className="selected-item"
                                        onClick={() => {
                                            const tempItems = [...order.items];
                                            tempItems.splice(idx, 1)
                                            setOrder(o => Object.assign({ ...o }, {
                                                items: tempItems
                                            }))
                                        }}
                                    >
                                        <BiTrash className="remove-btn" />
                                        <span className="name">{x.name.zh ? x.name.zh : x.name.en}</span>
                                        <span className="unit-price">${x.price.toLocaleString(undefined, {minimumFractionDigits: 2})}</span>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                    <div className="checkout-area">
                        <div className="total-panel">
                            TOTAL:
                            <div className="total">
                                ${order.items.reduce((p,v) => p + v.price, 0).toLocaleString(undefined, {minimumFractionDigits: 2})}
                            </div>
                        </div>
                        <div className="action-panel">
                            <button
                                onClick={() => {
                                    if (order.items.length <= 0) {
                                        return
                                    }
                                    setOrder(o => Object.assign({ ...o }, { paymentType: `cash` }));
                                    setCheckingOut('confirming');
                                }}
                            >CASH</button>
                            <button
                                onClick={() => {
                                    if (order.items.length <= 0) {
                                        return
                                    }
                                    setOrder(o => Object.assign({ ...o }, { paymentType: `cc` }));
                                    setCheckingOut('confirming');
                                }}
                            >CARD</button>
                            <button className="reset"
                                onClick={() => {
                                    setOrder(newOrder);
                                    setSelectedCat(null);
                                }}
                            >RESET</button>
                        </div>
                    </div>
                </div>
                <IonAlert
                    isOpen={checkingOut === 'confirming'}
                    header={'Confirm Order'}
                    message={`Please Provide Order Number`}
                    inputs={[
                        {
                            placeholder: 'e.g. 2',
                            min: 1,
                            max: 99,
                            type: 'number',
                            name: 'orderNumber'
                        }
                    ]}
                    buttons={[
                        {
                            text: `Paid`,
                            handler: async (alertData) => {
                                handleCheckout(alertData.orderNumber);
                            }
                        },
                        {
                            text: `Cancel`,
                            role: `cancel`,
                            handler: () => {
                                setCheckingOut('idle')
                            }
                        }
                    ]}
                />
                <IonLoading isOpen={checkingOut === 'creating'} spinner={'dots'} message={`Creating Order`} />
                <IonAlert
                    isOpen={checkingOut === 'confirmed'}
                    header={`New Order: ${orderNumber}`}
                    buttons={[
                        {
                            text: `OK`,
                            handler: () => {
                                setOrder(newOrder);
                                setSelectedCat(null);
                                setCheckingOut('idle');
                            }
                        },
                    ]}
                />
            </IonContent>
        </IonPage>
    );
};

export default CafeCashier;

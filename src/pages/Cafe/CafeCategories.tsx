import {
    IonAlert,
    IonButton,
    IonButtons, IonContent,
    IonIcon,
    IonItem,
    IonLabel, IonListHeader,
    IonLoading,
    IonPage,
    IonReorder,
    IonReorderGroup,
    IonToggle,
    ItemReorderEventDetail
} from "@ionic/react";
import * as CafeTypes from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { deleteCafeCat, saveCafeCatsOrder } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectCafeCats } from "app/slices/cafeSlice";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { addOutline, createOutline, menuOutline, swapVerticalOutline, trashOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import CafeCatModal from "./CafeCatModal";
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const newCat: CafeTypes.CafeCat = {
    id: "new",
    name: { en: "", zh: "" },
    createdAt: dayjs().toDate(),
    index: 0,
};

const CafeCategories: React.FC = () => {
    const history = useHistory();
    const [selectedCat, setSelectedCat] = useState<CafeTypes.CafeCat | null>(null);
    const allCatsRaw: CafeTypes.CafeCat[] = useAppSelector(selectCafeCats);
    const [toggleReorder, setToggleReorder] = useState(false);
    const [allCats, setAllCats] = useState(allCatsRaw);
    const [deleting, setDeleting] = useState('');

    useEffect(() => {
        setAllCats(allCatsRaw);
    }, [allCatsRaw])

    const doReorder = async (event: CustomEvent<ItemReorderEventDetail>) => {
        // The `from` and `to` properties contain the index of the item
        // when the drag started and ended, respectively
        console.log("Dragged from index", event.detail.from, "to", event.detail.to);
        event.detail.complete(false);

        let tempCats = [...allCats].sort((a, b) => a.index - b.index);
        
        const fromItem = tempCats.splice(event.detail.from, 1);
        tempCats.splice(event.detail.to, 0, ...fromItem);
        tempCats.forEach((x, idx) => {
            tempCats[idx] = Object.assign({ ...x }, { index: idx });
        })
        setAllCats(tempCats);
        saveCafeCatsOrder(tempCats);
    };

    const handleDelete = async () => {
        if (deleting === '' || deleting === 'deleting' || deleting === 'deleted') { return };
        const taregetId = `${deleting}`;
        setDeleting('deleting');
        await deleteCafeCat(taregetId);
        setDeleting('');
    }

    return (
        <IonPage>
            <Header title="Cafe Menu" />
            <IonContent>
                {/* <IonList> */}
                <IonListHeader>
                    <IonLabel>All Categories</IonLabel>
                    <IonButtons>
                        <IonButton
                            color={`danger`}
                            size="default"
                            onClick={() => {
                                setSelectedCat({ ...newCat });
                            }}
                        >
                            <IonIcon color={`danger`} slot="start" icon={addOutline} />
                            <IonLabel color={`danger`} className="">
                                <h3>New Category</h3>
                            </IonLabel>
                        </IonButton>
                    </IonButtons>
                </IonListHeader>
                <IonItem>
                    <IonToggle checked={toggleReorder} slot='end'
                        onIonChange={(e) => {
                            setToggleReorder(e.detail.checked)
                        }}
                    />
                    <IonLabel slot='end'><p>Reorder Menu</p></IonLabel>
                </IonItem>
                <IonReorderGroup disabled={!toggleReorder} onIonItemReorder={doReorder}>
                    {[...allCats].sort((a, b) => a.index - b.index).map((x) => (
                        <IonItem key={`cafe-cats-${x.id}`}>
                            <IonLabel>
                                <h3>{x.name.en}</h3>
                                {x.name.zh ? <p>{x.name.zh}</p> : null}
                            </IonLabel>
                            <IonButtons slot="end">
                                <IonButton color={`warning`} fill='solid' size="small" shape="round"
                                    onClick={() => {
                                        history.push(`/cafe/cats/${x.id}`)
                                    }}
                                >
                                    <IonIcon icon={menuOutline} slot='start' size="small" />
                                    <IonLabel><h4>Edit Menu</h4></IonLabel>
                                </IonButton>
                                <IonButton color={'secondary'}
                                    onClick={() => {
                                        setSelectedCat({...x})
                                    }}
                                >
                                    <IonIcon icon={createOutline} slot='start'/>
                                </IonButton>
                                <IonButton color={'danger'}
                                    onClick={() => {
                                        setDeleting(x.id);
                                    }}
                                >
                                    <IonIcon icon={trashOutline} slot='start'/>
                                </IonButton>
                            </IonButtons>
                            <IonReorder slot="end">
                                <IonIcon icon={swapVerticalOutline}/>
                            </IonReorder>
                        </IonItem>
                    ))}
                </IonReorderGroup>
                {/* </IonList> */}
            </IonContent>

            <CafeCatModal isOpen={!!selectedCat} onDismiss={() => setSelectedCat(null)} cat={selectedCat} />
            <IonLoading isOpen={deleting === "deleting"} message="Deleting" spinner="dots" />
            <IonAlert
                isOpen={deleting !== "" && deleting !== "deleting" && deleting !== "deleted"}
                message="Are you sure?"
                buttons={[
                    {
                        text: "Delete",
                        role: "confirm",
                        cssClass: "alert-delete",
                        handler: () => {
                            handleDelete();
                        },
                    },
                    {
                        text: "Back",
                        role: "cancel",
                        handler: () => {
                            setDeleting("");
                        },
                    },
                ]}
            />
        </IonPage>
    );
};

export default CafeCategories;

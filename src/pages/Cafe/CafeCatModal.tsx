import {
    IonAlert,
    IonButton,
    IonCard,
    IonContent,
    IonFooter,
    IonHeader,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonLoading,
    IonModal,
    IonTitle,
    IonToolbar,
} from "@ionic/react";
import { CafeCat } from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { saveCafeCat } from "app/firebase";
import { checkmarkCircleOutline, closeOutline } from "ionicons/icons";
import { useEffect, useState } from "react";

interface Props {
    isOpen: boolean;
    onDismiss: () => void;
    cat: CafeCat | null;
}

const CafeCatModal: React.FC<Props> = (props) => {
    const { isOpen, onDismiss, cat } = props;
    const [catClone, setCatClone] = useState<CafeCat | null>(null);
    const [disableSave, setDisableSave] = useState(true);
    const [saving, setSaving] = useState("idle");

    useEffect(() => console.log(catClone), [catClone]);
    useEffect(() => {
        if (!cat) {
            return;
        }
        setCatClone(cat);
    }, [cat]);
    useEffect(() => {
        if (!catClone || catClone.name.en === "") {
            setDisableSave(true);
        } else {
            setDisableSave(false);
        }
    }, [catClone]);

    const handleSave = async () => {
        if (!catClone) {
            return;
        }
        setSaving("saving");
        await saveCafeCat(catClone);
        setSaving("saved");
    };

    return (
        <>
            <IonModal isOpen={isOpen} onDidDismiss={() => onDismiss()}>
                <IonHeader>
                    <IonToolbar>
                        <IonButton slot="start" fill="clear" onClick={() => onDismiss()}>
                            <IonIcon icon={closeOutline} slot="start" />
                            <IonLabel>Cancel</IonLabel>
                        </IonButton>
                        <IonTitle>New Category</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    {catClone ? (
                        <IonCard color={`light`}>
                            <IonList inset>
                                <IonItem>
                                    <IonLabel position="stacked">Name</IonLabel>
                                    <IonInput
                                        type="text"
                                        placeholder="e.g. Rice Dishes"
                                        value={catClone.name.en}
                                        onIonChange={(e) => {
                                            setCatClone((cc) =>
                                                cc
                                                    ? Object.assign(
                                                          { ...cc },
                                                          {
                                                              name: Object.assign({ ...cc.name }, { en: e.detail.value! }),
                                                          }
                                                      )
                                                    : cc
                                            );
                                        }}
                                    />
                                </IonItem>
                                <IonItem>
                                    <IonLabel position="stacked">中文名稱</IonLabel>
                                    <IonInput
                                        type="text"
                                        placeholder="e.g. 飯類"
                                        value={catClone.name.zh}
                                        onIonChange={(e) => {
                                            setCatClone((cc) =>
                                                cc
                                                    ? Object.assign(
                                                          { ...cc },
                                                          {
                                                              name: Object.assign({ ...cc.name }, { zh: e.detail.value! }),
                                                          }
                                                      )
                                                    : cc
                                            );
                                        }}
                                    />
                                </IonItem>
                            </IonList>
                        </IonCard>
                    ) : null}
                </IonContent>
                <IonFooter>
                    <IonToolbar color={disableSave ? `light` : `warning`}>
                        <IonButton color={disableSave ? `light` : `warning`} expand={"block"} disabled={disableSave}
                            onClick={() => handleSave()}
                        >
                            <IonIcon icon={checkmarkCircleOutline} slot={"start"} />
                            <IonLabel>Save Changes</IonLabel>
                        </IonButton>
                    </IonToolbar>
                </IonFooter>
            </IonModal>
            <IonLoading isOpen={saving === "saving"} spinner="dots" message="Saving Changes" />
            <IonAlert
                isOpen={saving === "saved"}
                onDidDismiss={() => {
                    setSaving("idle");
                    onDismiss()
                }}
                header="Category Saved"
                buttons={[
                    {
                        text: "OK",
                        role: "confirm",
                        handler: () => {
                            setSaving("idle");
                            onDismiss()
                        },
                    },
                ]}
            />
        </>
    );
};

export default CafeCatModal;

import {
    IonAccordion,
    IonAccordionGroup,
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonChip,
    IonContent,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonLoading,
    IonPage,
    IonReorder,
    IonReorderGroup,
    IonSpinner,
    IonToggle,
    ItemReorderEventDetail,
} from "@ionic/react";
import { CafeCat, CafeItem } from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { createNewCafeItem, deleteCafeItem, saveCafeItemsOrder, updateCafeItemStock } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectAllCafeItems, selectCafeCats, selectCafeIsInit } from "app/slices/cafeSlice";
import Header from "components/Global/Header";
import LoadingCard from "components/Global/LoadingCard";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { arrowForwardCircleOutline, createOutline, imageOutline, swapVerticalOutline, trashOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useParams } from "react-router";
import CafeItemEditModal from "./CafeItemEditModal";
import CafeItemImgModal from "./CafeItemImgModal";
import ImageComp from "./ImageComp";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const CafeItems: React.FC = () => {
    const allCats: CafeCat[] = useAppSelector(selectCafeCats);
    const isInit: boolean = useAppSelector(selectCafeIsInit);
    const [cat, setCat] = useState<CafeCat | null>(null);
    const { catId } = useParams<{ catId: string }>();
    const itemsRaw: CafeItem[] = useAppSelector(selectAllCafeItems);
    const [items, setItems] = useState<CafeItem[]>([]);
    const [newItem, setNewItem] = useState<CafeItem | null>(null);
    const [newSaving, setNewSaving] = useState("idle");
    const [imgModalItem, setImgModalItem] = useState<CafeItem | null>(null);
    const [editModalItem, setEditModalItem] = useState<CafeItem | null>(null);
    const [deleting, setDeleting] = useState("");
    const [toggleReorder, setToggleReorder] = useState(false);
    const [stockChangeItem, setStockChangeItem] = useState<CafeItem | null>(null);

    const getNewItem = (cat: CafeCat) => {
        return {
            id: "new",
            name: { en: "", zh: "" },
            createdAt: dayjs().toDate(),
            images: "",
            outOfStock: false,
            price: 0,
            index: 0,
            cat,
        } as CafeItem;
    };

    useEffect(() => {
        setItems([...itemsRaw].filter((x) => x.cat.id === catId));
        setImgModalItem((x) => x && (itemsRaw.find((y) => y.id === x.id) || x));
    }, [itemsRaw, catId]);

    useEffect(() => {
        if (!catId || !isInit || allCats.length <= 0) {
            return;
        }
        const tempCat = allCats.find((x) => x.id === catId);
        if (!tempCat) {
            return;
        }
        setCat({ ...tempCat });

        const newItemTemplate: CafeItem = getNewItem({ ...tempCat });
        setNewItem(newItemTemplate);
    }, [catId, allCats, isInit]);

    const handleSubmitNewItem = async () => {
        if (!newItem) {
            return;
        }
        setNewSaving(`saving`);
        await createNewCafeItem(newItem);
        setNewSaving("idle");
        if (cat) {
            const newItemTemplate: CafeItem = getNewItem({ ...cat });
            setNewItem(newItemTemplate);
        }
    };

    const doReorder = async (event: CustomEvent<ItemReorderEventDetail>) => {
        // The `from` and `to` properties contain the index of the item
        // when the drag started and ended, respectively
        console.log("Dragged from index", event.detail.from, "to", event.detail.to);
        event.detail.complete(false);

        let tempItems = [...items].sort((a, b) => a.index - b.index);

        const fromItem = tempItems.splice(event.detail.from, 1);
        tempItems.splice(event.detail.to, 0, ...fromItem);
        tempItems.forEach((x, idx) => {
            tempItems[idx] = Object.assign({ ...x }, { index: idx });
        });
        setItems(tempItems);
        saveCafeItemsOrder(tempItems);
    };

    const handleDelete = async () => {
        if (deleting === "" || deleting === "deleting" || deleting === "deleted") {
            return;
        }
        const taregetId = `${deleting}`;
        setDeleting("deleting");
        await deleteCafeItem(taregetId);
        setDeleting("");
    };

    return (
        <IonPage>
            {isInit ? (
                <>
                    {cat ? (
                        <>
                            <Header title={`${cat.name.en} Menu`} back />
                            <IonContent>
                                {newItem ? (
                                    <IonCard color={"light"}>
                                        <IonAccordionGroup color="light">
                                            <IonAccordion color="light">
                                                <IonItem slot="header" color={"light"}>
                                                    <IonLabel>+ New Item</IonLabel>
                                                </IonItem>
                                                <IonList slot="content" inset>
                                                    <IonItem>
                                                        <IonLabel position="stacked">Name</IonLabel>
                                                        <IonInput
                                                            type="text"
                                                            placeholder="e.g. Cheese Burger"
                                                            value={newItem.name.en}
                                                            onIonChange={(e) => {
                                                                setNewItem(
                                                                    (n) =>
                                                                        n &&
                                                                        Object.assign(
                                                                            { ...n },
                                                                            {
                                                                                name: Object.assign(
                                                                                    { ...n.name },
                                                                                    {
                                                                                        en: e.detail.value!,
                                                                                    }
                                                                                ),
                                                                            }
                                                                        )
                                                                );
                                                            }}
                                                        />
                                                    </IonItem>
                                                    <IonItem>
                                                        <IonLabel position="stacked">中文名稱</IonLabel>
                                                        <IonInput
                                                            type="text"
                                                            placeholder="e.g. 芝士漢堡"
                                                            value={newItem.name.zh}
                                                            onIonChange={(e) => {
                                                                setNewItem(
                                                                    (n) =>
                                                                        n &&
                                                                        Object.assign(
                                                                            { ...n },
                                                                            {
                                                                                name: Object.assign(
                                                                                    { ...n.name },
                                                                                    {
                                                                                        zh: e.detail.value!,
                                                                                    }
                                                                                ),
                                                                            }
                                                                        )
                                                                );
                                                            }}
                                                        />
                                                    </IonItem>
                                                    <IonItem>
                                                        <IonLabel position="stacked">Price (HKD)</IonLabel>
                                                        <IonInput
                                                            type="number"
                                                            placeholder="12.34"
                                                            value={newItem.price}
                                                            onIonChange={(e) => {
                                                                setNewItem(
                                                                    (n) =>
                                                                        n &&
                                                                        Object.assign(
                                                                            { ...n },
                                                                            {
                                                                                price: Number(e.detail.value!),
                                                                            }
                                                                        )
                                                                );
                                                            }}
                                                        />
                                                    </IonItem>
                                                    <br />
                                                    <IonButton
                                                        size="default"
                                                        expand="block"
                                                        color={newItem.name.en === "" ? "medium" : "warning"}
                                                        disabled={newItem.name.en === "" || newSaving === "saving"}
                                                        onClick={() => handleSubmitNewItem()}
                                                    >
                                                        {newSaving === "idle" ? (
                                                            <>
                                                                <IonIcon icon={arrowForwardCircleOutline} slot="end" />
                                                                <IonLabel>Create Item</IonLabel>
                                                            </>
                                                        ) : (
                                                            <IonSpinner name="dots" />
                                                        )}
                                                    </IonButton>
                                                </IonList>
                                            </IonAccordion>
                                        </IonAccordionGroup>
                                    </IonCard>
                                ) : (
                                    <LoadingCard />
                                )}
                                <IonList>
                                    <IonListHeader>
                                        <IonLabel>Menu Items</IonLabel>
                                    </IonListHeader>
                                    <IonItem>
                                        <IonToggle
                                            checked={toggleReorder}
                                            slot="end"
                                            onIonChange={(e) => {
                                                setToggleReorder(e.detail.checked);
                                            }}
                                        />
                                        <IonLabel slot="end">
                                            <p>Reorder Menu</p>
                                        </IonLabel>
                                    </IonItem>
                                    <IonReorderGroup disabled={!toggleReorder} onIonItemReorder={doReorder}>
                                        {[...items]
                                            .sort((a, b) => a.index - b.index)
                                            .map((x) => (
                                                <IonItem key={`cat-${cat.id}-item-${x.id}`}>
                                                    <ImageComp src={x.images} fit='cover' />
                                                    <IonLabel>
                                                        <h3>{x.name.en}</h3>
                                                        {x.name.zh ? <p>{x.name.zh}</p> : null}
                                                        <p>${x.price.toLocaleString(undefined, {minimumFractionDigits: 2})}</p>
                                                    </IonLabel>
                                                    <IonButtons slot="end">
                                                        <IonChip
                                                            color={x.outOfStock ? 'medium' : 'success'}
                                                            onClick={() => setStockChangeItem({...x})}
                                                        >
                                                            <IonLabel>{x.outOfStock ? 'Out of Stock' : 'In Stock'}</IonLabel>
                                                        </IonChip>
                                                        <IonButton
                                                            color={`secondary`}
                                                            fill="solid"
                                                            onClick={() => {
                                                                setImgModalItem({ ...x });
                                                            }}
                                                        >
                                                            <IonIcon icon={imageOutline} slot="start" size="small" />
                                                            <IonLabel>Change Photo</IonLabel>
                                                        </IonButton>
                                                        <IonButton
                                                            color={`primary`}
                                                            fill="solid"
                                                            onClick={() => {
                                                                setEditModalItem({ ...x });
                                                            }}
                                                        >
                                                            <IonIcon icon={createOutline} slot="start" size="small" />
                                                            <IonLabel>Edit</IonLabel>
                                                        </IonButton>
                                                        <IonButton
                                                            color={`danger`}
                                                            fill="solid"
                                                            onClick={() => {
                                                                setDeleting(x.id);
                                                            }}
                                                        >
                                                            <IonIcon icon={trashOutline} slot="start" size="small" />
                                                            <IonLabel>Delete</IonLabel>
                                                        </IonButton>
                                                    </IonButtons>
                                                    <IonReorder slot="end">
                                                        <IonIcon icon={swapVerticalOutline} />
                                                    </IonReorder>
                                                </IonItem>
                                            ))}
                                    </IonReorderGroup>
                                </IonList>
                            </IonContent>
                        </>
                    ) : (
                        <>
                            <Header title={`Unknown Category`} back />
                            <IonContent>
                                <IonCard>
                                    <IonCardContent className="ion-text-center">
                                        <IonLabel>
                                            <h2>Unknown Category</h2>
                                            <p>We don't seems to find this category, please go back to categories page and try again.</p>
                                        </IonLabel>
                                    </IonCardContent>
                                </IonCard>
                            </IonContent>
                        </>
                    )}
                </>
            ) : null}
            <IonLoading isOpen={!isInit} spinner={"dots"} />
            <CafeItemImgModal isOpen={!!imgModalItem} onDismiss={() => setImgModalItem(null)} item={imgModalItem} />
            <CafeItemEditModal isOpen={!!editModalItem} onDismiss={() => setEditModalItem(null)} item={editModalItem} />
            <IonLoading isOpen={deleting === "deleting"} message="Deleting" spinner="dots" />
            <IonAlert
                isOpen={deleting !== "" && deleting !== "deleting" && deleting !== "deleted"}
                message="Are you sure?"
                buttons={[
                    {
                        text: "Delete",
                        role: "confirm",
                        cssClass: "alert-delete",
                        handler: () => {
                            handleDelete();
                        },
                    },
                    {
                        text: "Back",
                        role: "cancel",
                        handler: () => {
                            setDeleting("");
                        },
                    },
                ]}
            />
            <IonAlert
                isOpen={!!stockChangeItem}
                header={`Update Stock`}
                message={`for ${stockChangeItem ? stockChangeItem.name.en : 'item'}`}
                onDidDismiss={() => setStockChangeItem(null)}
                buttons={[
                    {
                        text: 'In Stock',
                        handler: async () => {
                            if (stockChangeItem) {
                                await updateCafeItemStock(stockChangeItem.id, false);
                            }
                            setStockChangeItem(null);
                        }
                    },
                    {
                        text: 'Out of Stock',
                        handler: async () => {
                            if (stockChangeItem) {
                                await updateCafeItemStock(stockChangeItem.id, true);
                            }
                            setStockChangeItem(null);
                        }
                    },
                    {
                        text: `Cancel`,
                        role: 'cancel',
                        handler: () => {
                            setStockChangeItem(null);
                        }
                    }
                ]}
            />
        </IonPage>
    );
};

export default CafeItems;

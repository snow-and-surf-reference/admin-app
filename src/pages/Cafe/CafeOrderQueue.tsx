import { IonContent, IonPage } from "@ionic/react"
import { CafeOrder } from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { updateCafeOrderStatus } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectCafeOrderQueue } from "app/slices/cafeSlice";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import './CafeOrderQueue.scss';


const CafeOrderQueue: React.FC = () => {
    const orders: CafeOrder[] = useAppSelector(selectCafeOrderQueue);

    return (
        <IonPage>
            <Header title={'Order Queue'}/>
            <IonContent fullscreen>
            <div className="order-queue-container">
                <div className="queue-header">
                    <div>準備中</div>
                    <div className="collect-header">取餐</div>
                </div>
                <div className="queues">
                <div className="queue preparing-queue">
                    {
                        orders.filter(x => x.status === 'preparing').sort((a, b) => {
                            if (dayjs(a.createdAt).isBefore(dayjs(b.createdAt))) {
                                return -1
                            } else if (dayjs(a.createdAt).isAfter(dayjs(b.createdAt))) {
                                return 1
                            } else {
                                return 0
                            }
                        }
                        ).map(y => (
                            <div key={`order-${y.id}`} className='order preparing'>
                                <p className="takeaway">{y.takeaway ? `外賣` : `堂食`}</p>
                                <h3 className="order-number">{y.orderNumber}</h3>
                                <ul className="items">
                                    {
                                        y.items.map((item, idx) => (
                                            <li key={`order-${y.id}-item-${item.id}-idx-${idx}`}>{item.name.zh ? item.name.zh : item.name.en}</li>
                                        ))
                                    }
                                </ul>
                                <button
                                    className="ready-btn"
                                    onClick={async () => {
                                        await updateCafeOrderStatus(y.id, 'ready');
                                    }}
                                >Ready</button>
                            </div>
                        ))
                    }
                </div>
                <div className="queue ready-queue">
                {
                        orders.filter(x => x.status === 'ready').sort((a, b) => {
                            if (dayjs(a.createdAt).isBefore(dayjs(b.createdAt))) {
                                return -1
                            } else if (dayjs(a.createdAt).isAfter(dayjs(b.createdAt))) {
                                return 1
                            } else {
                                return 0
                            }
                        }
                        ).map(y => (
                            <div key={`order-${y.id}`} className='order preparing'>
                                <p className="takeaway">{y.takeaway ? `外賣` : `堂食`}</p>
                                <h3 className="order-number">{y.orderNumber}</h3>
                                <ul className="items">
                                    {
                                        y.items.map((item, idx) => (
                                            <li key={`order-${y.id}-item-${item.id}-idx-${idx}`}>{item.name.zh ? item.name.zh : item.name.en}</li>
                                        ))
                                    }
                                </ul>
                                <button
                                    className="collected-btn"
                                    onClick={async () => {
                                        await updateCafeOrderStatus(y.id, 'collected');
                                    }}
                                >Collected</button>
                            </div>
                        ))
                    }
                </div>
                </div>
            </div>
            </IonContent>
        </IonPage>
    )
}

export default CafeOrderQueue;
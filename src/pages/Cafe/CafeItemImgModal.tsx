import {
    IonButton, IonContent,
    IonFooter, IonHeader,
    IonIcon,
    IonLabel,
    IonModal, IonSpinner,
    IonTitle,
    IonToolbar
} from "@ionic/react";
import { CafeItem } from "@tsanghoilun/snow-n-surf-interface/types/cafe";
import { uploadCafeItemImg } from "app/firebase";
import { closeOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import Dropzone from "react-dropzone";
import ImageComp from "./ImageComp";

interface Props {
    item: CafeItem | null;
    isOpen: boolean;
    onDismiss: () => void;
}

const CafeItemImgModal: React.FC<Props> = (props) => {
    const { item, isOpen, onDismiss } = props;
    const [uploading, setUploading] = useState(false);
    const [imgUrl, setImgUrl] = useState<string>(item?.images || "");

    useEffect(() => {
        if (!item || !item.images) {
            return;
        }

        setImgUrl(item.images);
        return () => {
            setImgUrl("");
        };
    }, [item]);

    return (
        <IonModal isOpen={isOpen} onDidDismiss={() => onDismiss()}>
            {item && (
                <>
                    <IonHeader>
                        <IonToolbar>
                            <IonButton slot="start" fill="clear" color={"medium"} onClick={() => onDismiss()}>
                                <IonIcon icon={closeOutline} slot="start" />
                                <IonLabel>Cancel</IonLabel>
                            </IonButton>
                            <IonTitle>
                                <IonLabel>
                                    <h3>Change Image</h3>
                                    <p>{item.name.en}{item.name.zh ? ` | ${item.name.zh}` : ''}</p>
                                </IonLabel>
                            </IonTitle>

                        </IonToolbar>
                    </IonHeader>
                    <IonContent>
                        <ImageComp src={imgUrl} width={`auto`} height={"70%"} style={{ margin: "auto" }} />
                        <br />
                        <Dropzone
                            disabled={uploading}
                            onDrop={async (acceptedFiles) => {
                                if (acceptedFiles.length <= 0) {
                                    return;
                                }
                                setUploading(true);
                                await uploadCafeItemImg(acceptedFiles[0], item.id);
                                setUploading(false);
                            }}
                        >
                            {({ getRootProps, getInputProps }) => (
                                <section>
                                    <div
                                        {...getRootProps()}
                                        style={{
                                            border: `2px dashed #888`,
                                            padding: `1rem`,
                                            margin: `1rem`,
                                            borderRadius: `12px`,
                                            cursor: `pointer`,
                                            textAlign: `center`
                                        }}
                                    >
                                        {uploading ? (
                                            <IonSpinner name="dots" />
                                        ) : (
                                            <>
                                                <input {...getInputProps()} />
                                                <p>Drop Image Here...</p>
                                            </>
                                        )}
                                    </div>
                                </section>
                            )}
                        </Dropzone>
                    </IonContent>
                    <IonFooter>
                        <IonToolbar color={`light`}>
                            <IonButton expand="block" color={`light`} onClick={() => onDismiss()}>
                                <IonLabel>Finish</IonLabel>
                            </IonButton>
                        </IonToolbar>
                    </IonFooter>
                </>
            )}
        </IonModal>
    );
};

export default CafeItemImgModal;

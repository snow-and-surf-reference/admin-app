import { IonSpinner } from "@ionic/react";
import { useState } from "react";
import FoodSvg from "../../assets/images/food.svg";

interface Props {
    src?: string | null | undefined;
    alt?: string;
    width?: string;
    height?: string;
    fit?: `fill` | `contain` | `cover` | `none` | `scale-down`;
    style?: Object;
}

const ImageComp: React.FC<Props> = (props) => {
    const { src, alt, width, height, style, fit } = props;
    const [loaded, setLoaded] = useState(false);

    return (
        <div
            style={{
                width: width ? width : "3rem",
                height: height ? height : "3rem",
                margin: "0.7rem",
                backgroundColor: `var(--ion-color-light)`,
                borderRadius: `0.15rem`,
                position: `relative`,
                ...style,
            }}
        >
            <img
                src={src ? src : FoodSvg}
                alt={alt ? alt : "food image"}
                style={{
                    width: `100%`,
                    height: `100%`,
                    objectPosition: `center`,
                    objectFit: fit ? fit : `contain`,
                    transition: `300ms`,
                    opacity: loaded ? `1` : `0`,
                    borderRadius: `0.2rem`,
                }}
                onLoad={() => setLoaded(true)}
            />
            {!loaded && <IonSpinner name="dots" className="overlay-spinner" />}
        </div>
    );
};

export default ImageComp;

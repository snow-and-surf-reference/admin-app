import {
    IonAlert,
    IonButton, IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCol,
    IonContent,
    IonFooter,
    IonGrid,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonLoading,
    IonPage,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonSpinner,
    IonToggle,
    IonToolbar
} from "@ionic/react";
import { Text_i18n } from "@tsanghoilun/snow-n-surf-interface/types/global";
import * as HomepageTypes from "@tsanghoilun/snow-n-surf-interface/types/homePage";
import posRoutes from "app/data/posRoutes";
import { deleteMediaByUrl, updateHomepageSlide, uploadSlideImage } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectHomepageSlideById } from "app/slices/posContentSlice";
import Header from "components/Global/Header";
import LoadingCard from "components/Global/LoadingCard";
import { checkmarkOutline, closeCircleOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import Dropzone from "react-dropzone";
import { useHistory, useParams } from "react-router";
import IMG_PLACEHOLDER from "../../assets/images/img-placeholder.svg";

const temp18n: Text_i18n = {
    en: "",
    zh: "",
};

const newSlide: HomepageTypes.HomePageSlide = {
    id: "new",
    media: "",
    mediaType: "contentImages",
    title: { ...temp18n },
    subtitle: { ...temp18n },
    desc: { ...temp18n },
    buttonText: { ...temp18n },
    isExternalLink: false,
    externalUrl: "",
    order: 0,
    enable: true,
    index: 0
};

const localRoutes = posRoutes();

const HomepageSlide: React.FC = () => {
    const history = useHistory();
    const { id } = useParams<{ id: string }>();
    const rawSlide: HomepageTypes.HomePageSlide | undefined = useAppSelector(selectHomepageSlideById(id));
    const [slide, setSlide] = useState<HomepageTypes.HomePageSlide | null | undefined>(null);
    const [uploading, setUploading] = useState(false);
    const [saving, setSaving] = useState("idle");
    const [disableSubmit, setDisableSubmit] = useState(true);

    // useEffect(() => console.log(slide), [slide]);

    useEffect(() => {
        // const tempSlide = { ...slide };
        if (slide && slide?.title?.en !== "") {
            setDisableSubmit(false);
        } else {
            setDisableSubmit(true);
        }
        if (slide && slide.media && slide.media !== "") {
            const decoded = decodeURIComponent(slide.media);
            const trimmed = decoded.split("?")[0];
            const splitted = trimmed.split("/");
            const folder = splitted[splitted.length - 2];
            if (folder === slide.mediaType) {
                return;
            }
            if (folder === "contentImages" || folder === "videos") {
                setSlide((s) =>
                    s
                        ? Object.assign(
                              { ...s },
                              {
                                  mediaType: folder,
                              }
                          )
                        : s
                );
            }
        }
    }, [slide]);

    useEffect(() => {
        if (!id) {
            setSlide(null);
            return;
        }
        if (id === "new") {
            setSlide({ ...newSlide });
            return;
        } else {
            if (rawSlide) {
                setSlide(rawSlide);
            }
        }
    }, [id, rawSlide]);

    const handleSubmit = async () => {
        if (!slide) {
            return;
        }
        setSaving("saving");
        await updateHomepageSlide(slide);
        setSaving("saved");
    };

    return (
        <IonPage>
            <Header title="New Homepage Slides" back />
            <IonContent>
                {slide === null ? (
                    <IonCard color={"dark"}>
                        <IonCardContent className="ion-text-center">
                            <IonLabel>
                                The ID of the slide you are looking for does not exist, please go back to slide list page and select a valid slide.
                            </IonLabel>
                        </IonCardContent>
                    </IonCard>
                ) : slide === undefined ? (
                    <LoadingCard />
                ) : (
                    <>
                        <IonCard color={"light"}>
                            <IonCardHeader>
                                <IonCardTitle>Slide Media</IonCardTitle>
                                <IonCardSubtitle>Media will be placed as backdrop of the slide, and looped playing if it is a video</IonCardSubtitle>
                            </IonCardHeader>
                            <IonCardContent>
                                <IonGrid>
                                    <IonRow>
                                        <IonCol>
                                            {slide.mediaType === "videos" ? (
                                                <>
                                                    <video
                                                        style={{
                                                            width: `100%`,
                                                        }}
                                                    >
                                                        <source src={slide.media} type="video/mp4"></source>
                                                    </video>
                                                </>
                                            ) : slide.mediaType === "contentImages" ? (
                                                <img
                                                    alt="media-preview"
                                                    src={slide.media ? slide.media : IMG_PLACEHOLDER}
                                                    style={{
                                                        height: `96px`,
                                                    }}
                                                />
                                            ) : null}
                                        </IonCol>
                                        <IonCol>
                                            {slide.media ? (
                                                <IonButton
                                                    onClick={async () => {
                                                        await deleteMediaByUrl(slide.media);
                                                        setSlide((s) => (s ? Object.assign({ ...s }, { media: "" }) : s));
                                                    }}
                                                >
                                                    <IonIcon icon={closeCircleOutline} slot="start" />
                                                    <IonLabel>Delete Media</IonLabel>
                                                </IonButton>
                                            ) : (
                                                <Dropzone
                                                    disabled={uploading}
                                                    onDrop={async (acceptedFiles) => {
                                                        if (acceptedFiles.length <= 0) {
                                                            return;
                                                        }
                                                        setUploading(true);
                                                        const url = await uploadSlideImage(acceptedFiles[0]);
                                                        setUploading(false);
                                                        setSlide((s) =>
                                                            s
                                                                ? Object.assign(
                                                                      { ...s },
                                                                      {
                                                                          media: url,
                                                                      }
                                                                  )
                                                                : s
                                                        );
                                                    }}
                                                >
                                                    {({ getRootProps, getInputProps }) => (
                                                        <section>
                                                            <div
                                                                {...getRootProps()}
                                                                style={{
                                                                    border: `2px dashed #888`,
                                                                    padding: `1rem`,
                                                                    margin: `1rem`,
                                                                    borderRadius: `12px`,
                                                                    cursor: `pointer`,
                                                                    textAlign: `center`,
                                                                }}
                                                            >
                                                                {uploading ? (
                                                                    <IonSpinner name="dots" />
                                                                ) : (
                                                                    <>
                                                                        <input {...getInputProps()} />
                                                                        <p>Drop Image / Video Here...</p>
                                                                    </>
                                                                )}
                                                            </div>
                                                        </section>
                                                    )}
                                                </Dropzone>
                                            )}
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>
                            </IonCardContent>
                        </IonCard>

                        <IonCard color={"light"}>
                            <IonCardHeader>
                                <IonCardTitle>Slide Content</IonCardTitle>
                            </IonCardHeader>
                            <IonCardContent>
                                <IonList inset>
                                    <IonListHeader>Title</IonListHeader>
                                    <IonItem>
                                        <IonLabel position="stacked">English</IonLabel>
                                        <IonInput
                                            type="text"
                                            placeholder={`Welcome to Snow & Surf`}
                                            value={slide.title.en}
                                            onIonChange={(e) => {
                                                setSlide((s) =>
                                                    s
                                                        ? Object.assign(
                                                              { ...s },
                                                              {
                                                                  title: Object.assign(
                                                                      { ...s.title },
                                                                      {
                                                                          en: e.detail.value!,
                                                                      }
                                                                  ),
                                                              }
                                                          )
                                                        : s
                                                );
                                            }}
                                        />
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel position="stacked">中文</IonLabel>
                                        <IonInput
                                            type="text"
                                            placeholder={`歡迎來到 Snow & Surf`}
                                            value={slide.title.zh}
                                            onIonChange={(e) => {
                                                setSlide((s) =>
                                                    s
                                                        ? Object.assign(
                                                              { ...s },
                                                              {
                                                                  title: Object.assign(
                                                                      { ...s.title },
                                                                      {
                                                                          zh: e.detail.value!,
                                                                      }
                                                                  ),
                                                              }
                                                          )
                                                        : s
                                                );
                                            }}
                                        />
                                    </IonItem>
                                </IonList>

                                <IonList inset>
                                    <IonListHeader>Subtitle</IonListHeader>
                                    <IonItem>
                                        <IonLabel position="stacked">English</IonLabel>
                                        <IonInput
                                            type="text"
                                            placeholder="Best Indoor Ski slobe and Surf Machine!"
                                            value={slide.subtitle.en}
                                            onIonChange={(e) => {
                                                setSlide((s) =>
                                                    s
                                                        ? Object.assign(
                                                              { ...s },
                                                              {
                                                                  subtitle: Object.assign(
                                                                      { ...s.subtitle },
                                                                      {
                                                                          en: e.detail.value!,
                                                                      }
                                                                  ),
                                                              }
                                                          )
                                                        : s
                                                );
                                            }}
                                        />
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel position="stacked">中文</IonLabel>
                                        <IonInput
                                            type="text"
                                            placeholder="香港最好的室內滑雪及滑水場！"
                                            value={slide.subtitle.zh}
                                            onIonChange={(e) => {
                                                setSlide((s) =>
                                                    s
                                                        ? Object.assign(
                                                              { ...s },
                                                              {
                                                                  subtitle: Object.assign(
                                                                      { ...s.subtitle },
                                                                      {
                                                                          zh: e.detail.value!,
                                                                      }
                                                                  ),
                                                              }
                                                          )
                                                        : s
                                                );
                                            }}
                                        />
                                    </IonItem>
                                </IonList>

                                <IonList inset>
                                    <IonListHeader>Description</IonListHeader>
                                    <IonItem>
                                        <IonLabel position="stacked">English</IonLabel>
                                        <IonInput
                                            type="text"
                                            placeholder="English Description goes here"
                                            value={slide.desc.en}
                                            onIonChange={(e) => {
                                                setSlide((s) =>
                                                    s
                                                        ? Object.assign(
                                                              { ...s },
                                                              {
                                                                  desc: Object.assign(
                                                                      { ...s.desc },
                                                                      {
                                                                          en: e.detail.value!,
                                                                      }
                                                                  ),
                                                              }
                                                          )
                                                        : s
                                                );
                                            }}
                                        />
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel position="stacked">中文</IonLabel>
                                        <IonInput
                                            type="text"
                                            placeholder="在這裡填上中文文字內容"
                                            value={slide.desc.zh}
                                            onIonChange={(e) => {
                                                setSlide((s) =>
                                                    s
                                                        ? Object.assign(
                                                              { ...s },
                                                              {
                                                                  desc: Object.assign(
                                                                      { ...s.desc },
                                                                      {
                                                                          zh: e.detail.value!,
                                                                      }
                                                                  ),
                                                              }
                                                          )
                                                        : s
                                                );
                                            }}
                                        />
                                    </IonItem>
                                </IonList>
                            </IonCardContent>
                        </IonCard>

                        <IonCard color={"light"}>
                            <IonCardHeader>
                                <IonCardTitle>Action Button</IonCardTitle>
                                <IonCardSubtitle>Call to action button, keep text empty if you do not want to show the button.</IonCardSubtitle>
                            </IonCardHeader>
                            <IonCardContent>
                                <IonList inset>
                                    <IonListHeader>display Text</IonListHeader>
                                    <IonItem>
                                        <IonLabel position="stacked">English</IonLabel>
                                        <IonInput
                                            type="text"
                                            placeholder="Book Now"
                                            value={slide.buttonText.en}
                                            onIonChange={(e) => {
                                                setSlide((s) =>
                                                    s
                                                        ? Object.assign(
                                                              { ...s },
                                                              {
                                                                  buttonText: Object.assign(
                                                                      { ...s.buttonText },
                                                                      {
                                                                          en: e.detail.value!,
                                                                      }
                                                                  ),
                                                              }
                                                          )
                                                        : s
                                                );
                                            }}
                                        />
                                    </IonItem>
                                    <IonItem>
                                        <IonLabel position="stacked">中文</IonLabel>
                                        <IonInput
                                            type="text"
                                            placeholder="立即預訂"
                                            value={slide.buttonText.zh}
                                            onIonChange={(e) => {
                                                setSlide((s) =>
                                                    s
                                                        ? Object.assign(
                                                              { ...s },
                                                              {
                                                                  buttonText: Object.assign(
                                                                      { ...s.buttonText },
                                                                      {
                                                                          zh: e.detail.value!,
                                                                      }
                                                                  ),
                                                              }
                                                          )
                                                        : s
                                                );
                                            }}
                                        />
                                    </IonItem>
                                </IonList>

                                <IonItem color={"light"} lines="none">
                                    <IonLabel>Link to external URL?</IonLabel>
                                    <IonToggle
                                        checked={slide.isExternalLink}
                                        onIonChange={(e) => {
                                            setSlide((s) =>
                                                s
                                                    ? Object.assign(
                                                          { ...s },
                                                          {
                                                              isExternalLink: e.detail.checked,
                                                          }
                                                      )
                                                    : s
                                            );
                                        }}
                                    />
                                </IonItem>
                                {slide.isExternalLink ? (
                                    <IonList inset>
                                        <IonItem>
                                            <IonLabel position="stacked">URL</IonLabel>
                                            <IonInput
                                                type="text"
                                                placeholder="e.g. https://www.google.com"
                                                value={slide.externalUrl}
                                                onIonChange={(e) => {
                                                    setSlide((s) =>
                                                        s
                                                            ? Object.assign(
                                                                  { ...s },
                                                                  {
                                                                      externalUrl: e.detail.value!,
                                                                  }
                                                              )
                                                            : s
                                                    );
                                                }}
                                            />
                                        </IonItem>
                                    </IonList>
                                ) : (
                                    <IonList inset>
                                        <IonItem>
                                            <IonLabel>Target Page</IonLabel>
                                            <IonSelect
                                                value={slide.externalUrl}
                                                onIonChange={(e) => {
                                                    setSlide((s) =>
                                                        s
                                                            ? Object.assign(
                                                                  { ...s },
                                                                  {
                                                                      externalUrl: e.detail.value!,
                                                                  }
                                                              )
                                                            : s
                                                    );
                                                }}
                                            >
                                                <IonSelectOption value={""}>None</IonSelectOption>
                                                {localRoutes.map((x) => (
                                                    <IonSelectOption value={x.path} key={`route-select-option-${x.path}`}>
                                                        {x.name}
                                                    </IonSelectOption>
                                                ))}
                                            </IonSelect>
                                        </IonItem>
                                    </IonList>
                                )}
                            </IonCardContent>
                        </IonCard>
                    </>
                )}
            </IonContent>
            <IonFooter>
                <IonToolbar>
                    <IonButton color={disableSubmit ? "medium" : "warning"} disabled={disableSubmit} slot="end" onClick={() => handleSubmit()}>
                        <IonIcon icon={checkmarkOutline} slot="start" />
                        <IonLabel>Save</IonLabel>
                    </IonButton>
                </IonToolbar>
            </IonFooter>
            <IonLoading isOpen={saving === "saving"} message={`Saving Updates`} />
            <IonAlert
                isOpen={saving === "saved"}
                onDidDismiss={() => setSaving("idle")}
                header={`Your Changes is Saved.`}
                buttons={[
                    {
                        text: "OK",
                        role: "confirm",
                        handler: () => {
                            setSaving("idle");
                            history.push("/homepageSlides");
                        },
                    },
                ]}
            />
        </IonPage>
    );
};

export default HomepageSlide;

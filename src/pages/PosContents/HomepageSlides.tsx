import {
    IonButton,
    IonButtons,
    IonCol,
    IonContent,
    IonGrid,
    IonIcon,
    IonItem,
    IonLabel, IonPage,
    IonReorder,
    IonReorderGroup,
    IonRow,
    IonToggle,
    ItemReorderEventDetail
} from "@ionic/react";
import { deleteHomepageSlide, saveHomepageSlidesOrder } from "app/firebase";
import { useAppSelector } from "app/hooks";
import { selectHomepageSlides } from "app/slices/posContentSlice";
import Header from "components/Global/Header";
import { createOutline, swapVerticalOutline, trashBinOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";

const HomepageSlides: React.FC = () => {
    const history = useHistory();
    const slidesRaw = useAppSelector(selectHomepageSlides);
    const [slides, setSlides] = useState(slidesRaw);
    const [toggleReorder, setToggleReorder] = useState(false);

    useEffect(() => {
        setSlides(slidesRaw)
    }, [slidesRaw])
    

    const doReorder = async (event: CustomEvent<ItemReorderEventDetail>) => {
        // The `from` and `to` properties contain the index of the item
        // when the drag started and ended, respectively
        console.log("Dragged from index", event.detail.from, "to", event.detail.to);
        event.detail.complete(false);
        let tempSlides = [...slides].sort((a, b) => a.index - b.index);
        const fromItem = tempSlides.splice(event.detail.from, 1);
        tempSlides.splice(event.detail.to, 0, ...fromItem);
        tempSlides.forEach((x, idx) => {
            tempSlides[idx] = Object.assign({ ...x }, { index: idx });
        })
        setSlides(tempSlides);
        saveHomepageSlidesOrder(tempSlides);
    };

    const handleDelete = async (id: string) => {
        await deleteHomepageSlide(id);
    }

    return (
        <IonPage>
            <Header title="Homepage Slides Management" />
            <IonContent>
                <IonGrid>
                    <IonRow>
                        <IonCol className="ion-text-end">
                            <IonButton
                                color={"warning"}
                                onClick={() => {
                                    history.push(`/homepageSlide/new`);
                                }}
                            >
                                <IonIcon icon={createOutline} slot="start" />
                                <IonLabel>New Slide</IonLabel>
                            </IonButton>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            <IonItem>
                                <IonToggle
                                    checked={toggleReorder}
                                    slot="end"
                                    onIonChange={(e) => {
                                        setToggleReorder(e.detail.checked);
                                    }}
                                />
                                <IonLabel slot="end">
                                    <p>Reorder Menu</p>
                                </IonLabel>
                            </IonItem>
                            <IonReorderGroup disabled={!toggleReorder} onIonItemReorder={doReorder}>
                                {[...slides]
                                    .sort((a, b) => a.index - b.index)
                                    .map((x) => (
                                        <IonItem key={`homepage-slides-item-${x.id}`}>
                                            <IonLabel>
                                                <h3>{x.title.en}</h3>
                                                <p>{x.subtitle.en}</p>
                                            </IonLabel>
                                            <IonButtons slot="end">
                                                <IonButton
                                                    color={"secondary"}
                                                    onClick={() => {
                                                        history.push(`/homepageSlide/${x.id}`);
                                                    }}
                                                >
                                                    <IonIcon icon={createOutline} slot="start" />
                                                    <IonLabel>Edit</IonLabel>
                                                </IonButton>
                                                <IonButton
                                                    color={"danger"}
                                                    onClick={() => {
                                                        handleDelete(x.id);
                                                    }}
                                                >
                                                    <IonIcon icon={trashBinOutline} slot="start" />
                                                    <IonLabel>Delete</IonLabel>
                                                </IonButton>
                                            </IonButtons>
                                            <IonReorder slot="end">
                                                <IonIcon icon={swapVerticalOutline} />
                                            </IonReorder>
                                        </IonItem>
                                    ))}
                            </IonReorderGroup>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
};

export default HomepageSlides;

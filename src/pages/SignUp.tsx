import {
    IonAlert,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonCardTitle,
    IonContent,
    IonHeader,
    IonInput,
    IonItem,
    IonLabel,
    IonMenuButton,
    IonPage,
    IonPopover,
    IonTitle,
    IonToolbar,
} from "@ionic/react";
import { getUserByEmail, getUserByMemberIdType } from "app/firebase";
import { RootState } from "app/store";
import "css/CheckIn.css";
import { useCallback, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import { singleAlertBtn } from "app/variables";

function SignUp() {
    const [memberID, setMemberID] = useState("");
    const [email, setEmail] = useState("");
    const [message, setMessage] = useState("");
    const customer = useSelector((state: RootState) => state.users.parentAccountUser);
    const history = useHistory();

    const resetState = useCallback(() => {
        setEmail("");
        setMemberID("");
    }, []);

    const handleSearch = async () => {
        await getUserByEmail(email, true).then((e) => {
            if (e !== "success") {
                setMessage(e);
            }
        });
    };

    const searchByMemberID = async () => {
        await getUserByMemberIdType(memberID, "main").then((e) => {
            if (e !== "success") {
                setMessage(e);
            }
        });
    };

    useEffect(() => {
        if (customer) {
            history.push("/customers/sub-account/create");
            resetState();
        }
    }, [customer, resetState, history]);

    return (
        <IonPage>
            <IonContent fullscreen>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuButton />
                        </IonButtons>
                        <IonTitle>{`Create Sub Account`}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonCard>
                    <IonPopover size="cover"></IonPopover>
                    <IonCardTitle className="ion-padding">Main account member ID</IonCardTitle>
                    <IonCardContent>
                        <IonItem>
                            <IonLabel color="medium">Member ID</IonLabel>
                            <IonInput
                                onFocus={() => {
                                    setEmail("");
                                }}
                                type="text"
                                required
                                value={memberID}
                                onIonChange={(e) => {
                                    setMemberID(e.detail.value!);
                                }}
                            />
                            <IonButton slot="end" expand="block" size="large" disabled={!memberID} onClick={searchByMemberID}>
                                Search
                            </IonButton>
                        </IonItem>
                    </IonCardContent>
                </IonCard>
                <IonCard>
                    <IonCardTitle className="ion-padding">Main account email address</IonCardTitle>
                    <IonCardContent>
                        <IonItem>
                            <IonLabel color="medium">Email address</IonLabel>
                            <IonInput
                                onFocus={() => {
                                    setMemberID("");
                                }}
                                type="text"
                                required
                                value={email}
                                onIonChange={(e) => {
                                    setEmail(e.detail.value!);
                                }}
                            />
                            <IonButton slot="end" expand="block" size="large" disabled={!email} onClick={handleSearch}>
                                Search
                            </IonButton>
                        </IonItem>
                    </IonCardContent>
                </IonCard>
                {/* <IonList>
          <IonItem>
            <IonLabel color="medium">Username</IonLabel>
            <IonInput
              type="text"
              required
              value={newUserData.usrname}
              onIonChange={(e) => {
                setNewUserData({
                  ...newUserData,
                  usrname: e.detail.value!,
                });
              }}
            />
          </IonItem>
          <IonItem>
            <IonLabel color="medium">Email address</IonLabel>
            <IonInput
              type="text"
              required
              value={newUserData.email}
              onIonChange={(e) => {
                setNewUserData({
                  ...newUserData,
                  email: e.detail.value!,
                });
              }}
            />
          </IonItem>

          <IonItem>
            <IonLabel color="medium">DOB</IonLabel>
            <IonInput
              type="date"
              value={new Date(newUserData.dob).toLocaleDateString("en-CA")}
              onIonChange={(e) => {
                setNewUserData({
                  ...newUserData,
                  dob: new Date(e.detail.value!),
                });
              }}
            />
          </IonItem>
          <IonItem>
            <IonLabel color="medium">{"Mobile (+852)"}</IonLabel>
            <IonInput
              name="mobile"
              type="tel"
              required
              maxlength={8}
              value={newUserData.phone}
              onIonChange={(e) => {
                setNewUserData({
                  ...newUserData,
                  phone: isNaN(parseInt(e.detail.value!)) ? "" : String(parseInt(e.detail.value!)),
                });
              }}
            />
          </IonItem>

          <IonItem>
            <IonLabel color="medium">Name as in HKID</IonLabel>
            <IonInput
              required
              value={newUserData.realName}
              onIonChange={(e) => {
                setNewUserData({
                  ...newUserData,
                  realName: e.detail.value!,
                });
              }}
            />
          </IonItem>
          <input
            ref={photoRef}
            type="file"
            multiple={false}
            accept="image/*"
            style={{ display: "none" }}
            onChange={async (e) => {
              if (!e.target.files) return;
              setTempFile(e.target.files[0]);
              getBase64(e.target.files[0]);
            }}
          />
          <IonItem>
            <IonLabel color="medium">Picture of yourself for check-in purpose</IonLabel>
            <IonCol
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <IonButton size="large" onClick={() => photoRef.current?.click()}>
                {tempImage ? "Retake photo" : "Take photo"}
              </IonButton>
            </IonCol>
          </IonItem>
          {tempImage && (
            <IonRow>
              <IonCol size="12" style={{ justifyContent: "center", display: "flex" }}>
                <IonImg src={tempImage} style={{ maxHeight: "320px" }}></IonImg>
              </IonCol>
            </IonRow>
          )}
          <IonRow>
            <IonCol size="12">
              <IonButton disabled={!newUserData.realName || !tempImage || !newUserData.email} style={{ width: "100%" }} onClick={handleSubmit}>
                Confirm
              </IonButton>
            </IonCol>
          </IonRow>
        </IonList> */}
            </IonContent>
            <IonAlert
                isOpen={!!message}
                onDidDismiss={() => {
                    setMessage("");
                }}
                header={`System Message`}
                message={message}
                buttons={singleAlertBtn(() => setMessage(""))}
            />
        </IonPage>
    );
}

export default SignUp;

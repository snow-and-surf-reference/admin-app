import {
    IonAccordion,
    IonAccordionGroup,
    IonAlert,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCol,
    IonContent,
    IonGrid,
    IonImg,
    IonItem,
    IonLabel,
    IonList,
    IonPage,
    IonRow,
    IonSpinner,
    IonThumbnail,
} from "@ionic/react";
import { Session } from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { getCircletDataByID } from "app/firebase";
import { setWristbandId } from "app/slices/wristbandSlice";
import { RootState } from "app/store";
import Header from "components/Global/Header";
import dayjs from "dayjs";
import { CustomerCard } from "pages/Sessions/UserSearch";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

function WristCheck() {
    const [message, setMessage] = useState("");
    const [session, setSession] = useState<Session | null>(null);
    const [customer, setCustomer] = useState<Customer | null>(null);
    const wristbandRedux = useSelector((state: RootState) => state.wristband);
    const connectToScanner = wristbandRedux.connectToScanner;
    const wristbandId = wristbandRedux.wristbandId;
    const dispatch = useDispatch();

    const alertButtons = [
        {
            text: "OK",
            role: "cancel",
            handler: () => setMessage(""),
        },
    ];

    const handleWristband = useCallback(async (nfcId: string) => {
        await getCircletDataByID(nfcId).then((e: { customer: Customer; session: Session }) => {
            setCustomer(e.customer);
            setSession(e.session);
            dispatch(setWristbandId(""));
        });
    }, [dispatch]);

    useEffect(() => {
        if (!!wristbandId) {
            handleWristband(wristbandId);
        }
    }, [handleWristband, wristbandId]);

    useEffect(() => {
        if (!connectToScanner) {
            setMessage("Please connect to scanner");
        }
    }, [connectToScanner]);

    return (
        <IonPage>
            <Header title={"Wristband check"} />
            <IonContent>
                <IonGrid>
                    <IonRow className="ion-padding">
                        {(!customer || !session) && (
                            <>
                                {!connectToScanner ? (
                                    <IonCol size="12" className="flex-center">
                                        Please connect to scanner
                                    </IonCol>
                                ) : (
                                    <>
                                        <IonCol size="12" className="flex-center">
                                            <IonSpinner />
                                        </IonCol>
                                        <IonCol size="12" className="flex-center">
                                            Please place the wristband on the scanner
                                        </IonCol>
                                    </>
                                )}
                            </>
                        )}
                    </IonRow>
                </IonGrid>
                {customer && <CustomerCard customer={customer} />}
                {session && (
                    <IonCard>
                        <IonCardHeader>
                            <IonCardTitle>Session Detail</IonCardTitle>
                        </IonCardHeader>

                        <IonList>
                            <IonItem>
                                <IonLabel>
                                    <p>Booking number</p>
                                </IonLabel>
                                <IonLabel slot="end">{session.bookingNumber}</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>
                                    <p>Date</p>
                                </IonLabel>
                                <IonLabel slot="end">{dayjs(Number(session.start)).format(`DD MMM YY`)}</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>
                                    <p>Time</p>
                                </IonLabel>
                                <IonLabel slot="end">
                                    {dayjs(session.start).format(`HH:mm`)} - {dayjs(session.end).format(`HH:mm`)}
                                </IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>
                                    <p>Player{session.pax > 1 ? `s` : ``}</p>
                                </IonLabel>
                                <IonLabel slot="end">{session.pax}</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>
                                    <p>Type</p>
                                </IonLabel>
                                <IonLabel slot="end">{session.playType}</IonLabel>
                            </IonItem>
                        </IonList>
                        <br />
                        <IonAccordionGroup value={`gears`}>
                            <IonAccordion value="gears">
                                <IonItem slot="header">Gear Rentals</IonItem>
                                <IonList slot="content">
                                    {session.gears.length <= 0 ? (
                                        <IonItem>
                                            <IonLabel>No Rentals for this session.</IonLabel>
                                        </IonItem>
                                    ) : (
                                        session.gears.map((gear, idx) => (
                                            <IonItem key={`session-${session.id}-gear-${idx}`}>
                                                {`${gear.gear.name}  ${gear.size}  ${gear.gear.unit.name} (${gear.class})`}
                                            </IonItem>
                                        ))
                                    )}
                                </IonList>
                            </IonAccordion>
                        </IonAccordionGroup>
                        <br />
                        <IonAccordionGroup value={`players`}>
                            <IonAccordion value="players">
                                <IonItem slot="header">
                                    Checked In Users ({session.checkedInUsers.length}/{session.pax})
                                </IonItem>
                                <IonList slot="content" style={{ paddingBottom: "16px", paddingRight: "16px" }}>
                                    {session.checkedInUsers.length <= 0 ? (
                                        <IonItem>
                                            <IonLabel>No Player checked in yet.</IonLabel>
                                        </IonItem>
                                    ) : (
                                        session.checkedInUsers.map((user) => (
                                            <IonItem key={`session-${session.id}-player-${user.id}`}>
                                                <IonThumbnail
                                                    slot="start"
                                                    // onClick={() => setViewUsr({ image: user.realImage, realName: user.realName })}
                                                >
                                                    <IonImg src={user.realImage} />
                                                </IonThumbnail>
                                                <IonLabel>{user.realName + " " + user.phone}</IonLabel>
                                            </IonItem>
                                        ))
                                    )}
                                </IonList>
                            </IonAccordion>
                        </IonAccordionGroup>
                    </IonCard>
                )}
            </IonContent>
            <IonAlert isOpen={!!message} header="System Message" onDidDismiss={() => {}} message={message} buttons={alertButtons} />
        </IonPage>
    );
}

export default WristCheck;
